--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE application_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE application (
    id integer DEFAULT nextval('application_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE application OWNER TO postgres;

--
-- Name: auto_reverse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_reverse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auto_reverse_id_seq OWNER TO postgres;

--
-- Name: auto_reverse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auto_reverse (
    id integer DEFAULT nextval('auto_reverse_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE auto_reverse OWNER TO postgres;

--
-- Name: brand_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brand_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brand_images_id_seq OWNER TO postgres;

--
-- Name: brand_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brand_images (
    id integer DEFAULT nextval('brand_images_id_seq'::regclass) NOT NULL,
    brand_id integer,
    title text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name text
);


ALTER TABLE brand_images OWNER TO postgres;

--
-- Name: brands_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brands_id_seq OWNER TO postgres;

--
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brands (
    id integer DEFAULT nextval('brands_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    manufacturer_id text[],
    business_to_year text,
    business_from_year text,
    tape_recorders_to_year text,
    tape_recorders_from_year text,
    description text,
    technical_innovations text,
    general_information text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE brands OWNER TO postgres;

--
-- Name: catalog_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_images (
    id integer NOT NULL,
    catalog_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalog_images OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_images_id_seq OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_images_id_seq OWNED BY catalog_images.id;


--
-- Name: catalog_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_types (
    id integer NOT NULL,
    name text
);


ALTER TABLE catalog_types OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_types_id_seq OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_types_id_seq OWNED BY catalog_types.id;


--
-- Name: catalogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalogs (
    id integer NOT NULL,
    title text,
    type integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalogs OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogs_id_seq OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalogs_id_seq OWNED BY catalogs.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    _lft integer DEFAULT 0 NOT NULL,
    _rgt integer DEFAULT 0 NOT NULL,
    parent_id integer,
    name text,
    id integer NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq1 OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq1 OWNED BY categories.id;


--
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE channels (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE channels OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE channels_id_seq OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE channels_id_seq OWNED BY channels.id;


--
-- Name: classical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE classical (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE classical OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE classical_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE classical_id_seq OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE classical_id_seq OWNED BY classical.id;


--
-- Name: duplications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE duplications (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE duplications OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE duplications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE duplications_id_seq OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE duplications_id_seq OWNED BY duplications.id;


--
-- Name: electronics_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE electronics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE electronics_id_seq OWNER TO postgres;

--
-- Name: electronics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE electronics (
    id integer DEFAULT nextval('electronics_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE electronics OWNER TO postgres;

--
-- Name: equalization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE equalization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE equalization_id_seq OWNER TO postgres;

--
-- Name: equalization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE equalization (
    id integer DEFAULT nextval('equalization_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE equalization OWNER TO postgres;

--
-- Name: expander_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expander_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expander_images_seq OWNER TO postgres;

--
-- Name: expander_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expander_images (
    id integer DEFAULT nextval('expander_images_seq'::regclass) NOT NULL,
    expander_id integer,
    image text,
    main_image integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expander_images OWNER TO postgres;

--
-- Name: expanders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expanders (
    id integer NOT NULL,
    manufacturer text,
    model text,
    tape_recorders_usable integer,
    outputs_for_record integer,
    recorder_inputs integer,
    copy_option_decks integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expanders OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expanders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expanders_id_seq OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE expanders_id_seq OWNED BY expanders.id;


--
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE genres (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE genres OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE genres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genres_id_seq OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE genres_id_seq OWNED BY genres.id;


--
-- Name: head_composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_composition_id_seq OWNER TO postgres;

--
-- Name: head_composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_composition (
    id integer DEFAULT nextval('head_composition_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_composition OWNER TO postgres;

--
-- Name: head_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_configuration_id_seq OWNER TO postgres;

--
-- Name: head_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_configuration (
    id integer DEFAULT nextval('head_configuration_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_configuration OWNER TO postgres;

--
-- Name: inputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inputs_id_seq OWNER TO postgres;

--
-- Name: inputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inputs (
    id integer DEFAULT nextval('inputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE inputs OWNER TO postgres;

--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturer_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturer_images_id_seq OWNER TO postgres;

--
-- Name: manufacturer_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturer_images (
    id integer DEFAULT nextval('manufacturer_images_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    name text,
    title text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE manufacturer_images OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturers (
    id integer DEFAULT nextval('manufacturers_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    origin_country text NOT NULL,
    year_to integer,
    year_from integer,
    history jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE manufacturers OWNER TO postgres;

--
-- Name: max_reel_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE max_reel_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE max_reel_size_id_seq OWNER TO postgres;

--
-- Name: max_reel_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE max_reel_size (
    id integer DEFAULT nextval('max_reel_size_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE max_reel_size OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: motors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE motors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE motors_id_seq OWNER TO postgres;

--
-- Name: motors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE motors (
    id integer DEFAULT nextval('motors_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE motors OWNER TO postgres;

--
-- Name: noise_reductions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE noise_reductions (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE noise_reductions OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE noise_reductions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE noise_reductions_id_seq OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE noise_reductions_id_seq OWNED BY noise_reductions.id;


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE number_of_heads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE number_of_heads_id_seq OWNER TO postgres;

--
-- Name: number_of_heads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE number_of_heads (
    id integer DEFAULT nextval('number_of_heads_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE number_of_heads OWNER TO postgres;

--
-- Name: outputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE outputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outputs_id_seq OWNER TO postgres;

--
-- Name: outputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE outputs (
    id integer DEFAULT nextval('outputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE outputs OWNER TO postgres;

--
-- Name: password_resets_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE password_resets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE password_resets_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    id integer DEFAULT nextval('password_resets_seq'::regclass) NOT NULL
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: reel_sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reel_sizes (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE reel_sizes OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reel_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reel_sizes_id_seq OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE reel_sizes_id_seq OWNED BY reel_sizes.id;


--
-- Name: speeds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE speeds (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE speeds OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE speeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE speeds_id_seq OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE speeds_id_seq OWNED BY speeds.id;


--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp (
    id integer DEFAULT nextval('tape_head_preamp_id_seq'::regclass) NOT NULL,
    name text,
    manufacturer_id integer,
    brand_id integer,
    model text,
    adjustable_gain text,
    adjustable_loading text,
    playback_eq text,
    level_controls text,
    isolated_power_supply text,
    signal_noise_ratio text,
    tubes text,
    suggested_input_wiring text,
    impedance text,
    accessories text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp OWNER TO postgres;

--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_images_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_images (
    id integer DEFAULT nextval('tape_head_preamp_images_id_seq'::regclass) NOT NULL,
    tape_head_preamp_id integer NOT NULL,
    title text,
    description text,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp_images OWNER TO postgres;

--
-- Name: tape_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_images_seq OWNER TO postgres;

--
-- Name: tape_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_images (
    id integer DEFAULT nextval('tape_images_seq'::regclass) NOT NULL,
    tape_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text
);


ALTER TABLE tape_images OWNER TO postgres;

--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorder_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorder_images_id_seq OWNER TO postgres;

--
-- Name: tape_recorder_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorder_images (
    id integer DEFAULT nextval('tape_recorder_images_id_seq'::regclass) NOT NULL,
    tape_recorder_id integer NOT NULL,
    title text,
    description text,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_recorder_images OWNER TO postgres;

--
-- Name: tape_recorders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorders_id_seq OWNER TO postgres;

--
-- Name: tape_recorders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorders (
    id integer DEFAULT nextval('tape_recorders_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    brand_id integer,
    name text,
    model text,
    country_of_manufacture text,
    serial_number text,
    orignal_price numeric,
    belts text,
    frequency_response text,
    wow_flutter text,
    signal_noice_ratio text,
    description text,
    details jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    release_to_date integer,
    release_from_date integer,
    original_price double precision
);


ALTER TABLE tape_recorders OWNER TO postgres;

--
-- Name: tape_track_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_track_list (
    id integer NOT NULL,
    side text,
    title text,
    time_length text,
    tape_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_track_list OWNER TO postgres;

--
-- Name: TABLE tape_track_list; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tape_track_list IS 'contains track lists in a tape for both sides';


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_track_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_track_list_id_seq OWNER TO postgres;

--
-- Name: tape_track_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tape_track_list_id_seq OWNED BY tape_track_list.id;


--
-- Name: tapes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tapes (
    id integer NOT NULL,
    title_composition text,
    artist_composer text,
    release_number text,
    release_date date,
    credits jsonb,
    label text,
    classical integer,
    genre integer,
    track integer,
    speed integer,
    channel integer,
    reel_size integer,
    noise_reduction integer,
    duplication integer,
    master_duplication integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tapes OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tapes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tapes_id_seq OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tapes_id_seq OWNED BY tapes.id;


--
-- Name: tracks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tracks (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tracks OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tracks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tracks_id_seq OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tracks_id_seq OWNED BY tracks.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    verification_key text,
    type text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    verified smallint,
    token_timestamp_updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: voltage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE voltage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE voltage_id_seq OWNER TO postgres;

--
-- Name: voltage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE voltage (
    id integer DEFAULT nextval('voltage_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE voltage OWNER TO postgres;

--
-- Name: catalog_images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images ALTER COLUMN id SET DEFAULT nextval('catalog_images_id_seq'::regclass);


--
-- Name: catalog_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types ALTER COLUMN id SET DEFAULT nextval('catalog_types_id_seq'::regclass);


--
-- Name: catalogs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs ALTER COLUMN id SET DEFAULT nextval('catalogs_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq1'::regclass);


--
-- Name: channels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels ALTER COLUMN id SET DEFAULT nextval('channels_id_seq'::regclass);


--
-- Name: classical id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical ALTER COLUMN id SET DEFAULT nextval('classical_id_seq'::regclass);


--
-- Name: duplications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications ALTER COLUMN id SET DEFAULT nextval('duplications_id_seq'::regclass);


--
-- Name: expanders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders ALTER COLUMN id SET DEFAULT nextval('expanders_id_seq'::regclass);


--
-- Name: genres id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres ALTER COLUMN id SET DEFAULT nextval('genres_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: noise_reductions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions ALTER COLUMN id SET DEFAULT nextval('noise_reductions_id_seq'::regclass);


--
-- Name: reel_sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes ALTER COLUMN id SET DEFAULT nextval('reel_sizes_id_seq'::regclass);


--
-- Name: speeds id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds ALTER COLUMN id SET DEFAULT nextval('speeds_id_seq'::regclass);


--
-- Name: tape_track_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list ALTER COLUMN id SET DEFAULT nextval('tape_track_list_id_seq'::regclass);


--
-- Name: tapes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes ALTER COLUMN id SET DEFAULT nextval('tapes_id_seq'::regclass);


--
-- Name: tracks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks ALTER COLUMN id SET DEFAULT nextval('tracks_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO application VALUES (1, 'Consumer', NULL, NULL, NULL);
INSERT INTO application VALUES (2, 'Portable', NULL, NULL, NULL);
INSERT INTO application VALUES (3, 'Semi-Pro', NULL, NULL, NULL);
INSERT INTO application VALUES (4, 'Studio', NULL, NULL, NULL);


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('application_id_seq', 4, true);


--
-- Data for Name: auto_reverse; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auto_reverse VALUES (1, 'Yes', NULL, NULL, NULL);
INSERT INTO auto_reverse VALUES (2, 'No', NULL, NULL, NULL);


--
-- Name: auto_reverse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_reverse_id_seq', 2, true);


--
-- Data for Name: brand_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: brand_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brand_images_id_seq', 1, false);


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: brands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brands_id_seq', 1, false);


--
-- Data for Name: catalog_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalog_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_images_id_seq', 1, false);


--
-- Data for Name: catalog_types; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalog_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_types_id_seq', 1, false);


--
-- Data for Name: catalogs; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalogs_id_seq', 1, false);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: categories_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq1', 1, false);


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO channels VALUES (1, 'Stero', NULL, NULL, NULL);
INSERT INTO channels VALUES (2, 'Mono', NULL, NULL, NULL);
INSERT INTO channels VALUES (3, 'Quad', NULL, NULL, NULL);
INSERT INTO channels VALUES (4, 'trest', NULL, NULL, '2018-01-05 11:00:36');
INSERT INTO channels VALUES (5, 'test', NULL, NULL, '2018-01-05 11:00:43');
INSERT INTO channels VALUES (10, 'test7', NULL, NULL, '2018-01-05 11:02:33');
INSERT INTO channels VALUES (11, 'test6', NULL, NULL, '2018-01-05 11:02:39');
INSERT INTO channels VALUES (9, 'test5', NULL, NULL, '2018-01-05 11:07:41');
INSERT INTO channels VALUES (7, 'test3', NULL, NULL, '2018-01-05 11:18:39');
INSERT INTO channels VALUES (8, 'test4', NULL, NULL, '2018-01-05 11:18:42');
INSERT INTO channels VALUES (6, 'test1', NULL, NULL, '2018-01-05 11:18:45');


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('channels_id_seq', 11, true);


--
-- Data for Name: classical; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO classical VALUES (1, 'Not Classical', NULL, NULL, NULL);
INSERT INTO classical VALUES (2, 'Orchastra', NULL, NULL, NULL);
INSERT INTO classical VALUES (3, 'concerto ', NULL, NULL, NULL);
INSERT INTO classical VALUES (4, 'sontanta ', NULL, NULL, NULL);


--
-- Name: classical_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('classical_id_seq', 9, true);


--
-- Data for Name: duplications; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO duplications VALUES (1, 'Ampex', NULL, NULL, NULL);
INSERT INTO duplications VALUES (2, 'Sterotape ', NULL, NULL, NULL);
INSERT INTO duplications VALUES (3, 'Columbia', NULL, NULL, NULL);
INSERT INTO duplications VALUES (4, 'Magtec', NULL, NULL, NULL);
INSERT INTO duplications VALUES (5, 'Barclay Croker', NULL, NULL, NULL);
INSERT INTO duplications VALUES (6, 'RCA', NULL, NULL, NULL);
INSERT INTO duplications VALUES (7, 'Mecury', NULL, NULL, NULL);
INSERT INTO duplications VALUES (8, 'Other', NULL, NULL, NULL);


--
-- Name: duplications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('duplications_id_seq', 8, true);


--
-- Data for Name: electronics; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO electronics VALUES (1, 'Solid State', NULL, NULL, NULL);
INSERT INTO electronics VALUES (2, 'Tube', NULL, NULL, NULL);
INSERT INTO electronics VALUES (3, 'Hybrid', NULL, NULL, NULL);


--
-- Name: electronics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('electronics_id_seq', 3, true);


--
-- Data for Name: equalization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO equalization VALUES (1, 'NAB', NULL, NULL, NULL);
INSERT INTO equalization VALUES (2, 'IEC', NULL, NULL, NULL);
INSERT INTO equalization VALUES (3, 'AES', NULL, NULL, NULL);
INSERT INTO equalization VALUES (4, 'Other', NULL, NULL, NULL);


--
-- Name: equalization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('equalization_id_seq', 4, true);


--
-- Data for Name: expander_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expander_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expander_images_seq', 1, false);


--
-- Data for Name: expanders; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expanders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expanders_id_seq', 1, false);


--
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO genres VALUES (1, 'Blues ', NULL, NULL, NULL);
INSERT INTO genres VALUES (2, 'Country', NULL, NULL, NULL);
INSERT INTO genres VALUES (3, 'Electronic ', NULL, NULL, NULL);
INSERT INTO genres VALUES (4, 'Folk', NULL, NULL, NULL);
INSERT INTO genres VALUES (5, 'Jazz', NULL, NULL, NULL);
INSERT INTO genres VALUES (6, 'Latin', NULL, NULL, NULL);
INSERT INTO genres VALUES (7, 'New Age', NULL, NULL, NULL);
INSERT INTO genres VALUES (8, 'Pop', NULL, NULL, NULL);
INSERT INTO genres VALUES (9, 'Reggae', NULL, NULL, NULL);
INSERT INTO genres VALUES (10, 'Rock', NULL, NULL, NULL);
INSERT INTO genres VALUES (11, 'Soul', NULL, NULL, NULL);
INSERT INTO genres VALUES (12, 'Soundtrack', NULL, NULL, NULL);
INSERT INTO genres VALUES (13, 'Misc', NULL, NULL, NULL);
INSERT INTO genres VALUES (14, 'CP (CrockPot)', NULL, NULL, NULL);
INSERT INTO genres VALUES (15, 'Other', NULL, NULL, NULL);


--
-- Name: genres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('genres_id_seq', 15, true);


--
-- Data for Name: head_composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_composition VALUES (1, 'F&F', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (2, 'Metal', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (3, 'Glass', NULL, NULL, NULL);


--
-- Name: head_composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_composition_id_seq', 3, true);


--
-- Data for Name: head_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_configuration VALUES (2, 'Mono - Full Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (3, 'Mono -Dual Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (4, 'Mono - Half Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (5, 'sterero ', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (6, 'Sterero - Stacked', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (7, 'Sterero - Staggered', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (8, 'Quad', NULL, NULL, NULL);


--
-- Name: head_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_configuration_id_seq', 7, true);


--
-- Data for Name: inputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO inputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO inputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO inputs VALUES (3, 'XLR (Unbalanced)', NULL, NULL, NULL);


--
-- Name: inputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inputs_id_seq', 3, true);


--
-- Data for Name: manufacturer_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturer_images_id_seq', 1, false);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 1, false);


--
-- Data for Name: max_reel_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO max_reel_size VALUES (1, '3', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (2, '5', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (3, '5 3/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (4, '8 1/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (5, '10 1/2', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (6, '10 1/2+', NULL, NULL, NULL);


--
-- Name: max_reel_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('max_reel_size_id_seq', 6, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations VALUES (3, '2017_12_21_130726_create_categories_nested_set', 1);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 3, true);


--
-- Data for Name: motors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO motors VALUES (1, '1', NULL, NULL, NULL);
INSERT INTO motors VALUES (2, '2', NULL, NULL, NULL);
INSERT INTO motors VALUES (3, '3', NULL, NULL, NULL);
INSERT INTO motors VALUES (4, '4', NULL, NULL, NULL);


--
-- Name: motors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('motors_id_seq', 4, true);


--
-- Data for Name: noise_reductions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO noise_reductions VALUES (1, 'Doblby B', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (2, 'Dobly C', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (3, 'DBX', NULL, NULL, NULL);


--
-- Name: noise_reductions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('noise_reductions_id_seq', 3, true);


--
-- Data for Name: number_of_heads; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO number_of_heads VALUES (1, '1 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (2, '2 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (3, '3 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (4, '4 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (5, '5 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (6, '6 (PB)', NULL, NULL, NULL);


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('number_of_heads_id_seq', 5, true);


--
-- Data for Name: outputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO outputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO outputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (3, 'XLR (unbalanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (4, 'CCIR/IEC1', NULL, NULL, NULL);
INSERT INTO outputs VALUES (5, 'NAB/IEC2', NULL, NULL, NULL);
INSERT INTO outputs VALUES (6, 'Class A', NULL, NULL, NULL);
INSERT INTO outputs VALUES (7, 'Class A/B', NULL, NULL, NULL);


--
-- Name: outputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('outputs_id_seq', 7, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: password_resets_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('password_resets_seq', 1, false);


--
-- Data for Name: reel_sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reel_sizes VALUES (1, '5"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (2, '7"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (3, '10"', NULL, NULL, NULL);


--
-- Name: reel_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reel_sizes_id_seq', 3, true);


--
-- Data for Name: speeds; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO speeds VALUES (1, '3 3/4', NULL, NULL, NULL);
INSERT INTO speeds VALUES (2, '7 1/2', NULL, NULL, NULL);
INSERT INTO speeds VALUES (3, '15 i.ps.', NULL, NULL, NULL);
INSERT INTO speeds VALUES (4, '30 i.p.s', NULL, NULL, NULL);


--
-- Name: speeds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('speeds_id_seq', 4, true);


--
-- Data for Name: tape_head_preamp; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_id_seq', 1, false);


--
-- Data for Name: tape_head_preamp_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_images_id_seq', 1, false);


--
-- Data for Name: tape_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_images VALUES (3, 3, 'images/bHZoBfbGF4cKnIE1STouQmckKkD0n81WSjVImlAJ.png', 0, '2018-01-09 10:40:44', '2018-01-09 10:40:44', NULL, NULL);
INSERT INTO tape_images VALUES (4, 4, 'images/VK1YUbejfxsMip5Sdp5Jqp9oDYfpRsNBszL6SF0i.png', 0, '2018-01-09 10:41:05', '2018-01-09 10:41:05', NULL, NULL);
INSERT INTO tape_images VALUES (5, 5, 'images/PelTClOkitBZC9Inny1o6LlSYdFQui8RdvdJvWF5.png', 0, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL, NULL);
INSERT INTO tape_images VALUES (6, 6, 'public/images/yMWImgJgmfjumoqXZ4v1fA3aXZeXpGcZzUg9VGwE.png', 0, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL, NULL);
INSERT INTO tape_images VALUES (7, 7, 'public/images/eOYSDlPwSRdW28i8UzDpq9aPssc9wh0j3jYeVjqW.jpeg', 0, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL, NULL);
INSERT INTO tape_images VALUES (8, 8, 'public/images/wvHFBGIVM9GBSOhNpLprjC9SM9JK7KtTW2KXeuc1.jpeg', 0, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL, NULL);
INSERT INTO tape_images VALUES (9, 9, 'public/images/6efg4UH8WkHdoJHxkcL8b5rSTuqj4Uhy0fFIcJft.jpeg', 0, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL, NULL);
INSERT INTO tape_images VALUES (10, 10, 'public/images/UtqamkdJd1K1e2IJWCJpjNne4ta4uoe24YyU7S8t.jpeg', 0, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL, NULL);
INSERT INTO tape_images VALUES (11, 11, 'public/images/r6EhIoWtj1fmXlRz4rQvwXwYB1xmNb9czghJCWoa.jpeg', 0, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL, NULL);
INSERT INTO tape_images VALUES (12, 12, 'public/images/Qmpqxp3YtBq0DEQaw2hRE1QpDAZkUdLsq4Q3gziZ.jpeg', 0, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL, NULL);
INSERT INTO tape_images VALUES (13, 13, 'public/images/m2IsUAivKBpLvq3izedTPt9Qo2o2pF1BGYqgLRV5.jpeg', 0, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL, NULL);
INSERT INTO tape_images VALUES (14, 14, 'public/images/9DDmZnqtE01lowpqtuobO0Ybkw5olLZIq6R4eLup.jpeg', 0, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL, NULL);
INSERT INTO tape_images VALUES (15, 15, 'public/images/tUjVEYv7EBLWrtUsIacrFzlt6E61H7IDUlC2GUbP.jpeg', 0, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL, NULL);
INSERT INTO tape_images VALUES (16, 16, 'public/images/wq0wkdhlxSkigpcNDRvTpW8C4AoXPLDT2GkFRMob.jpeg', 0, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL, NULL);
INSERT INTO tape_images VALUES (17, 17, 'public/images/7uebFnf4aBNnUxg72f0p6Lvpxwg41N6tWAIlGoYH.jpeg', 0, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL, NULL);
INSERT INTO tape_images VALUES (18, 18, 'public/images/Wrh0WUFeLLUyUQJQQlBpFpk5PcdXWnFY0zK2llj7.jpeg', 0, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL, NULL);
INSERT INTO tape_images VALUES (19, 19, 'public/images/Qn4rFtw6oKshVZ38wXdfxldqoN29iuugrDRR31td.jpeg', 0, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL, NULL);
INSERT INTO tape_images VALUES (20, 20, 'tapes/lwhLcTBWJxNeLJ9MobvULBPiAaRx3kihxdZylkK4.png', 0, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL, 'tapes/dArtNQKvbAQOsZLMUSLb0qCOlGKyq1aoRVWMNvHw.png');


--
-- Name: tape_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_images_seq', 20, true);


--
-- Data for Name: tape_recorder_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorder_images_id_seq', 1, false);


--
-- Data for Name: tape_recorders; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_recorders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorders_id_seq', 1, false);


--
-- Data for Name: tape_track_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_track_list VALUES (1, 'a', 'asdas', '01:48', 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (2, 'a', 'adadsas', '11:48', 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (3, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (4, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (5, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (6, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (7, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (8, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (9, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (10, 'a', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (11, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (12, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (13, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (14, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (15, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (16, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (17, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (18, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (19, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (20, 'b', NULL, NULL, 5, '2018-01-09 11:19:42', '2018-01-09 11:19:42', NULL);
INSERT INTO tape_track_list VALUES (21, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (22, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (23, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (24, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (25, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (26, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (27, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (28, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (29, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (30, 'a', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (31, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (32, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (33, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (34, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (35, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (36, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (37, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (38, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (39, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (40, 'b', NULL, NULL, 6, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tape_track_list VALUES (41, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (42, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (43, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (44, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (45, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (46, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (47, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (48, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (49, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (50, 'a', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (51, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (52, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (53, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (54, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (55, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (56, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (57, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (58, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (59, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (60, 'b', NULL, NULL, 7, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tape_track_list VALUES (61, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (62, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (63, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (64, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (65, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (66, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (67, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (68, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (69, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (70, 'a', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (71, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (72, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (73, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (74, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (75, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (76, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (77, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (78, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (79, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (80, 'b', NULL, NULL, 8, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (81, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (82, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (83, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (84, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (85, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (86, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (87, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (88, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (89, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (90, 'a', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (91, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (92, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (93, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (94, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (95, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (96, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (97, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (98, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (99, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (100, 'b', NULL, NULL, 9, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (101, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (102, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (103, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (104, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (105, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (106, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (107, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (108, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (109, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (110, 'a', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (111, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (112, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (113, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (114, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (115, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (116, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (117, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (118, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (119, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (120, 'b', NULL, NULL, 10, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tape_track_list VALUES (121, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (122, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (123, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (124, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (125, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (126, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (127, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (128, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (129, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (130, 'a', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (131, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (132, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (133, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (134, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (135, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (136, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (137, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (138, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (139, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (140, 'b', NULL, NULL, 11, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (141, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (142, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (143, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (144, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (145, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (146, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (147, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (148, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (149, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (150, 'a', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (151, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (152, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (153, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (154, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (155, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (156, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (157, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (158, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (159, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (160, 'b', NULL, NULL, 12, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (161, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (162, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (163, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (164, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (165, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (166, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (167, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (168, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (169, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (170, 'a', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (171, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (172, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (173, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (174, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (175, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (176, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (177, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (178, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (179, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (180, 'b', NULL, NULL, 13, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tape_track_list VALUES (181, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (182, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (183, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (184, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (185, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (186, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (187, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (188, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (189, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (190, 'a', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (191, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (192, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (193, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (194, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (195, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (196, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (197, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (198, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (199, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (200, 'b', NULL, NULL, 14, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tape_track_list VALUES (201, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (202, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (203, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (204, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (205, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (206, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (207, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (208, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (209, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (210, 'a', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (211, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (212, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (213, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (214, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (215, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (216, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (217, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (218, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (219, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (220, 'b', NULL, NULL, 15, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tape_track_list VALUES (221, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (222, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (223, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (224, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (225, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (226, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (227, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (228, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (229, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (230, 'a', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (231, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (232, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (233, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (234, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (235, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (236, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (237, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (238, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (239, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (240, 'b', NULL, NULL, 16, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (241, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (242, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (243, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (244, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (245, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (246, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (247, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (248, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (249, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (250, 'a', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (251, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (252, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (253, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (254, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (255, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (256, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (257, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (258, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (259, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (260, 'b', NULL, NULL, 17, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tape_track_list VALUES (261, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (262, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (263, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (264, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (265, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (266, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (267, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (268, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (269, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (270, 'a', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (271, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (272, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (273, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (274, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (275, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (276, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (277, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (278, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (279, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (280, 'b', NULL, NULL, 18, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (281, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (282, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (283, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (284, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (285, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (286, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (287, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (288, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (289, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (290, 'a', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (291, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (292, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (293, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (294, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (295, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (296, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (297, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (298, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (299, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (300, 'b', NULL, NULL, 19, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tape_track_list VALUES (301, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (302, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (303, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (304, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (305, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (306, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (307, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (308, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (309, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (310, 'a', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (311, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (312, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (313, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (314, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (315, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (316, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (317, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (318, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (319, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);
INSERT INTO tape_track_list VALUES (320, 'b', NULL, NULL, 20, '2018-01-25 12:58:43', '2018-01-25 12:58:43', NULL);


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_track_list_id_seq', 320, true);


--
-- Data for Name: tapes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tapes VALUES (3, 'title', 'firstname lastname', '10', '2018-01-13', '{"credit": null, "musician": null}', 'lakdn', 3, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-09 10:40:44', '2018-01-09 10:40:44', NULL);
INSERT INTO tapes VALUES (4, 'title', 'firstname lastname', '10', '2018-01-13', '{"credit": null, "musician": null}', 'lakdn', 3, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-09 10:41:05', '2018-01-09 10:41:05', NULL);
INSERT INTO tapes VALUES (5, 'title', 'firstname lastname', '123', '2018-01-23', '{"credit": null, "musician": null}', 'lakdn', 2, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-09 11:19:41', '2018-01-09 11:19:41', NULL);
INSERT INTO tapes VALUES (6, 'title', 'firstname lastname', '231', '2018-01-13', '{"credit": null, "musician": null}', 'adas', 1, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-09 14:00:40', '2018-01-09 14:00:40', NULL);
INSERT INTO tapes VALUES (7, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:00', '2018-01-12 12:55:00', NULL);
INSERT INTO tapes VALUES (8, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:01', '2018-01-12 12:55:01', NULL);
INSERT INTO tapes VALUES (9, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tapes VALUES (10, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:02', '2018-01-12 12:55:02', NULL);
INSERT INTO tapes VALUES (11, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tapes VALUES (12, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tapes VALUES (13, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:55:03', '2018-01-12 12:55:03', NULL);
INSERT INTO tapes VALUES (14, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:58:14', '2018-01-12 12:58:14', NULL);
INSERT INTO tapes VALUES (15, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 12:58:15', '2018-01-12 12:58:15', NULL);
INSERT INTO tapes VALUES (16, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, 3, 1, NULL, NULL, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tapes VALUES (17, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, 3, 1, NULL, NULL, '2018-01-12 12:59:11', '2018-01-12 12:59:11', NULL);
INSERT INTO tapes VALUES (18, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, 3, 1, NULL, NULL, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tapes VALUES (19, 'Test tape', 'Demo User', '482424294824924', '2010-11-24', '{"credit": null, "musician": null}', 'Magnetic', 3, 10, NULL, 2, NULL, 1, 3, 1, NULL, NULL, '2018-01-12 12:59:12', '2018-01-12 12:59:12', NULL);
INSERT INTO tapes VALUES (20, 'title', 'firstname lastname', '2', '2018-01-25', '{"credit": null, "musician": null}', 'lakdn', 1, 1, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-25 12:58:39', '2018-01-25 12:58:39', NULL);


--
-- Name: tapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tapes_id_seq', 20, true);


--
-- Data for Name: tracks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tracks VALUES (1, '4 Track', NULL, NULL, NULL);
INSERT INTO tracks VALUES (2, '2 Track Inline/Stacked', NULL, NULL, NULL);
INSERT INTO tracks VALUES (3, '2 Track Staggered', NULL, NULL, NULL);
INSERT INTO tracks VALUES (4, 'Quad', NULL, NULL, NULL);


--
-- Name: tracks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tracks_id_seq', 4, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'ashish', 'ashish@citrusleaf.in', '$2y$10$XewljynHY4tUK7EKFzWd4u0CCeFAN9C5aQSXPhMvbnm8nz.RoyI8G', NULL, 'U47FsYeh', 'Admin', '2017-12-23 17:35:25', '2017-12-23 17:35:25', NULL, NULL, NULL);
INSERT INTO users VALUES (2, 'ashish', 'ashish+1@citrusleaf.in', '$2y$10$CqPv0zZga7AR62oOr88zSOFe8HnGgAHa5Smqic6rwATlwCz9uJncm', NULL, 'N6COqRLS', 'Admin', '2017-12-23 17:36:25', '2017-12-23 17:36:25', NULL, NULL, NULL);
INSERT INTO users VALUES (3, 'ashish', 'ashish+2@citrusleaf.in', '$2y$10$1PWsw0gysZW6ecQ0EfiOBuM7vYtRSub5N5qw.L2SUsltnn0D/SgAa', NULL, 'fFiXI1HP', 'Admin', '2017-12-23 17:37:40', '2017-12-23 17:37:40', NULL, NULL, NULL);
INSERT INTO users VALUES (5, 'ashish', 'ashish.verma250990+1@gmail.com', '$2y$10$Itz8nKtn2.xjupeJQVWd9urLi5mgg67Rv6OimWyunl8ctKvtrtkyy', NULL, 'XUS4Zwrg', 'Admin', '2017-12-23 17:54:55', '2017-12-23 17:54:55', NULL, NULL, NULL);
INSERT INTO users VALUES (6, 'ashish', 'ashish.verma250990+2@gmail.com', '$2y$10$/aaaIUidyZAQEktsO3LAIOCFUZkaOY1rxThjkjVKjqM9.37SNxA7a', NULL, 'ghAZbnTd', 'Admin', '2017-12-23 17:56:51', '2017-12-23 17:56:51', NULL, NULL, NULL);
INSERT INTO users VALUES (7, 'ashish', 'ashish.verma250990+3@gmail.com', '$2y$10$1WZi.pCId3BGHH86SAUTKe46BpUmNeUSnsjKtOfWJh730XyJU3rZC', NULL, 'N3KzZTdY', 'Admin', '2017-12-23 17:59:48', '2017-12-23 17:59:48', NULL, NULL, NULL);
INSERT INTO users VALUES (8, 'ashish', 'ashish.verma250990+4@gmail.com', '$2y$10$ZzXJL0z0vwmBcepSix58u.kC.z2ilPvABGNhzKA.6luOYaXD70WCu', NULL, 'gFv2aEh0', 'Admin', '2017-12-24 11:33:45', '2017-12-24 11:33:45', NULL, NULL, NULL);
INSERT INTO users VALUES (9, 'ashish', 'ashish.verma250990+5@gmail.com', '$2y$10$Gn0v/hmLZo4oRk7OOWUpUuerCDLZqSbQlQRbhBEA.k.Al3PGZ4E2G', NULL, NULL, 'Admin', '2017-12-24 11:35:37', '2017-12-24 11:53:28', 1, NULL, NULL);
INSERT INTO users VALUES (10, 'ashish', 'ashish.verma250990+6@gmail.com', '$2y$10$uj4y13hdIML.XSXijQgC5.Wk5RtZmn6KfbcvC3VZTfPLYLM4bVb3q', NULL, NULL, 'Subscriber', '2017-12-24 11:54:56', '2017-12-24 17:27:08', 1, NULL, NULL);
INSERT INTO users VALUES (11, 'Admin', 'admin@gmail.com', '$2y$10$EF2Qbmqm50izj4u08R9aVuykXvdoDVTv8JqqwMXhvNMd17Pm8P8BK', NULL, NULL, 'Admin', '2018-01-25 12:47:35', '2018-01-29 07:10:28', 1, '2018-01-29 07:10:28', NULL);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 11, true);


--
-- Data for Name: voltage; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO voltage VALUES (1, '100v Japan', NULL, NULL, NULL);
INSERT INTO voltage VALUES (2, '110-120v', NULL, NULL, NULL);
INSERT INTO voltage VALUES (3, 'Multi', NULL, NULL, NULL);
INSERT INTO voltage VALUES (4, 'other', NULL, NULL, NULL);


--
-- Name: voltage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('voltage_id_seq', 4, true);


--
-- Name: application application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- Name: auto_reverse auto_reverse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auto_reverse
    ADD CONSTRAINT auto_reverse_pkey PRIMARY KEY (id);


--
-- Name: brand_images brand_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_brand_id_key UNIQUE (brand_id);


--
-- Name: brand_images brand_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_pkey PRIMARY KEY (id);


--
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);


--
-- Name: catalog_images catalog_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_pkey PRIMARY KEY (id);


--
-- Name: catalog_types catalog_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types
    ADD CONSTRAINT catalog_types_pkey PRIMARY KEY (id);


--
-- Name: catalogs catalogs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: classical classical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical
    ADD CONSTRAINT classical_pkey PRIMARY KEY (id);


--
-- Name: duplications duplications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications
    ADD CONSTRAINT duplications_pkey PRIMARY KEY (id);


--
-- Name: electronics electronics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY electronics
    ADD CONSTRAINT electronics_pkey PRIMARY KEY (id);


--
-- Name: equalization equalization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equalization
    ADD CONSTRAINT equalization_pkey PRIMARY KEY (id);


--
-- Name: expander_images expander_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_pkey PRIMARY KEY (id);


--
-- Name: expanders expanders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders
    ADD CONSTRAINT expanders_pkey PRIMARY KEY (id);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id);


--
-- Name: head_composition head_composition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_composition
    ADD CONSTRAINT head_composition_pkey PRIMARY KEY (id);


--
-- Name: head_configuration head_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_configuration
    ADD CONSTRAINT head_configuration_pkey PRIMARY KEY (id);


--
-- Name: inputs inputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inputs
    ADD CONSTRAINT inputs_pkey PRIMARY KEY (id);


--
-- Name: manufacturer_images manufacturer_images_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: manufacturer_images manufacturer_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_pkey PRIMARY KEY (id);


--
-- Name: max_reel_size max_reel_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY max_reel_size
    ADD CONSTRAINT max_reel_size_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: motors motors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY motors
    ADD CONSTRAINT motors_pkey PRIMARY KEY (id);


--
-- Name: noise_reductions noise_reductions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions
    ADD CONSTRAINT noise_reductions_pkey PRIMARY KEY (id);


--
-- Name: number_of_heads number_of_heads_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY number_of_heads
    ADD CONSTRAINT number_of_heads_pkey PRIMARY KEY (id);


--
-- Name: outputs outputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY outputs
    ADD CONSTRAINT outputs_pkey PRIMARY KEY (id);


--
-- Name: reel_sizes reel_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes
    ADD CONSTRAINT reel_sizes_pkey PRIMARY KEY (id);


--
-- Name: speeds speeds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds
    ADD CONSTRAINT speeds_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp tape_head_preamp_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_brand_id_key UNIQUE (brand_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_brand_id_key UNIQUE (tape_head_preamp_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp tape_head_preamp_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: tape_head_preamp tape_head_preamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_pkey PRIMARY KEY (id);


--
-- Name: tape_images tape_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorders tape_recorders_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_brand_id_key UNIQUE (brand_id);


--
-- Name: tape_recorders tape_recorders_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: tape_recorders tape_recorders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_pkey PRIMARY KEY (id);


--
-- Name: tape_track_list tape_track_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_pkey PRIMARY KEY (id);


--
-- Name: tapes tapes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_pkey PRIMARY KEY (id);


--
-- Name: tracks tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks
    ADD CONSTRAINT tracks_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: voltage voltage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY voltage
    ADD CONSTRAINT voltage_pkey PRIMARY KEY (id);


--
-- Name: categories__lft__rgt_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories__lft__rgt_parent_id_index ON categories USING btree (_lft, _rgt, parent_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: catalog_images catalog_images_catalog_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_catalog_id_fkey FOREIGN KEY (catalog_id) REFERENCES catalogs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: catalogs catalogs_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_type_fkey FOREIGN KEY (type) REFERENCES catalog_types(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expander_images expander_images_expander_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_expander_id_fkey FOREIGN KEY (expander_id) REFERENCES expanders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_images tape_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_track_list tape_track_list_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE SET DEFAULT;


--
-- Name: tapes tapes_channel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_channel_fkey FOREIGN KEY (channel) REFERENCES channels(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_classical_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_classical_fkey FOREIGN KEY (classical) REFERENCES classical(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_duplication_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_duplication_fkey FOREIGN KEY (duplication) REFERENCES duplications(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_genre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_genre_fkey FOREIGN KEY (genre) REFERENCES genres(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_noise_reduction_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_noise_reduction_fkey FOREIGN KEY (noise_reduction) REFERENCES noise_reductions(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_reel_size_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_reel_size_fkey FOREIGN KEY (reel_size) REFERENCES reel_sizes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_speed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_speed_fkey FOREIGN KEY (speed) REFERENCES speeds(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_track_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_track_fkey FOREIGN KEY (track) REFERENCES tracks(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO rathi;


--
-- PostgreSQL database dump complete
--

