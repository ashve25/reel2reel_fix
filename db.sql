--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE application_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE application (
    id integer DEFAULT nextval('application_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE application OWNER TO postgres;

--
-- Name: auto_reverse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_reverse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auto_reverse_id_seq OWNER TO postgres;

--
-- Name: auto_reverse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auto_reverse (
    id integer DEFAULT nextval('auto_reverse_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE auto_reverse OWNER TO postgres;

--
-- Name: brand_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brand_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brand_images_id_seq OWNER TO postgres;

--
-- Name: brand_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brand_images (
    id integer DEFAULT nextval('brand_images_id_seq'::regclass) NOT NULL,
    brand_id integer,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type smallint
);


ALTER TABLE brand_images OWNER TO postgres;

--
-- Name: brands_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brands_id_seq OWNER TO postgres;

--
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brands (
    id integer DEFAULT nextval('brands_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    description text,
    innovations text,
    information text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    manufacturer_id integer,
    year_to integer,
    year_from integer,
    location text
);


ALTER TABLE brands OWNER TO postgres;

--
-- Name: catalog_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_images (
    id integer NOT NULL,
    catalog_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text
);


ALTER TABLE catalog_images OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_images_id_seq OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_images_id_seq OWNED BY catalog_images.id;


--
-- Name: catalog_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_types (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalog_types OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_types_id_seq OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_types_id_seq OWNED BY catalog_types.id;


--
-- Name: catalogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalogs (
    id integer NOT NULL,
    title text,
    type integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalogs OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogs_id_seq OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalogs_id_seq OWNED BY catalogs.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    _lft integer DEFAULT 0 NOT NULL,
    _rgt integer DEFAULT 0 NOT NULL,
    parent_id integer,
    name text,
    id integer NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq1 OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq1 OWNED BY categories.id;


--
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE channels (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE channels OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE channels_id_seq OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE channels_id_seq OWNED BY channels.id;


--
-- Name: classical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE classical (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE classical OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE classical_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE classical_id_seq OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE classical_id_seq OWNED BY classical.id;


--
-- Name: duplications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE duplications (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE duplications OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE duplications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE duplications_id_seq OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE duplications_id_seq OWNED BY duplications.id;


--
-- Name: electronics_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE electronics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE electronics_id_seq OWNER TO postgres;

--
-- Name: electronics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE electronics (
    id integer DEFAULT nextval('electronics_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE electronics OWNER TO postgres;

--
-- Name: equalization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE equalization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE equalization_id_seq OWNER TO postgres;

--
-- Name: equalization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE equalization (
    id integer DEFAULT nextval('equalization_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE equalization OWNER TO postgres;

--
-- Name: expander_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expander_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expander_images_seq OWNER TO postgres;

--
-- Name: expander_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expander_images (
    id integer DEFAULT nextval('expander_images_seq'::regclass) NOT NULL,
    expander_id integer,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    type smallint,
    thumbnail text
);


ALTER TABLE expander_images OWNER TO postgres;

--
-- Name: expanders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expanders (
    id integer NOT NULL,
    model text,
    tape_recorders_usable integer,
    outputs_for_record integer,
    copy_option_decks integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    brand_id integer,
    main_image smallint
);


ALTER TABLE expanders OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expanders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expanders_id_seq OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE expanders_id_seq OWNED BY expanders.id;


--
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE genres (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE genres OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE genres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genres_id_seq OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE genres_id_seq OWNED BY genres.id;


--
-- Name: head_composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_composition_id_seq OWNER TO postgres;

--
-- Name: head_composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_composition (
    id integer DEFAULT nextval('head_composition_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_composition OWNER TO postgres;

--
-- Name: head_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_configuration_id_seq OWNER TO postgres;

--
-- Name: head_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_configuration (
    id integer DEFAULT nextval('head_configuration_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_configuration OWNER TO postgres;

--
-- Name: inputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inputs_id_seq OWNER TO postgres;

--
-- Name: inputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inputs (
    id integer DEFAULT nextval('inputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE inputs OWNER TO postgres;

--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturer_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturer_images_id_seq OWNER TO postgres;

--
-- Name: manufacturer_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturer_images (
    id integer DEFAULT nextval('manufacturer_images_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    type integer
);


ALTER TABLE manufacturer_images OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturers (
    id integer DEFAULT nextval('manufacturers_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    origin_country text NOT NULL,
    year_to integer,
    year_from integer,
    history text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE manufacturers OWNER TO postgres;

--
-- Name: max_reel_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE max_reel_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE max_reel_size_id_seq OWNER TO postgres;

--
-- Name: max_reel_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE max_reel_size (
    id integer DEFAULT nextval('max_reel_size_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE max_reel_size OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: motors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE motors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE motors_id_seq OWNER TO postgres;

--
-- Name: motors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE motors (
    id integer DEFAULT nextval('motors_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE motors OWNER TO postgres;

--
-- Name: noise_reductions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE noise_reductions (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE noise_reductions OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE noise_reductions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE noise_reductions_id_seq OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE noise_reductions_id_seq OWNED BY noise_reductions.id;


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE number_of_heads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE number_of_heads_id_seq OWNER TO postgres;

--
-- Name: number_of_heads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE number_of_heads (
    id integer DEFAULT nextval('number_of_heads_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE number_of_heads OWNER TO postgres;

--
-- Name: outputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE outputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outputs_id_seq OWNER TO postgres;

--
-- Name: outputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE outputs (
    id integer DEFAULT nextval('outputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE outputs OWNER TO postgres;

--
-- Name: password_resets_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE password_resets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE password_resets_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    id integer DEFAULT nextval('password_resets_seq'::regclass) NOT NULL
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: reel_sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reel_sizes (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE reel_sizes OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reel_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reel_sizes_id_seq OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE reel_sizes_id_seq OWNED BY reel_sizes.id;


--
-- Name: speeds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE speeds (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE speeds OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE speeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE speeds_id_seq OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE speeds_id_seq OWNED BY speeds.id;


--
-- Name: tape_head_preamp_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_comments (
    id integer NOT NULL,
    user_id integer,
    comment text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    tape_head_preamp_id integer
);


ALTER TABLE tape_head_preamp_comments OWNER TO postgres;

--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_images_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_images (
    id integer DEFAULT nextval('tape_head_preamp_images_id_seq'::regclass) NOT NULL,
    tape_head_preamp_id integer NOT NULL,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text,
    type smallint
);


ALTER TABLE tape_head_preamp_images OWNER TO postgres;

--
-- Name: tape_head_preamps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamps (
    id integer DEFAULT nextval('tape_head_preamp_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    model text,
    adjustable_gain text,
    adjustable_loading text,
    playback_eq text,
    level_controls text,
    isolated_power_supply text,
    signal_noise_ratio text,
    tubes text,
    suggested_input_wiring text,
    impedance text,
    accessories text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    main_image integer,
    electronic_id integer,
    input_id integer,
    output_id integer
);


ALTER TABLE tape_head_preamps OWNER TO postgres;

--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamps_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamps_comments_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tape_head_preamps_comments_id_seq OWNED BY tape_head_preamp_comments.id;


--
-- Name: tape_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_images_seq OWNER TO postgres;

--
-- Name: tape_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_images (
    id integer DEFAULT nextval('tape_images_seq'::regclass) NOT NULL,
    tape_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text
);


ALTER TABLE tape_images OWNER TO postgres;

--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorder_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorder_images_id_seq OWNER TO postgres;

--
-- Name: tape_recorder_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorder_images (
    id integer DEFAULT nextval('tape_recorder_images_id_seq'::regclass) NOT NULL,
    tape_recorder_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text
);


ALTER TABLE tape_recorder_images OWNER TO postgres;

--
-- Name: tape_recorders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorders_id_seq OWNER TO postgres;

--
-- Name: tape_recorders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorders (
    id integer DEFAULT nextval('tape_recorders_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    brand_id integer,
    model text,
    country_of_manufacturer text,
    serial_number text,
    original_price double precision,
    belts text,
    frequency_response text,
    wow_flutter text,
    signal_noise_ratio text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    release_to_date integer,
    release_from_date integer,
    head_configuration_id integer,
    number_of_head_id integer,
    speed_id integer,
    track_id integer,
    head_composition_id integer,
    equalization_id integer,
    voltage_id integer,
    auto_reverse_id integer,
    application_id integer,
    max_reel_size_id integer,
    noise_reduction_id integer,
    motor_id integer,
    main_image integer
);


ALTER TABLE tape_recorders OWNER TO postgres;

--
-- Name: tape_track_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_track_list (
    id integer NOT NULL,
    side text,
    title text,
    time_length text,
    tape_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_track_list OWNER TO postgres;

--
-- Name: TABLE tape_track_list; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tape_track_list IS 'contains track lists in a tape for both sides';


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_track_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_track_list_id_seq OWNER TO postgres;

--
-- Name: tape_track_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tape_track_list_id_seq OWNED BY tape_track_list.id;


--
-- Name: tapes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tapes (
    id integer NOT NULL,
    title_composition text,
    artist_composer text,
    release_number text,
    release_date date,
    credits jsonb,
    label text,
    classical_id integer,
    genre_id integer,
    track_id integer,
    speed_id integer,
    channel_id integer,
    reel_size_id integer,
    noise_reduction_id integer,
    duplication_id integer,
    master_duplication integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tapes OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tapes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tapes_id_seq OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tapes_id_seq OWNED BY tapes.id;


--
-- Name: tracks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tracks (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tracks OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tracks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tracks_id_seq OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tracks_id_seq OWNED BY tracks.id;


--
-- Name: user_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_types (
    type text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    id integer NOT NULL
);


ALTER TABLE user_types OWNER TO postgres;

--
-- Name: user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_types_id_seq OWNER TO postgres;

--
-- Name: user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_types_id_seq OWNED BY user_types.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    verification_key text,
    type text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    verified smallint,
    token_timestamp_updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    user_type smallint,
    profile_picture text,
    country text,
    city text
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: voltage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE voltage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE voltage_id_seq OWNER TO postgres;

--
-- Name: voltage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE voltage (
    id integer DEFAULT nextval('voltage_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE voltage OWNER TO postgres;

--
-- Name: catalog_images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images ALTER COLUMN id SET DEFAULT nextval('catalog_images_id_seq'::regclass);


--
-- Name: catalog_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types ALTER COLUMN id SET DEFAULT nextval('catalog_types_id_seq'::regclass);


--
-- Name: catalogs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs ALTER COLUMN id SET DEFAULT nextval('catalogs_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq1'::regclass);


--
-- Name: channels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels ALTER COLUMN id SET DEFAULT nextval('channels_id_seq'::regclass);


--
-- Name: classical id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical ALTER COLUMN id SET DEFAULT nextval('classical_id_seq'::regclass);


--
-- Name: duplications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications ALTER COLUMN id SET DEFAULT nextval('duplications_id_seq'::regclass);


--
-- Name: expanders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders ALTER COLUMN id SET DEFAULT nextval('expanders_id_seq'::regclass);


--
-- Name: genres id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres ALTER COLUMN id SET DEFAULT nextval('genres_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: noise_reductions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions ALTER COLUMN id SET DEFAULT nextval('noise_reductions_id_seq'::regclass);


--
-- Name: reel_sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes ALTER COLUMN id SET DEFAULT nextval('reel_sizes_id_seq'::regclass);


--
-- Name: speeds id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds ALTER COLUMN id SET DEFAULT nextval('speeds_id_seq'::regclass);


--
-- Name: tape_head_preamp_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_comments ALTER COLUMN id SET DEFAULT nextval('tape_head_preamps_comments_id_seq'::regclass);


--
-- Name: tape_track_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list ALTER COLUMN id SET DEFAULT nextval('tape_track_list_id_seq'::regclass);


--
-- Name: tapes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes ALTER COLUMN id SET DEFAULT nextval('tapes_id_seq'::regclass);


--
-- Name: tracks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks ALTER COLUMN id SET DEFAULT nextval('tracks_id_seq'::regclass);


--
-- Name: user_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_types ALTER COLUMN id SET DEFAULT nextval('user_types_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO application VALUES (1, 'Consumer', NULL, NULL, NULL);
INSERT INTO application VALUES (2, 'Portable', NULL, NULL, NULL);
INSERT INTO application VALUES (3, 'Semi-Pro', NULL, NULL, NULL);
INSERT INTO application VALUES (4, 'Studio', NULL, NULL, NULL);


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('application_id_seq', 4, true);


--
-- Data for Name: auto_reverse; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auto_reverse VALUES (1, 'Yes', NULL, NULL, NULL);
INSERT INTO auto_reverse VALUES (2, 'No', NULL, NULL, NULL);


--
-- Name: auto_reverse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_reverse_id_seq', 2, true);


--
-- Data for Name: brand_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO brand_images VALUES (9, 9, 'public/images/z4qHfyd4Z0u7bN7MaRqStrSVrpWZjkTSQrOTsFLx.jpeg', '2018-02-06 05:32:09', '2018-02-06 05:32:09', 0);


--
-- Name: brand_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brand_images_id_seq', 9, true);


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO brands VALUES (9, 'daz', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (10, 'daz1', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (11, 'daz2', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (12, 'daz3', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (14, 'daz5', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (15, 'daz6', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (16, 'daz7', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (17, 'daz8', 'sad', 'sd', 'asd', '2018-02-06 05:31:58', '2018-02-06 05:31:58', NULL, NULL, 23, 23, 'as');
INSERT INTO brands VALUES (13, 'daz4', 'sad', NULL, NULL, NULL, '2018-02-06 12:17:16', NULL, 22, NULL, NULL, 'aef');


--
-- Name: brands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brands_id_seq', 9, true);


--
-- Data for Name: catalog_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO catalog_images VALUES (1, 5, 'catalog_cover/9tA6daGKQMeptO5dwRhwMZoC740vJY9BzjU40qTm.png', 0, '2018-02-06 09:11:52', '2018-02-06 09:11:52', NULL, NULL);
INSERT INTO catalog_images VALUES (2, 5, 'catalog_others/HJP9Rzen9E3SLEpuV394YphZEeQjk2VlUvovixB4.png', 1, '2018-02-06 09:11:55', '2018-02-06 09:11:55', NULL, NULL);
INSERT INTO catalog_images VALUES (3, 5, 'catalog_others/UzHQYj7218jVnNfMrCrcFKkAwDNgj09AsGdwcgGj.png', 1, '2018-02-06 09:11:58', '2018-02-06 09:11:58', NULL, NULL);
INSERT INTO catalog_images VALUES (4, 6, 'catalog_cover/oIddceqX0m9UP5fJ0XbNGWa3dsFfuNSWgX6cMurt.png', 0, '2018-02-06 09:15:21', '2018-02-06 09:15:21', NULL, NULL);
INSERT INTO catalog_images VALUES (5, 6, 'catalog_others/5XMDHX4OPEFzqEyTEhrGyYT5U4ZneSl5C8gao3Tr.png', 1, '2018-02-06 09:15:26', '2018-02-06 09:15:26', NULL, NULL);
INSERT INTO catalog_images VALUES (6, 6, 'catalog_others/335kxdXHou4Al9qMHTnaBLB45kPXR4oGt5L9wtJk.png', 1, '2018-02-06 09:15:30', '2018-02-06 09:15:30', NULL, NULL);
INSERT INTO catalog_images VALUES (7, 7, 'catalog_cover/dOYLKHrPewPnQ6N6NlH2monBzfXHBxIvZBiQAUVd.png', 0, '2018-02-06 09:21:16', '2018-02-06 09:21:16', NULL, NULL);
INSERT INTO catalog_images VALUES (8, 7, 'catalog_others/ZsLsq7jnOp1f4bcoIoe0Y8nE2yeMf5OEgqiMnAeN.png', 1, '2018-02-06 09:21:20', '2018-02-06 09:21:20', NULL, NULL);
INSERT INTO catalog_images VALUES (9, 8, 'catalogs/Ns3vv5Z7fqSTKvISIfyD9bgT3DfFn3I71zwfyv0j.png', 0, '2018-02-06 09:29:10', '2018-02-06 09:29:10', NULL, NULL);
INSERT INTO catalog_images VALUES (10, 8, 'catalogs/T2OHeZtunFu0GwZVVbXcaFYOOE12NeAcr8pWKUhq.png', 1, '2018-02-06 09:29:13', '2018-02-06 09:29:13', NULL, NULL);
INSERT INTO catalog_images VALUES (11, 23, 'catalogs/QMUVTZ72I1kMAHdykpGA0gKcwLGD2NgCOr2KidYu.png', 0, '2018-02-12 11:53:27', '2018-02-12 11:53:27', NULL, 'catalogs/2MnqcJNPO8ap2hNN7Hpfy1KfK6LMV2sZtoy0alFi.png');


--
-- Name: catalog_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_images_id_seq', 11, true);


--
-- Data for Name: catalog_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO catalog_types VALUES (3, 'Other Catalogs', NULL, NULL, NULL);
INSERT INTO catalog_types VALUES (2, 'Ampex Catalogs', NULL, NULL, NULL);
INSERT INTO catalog_types VALUES (1, 'Harrison Catalogs', NULL, NULL, NULL);


--
-- Name: catalog_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_types_id_seq', 1, false);


--
-- Data for Name: catalogs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO catalogs VALUES (5, 'fasfs', 1, 'afsf', '2018-02-06 09:11:47', '2018-02-06 09:11:47', NULL);
INSERT INTO catalogs VALUES (6, 'asfs', 2, 'assdfas', '2018-02-06 09:15:15', '2018-02-06 09:15:15', NULL);
INSERT INTO catalogs VALUES (7, 'assdas', 2, 'asasd', '2018-02-06 09:21:13', '2018-02-06 09:21:13', NULL);
INSERT INTO catalogs VALUES (8, 'zcsfa', 2, 'afdasd', '2018-02-06 09:29:07', '2018-02-06 09:29:07', NULL);
INSERT INTO catalogs VALUES (23, 'zxcxzc', 2, 'saddsd', '2018-02-12 11:53:10', '2018-02-12 11:53:10', NULL);


--
-- Name: catalogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalogs_id_seq', 23, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: categories_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq1', 1, false);


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO channels VALUES (1, 'Stero', NULL, NULL, NULL);
INSERT INTO channels VALUES (2, 'Mono', NULL, NULL, NULL);
INSERT INTO channels VALUES (3, 'Quad', NULL, NULL, NULL);
INSERT INTO channels VALUES (4, 'trest', NULL, NULL, '2018-01-05 11:00:36');
INSERT INTO channels VALUES (5, 'test', NULL, NULL, '2018-01-05 11:00:43');
INSERT INTO channels VALUES (10, 'test7', NULL, NULL, '2018-01-05 11:02:33');
INSERT INTO channels VALUES (11, 'test6', NULL, NULL, '2018-01-05 11:02:39');
INSERT INTO channels VALUES (9, 'test5', NULL, NULL, '2018-01-05 11:07:41');
INSERT INTO channels VALUES (7, 'test3', NULL, NULL, '2018-01-05 11:18:39');
INSERT INTO channels VALUES (8, 'test4', NULL, NULL, '2018-01-05 11:18:42');
INSERT INTO channels VALUES (6, 'test1', NULL, NULL, '2018-01-05 11:18:45');


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('channels_id_seq', 11, true);


--
-- Data for Name: classical; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO classical VALUES (1, 'Not Classical', NULL, NULL, NULL);
INSERT INTO classical VALUES (2, 'Orchastra', NULL, NULL, NULL);
INSERT INTO classical VALUES (3, 'concerto ', NULL, NULL, NULL);
INSERT INTO classical VALUES (4, 'sontanta ', NULL, NULL, NULL);


--
-- Name: classical_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('classical_id_seq', 9, true);


--
-- Data for Name: duplications; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO duplications VALUES (1, 'Ampex', NULL, NULL, NULL);
INSERT INTO duplications VALUES (2, 'Sterotape ', NULL, NULL, NULL);
INSERT INTO duplications VALUES (3, 'Columbia', NULL, NULL, NULL);
INSERT INTO duplications VALUES (4, 'Magtec', NULL, NULL, NULL);
INSERT INTO duplications VALUES (5, 'Barclay Croker', NULL, NULL, NULL);
INSERT INTO duplications VALUES (6, 'RCA', NULL, NULL, NULL);
INSERT INTO duplications VALUES (7, 'Mecury', NULL, NULL, NULL);
INSERT INTO duplications VALUES (8, 'Other', NULL, NULL, NULL);


--
-- Name: duplications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('duplications_id_seq', 8, true);


--
-- Data for Name: electronics; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO electronics VALUES (1, 'Solid State', NULL, NULL, NULL);
INSERT INTO electronics VALUES (2, 'Tube', NULL, NULL, NULL);
INSERT INTO electronics VALUES (3, 'Hybrid', NULL, NULL, NULL);


--
-- Name: electronics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('electronics_id_seq', 3, true);


--
-- Data for Name: equalization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO equalization VALUES (1, 'NAB', NULL, NULL, NULL);
INSERT INTO equalization VALUES (2, 'IEC', NULL, NULL, NULL);
INSERT INTO equalization VALUES (3, 'AES', NULL, NULL, NULL);
INSERT INTO equalization VALUES (4, 'Other', NULL, NULL, NULL);


--
-- Name: equalization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('equalization_id_seq', 4, true);


--
-- Data for Name: expander_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expander_images VALUES (1, 1, 'expanders/Ao5CURrDePal3PKoeEttpDcK0gmCOyET84rx48Oo.jpeg', '2018-02-07 12:34:18', '2018-02-07 12:34:18', NULL, 0, 'expanders/d1rnKAgTqXU69sOlOw7znaYOheCvEYuHGS5kazgK.jpeg');
INSERT INTO expander_images VALUES (2, 2, 'expanders/Ap0Rk2iesQLbRBk31d5pK6U8vNfgBOo0aJEbSwi6.jpeg', '2018-02-07 12:36:45', '2018-02-07 12:36:45', NULL, 0, 'expanders/A8zZUfZgAUJS1iXuB9ZRMrx7QQXWQdHlfGM6FYX8.jpeg');
INSERT INTO expander_images VALUES (3, 3, 'expanders/ehlURNXBUI4vhqJsMnqWn2eQFptoar2hCU3eQC8v.jpeg', '2018-02-07 12:39:40', '2018-02-07 12:39:40', NULL, 0, 'expanders/LKIQBJqGMlc2gDRzUfoct8g2vZ5OYFoVks14p4x2.jpeg');
INSERT INTO expander_images VALUES (4, 1, 'expanders/xJgsGBKObSCIcRDAdqZ2aEiI1ND0hMI0nspjiSmZ.jpeg', '2018-02-07 13:14:36', '2018-02-07 13:14:36', NULL, 1, 'expanders/5pwU7MMXUHmMivb1KYo1IhV9NmFJ1nqL3a3Ch7tH.jpeg');


--
-- Name: expander_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expander_images_seq', 4, true);


--
-- Data for Name: expanders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO expanders VALUES (1, 'gsdfaf', NULL, NULL, NULL, '2018-02-07 12:34:05', '2018-02-07 12:34:05', NULL, 14, 0);
INSERT INTO expanders VALUES (2, 'asd af faf sasa f egwwgt e sad FS', NULL, NULL, NULL, '2018-02-07 12:36:31', '2018-02-07 12:36:31', NULL, 14, 0);
INSERT INTO expanders VALUES (3, 'assad as as dd asd d ad DAD d d', NULL, NULL, NULL, '2018-02-07 12:39:23', '2018-02-07 12:39:23', NULL, 12, 0);


--
-- Name: expanders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expanders_id_seq', 3, true);


--
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO genres VALUES (1, 'Blues ', NULL, NULL, NULL);
INSERT INTO genres VALUES (2, 'Country', NULL, NULL, NULL);
INSERT INTO genres VALUES (3, 'Electronic ', NULL, NULL, NULL);
INSERT INTO genres VALUES (4, 'Folk', NULL, NULL, NULL);
INSERT INTO genres VALUES (5, 'Jazz', NULL, NULL, NULL);
INSERT INTO genres VALUES (6, 'Latin', NULL, NULL, NULL);
INSERT INTO genres VALUES (7, 'New Age', NULL, NULL, NULL);
INSERT INTO genres VALUES (8, 'Pop', NULL, NULL, NULL);
INSERT INTO genres VALUES (9, 'Reggae', NULL, NULL, NULL);
INSERT INTO genres VALUES (10, 'Rock', NULL, NULL, NULL);
INSERT INTO genres VALUES (11, 'Soul', NULL, NULL, NULL);
INSERT INTO genres VALUES (12, 'Soundtrack', NULL, NULL, NULL);
INSERT INTO genres VALUES (13, 'Misc', NULL, NULL, NULL);
INSERT INTO genres VALUES (14, 'CP (CrockPot)', NULL, NULL, NULL);
INSERT INTO genres VALUES (15, 'Other', NULL, NULL, NULL);


--
-- Name: genres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('genres_id_seq', 15, true);


--
-- Data for Name: head_composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_composition VALUES (1, 'F&F', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (2, 'Metal', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (3, 'Glass', NULL, NULL, NULL);


--
-- Name: head_composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_composition_id_seq', 3, true);


--
-- Data for Name: head_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_configuration VALUES (2, 'Mono - Full Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (3, 'Mono -Dual Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (4, 'Mono - Half Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (5, 'sterero ', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (6, 'Sterero - Stacked', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (7, 'Sterero - Staggered', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (8, 'Quad', NULL, NULL, NULL);


--
-- Name: head_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_configuration_id_seq', 7, true);


--
-- Data for Name: inputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO inputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO inputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO inputs VALUES (3, 'XLR (Unbalanced)', NULL, NULL, NULL);


--
-- Name: inputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inputs_id_seq', 3, true);


--
-- Data for Name: manufacturer_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO manufacturer_images VALUES (25, 24, 'public/images/gN2EnNYNEf5PW9p7R3pCyw6cWl4iS9aKFZhLKaSe.jpeg', '2018-02-06 05:40:20', '2018-02-06 05:40:20', NULL, 0);
INSERT INTO manufacturer_images VALUES (26, 25, 'public/images/8y80UqCZHhEzM4L72ZGyOsaiqc4yZwziNXk9WOAY.jpeg', '2018-02-06 05:40:35', '2018-02-06 05:40:35', NULL, 0);
INSERT INTO manufacturer_images VALUES (27, 23, 'public/images/u0VvP61ijwkl1O0YKVyLB5gkComeHJ6OMIKtKmnt.jpeg', '2018-02-06 11:13:57', '2018-02-06 11:13:57', NULL, 0);
INSERT INTO manufacturer_images VALUES (34, 22, 'public/images/VxzS5TpQKaFyn6WUx4bLGl3KMK90G9w1tuLXzN00.jpeg', '2018-02-07 10:27:48', '2018-02-07 10:27:48', NULL, 1);
INSERT INTO manufacturer_images VALUES (29, 22, 'public/images/hAMTdMiFzvYuHflKgNnGfSR0gviFn3JOR5yUXbA2.jpeg', '2018-02-07 09:46:36', '2018-02-07 09:46:36', NULL, 1);


--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturer_images_id_seq', 34, true);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO manufacturers VALUES (22, 'test1', 'india', 32, 113, '2018-01-29 11:53:52', '2018-01-29 11:53:52', NULL, NULL);
INSERT INTO manufacturers VALUES (23, 'assd', 'ad', 23, 324, 'dsf', '2018-02-06 05:36:11', '2018-02-06 05:36:11', NULL);
INSERT INTO manufacturers VALUES (24, 'asd', 'fasdf43', 325, 4322, 'asda', '2018-02-06 05:40:05', '2018-02-06 05:40:05', NULL);
INSERT INTO manufacturers VALUES (25, 'asd', 'fasdf43', 325, 4322, 'asda', '2018-02-06 05:40:20', '2018-02-06 05:40:20', NULL);


--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 26, true);


--
-- Data for Name: max_reel_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO max_reel_size VALUES (1, '3', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (2, '5', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (3, '5 3/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (4, '8 1/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (5, '10 1/2', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (6, '10 1/2+', NULL, NULL, NULL);


--
-- Name: max_reel_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('max_reel_size_id_seq', 6, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations VALUES (3, '2017_12_21_130726_create_categories_nested_set', 1);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 3, true);


--
-- Data for Name: motors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO motors VALUES (1, '1', NULL, NULL, NULL);
INSERT INTO motors VALUES (2, '2', NULL, NULL, NULL);
INSERT INTO motors VALUES (3, '3', NULL, NULL, NULL);
INSERT INTO motors VALUES (4, '4', NULL, NULL, NULL);


--
-- Name: motors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('motors_id_seq', 4, true);


--
-- Data for Name: noise_reductions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO noise_reductions VALUES (1, 'Doblby B', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (2, 'Dobly C', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (3, 'DBX', NULL, NULL, NULL);


--
-- Name: noise_reductions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('noise_reductions_id_seq', 3, true);


--
-- Data for Name: number_of_heads; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO number_of_heads VALUES (1, '1 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (2, '2 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (3, '3 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (4, '4 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (5, '5 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (6, '6 (PB)', NULL, NULL, NULL);


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('number_of_heads_id_seq', 5, true);


--
-- Data for Name: outputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO outputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO outputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (3, 'XLR (unbalanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (4, 'CCIR/IEC1', NULL, NULL, NULL);
INSERT INTO outputs VALUES (5, 'NAB/IEC2', NULL, NULL, NULL);
INSERT INTO outputs VALUES (6, 'Class A', NULL, NULL, NULL);
INSERT INTO outputs VALUES (7, 'Class A/B', NULL, NULL, NULL);


--
-- Name: outputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('outputs_id_seq', 7, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: password_resets_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('password_resets_seq', 1, false);


--
-- Data for Name: reel_sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reel_sizes VALUES (1, '5"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (2, '7"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (3, '10"', NULL, NULL, NULL);


--
-- Name: reel_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reel_sizes_id_seq', 3, true);


--
-- Data for Name: speeds; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO speeds VALUES (1, '3 3/4', NULL, NULL, NULL);
INSERT INTO speeds VALUES (2, '7 1/2', NULL, NULL, NULL);
INSERT INTO speeds VALUES (3, '15 i.ps.', NULL, NULL, NULL);
INSERT INTO speeds VALUES (4, '30 i.p.s', NULL, NULL, NULL);


--
-- Name: speeds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('speeds_id_seq', 4, true);


--
-- Data for Name: tape_head_preamp_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_id_seq', 8, true);


--
-- Data for Name: tape_head_preamp_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_head_preamp_images VALUES (1, 4, 'tapes/4CtBXMM1Pf1j7rAFxEDESQMklOouvIlAL1fw6ma5.jpeg', '2018-02-07 05:13:29', '2018-02-07 05:13:29', NULL, 'tapes/YgLHRWb23UmAqeDlHeywqHHgCmf1rH0CcOsuVfQl.jpeg', 0);
INSERT INTO tape_head_preamp_images VALUES (3, 7, 'tapes/HAXIMxxL0v400Dv34PduCOH1RVEREajDAC8opFCD.jpeg', '2018-02-07 05:18:50', '2018-02-07 05:18:50', NULL, 'tapes/TXOaDRY0V3asPUsBnXmUvLjkUANdS12lv5hTqwnl.jpeg', 0);
INSERT INTO tape_head_preamp_images VALUES (2, 6, 'tapes/AgcEpOA1xjmOhRO9Dui48koROIwVsWz1BOXB2iAO.jpeg', '2018-02-07 05:17:41', '2018-02-07 08:57:43', NULL, 'tapes/k3rVxoWSvi9FmzO7HOlLQJUAv0s2hPuTngMzhZUl.jpeg', 0);


--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_images_id_seq', 3, true);


--
-- Data for Name: tape_head_preamps; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_head_preamps VALUES (4, 22, 'ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-07 05:13:10', '2018-02-07 05:13:10', NULL, 0, 2, NULL, NULL);
INSERT INTO tape_head_preamps VALUES (7, 23, 'zsd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-07 05:18:33', '2018-02-07 05:18:33', NULL, 0, 1, NULL, NULL);
INSERT INTO tape_head_preamps VALUES (6, 22, 'ad', NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, '2018-02-07 05:17:22', '2018-02-07 08:55:54', NULL, 0, 2, 2, 6);


--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamps_comments_id_seq', 1, false);


--
-- Data for Name: tape_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_images VALUES (41, 41, 'tapes/kFFGFC18S9iXBzlmrHvWEOMfNvmN1JZYHIAzqyAv.jpeg', 0, '2018-02-08 09:41:15', '2018-02-08 09:41:15', NULL, 'tapes/5mlFjohMRltOBgffFmPacdQgaO72SZ8En9uCUaaT.jpeg');
INSERT INTO tape_images VALUES (42, 42, 'tapes/VyLryV2iCxp9SGRSsF6JAokBBgDTxEOd55cbGyc1.jpeg', 0, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL, 'tapes/u0FRGC2HgqXkoqZna4HeIlwG9h9gnO6Sbv9se6I6.jpeg');
INSERT INTO tape_images VALUES (43, 43, 'tapes/PNUWGRWchxtNQAv7d2Y8iWWukUOnCng3nirNh3Bo.jpeg', 0, '2018-02-08 13:50:14', '2018-02-08 13:50:14', NULL, 'tapes/miT3K3sacTdyIX7bwqCsBRV2dy9hbIiWYy9HwUte.jpeg');
INSERT INTO tape_images VALUES (44, 43, 'public/images/TA9h9XHQEXZ3UWGRPqjIYSyf93j6KnENvOIimE89.jpeg', 1, '2018-02-08 13:57:10', '2018-02-08 13:57:10', NULL, NULL);
INSERT INTO tape_images VALUES (45, 57, 'tapes/sBbxifD62eKh6n1bwU3QHslOR8Jghsch8cDEWwYc.jpeg', 0, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL, 'tapes/qxXEjhJKJqw5GlRvNZep05rC4kqEduO3VyZidoMx.jpg');
INSERT INTO tape_images VALUES (46, 58, 'tapes/Cv3n0bvh5vWWn8kHRZhUXhe1MzoPmU8CdRswE8dR.jpeg', 0, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL, 'tapes/hVQnKIxkd582o3GeErCDoukvHUh9SEkTjarBYOtS.jpeg');


--
-- Name: tape_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_images_seq', 46, true);


--
-- Data for Name: tape_recorder_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_recorder_images VALUES (9, 6, 'public/images/Vyw7Vjm5MNF6kq1XU8TSEfgX2SOFyZs44MBEdPa3.jpeg', 0, '2018-02-06 06:01:08', '2018-02-06 06:01:08', NULL, NULL);
INSERT INTO tape_recorder_images VALUES (10, 6, 'public/images/Q0crl83cBokkqpBucVhTNPA12SK45NsS0oZdOwLM.jpeg', 1, '2018-02-06 06:01:22', '2018-02-06 06:01:22', NULL, NULL);
INSERT INTO tape_recorder_images VALUES (11, 7, 'public/images/scModr1QmQBH9rnTrUCghr5G5m0DjOfllo6OZzar.jpeg', 0, '2018-02-06 06:13:15', '2018-02-06 06:13:15', NULL, NULL);
INSERT INTO tape_recorder_images VALUES (12, 7, 'public/images/0Hq2lU9MsO8h9itnqaUq2PbkEhjYDk4djVwv23Zs.jpeg', 1, '2018-02-06 06:13:31', '2018-02-06 06:13:31', NULL, NULL);
INSERT INTO tape_recorder_images VALUES (13, 8, 'public/images/8S3Eo1TkQctSraGfYUEYkcCTdfF3aYeyF3KTe8B5.jpeg', 0, '2018-02-06 10:26:33', '2018-02-06 10:26:33', NULL, 'tape_recorders/3uMLJd46GaMjOBPe8KMjrcBOrEhnM9B7KG4SNijo.jpeg');
INSERT INTO tape_recorder_images VALUES (14, 8, 'public/images/WbQIxJ18yfIyYI7FNCA1rthTWd5Sx5v0VZSqywcK.jpeg', 1, '2018-02-06 10:26:50', '2018-02-06 10:26:50', NULL, 'tape_recorders/XtIxQScNaT0KA3o6lPyHAW7mOlZGaJJqUfiFEzav.jpeg');
INSERT INTO tape_recorder_images VALUES (15, 9, 'public/images/SA8S36UXjxpXmUG98cddTo5SE8Bf9nrM4YE3KVO8.jpeg', 0, '2018-02-06 10:30:12', '2018-02-06 10:30:12', NULL, 'tape_recorders/zs41NNPzztIjvvhzZvdrt8WQueRSIO2cVyZ7BbbL.jpeg');
INSERT INTO tape_recorder_images VALUES (16, 10, 'public/images/vnbqlmwLk3wN5IYlWZdb8JjGQpkAsIcPYw5WHdsX.jpeg', 0, '2018-02-06 10:31:05', '2018-02-06 10:31:05', NULL, 'tape_recorders/NC8xpC7qMYXsSkCZi4dQqDjQ4Wcw42RDUyNb3m40.jpeg');
INSERT INTO tape_recorder_images VALUES (17, 11, 'public/images/nfJx2bpTheaxrshgw7oC0MFj5ujGJeFvWKe0Acf5.jpeg', 0, '2018-02-06 10:31:50', '2018-02-06 10:31:50', NULL, 'tape_recorders/xiVb94HxSiXLtmgUd4tgg6DTEHydzgTG38DbF9d0.jpeg');
INSERT INTO tape_recorder_images VALUES (18, 12, 'public/images/aj5AhDZvL3FYba8m1YHwGDO9qW0Cwp52nfmIydBM.jpeg', 0, '2018-02-06 10:32:43', '2018-02-06 10:32:43', NULL, 'tape_recorders/cscBHp7lxru6OxzkfpKrOgd6uZVxxqWTXpce4L8t.jpeg');
INSERT INTO tape_recorder_images VALUES (19, 13, 'public/images/DzyEdXz5oY82DqN5fRZ2pJLZkheC7nYy0VCszdmO.jpeg', 0, '2018-02-06 10:34:08', '2018-02-06 10:34:08', NULL, 'tape_recorders/wLapxkZZuqBwGUPwH1qKVhIK540soFU53mR3jEgt.jpeg');
INSERT INTO tape_recorder_images VALUES (20, 14, 'public/images/O8b0sgqugVXkicJsTZRe8FuNQ6UNhtkIz6e81rcy.jpeg', 0, '2018-02-06 10:35:02', '2018-02-06 10:35:02', NULL, 'tape_recorders/pVBtLVR6L5Ht0fUzbaObaYk0WxT6Btqo6MJngR8f.jpeg');
INSERT INTO tape_recorder_images VALUES (21, 15, 'public/images/flpQNP4yBocIhydSHzUVCVA85m6uT622R1hRC967.jpeg', 0, '2018-02-06 10:36:13', '2018-02-06 10:36:13', NULL, 'tape_recorders/CxXwRUn36Ay4LSDRsLjj4EMd8DcrOrDTPrZHADE2.jpeg');
INSERT INTO tape_recorder_images VALUES (22, 16, 'public/images/HcUj7Tx7jMqaSe4buBaiFVm1XcbYDtWDJtT6UKST.jpeg', 0, '2018-02-06 10:36:57', '2018-02-06 10:36:57', NULL, 'tape_recorders/OQeibEaALkePvRWGpfbr35NombWB6cMBuZY7WCwV.jpeg');
INSERT INTO tape_recorder_images VALUES (23, 17, 'public/images/KmBP7e7IfAceyGQEHImXfytmYhJxEh7FjWkEZ6Ht.jpeg', 0, '2018-02-06 10:37:44', '2018-02-06 10:37:44', NULL, 'tape_recorders/QeycuF0Yz1KUGTas3RaoL9tLhvhtz4nAfLMFjhj4.jpeg');
INSERT INTO tape_recorder_images VALUES (24, 19, 'tapes/RVBdEDmylaCmAwwdoZ6b5sn3kQPwlbn39rPangD8.jpeg', 0, '2018-02-07 05:50:46', '2018-02-07 05:50:46', NULL, 'tapes/7bPn9x4RsxgvsI6oDM2uCq9JifYibMTqiA1q9T0Z.jpeg');
INSERT INTO tape_recorder_images VALUES (25, 8, 'tapes/SBltB0S4Nazq6x8fk0tlasPv119diuj8yhf3TfKy.jpeg', 2, '2018-02-12 07:27:32', '2018-02-12 07:27:32', NULL, 'tapes/08a9Q29XVkTn5Zolms3bxFR8chBGOUwWtwiHMPFs.jpg');


--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorder_images_id_seq', 25, true);


--
-- Data for Name: tape_recorders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_recorders VALUES (8, 23, 9, 'asds', 'asdf', 'sdafa', 3245, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:26:20', '2018-02-06 10:26:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (9, 23, 9, 'adsa', 'sdfd', 'sdf', 543, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:29:53', '2018-02-06 10:29:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (10, 22, 10, 'sdff', '45sa', 'asdsd', 522, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:30:48', '2018-02-06 10:30:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (11, 22, 13, 'asdSD', 'SADF', 'WEET35', 45, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:31:31', '2018-02-06 10:31:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (12, 22, 13, 'SDASD', 'sdfd', 'ERW', 2345, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:32:25', '2018-02-06 10:32:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (13, 23, 9, 'ADSA', 'SDF', '23', 234, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:33:55', '2018-02-06 10:33:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (14, 24, 10, 'A657SD', 'QDSDA', '2435', 234, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:34:46', '2018-02-06 10:34:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (15, 23, 13, 'SDDSAF', 'DASSDA', 'DSAFDS', 900, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:35:58', '2018-02-06 10:35:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (16, 22, 13, 'DDZX', 'SVFS', 'WSEW32', 465, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:36:39', '2018-02-06 10:36:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (7, 23, 10, 'czw52522 sadfa  sdf af', 'q23', 'asd', 23, NULL, NULL, NULL, NULL, NULL, '2018-02-06 06:13:00', '2018-02-06 10:48:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO tape_recorders VALUES (19, 22, 9, 'dsfds', 'err', 's23', 24, NULL, NULL, NULL, NULL, NULL, '2018-02-07 05:50:30', '2018-02-07 05:50:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO tape_recorders VALUES (6, 23, 11, 'asds435', '43', 'qwewe', 321, NULL, NULL, NULL, NULL, NULL, '2018-02-06 06:00:56', '2018-02-08 13:31:07', NULL, 324, 435, 3, 3, 2, 2, 2, 2, 2, 1, NULL, NULL, NULL, NULL, 1);
INSERT INTO tape_recorders VALUES (17, 24, 13, 'ZXC', 'WE', 'R56T', 2345, NULL, NULL, NULL, NULL, NULL, '2018-02-06 10:37:25', '2018-02-12 12:37:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);


--
-- Name: tape_recorders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorders_id_seq', 19, true);


--
-- Data for Name: tape_track_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_track_list VALUES (861, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (862, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (863, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (864, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (865, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (866, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (867, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (868, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (869, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (870, 'a', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (871, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (872, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (873, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (874, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (875, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (876, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (877, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (878, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (879, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (880, 'b', NULL, NULL, 57, '2018-02-12 07:28:31', '2018-02-12 07:28:31', NULL);
INSERT INTO tape_track_list VALUES (801, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:12', NULL);
INSERT INTO tape_track_list VALUES (802, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:12', NULL);
INSERT INTO tape_track_list VALUES (803, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (804, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (805, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (806, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (807, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (808, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (809, 'a', NULL, NULL, 41, '2018-02-08 09:41:15', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (810, 'a', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (811, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (812, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (813, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (814, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (815, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (816, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (817, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (818, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (819, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (820, 'b', NULL, NULL, 41, '2018-02-08 09:41:16', '2018-02-08 09:46:13', NULL);
INSERT INTO tape_track_list VALUES (821, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (822, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (823, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (824, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (825, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (826, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (827, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (828, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (829, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (830, 'a', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (831, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (832, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (833, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (834, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (835, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (836, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (837, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (838, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (839, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (840, 'b', NULL, NULL, 42, '2018-02-08 10:09:27', '2018-02-08 10:09:27', NULL);
INSERT INTO tape_track_list VALUES (841, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:10', NULL);
INSERT INTO tape_track_list VALUES (842, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:10', NULL);
INSERT INTO tape_track_list VALUES (843, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:10', NULL);
INSERT INTO tape_track_list VALUES (844, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:10', NULL);
INSERT INTO tape_track_list VALUES (845, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (846, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (847, 'a', NULL, NULL, 43, '2018-02-08 13:50:14', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (848, 'a', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (849, 'a', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (850, 'a', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (851, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (852, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (853, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (854, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (855, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (856, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (857, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (858, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (859, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (860, 'b', NULL, NULL, 43, '2018-02-08 13:50:15', '2018-02-08 13:57:11', NULL);
INSERT INTO tape_track_list VALUES (881, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (882, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (883, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (884, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (885, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (886, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (887, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (888, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (889, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (890, 'a', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (891, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (892, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (893, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (894, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (895, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (896, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (897, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (898, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (899, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);
INSERT INTO tape_track_list VALUES (900, 'b', NULL, NULL, 58, '2018-02-12 11:09:18', '2018-02-12 11:09:18', NULL);


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_track_list_id_seq', 900, true);


--
-- Data for Name: tapes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tapes VALUES (1, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:32', '2018-01-12 13:14:32', NULL);
INSERT INTO tapes VALUES (2, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tapes VALUES (3, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tapes VALUES (4, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tapes VALUES (5, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tapes VALUES (6, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tapes VALUES (7, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tapes VALUES (8, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tapes VALUES (9, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tapes VALUES (10, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (11, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (12, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (13, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tapes VALUES (14, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tapes VALUES (15, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tapes VALUES (16, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tapes VALUES (17, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tapes VALUES (18, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tapes VALUES (19, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tapes VALUES (20, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tapes VALUES (21, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tapes VALUES (22, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tapes VALUES (23, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tapes VALUES (24, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tapes VALUES (25, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tapes VALUES (26, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (27, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (28, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (29, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tapes VALUES (30, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tapes VALUES (31, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tapes VALUES (32, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tapes VALUES (33, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tapes VALUES (34, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tapes VALUES (35, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tapes VALUES (36, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tapes VALUES (37, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tapes VALUES (38, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tapes VALUES (39, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tapes VALUES (40, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tapes VALUES (41, 'cvcv', 'dsf asd', '00000', '2018-02-05', '{"credit": null, "musician": null}', 'sdsdf', 3, 11, NULL, 3, NULL, 2, NULL, NULL, NULL, NULL, '2018-02-08 09:40:58', '2018-02-08 09:46:12', NULL);
INSERT INTO tapes VALUES (42, 'xzxc', 'c sasd', '3452', '2018-02-21', '{"credit": null, "musician": null}', 'dsD', 1, 1, NULL, 3, NULL, 1, NULL, NULL, NULL, NULL, '2018-02-08 10:09:13', '2018-02-08 10:09:13', NULL);
INSERT INTO tapes VALUES (43, 'sfasdf', 'dsfa sdgsd', '234234', '2018-02-12', '{"credit": null, "musician": null}', 'asdfd', 3, 5, NULL, 3, NULL, 3, NULL, NULL, NULL, NULL, '2018-02-08 13:49:57', '2018-02-08 13:56:59', NULL);
INSERT INTO tapes VALUES (57, 'zzcx', 'sdfdf dsdf', '3435', '2018-02-26', '{"credit": null, "musician": null}', 'sdfsdf', 3, 4, NULL, 4, NULL, 2, NULL, NULL, NULL, NULL, '2018-02-12 07:28:15', '2018-02-12 07:28:15', NULL);
INSERT INTO tapes VALUES (58, 'ddsdfds', 'dfsdf dssdf', '3244234', '2018-02-20', '{"credit": null, "musician": null}', 'dfsafasf', 2, 5, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, '2018-02-12 11:08:58', '2018-02-12 11:08:58', NULL);


--
-- Name: tapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tapes_id_seq', 58, true);


--
-- Data for Name: tracks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tracks VALUES (1, '4 Track', NULL, NULL, NULL);
INSERT INTO tracks VALUES (2, '2 Track Inline/Stacked', NULL, NULL, NULL);
INSERT INTO tracks VALUES (3, '2 Track Staggered', NULL, NULL, NULL);
INSERT INTO tracks VALUES (4, 'Quad', NULL, NULL, NULL);


--
-- Name: tracks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tracks_id_seq', 4, true);


--
-- Data for Name: user_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_types VALUES ('Admin', NULL, NULL, NULL, 1);
INSERT INTO user_types VALUES ('Moderator', NULL, NULL, NULL, 2);
INSERT INTO user_types VALUES ('Subscriber', NULL, NULL, NULL, 3);
INSERT INTO user_types VALUES ('Editor', NULL, NULL, NULL, 4);


--
-- Name: user_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_types_id_seq', 4, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'ashish', 'ashish@citrusleaf.in', '$2y$10$XewljynHY4tUK7EKFzWd4u0CCeFAN9C5aQSXPhMvbnm8nz.RoyI8G', NULL, 'U47FsYeh', 'Admin', '2017-12-23 17:35:25', '2017-12-23 17:35:25', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (2, 'ashish', 'ashish+1@citrusleaf.in', '$2y$10$CqPv0zZga7AR62oOr88zSOFe8HnGgAHa5Smqic6rwATlwCz9uJncm', NULL, 'N6COqRLS', 'Admin', '2017-12-23 17:36:25', '2017-12-23 17:36:25', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (3, 'ashish', 'ashish+2@citrusleaf.in', '$2y$10$1PWsw0gysZW6ecQ0EfiOBuM7vYtRSub5N5qw.L2SUsltnn0D/SgAa', NULL, 'fFiXI1HP', 'Admin', '2017-12-23 17:37:40', '2017-12-23 17:37:40', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (5, 'ashish', 'ashish.verma250990@gmail.com', '$2y$10$Itz8nKtn2.xjupeJQVWd9urLi5mgg67Rv6OimWyunl8ctKvtrtkyy', NULL, 'XUS4Zwrg', 'Admin', '2017-12-23 17:54:55', '2017-12-23 17:54:55', 1, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (6, 'ashish', 'ashish.verma250990+2@gmail.com', '$2y$10$/aaaIUidyZAQEktsO3LAIOCFUZkaOY1rxThjkjVKjqM9.37SNxA7a', NULL, 'ghAZbnTd', 'Admin', '2017-12-23 17:56:51', '2017-12-23 17:56:51', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (7, 'ashish', 'ashish.verma250990+3@gmail.com', '$2y$10$1WZi.pCId3BGHH86SAUTKe46BpUmNeUSnsjKtOfWJh730XyJU3rZC', NULL, 'N3KzZTdY', 'Admin', '2017-12-23 17:59:48', '2017-12-23 17:59:48', NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO users VALUES (10, 'ashish', 'ashish.verma250990+6@gmail.com', '$2y$10$uj4y13hdIML.XSXijQgC5.Wk5RtZmn6KfbcvC3VZTfPLYLM4bVb3q', NULL, NULL, 'Subscriber', '2017-12-24 11:54:56', '2017-12-24 17:27:08', 1, NULL, NULL, 3, NULL, NULL, NULL);
INSERT INTO users VALUES (4, 'Mahendra', 'admin@gmail.com', '$2y$10$KtM/rSQimX2Db0.lwMjPA.cLnFjnOGV8r/3Z91wRk0miyYbTpppcC', NULL, NULL, 'Admin', '2017-12-23 17:53:44', '2018-03-08 08:46:55', 1, '2018-03-08 08:46:55', NULL, 1, 'images/a3LFVbkHMAtBD9aFGz0AAfaxWtVJhcqC4XoymUPE.png', 'India', 'Indore');
INSERT INTO users VALUES (9, 'ashish', 'ashish.verma250990+5@gmail.com', '$2y$10$Gn0v/hmLZo4oRk7OOWUpUuerCDLZqSbQlQRbhBEA.k.Al3PGZ4E2G', NULL, NULL, 'Admin', '2017-12-24 11:35:37', '2018-02-09 11:50:04', 1, NULL, '2018-02-09 11:50:04', 1, NULL, NULL, NULL);
INSERT INTO users VALUES (8, 'ashish', 'ashish.verma250990+4@gmail.com', '$2y$10$ZzXJL0z0vwmBcepSix58u.kC.z2ilPvABGNhzKA.6luOYaXD70WCu', NULL, 'gFv2aEh0', 'Admin', '2017-12-24 11:33:45', '2018-02-09 11:50:20', NULL, NULL, '2018-02-09 11:50:20', 1, NULL, NULL, NULL);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 12, true);


--
-- Data for Name: voltage; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO voltage VALUES (1, '100v Japan', NULL, NULL, NULL);
INSERT INTO voltage VALUES (2, '110-120v', NULL, NULL, NULL);
INSERT INTO voltage VALUES (3, 'Multi', NULL, NULL, NULL);
INSERT INTO voltage VALUES (4, 'other', NULL, NULL, NULL);


--
-- Name: voltage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('voltage_id_seq', 4, true);


--
-- Name: application application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- Name: auto_reverse auto_reverse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auto_reverse
    ADD CONSTRAINT auto_reverse_pkey PRIMARY KEY (id);


--
-- Name: brand_images brand_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_pkey PRIMARY KEY (id);


--
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);


--
-- Name: catalog_images catalog_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_pkey PRIMARY KEY (id);


--
-- Name: catalog_types catalog_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types
    ADD CONSTRAINT catalog_types_pkey PRIMARY KEY (id);


--
-- Name: catalogs catalogs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: classical classical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical
    ADD CONSTRAINT classical_pkey PRIMARY KEY (id);


--
-- Name: duplications duplications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications
    ADD CONSTRAINT duplications_pkey PRIMARY KEY (id);


--
-- Name: electronics electronics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY electronics
    ADD CONSTRAINT electronics_pkey PRIMARY KEY (id);


--
-- Name: equalization equalization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equalization
    ADD CONSTRAINT equalization_pkey PRIMARY KEY (id);


--
-- Name: expander_images expander_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_pkey PRIMARY KEY (id);


--
-- Name: expanders expanders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders
    ADD CONSTRAINT expanders_pkey PRIMARY KEY (id);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id);


--
-- Name: head_composition head_composition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_composition
    ADD CONSTRAINT head_composition_pkey PRIMARY KEY (id);


--
-- Name: head_configuration head_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_configuration
    ADD CONSTRAINT head_configuration_pkey PRIMARY KEY (id);


--
-- Name: inputs inputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inputs
    ADD CONSTRAINT inputs_pkey PRIMARY KEY (id);


--
-- Name: manufacturer_images manufacturer_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_pkey PRIMARY KEY (id);


--
-- Name: manufacturers manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: max_reel_size max_reel_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY max_reel_size
    ADD CONSTRAINT max_reel_size_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: motors motors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY motors
    ADD CONSTRAINT motors_pkey PRIMARY KEY (id);


--
-- Name: noise_reductions noise_reductions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions
    ADD CONSTRAINT noise_reductions_pkey PRIMARY KEY (id);


--
-- Name: number_of_heads number_of_heads_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY number_of_heads
    ADD CONSTRAINT number_of_heads_pkey PRIMARY KEY (id);


--
-- Name: outputs outputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY outputs
    ADD CONSTRAINT outputs_pkey PRIMARY KEY (id);


--
-- Name: reel_sizes reel_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes
    ADD CONSTRAINT reel_sizes_pkey PRIMARY KEY (id);


--
-- Name: speeds speeds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds
    ADD CONSTRAINT speeds_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp_comments tape_head_preamp_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_comments
    ADD CONSTRAINT tape_head_preamp_comments_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_brand_id_key UNIQUE (tape_head_preamp_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamps tape_head_preamps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamps_pkey PRIMARY KEY (id);


--
-- Name: tape_images tape_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorder_images tape_recorder_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorder_images
    ADD CONSTRAINT tape_recorder_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorders tape_recorders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_pkey PRIMARY KEY (id);


--
-- Name: tape_track_list tape_track_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_pkey PRIMARY KEY (id);


--
-- Name: tapes tapes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_pkey PRIMARY KEY (id);


--
-- Name: tracks tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks
    ADD CONSTRAINT tracks_pkey PRIMARY KEY (id);


--
-- Name: user_types user_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_types
    ADD CONSTRAINT user_types_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: voltage voltage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY voltage
    ADD CONSTRAINT voltage_pkey PRIMARY KEY (id);


--
-- Name: categories__lft__rgt_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories__lft__rgt_parent_id_index ON categories USING btree (_lft, _rgt, parent_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: brand_images brand_images_brand_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES brands(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: brands brands_manufacturer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brands
    ADD CONSTRAINT brands_manufacturer_id_fkey FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: catalog_images catalog_images_catalog_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_catalog_id_fkey FOREIGN KEY (catalog_id) REFERENCES catalogs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: catalogs catalogs_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_type_fkey FOREIGN KEY (type) REFERENCES catalog_types(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expander_images expander_images_expander_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_expander_id_fkey FOREIGN KEY (expander_id) REFERENCES expanders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: expanders expanders_brand_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders
    ADD CONSTRAINT expanders_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES brands(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: manufacturer_images manufacturer_images_manufacturer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_manufacturer_id_fkey FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_head_preamp_comments tape_head_preamp_comments_tape_head_preamp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_comments
    ADD CONSTRAINT tape_head_preamp_comments_tape_head_preamp_id_fkey FOREIGN KEY (tape_head_preamp_id) REFERENCES tape_head_preamps(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_head_preamp_comments tape_head_preamp_comments_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_comments
    ADD CONSTRAINT tape_head_preamp_comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_head_preamp_images tape_head_preamp_images_tape_head_preamp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_tape_head_preamp_id_fkey FOREIGN KEY (tape_head_preamp_id) REFERENCES tape_head_preamps(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_head_preamps tape_head_preamps_electronic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamps_electronic_id_fkey FOREIGN KEY (electronic_id) REFERENCES electronics(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_head_preamps tape_head_preamps_input_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamps_input_id_fkey FOREIGN KEY (input_id) REFERENCES inputs(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_head_preamps tape_head_preamps_manufacturer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamps_manufacturer_id_fkey FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_head_preamps tape_head_preamps_output_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamps_output_id_fkey FOREIGN KEY (output_id) REFERENCES outputs(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_images tape_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_recorder_images tape_recorder_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorder_images
    ADD CONSTRAINT tape_recorder_images_tape_id_fkey FOREIGN KEY (tape_recorder_id) REFERENCES tape_recorders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_recorders tape_recorders_application_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_application_id_fkey FOREIGN KEY (application_id) REFERENCES application(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_auto_reverse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_auto_reverse_id_fkey FOREIGN KEY (auto_reverse_id) REFERENCES auto_reverse(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_brand_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES brands(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_equalization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_equalization_id_fkey FOREIGN KEY (equalization_id) REFERENCES equalization(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_head_composition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_head_composition_id_fkey FOREIGN KEY (head_composition_id) REFERENCES head_composition(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_head_composition_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_head_composition_id_fkey1 FOREIGN KEY (head_composition_id) REFERENCES head_composition(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_head_configuration_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_head_configuration_id_fkey FOREIGN KEY (head_configuration_id) REFERENCES head_configuration(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_manufacturer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_manufacturer_id_fkey FOREIGN KEY (manufacturer_id) REFERENCES manufacturers(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_max_reel_size_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_max_reel_size_id_fkey FOREIGN KEY (max_reel_size_id) REFERENCES max_reel_size(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_motor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_motor_id_fkey FOREIGN KEY (motor_id) REFERENCES motors(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_noise_reduction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_noise_reduction_id_fkey FOREIGN KEY (noise_reduction_id) REFERENCES noise_reductions(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_number_of_head_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_number_of_head_id_fkey FOREIGN KEY (number_of_head_id) REFERENCES number_of_heads(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_speed_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_speed_id_fkey FOREIGN KEY (speed_id) REFERENCES speeds(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_track_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_track_id_fkey FOREIGN KEY (track_id) REFERENCES tracks(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_recorders tape_recorders_voltage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_voltage_id_fkey FOREIGN KEY (voltage_id) REFERENCES voltage(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tape_track_list tape_track_list_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tapes tapes_channel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_channel_fkey FOREIGN KEY (channel_id) REFERENCES channels(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_classical_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_classical_fkey FOREIGN KEY (classical_id) REFERENCES classical(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_duplication_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_duplication_fkey FOREIGN KEY (duplication_id) REFERENCES duplications(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_genre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_genre_fkey FOREIGN KEY (genre_id) REFERENCES genres(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_noise_reduction_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_noise_reduction_fkey FOREIGN KEY (noise_reduction_id) REFERENCES noise_reductions(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_reel_size_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_reel_size_fkey FOREIGN KEY (reel_size_id) REFERENCES reel_sizes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_speed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_speed_fkey FOREIGN KEY (speed_id) REFERENCES speeds(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_track_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_track_fkey FOREIGN KEY (track_id) REFERENCES tracks(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

