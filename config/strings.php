<?php

return [
		'warning' => [
				'invalid_user' => 'User does not exist',
				'incorect_password' => 'Incorrect password',
				'unauthorized_user' => 'Unauthorized user',
				'unverified_user' => 'Unverified user',
				'invalid_link' => 'Invalid link',
				'incorect_old_password' => 'Password not matched with old password',
				'invalid_token' => 'Invalid token',
				'expire_token'	=> 'Token Expired',
				'user_not_found' => 'User not found',
				'item_not_created' => 'Item not created',
				'item_not_updated' => 'Item not updated',
				'email_or_username_invalid' => 'Invalid email or username',
				'unverified_user' => 'Unverified User',
				'incorrect_password' => 'Incorrect Password',
				'passwords_not_matched' => 'Passsword and confirm password fields are not matched',
				'confirm_password_is_empty' => 'Confirm password field is mendatory for password update',
				
			],
		'success' => [
			'account_created' => 'Your account is created successfully and sent to you an email, Please verify your email with us before login.',
			'verified' => 'Your account is verified, You can login now.',
			'reset_password_link_sent' => 'Please check your email the reset password link sent.',
			'password_reset_successfully' => 'Passsword reset successfully',
			'profile_updated' => 'Profile updated',
		],
		'emails' => [
				'subjects' => [
					'forgot_password' => 'Forgot Passsword',
					'verify_email' => 'Verify Reel-Reel Account',
				],
				'messages' =>  [
						'forgot_password' => 'Below the link to reset your passsword',
						'verify_email' => 'The link to verify your account is provided below',
					],
			],	
		];