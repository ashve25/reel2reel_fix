<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Hash;
use DB;
use App\Events\SendMailEvent;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    private function getBaseQuery(){
        $filePath = config('filesystems.disks.s3.endpoint')."/".config('filesystems.disks.s3.bucket')."/";
        return self::select('users.id', 'users.name', 'users.email', 'users.user_type', 'users.verification_key', 'users.verified',  'users.token_timestamp_updated_at', 'users.country', 'users.city', DB::raw("concat('".$filePath."', users.profile_picture) as profile_picture_url"), 'user_types.type as user_type_name')
                ->join('user_types', 'user_types.id', '=', 'users.user_type');

    }
    
    public function getUsers($request)
    {
        $users = $this->getBaseQuery();
        if($request->has('type')){
            $users->where('users.user_type', $request->type);
        }
        if($request->has('page')){
           $users = $users->paginate(config('app.paginate'));
        }else{  
           $users = $users->get();
        }
        return $users;
    }


    public function getUser($id)
    {
        return $user = $this->getBaseQuery()->where('users.id', $id)->first();
    }

    public function deleteUser($id){
        self::find($id)->delete();
    }

    public function insertUser($request){
        $user = new self;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_type = $request->type;
        $user->country = $request->country;
        if($request->has('city')){
            $user->city = $request->city;
        }

        if($request->has('profile_picture') && !empty($request->profile_picture)){
            $hash = Str::random(40);
            $pictureName = "images/".$hash.".png";

            $picture = str_replace('data:image/jpeg;base64,', '', $request->profile_picture);
            $picture = file_get_contents($picture);
            \Storage::put($pictureName, $picture);
            $user->profile_picture = $pictureName;
        }
        $user->save();
        return $user;
    }


    public function updateUser($request, $id){
        $user = self::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->has('type')){
            $user->user_type = $request->type;
        }
        $user->country = $request->country;
        if($request->has('city')){
            $user->city = $request->city;
        }
        if($request->has('password') && $request->has('confirm_password')){
            if($request->password !== $request->confirm_password){
                $data = ['error'=>400, 'message'=>'Passwords Mismatched'];
                return $data;
            }
            if(!empty($request->password)){
                $user->password = Hash::make($request->password);
            }
        }
        if($request->has('profile_picture') && !empty($request->profile_picture)){
            $hash = Str::random(40);
            $pictureName = "images/".$hash.".png";
            $picture = str_replace('data:image/jpeg;base64,', '', $request->profile_picture);
            $picture = file_get_contents($picture);
            \Storage::put($pictureName, $picture);
            $user->profile_picture = $pictureName;
        }
        $user->save();
        return $user;
    }


    public function registerUser($request){
        $this->name = $request->name;
        $this->username = $request->username;
        $this->email = $request->email;
        $this->password = Hash::make($request->password);
        $this->country = $request->country;
        if($request->has('city')){
            $this->city = $request->city;
        }
        if($request->has('terms_and_conditions')){
            $this->terms_and_conditions = 1;
        }
        if($request->hasFile('profile_picture')){
            $path = $request->file('profile_picture')->store('public/images');
            $this->profile_picture = $path;
        }
        $this->verification_key = $this->generateKey();
        $this->verified = 0;
        $this->user_type = $request->user_type; 
        $this->save();

        $eventData['data'] = ['full_name' => $this->name, 
            'msg' => config('strings.emails.messages.verify_email'), 
            'link' => url('/users/'.$this->id.'/verification/'.$this->verification_key)
        ];
        $eventData['email'] = $this->email;
        $eventData['subject'] = config('strings.emails.subjects.verify_email');
        $eventData['page'] = 'Base::emails.account-verify-email';
        //info($eventData);
        event(new SendMailEvent($eventData));
    }

    public function generateKey(){
        $chars = config('app.key_generator_string');
        return substr( str_shuffle( $chars ), 0, 8);    
    }


    public function verifyMe(){
        $this->verified = 1;
        $this->verification_key = NULL;
        $this->save();
    }

    public function sendResetPasswordMail(){
        $this->verification_key = $this->generateKey();
        $this->save();
        $eventData['data'] = ['full_name' => $this->name, 
                                'msg' => config('strings.emails.messages.forgot_password'), 
                                'link' => url('/users/'.$this->id.'/reset-pass/'.$this->verification_key)
                            ];
        $eventData['email'] = $this->email;
        $eventData['subject'] = config('strings.emails.subjects.forgot_password');
        $eventData['page'] = 'Base::emails.forgot-password-email';
        //info($eventData);
        event(new SendMailEvent($eventData));
    }

    public function savePassword($request){
        $this->password = Hash::make($request->password);
        $this->verification_key = null;
        $this->verified = 1;
        $this->save();
    }


    public function updateProfile($request){
        $this->name = $request->name;
        $this->username = $request->username;
        $this->email = $request->email;
        $this->country = $request->country;
        if($request->has('password') && !empty($request->password)){
            $this->password = Hash::make($request->password); 
        }
        if($request->has('city')){
            $this->city = $request->city;
        }
        if($request->hasFile('profile_picture')){
            $path = $request->file('profile_picture')->store('public/images');
            $this->profile_picture = $path;
        }
        $this->save();
    }
}
