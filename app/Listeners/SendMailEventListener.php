<?php

namespace App\Listeners;

use App\Events\SendMailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendMailEventListener implements ShouldQueue
{
     use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(SendMailEvent $event)
    {
        $mailData = $event->mailData;
        $subject = $mailData['subject'];
        $email = $mailData['email'];
        Mail::send($mailData['page'], $mailData['data'], function($message) use ($email, $subject)
        {
            $message->from(config('app.mail_from_address'), config('app.mail_from_name'));
            $message->to($email);
            $message->subject($subject);
        });
    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(SendMailEvent $event, $exception)
    {
        info('mail send exception');
    }
}
