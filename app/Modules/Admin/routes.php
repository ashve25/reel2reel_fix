<?php
/*Route::prefix('admin')->group(function(){
	Route::namespace('App\Modules\Admin\Controllers')->group(function() {
		Route::get('/', "AuthController@loginForm");
	});	

	Route::get('create-demo-account', function () {
		$user = new App\Modules\Base\Models\User;
		$user->name = 'Admin';
		$user->email = 'admin@gmail.com';
		$user->password = bcrypt('abc123');
		$user->save();
		return json_encode($user);
	});
});

*/

Route::group(['namespace' => 'App\Modules\Admin\Controllers'], function(){
	Route::post('admin/data/{id}/delete', 'MasterDataController@delete');
	Route::resource('admin/data', 'MasterDataController');
});