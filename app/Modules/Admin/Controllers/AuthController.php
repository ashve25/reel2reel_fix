<?php
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Modules\Base\Models\FileResizeHelper;

class AuthController extends Controller{

	use FileResizeHelper;
	
	public function loginForm() {
		return view('vue');
	}

	public function resize(Request $request) {
		$file = $request->input('image');
		$path = 'images';
		$stored = $this->resizeAndStoreBase64($file, $path);
		if($stored) {
			return 'stored';
		}
		return 'not stored';
		
	}
	
}