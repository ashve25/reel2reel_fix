<?php
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\APIResponseTrait;
use App\Http\Traits\JWTTrait;
use Carbon\Carbon;
use DB;

class MasterDataController extends Controller
{
    use APIResponseTrait, JWTTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        info('storing..');
        info($request);
        $id = DB::table($request->type)->insertGetId(
            ['name' => $request->name]
        );
        if($id){
            $response['id'] = $id;
            return $this->success($response);
        }
        return $this->error(400, config('strings.warning.item_not_created'), 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($table)
    {
        info("table ".$table);
        $data['columns'] = DB::select("select column_name from information_schema.columns where table_name='$table'");
        $data['data'] = DB::select("select id,name from $table where deleted_at is null");
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        info($request);
        info($id);
        info("update");
        DB::table($request->type)->where('id', $id)->update(['name' => $request->name]);
        $response['id'] = $id;
        return $this->success($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request, $id){
        info($request);
        DB::table($request->type)->where('id', $id)->update(['deleted_at' => Carbon::now()]);
        $response['message'] = true;
        return $this->success($response);
    }
}
