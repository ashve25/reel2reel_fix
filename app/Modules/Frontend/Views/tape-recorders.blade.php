@extends('Frontend::master')
@section('css')
<style type="text/css">
#tape-recorders > .col-lg-2,  #tape-recorders > .col-md-4,  #tape-recorders > .col-sm-6 #tape-recorders > .col-xs-12{
	padding-bottom: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="tape-recorders" v-cloak>
		@include('Frontend::tape-recorders-base')
	</div>	
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#tape-recorders',
		data:{
	        tapeRecorders:{!!json_encode($tapeRecorders)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        tapeRecorderRoute:"{{route('tape-recorders.index')}}",
	        applyFilter:false,
	        tapeRecordersListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        brands: [],
			manufacturers: [],
			head_configurations:[],
			number_of_heads: [],
			speeds: [],
			tracks: [],
			head_compositions: [],
			equalizations: [],
			voltages: [],
			auto_reverses: [],
			applications: [],
			max_reel_sizes: [],
			noise_reductions: [],
			motors: [],
			filter: {
				manufacturer_id: [],
				brand_id: [],
				head_configuration_id: [],
				number_of_head_id: [],
				speed_id: [],
				track_id: [],
				head_composition_id: [],
				equalization_id: [],
				voltage_id: [],
				auto_reverse_id: [],
				application_id: [],
				max_reel_size_id: [],
				noise_reduction_id: [],
				motor_id: []
			}
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  },
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	
		   this.getMasterData();    
		},
		methods: {
			getMasterData: function(){
		  		var self = this;
		  		var url = "{{url('/admin/master-data/tape-recorders')}}";
		  		$.ajax({
		  			url: url,
		  			type: 'GET',
		  			success: function(response){
		  				console.log(response.data);
		  				var data = response.data;
			  			self.brands = data.brands;
			  			self.manufacturers = data.manufacturers;
			  			self.head_configurations = data.head_configuration;
			  			self.number_of_heads = data.number_of_heads;
			  			self.speeds = data.speeds;
			  			self.tracks = data.tracks;
			  			self.head_compositions = data.head_composition;
			  			self.equalizations = data.equalizations;
			  			self.voltages = data.voltages;
			  			self.auto_reverses = data.auto_reverses;
			  			self.applications = data.applications;
			  			self.max_reel_sizes = data.max_reel_sizes;
			  			self.noise_reductions = data.noise_reductions;
			            self.motors = data.motors;  
		  			},
		  			error: function(error){
		  				console.log(error);
		  			}
		  		});
		  	},
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeRecorderForm: function(id) {
		        window.location = "{{url('tape-recorders')}}/"+id;
		    },
		    editTapeForm: function(id) {
		        this.$router.push('/tapes/'+id+'/edit');
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.tapeRecordersListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.tapeRecordersListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
		    getTapeRecorders:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapeRecorders = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			console.log(error);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getTapeRecorders();
		    },
		    addNewTapeRecorder:function(){
		    	window.location = "{{url('tape-recorders/create')}}";
		    }
	  	}
	});
</script>
@endsection