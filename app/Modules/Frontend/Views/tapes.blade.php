@extends('Frontend::master')
@section('css')
<style type="text/css">
#tapes > .col-lg-2,  #tapes > .col-md-4,  #tapes > .col-sm-6 #tapes > .col-xs-12{
	padding-bottom: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="tapes" v-cloak>
		@include('Frontend::tapes-base')
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#tapes',
		data:{
	        tapes:{!!json_encode($tapes)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        tapeRoute:"{{route('tapes.index')}}",
	        applyFilter:false,
	        tapesListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        tracks: [],
			genres: [],
			classicals: [],
			speeds: [],
			channels: [],
			reel_sizes: [],
			noise_reductions: [],
			duplications: [],
			filter: {
				track_id: [],
				genre_id: [],
				classical_id: [],
				speed_id: [],
				channel_id: [],
				reel_size_id: [],
				noise_reduction_id: [],
				duplication_id: []
			}
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	  
		   this.getMasterData();  
		},
		methods: {
			getMasterData: function(){
		  		var self = this;
		  		var url = "{{url('/admin/master-data/tapes')}}";
		  		$.ajax({
		  			url: url,
		  			type: 'GET',
		  			success: function(response){
		  				var data = response.data;
		  				self.tracks = data.tracks;
						self.genres = data.genres;
						self.classicals = data.classicals;
						self.speeds = data.speeds;
						self.channels = data.channels;
						self.reel_sizes = data.reel_sizes;
						self.noise_reductions = data.noise_reductions;
						self.duplications = data.duplications;
		  			},
		  			error: function(error){
		  				console.log(error);
		  			}
		  		});
		  	},
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeForm: function(id) {
		        window.location = "{{url('/tapes/')}}/"+id;
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.tapesListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.tapesListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapes();
		        console.log(this.tapes);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapes();
		        console.log(this.tapes);
		    },
		    getTapes:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapes = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getTapes();
		    },
		    addNewTape:function(){
		    	window.location = "{{url('tapes/create')}}";
		    }
	  	},
	});
</script>
@endsection