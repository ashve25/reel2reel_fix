@extends('Frontend::master')
@section('css')
<style type="text/css">
#form-div{
	margin-top:20px;
}

.col-md-6{
	padding: 20px;
}

#item-header-cover-image {
    padding: 0 1em;
    position: relative;
    z-index: 2;
}   

#item-header-cover-image #item-header-avatar {
    margin-top: 105px;
    float: left;
    overflow: visible;
    width: auto;
}   

#item-header-avatar {
    padding: 10px !important;
    margin-top: 0 !important;
    border-radius: 10px !important;
    background-color: rgba(35, 40, 45, 0.06) !important;
} 

.profile-info {
    display: inline-block;
}

.profile-city-country {
    font-family: 'Playfair Display', serif;
    padding: 20px;
}

.profile-numbers {
    position: absolute;
    width: 100%;
    /* bottom: -66px; */
    padding-left: 234px;
    margin-top: 103px;
}

.user-nicename {
    padding: 20px 15px;
    text-shadow: none !important;
    font-family: inherit !important;
    font-weight: 300 !important;
    color: #383e4d !important;
    float: left;
}

.profile-numbers p:first-of-type {
    padding-left: 0;
}

.profile-numbers p {
    padding-right: 14px;
    padding-left: 14px;
    display: inline-block;
    font-size: 21px;
    border-right: 1px solid;
}

.profile-numbers p span {
    color: #b45b30;
    font-size: 35px;
    font-family: 'Playfair Display', serif;
    font-weight: bold;
}

.tab-content{
	margin-top:30px; 
}


</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		@if(session('success'))
			<div class="alert alert-success alert-dismissible">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> {{session('success')}}
			</div>
		@endif
		@if(session('warning'))
			<div class="alert alert-danger alert-dismissible">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Warning!</strong>
			  	{{session('warning')}}
			</div>
		@endif
		<div class="col-md-12">
			<div class="col-md-6">
				<div id="form-div">
					<div class="row">
						<div class="col-md-12">
							<div id="item-header-cover-image">
								<div id="item-header-avatar">
									<a href="#">
										@if($user->profile_picture)
										<img src="{{$user->profile_picture}}" class="avatar user-27-avatar avatar-185 photo" style="width:185px; height:230px;" " width="185" height="230" alt="Profile picture of {{$user->name}}">
										@else
										<img src="{{asset('images/user.jpg')}}" class="avatar user-27-avatar avatar-185 photo" style="width:185px; height:230px;" " width="185" height="230" alt="Profile picture of {{$user->name}}">
										@endif
									</a>
								</div><!-- #item-header-avatar -->

						        <div class="profile-info">
						            <h3 class="profile-city-country">
						                ,
						                <br>
						                <span class="country"></span>
						            </h3>
						        </div>

						        <div class="profile-numbers">
						            <p class="title-of-owned-items">
						                Tapes
						                <br>
						                <span class="number-of-owned-items">{{count($tapes)}}</span>
						            </p>
						            <p class="title-of-owned-items">
						                Tape Recorders
						                <br>
						                <span class="number-of-owned-items">{{count($tapeRecorders)}}</span>
						            </p>
						        </div>
							</div>	
						</div>
					</div>
				</div>
				<h2 class="user-nicename">{{$user->name}}</h2>
			</div>
		</div>
		<div class="col-md-12">
			<ul class="nav nav-pills">
			    <li class="active"><a data-toggle="pill" href="#owned_tapes">Owned Tapes</a></li>
			    <li><a data-toggle="pill" href="#owned_tape_recorders">Owned Tape Recorders</a></li>
			    <li><a data-toggle="pill" href="#profile">Profile</a></li>
			    <li><a data-toggle="pill" href="#notifications">Notifications</a></li>
			    <li><a data-toggle="pill" href="#friends">Friends</a></li>
			    <li><a data-toggle="pill" href="#settings">Settings</a></li>
			    <li><a href="#">Messages</a></li>
			    <li><a href="#">Activity</a></li>
			    <li><a href="#">Forums</a></li>
			</ul>
			<div class="tab-content">
			    <div id="owned_tapes" class="tab-pane fade in active" v-cloak>
			      @include('Frontend::tapes-base')
			    </div>
			    <div id="owned_tape_recorders" class="tab-pane fade" v-cloak>
			      @include('Frontend::tape-recorders-base')
			    </div>
			    <div id="profile" class="tab-pane fade">
			        <ul class="nav nav-pills">
					    <li class="active"><a data-toggle="pill" href="#view">View</a></li>
					    <li><a data-toggle="pill" href="#edit">Edit</a></li>
					    <!-- <li><a data-toggle="pill" href="#profile-photo">Change Profile Photo</a></li> -->
					    <li><a data-toggle="pill" href="#cover-photo">Change Cover Image</a></li>
					</ul>
					<div class="tab-content">
			    		<div id="view" class="tab-pane fade in active">
			    			<div class="col-md-8">
		    					<div class="form-group">
									<div class="col-md-12">
									  <label>Name</label>
									  <input type="text" class="form-control" value="{{$user->name}}" disabled>
									</div>
								</div>
								<div class="form-group">
								    <div class="col-md-12">
								      <label>Username</label>
								      <input type="text" class="form-control" value="{{$user->username}}" disabled>
								    </div>
								</div>
							  	<div class="form-group">
							    	<div class="col-md-12">
							      		<label>Email</label>
							      		<input type="email" class="form-control" value="{{$user->email}}" disabled>
							    	</div>
							  	</div>
								<div class="form-group">
								    <div class="col-md-12">
								      <label>Country</label>
								      <input type="text" class="form-control" value="{{$user->country}}" disabled>
								    </div>
								</div>
								<div class="form-group">
								    <div class="col-md-12">
								      <label>City</label>
								      <input type="text" class="form-control" value="{{$user->city}}" disabled>
								    </div>
								</div>
							</div>
			    		</div>
			    		<div id="edit" class="tab-pane fade">
			    			<div class="col-md-8">
			    				<form action="{{url('profile')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
									<div class="form-group">
										<div class="col-md-12">
										  <label>Name*</label>
										  <input type="text" name="name" class="form-control" placeholder="Enter Name" required value="{{$user->name}}">
										</div>
									</div>
									<div class="form-group">
									    <div class="col-md-12">
									      <label>Username*</label>
									      <input type="text" name="username" class="form-control" placeholder="Enter User Name" required value="{{$user->username}}">
									    </div>
									</div>
								  	<div class="form-group">
								    	<div class="col-md-12">
								      		<label>Email*</label>
								      		<input type="email" name="email" class="form-control" placeholder="Enter Email" required value="{{$user->email}}">
								    	</div>
								  	</div>
							  		<div class="form-group">
							    		<div class="col-md-12">
							      			<label>Password</label> (leave blank for no change)
							      			<input type="password" name="password" class="form-control" placeholder="Enter Password">
							    		</div>
							  		</div>
							  		<div class="form-group">
							    		<div class="col-md-12">
							      			<label>Confirm Password</label> (leave blank for no change)
							      			<input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password">
							    		</div>
							  		</div>
									<div class="form-group">
									    <div class="col-md-12">
									      <label>Country*</label>
									      <input type="text" name="country" class="form-control" placeholder="Enter Country" required value="{{$user->country}}">
									    </div>
									</div>
									<div class="form-group">
									    <div class="col-md-12">
									      <label>City</label>
									      <input type="text" name="city" class="form-control" placeholder="Enter City" value="{{$user->city}}">
									    </div>
									</div>
									<div class="form-group">
									    <div class="col-md-12">
									      <label>Profile Picture</label>
									      <input type="file" name="profile_picture" class="form-control" placeholder="Enter Name">
									    </div>
									</div>
									<div class="form-group">
									  	<div class="col-md-12">
									  		<button type="submit" class="btn btn-sm btn-success">Submit</button>
									  	</div>
									</div>
								</form>
							</div>
			    		</div>
			    		<!-- <div id="profile-photo" class="tab-pane fade">
			    			<p>Change Profile Photo</p>
			    		</div> -->
			    		<div id="profile-photo" class="tab-pane fade">
			    			<p>Change Cover Image</p>
			    		</div>
			    	</div>	
			    </div>
			    <div id="notifications" class="tab-pane fade">
			      <h3>Menu 2</h3>
			      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
			    </div>
			    <div id="friends" class="tab-pane fade">
			      <h3>Menu 3</h3>
			      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
			    </div>
			    <div id="settings" class="tab-pane fade">
			    	<ul class="nav nav-pills">
					    <li class="active"><a data-toggle="pill" href="#general">General</a></li>
					    <li><a data-toggle="pill" href="#email">Email</a></li>
					    <li><a data-toggle="pill" href="#visibility">Profile Visibility</a></li>
					</ul>
					<div class="tab-content">
						<div id="general" class="tab-pane fade">
						</div>
						<div id="email" class="tab-pane fade">
							<p>Send an email notice when:</p>
							<br>
							<form action="" method="post">
								<table class="table table-striped" id="activity-notification-settings">
									<thead>
										<tr>
											<th class="icon">&nbsp;</th>
											<th class="title">Activity</th>
											<th class="yes">Yes</th>
											<th class="no">No</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td>&nbsp;</td>
											<td>A member mentions you in an update using {{$user->name}}</td>
											<td class="yes">
												<input type="radio" name="notifications[notification_activity_new_mention]" value="yes" checked="checked">
												 <label>Yes, send email</label>
											</td>
											<td class="no">
												<input type="radio" name="notifications[notification_activity_new_mention]" value="no">
												 <label>No, do not send email</label>
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;</td>
											<td>A member replies to an update or comment you've posted</td>
											<td class="yes">
												<input type="radio" name="notifications[notification_activity_new_reply]" value="yes" checked="checked">
												 <label>Yes, send email</label>
											</td>
											<td class="no">
												<input type="radio" name="notifications[notification_activity_new_reply]" value="no">
												 <label>No, do not send email</label>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table table-striped" id="messages-notification-settings">
									<thead>
										<tr>
											<th class="icon"></th>
											<th class="title">Messages</th>
											<th class="yes">Yes</th>
											<th class="no">No</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td>A member sends you a new message</td>
											<td class="yes">
												<input type="radio" name="notifications[notification_messages_new_message]" value="yes" checked="checked">
												 <label>Yes, send email</label>
											</td>
											<td class="no">
												<input type="radio" name="notifications[notification_messages_new_message]" value="no">
												<label>No, do not send email</label>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table table-striped" id="friends-notification-settings">
									<thead>
										<tr>
											<th class="icon"></th>
											<th class="title">Friends</th>
											<th class="yes">Yes</th>
											<th class="no">No</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td>A member sends you a friendship request</td>
											<td class="yes">
												<input type="radio" name="notifications[notification_friends_friendship_request]" value="yes" checked="checked">
												 <label>Yes, send email</label>
											</td>
											<td class="no">
												<input type="radio" name="notifications[notification_friends_friendship_request]" value="no">
												 <label>No, do not send email</label>
											</td>
										</tr>
										<tr>
											<td></td>
											<td>A member accepts your friendship request</td>
											<td class="yes">
												<input type="radio" name="notifications[notification_friends_friendship_accepted]" value="yes" checked="checked">
												 <label>Yes, send email</label>
											</td>
											<td class="no">
												<input type="radio" name="notifications[notification_friends_friendship_accepted]" value="no">
												 <label>No, do not send email</label>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="submit">
									<input type="submit" name="submit" value="Save Changes" id="submit" class="btn btn-sm btn-primary">
								</div>
							</form>
						</div>
						<div id="visibility" class="tab-pane fade">
							<form action="" method="post">
								<table class="table table-striped">
									<thead>
										<tr>
											<th class="title field-group-name">Profile Required Info</th>
											<th class="title">Visibility</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Name</td>
											<td>
												<span>Everyone</span>
											</td>
										</tr>
										<tr>
											<td class="field-name">Country</td>
											<td class="field-visibility">
												<label>Select visibility</label>
												<select name="field_2_visibility" class="form-control">
													<option value="public" selected="selected">Everyone</option>
													<option value="adminsonly">Only Me</option>
													<option value="loggedin">All Members</option>
													<option value="friends">My Friends</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>City</td>
											<td>
												<label>Select visibility</label>
												<select class="form-control" name="field_3_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Terms and Conditions</td>
											<td>
												<label>Select visibility</label>
												<select class="form-control" name="field_44_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
												</select>
											</td>
										</tr>
									</tbody>
								</table>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Profile Optional Info</th>
											<th>Visibility</th>
										</tr>
									</thead>
									<tbody>
											<tr>
												<td>I am mainly interested in (check any or all) :</td>
												<td>
												<label>Select visibility</label>
													<select class="form-control" name="field_5_visibility">
															<option value="public" selected="selected">Everyone</option>
															<option value="adminsonly">Only Me</option>
															<option value="loggedin">All Members</option>
															<option value="friends">My Friends</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>Tell Us About Your Audio Interests &amp; System. I have been interested in audio for :</td>
												<td>
													<label>Select visibility</label>
													<select class="form-control" name="field_15_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
													</select>

												</td>
											</tr>
											<tr>
												<td>I have:</td>
												<td>
													<label>Select visibility</label>
													<select class="form-control" name="field_24_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>Electronics. I prefer: </td>
												<td>
													<label>Select visibility</label>
													<select class="form-control" name="field_27_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>My System(s) Value:</td>
												<td >
													<label>Select visibility</label>
													<select class="form-control" name="field_33_visibility">
														<option value="public" selected="selected">Everyone</option>
														<option value="adminsonly">Only Me</option>
														<option value="loggedin">All Members</option>
														<option value="friends">My Friends</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>I attend Audio Shows:</td>
												<td>
													<label>Select visibility</label>
													<select class="form-control" name="field_41_visibility">
															<option value="public" selected="selected">Everyone</option>
															<option value="adminsonly">Only Me</option>
															<option value="loggedin">All Members</option>
															<option value="friends">My Friends</option>
													</select>
												</td>
											</tr>
									</tbody>
								</table>
								<div class="submit">
									<input type="submit" name="submit" value="Save Settings" class="btn btn-sm btn-primary">
								</div>
							</form>
						</div>
					</div>
			    </div>
			</div>
		</div>
	</div>
</div>				
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#owned_tape_recorders',
		data:{
	        tapeRecorders:{!!json_encode($tapeRecorders)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        tapeRecorderRoute:"{{route('tape-recorders.index')}}",
	        applyFilter:false,
	        tapeRecordersListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        brands: [],
			manufacturers: [],
			head_configurations:[],
			number_of_heads: [],
			speeds: [],
			tracks: [],
			head_compositions: [],
			equalizations: [],
			voltages: [],
			auto_reverses: [],
			applications: [],
			max_reel_sizes: [],
			noise_reductions: [],
			motors: [],
			filter: {
				manufacturer_id: [],
				brand_id: [],
				head_configuration_id: [],
				number_of_head_id: [],
				speed_id: [],
				track_id: [],
				head_composition_id: [],
				equalization_id: [],
				voltage_id: [],
				auto_reverse_id: [],
				application_id: [],
				max_reel_size_id: [],
				noise_reduction_id: [],
				motor_id: []
			}
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  },
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	
		   this.getMasterData();    
		},
		methods: {
			getMasterData: function(){
		  		var self = this;
		  		var url = "{{url('/admin/master-data/tape-recorders')}}";
		  		$.ajax({
		  			url: url,
		  			type: 'GET',
		  			success: function(response){
		  				console.log(response.data);
		  				var data = response.data;
			  			self.brands = data.brands;
			  			self.manufacturers = data.manufacturers;
			  			self.head_configurations = data.head_configuration;
			  			self.number_of_heads = data.number_of_heads;
			  			self.speeds = data.speeds;
			  			self.tracks = data.tracks;
			  			self.head_compositions = data.head_composition;
			  			self.equalizations = data.equalizations;
			  			self.voltages = data.voltages;
			  			self.auto_reverses = data.auto_reverses;
			  			self.applications = data.applications;
			  			self.max_reel_sizes = data.max_reel_sizes;
			  			self.noise_reductions = data.noise_reductions;
			            self.motors = data.motors;  
		  			},
		  			error: function(error){
		  				console.log(error);
		  			}
		  		});
		  	},
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeRecorderForm: function(id) {
		        window.location = "{{url('tape-recorders')}}/"+id;
		    },
		    editTapeForm: function(id) {
		        this.$router.push('/tapes/'+id+'/edit');
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.tapeRecordersListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.tapeRecordersListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
		    getTapeRecorders:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapeRecorders = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			console.log(error);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getTapeRecorders();
		    },
		    addNewTapeRecorder:function(){
		    	window.location = "{{url('tape-recorders/create')}}";
		    }
	  	}
	});

	new Vue ({
		el: '#owned_tapes',
		data:{
	        tapes:{!!json_encode($tapes)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        tapeRoute:"{{route('tapes.index')}}",
	        applyFilter:false,
	        tapesListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        tracks: [],
			genres: [],
			classicals: [],
			speeds: [],
			channels: [],
			reel_sizes: [],
			noise_reductions: [],
			duplications: [],
			filter: {
				track_id: [],
				genre_id: [],
				classical_id: [],
				speed_id: [],
				channel_id: [],
				reel_size_id: [],
				noise_reduction_id: [],
				duplication_id: []
			}
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	  
		   this.getMasterData();  
		},
		methods: {
			getMasterData: function(){
		  		var self = this;
		  		var url = "{{url('/admin/master-data/tapes')}}";
		  		$.ajax({
		  			url: url,
		  			type: 'GET',
		  			success: function(response){
		  				var data = response.data;
		  				self.tracks = data.tracks;
						self.genres = data.genres;
						self.classicals = data.classicals;
						self.speeds = data.speeds;
						self.channels = data.channels;
						self.reel_sizes = data.reel_sizes;
						self.noise_reductions = data.noise_reductions;
						self.duplications = data.duplications;
		  			},
		  			error: function(error){
		  				console.log(error);
		  			}
		  		});
		  	},
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeForm: function(id) {
		        window.location = "{{url('/tapes/')}}/"+id;
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.tapesListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.tapesListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapes();
		        console.log(this.tapes);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapes();
		        console.log(this.tapes);
		    },
		    getTapes:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapes = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getTapes();
		    },
		    addNewTape:function(){
		    	window.location = "{{url('tapes/create')}}";
		    }
	  	},
	});
</script>
@endsection