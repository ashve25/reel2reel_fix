@extends('Frontend::master')
@section('css')
<style type="text/css">
#new-input-expander > .col-lg-2,  #new-input-expander > .col-md-4,  #new-input-expander > .col-sm-6 #new-input-expander > .col-xs-12{
	padding-bottom: 30px;
}

#form-div{
	margin-top:30px; 
}

</style>
@endsection
@section('content')
<div class="container">
	<div id="new-input-expander" v-cloak>
		<h1 class="text-center">New Input Expander</h1>
		<div id="form-div">
			<div class="row">	
				<div class="col-md-12">
					<form role="form" @submit="submitForm($event)">
						<input type="hidden" name="status" value="0">
						<input type="hidden" name="user_id" @if(Auth::user()) value="{{Auth::user()->id}}" @endif>
						<div class="box-body">
							<div class="row">
								<div class="form-group col-sm-6">
									<label>Model*</label>
									<input required="" class="form-control" type="text" name="model"/>
								</div>
								<div class="form-group col-sm-6">
									<label>Brand*</label>
									<select  class="form-control" name="brand_id" required="">
										<option value="">Select Brand</option>
										<option v-for="brand in brands" :value="brand.id">@{{brand.name}}</option>
									</select>
								</div>		
							</div>
							<label>Images</label>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label>Image 1*</label>
									<input required="" type="file" class="form-control" name="images[0]" accept="image/*">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 2</label>
									<input type="file" class="form-control" name="images[1]" accept="image/*">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 3</label>
									<input type="file" class="form-control" name="images[2]" accept="image/*">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 4</label>
									<input type="file" class="form-control" name="images[3]" accept="image/*">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Main Image*</label>
									<select required="" class="form-control" name="main_image">
										<option value="0">Pic 1</option>
										<option value="1">Pic 2</option>
										<option value="2">Pic 3</option>
										<option value="3">Pic 4</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Tape Recorders Usable</label>
									<input class="form-control" type="number" name="tape_recorders_usable"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Outputs for Record(1-10)</label>
									<input class="form-control" type="number" name="outputs_for_record"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Tape Copy Option(# Decks)</label>
									<input class="form-control" type="number" name="copy_option_decks"/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<button @click="cancelForm($event)" type="button" class="btn btn-danger pull-right">Back</button>
									<button type="submit" class="btn btn-success pull-right" style="margin-right: 10px">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>	
@endsection
@section('js')
<script type="text/javascript">
	new Vue({
		el:"#new-input-expander",
		data:{
			brands:{},
		},
		mounted: function(){
		  	this.getBrands();
		},
		methods:{
			getBrands:function(){
				var self = this;
				$.ajax({
					type:'get',
					url:"{{url('admin/brands')}}",
					success:function(response){
						self.brands = response.data;
					},
					error:function(error){
						console.log(error.responseText);
					}
				});
			},
			submitForm: function(event) {
				event.preventDefault();
				var formData = new FormData($('form')[0]);
				var self = this;
				$('input[type=file]').each(function(index, field){
					if(field.files[0] && field.files[0].size > 2000000){
						bootbox.alert('File size cannot be greater than 2MB');
						return false;
					}
				});
				$.ajax({
					type:"post",
					url:"{{url('admin/input-expanders')}}",
					data:formData,
					contentType:false,
					processData:false,
					success:function(response){
						console.log(response);
						if(response.meta.http_code == 200){
							bootbox.alert('Input expander details added', function(){
								window.location = "{{url('input-expanders')}}";	
							});
						}
					},
					error:function(error){
						console.log(error.responseText);
						if(error.responseText.meta.error_code == 400){
							$.each(error.response.meta.message, function(index, item){
								bootbox.alert(item);
							});	
						}
					}
				});
			},
			cancelForm: function(event){
				event.preventDefault();
				window.location = "{{url('input-expanders')}}";
			}
		}
	});
</script>
@endsection			