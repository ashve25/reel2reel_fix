@extends('Frontend::master')
@section('css')
<style type="text/css">
.row{
	margin-top: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="brand" v-cloak>
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Brand</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<h4>@{{brand.name | capitalize}}</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<center></center>
					<ul class="list-group">
						<li class="list-group-item">Company Profile : <a href="#" class="btn btn-sm btn-primary">Submit new info</a></li>
						<li class="list-group-item">Years in Business : @{{brand.year_to}}  -  @{{brand.year_from}}</li>
						<li class="list-group-item">Years making R-R Tape Recorders : </li>
						<li class="list-group-item">Technical Innovations : </li>
						<li class="list-group-item">General Information : </li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="list-group">
						<li class="list-group-item">Company description : <a href="#" class="btn btn-sm btn-primary">Submit new info</a></li>
					</ul>
					<p>
						@{{brand.description}}
					</p>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>@{{brand.name | capitalize}} R-R Tape Recorder Models
					<a class="btn btn-sm btn-primary pull-right">Submit New Model</a>
				</h2>
			</div>
		</div>
		<div class="row">
			<div v-for="tapeRecorder, index of tapeRecorders.data"  class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
				<div class="card" style="background: white" @click="editTapeRecorderForm(tapeRecorder.id)">
					<img v-if="tapeRecorder.images[0] && tapeRecorder.images[0].thumbnail" class="img-responsive center-block" :src="tapeRecorder.images[0].thumbnail" @error="showPlaceholder($event)">
	            	<img v-else class="img-responsive center-block" :src="placeholder">
	            	<p class="text-center">@{{tapeRecorder.model | capitalize}}</p>
	        	</div>
	        	<br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="tapeRecorders.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="tapeRecorders.last_page>tapeRecorders.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
			        </div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection	
@section('js')
<script type="text/javascript">
	var brandDetailInstance = new Vue({
		el:"#brand",
		data:{
			tapeRecorders : {!!json_encode($tapeRecorders)!!},
			brand : {!!json_encode($brand)!!},
			placeholder:"{{ asset('images/placeholder.jpg') }}",
			page:"{{Request::input('page')}}"
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }
		},
		methods:{
			getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapeRecorders();
		        console.log(this.tapeRecorders);
		    },
			editTapeRecorderForm:function(tapeRecorderId) {
				window.location = "{{url('tape-recorders')}}/"+tapeRecorderId;
			},
			showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    getTapeRecorders:function(){
		    	var url = "{{url('tape-recorders')}}?page="+this.page+'&ajax=true&brand_id='+this.brand.id;
		    	console.log(url);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapeRecorders = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			console.log(error);
		    		},
		    	});
		    }
		}
	});
</script>
@endsection
		