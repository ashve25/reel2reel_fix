@extends('Frontend::master')
@section('css')
<style type="text/css">
#catalog-images > .col-lg-2,  #catalog-images > .col-md-4,  #catalog-images > .col-sm-6 #catalog-images > .col-xs-12{
	padding-bottom: 30px;
}
.hidden {
    display: none;
}
 .fancybox-next span {
  left: auto;
  right: 20px;
 }
 .fancybox-prev span {
  left: 20px;
 }
</style>
@endsection
@section('content')
<div class="container">
	<div id="catalog-images" v-cloak>
		<h1 class="text-center">@{{catalogTitle | capitalize}} Catalog Images
			<a class="btn btn-sm btn-primary pull-right" title="Back to Catalogs" href="{{url('catalogs')}}"><i class="fa fa-arrow-left"></i> Catalogs</a>
		</h1>
		<div class="row">
			<br><br>
			<div v-for="images, index in catalogOtherImages" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
		  		<div class="card" style="background: white">
                	
                	<a class="fancybox-thumbs" data-width="600" data-height="420" data-fancybox-group="thumb1"  :href="images.thumbnail_image">
                		<img v-if="images.thumbnail_image" class="img-responsive center-block" :src="images.thumbnail_image" @error="showPlaceholder($event)">
                		<img v-else class="img-responsive center-block" :src="placeholder">
                	</a>
		  			<!-- <a class="fancybox" :href="images.thumbnail_image">
		  				<img v-if="images.thumbnail_image" class="img-responsive center-block" :src="images.thumbnail_image" @error="showPlaceholder($event)">
                		<img v-else class="img-responsive center-block" :src="placeholder">
		  			</a> -->
            	</div>
		  	</div>
		  	<br>
		  	<br>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#catalog-images',
		data:{
			catalogTitle:{!!json_encode($catalogTitle)!!},
	        catalogOtherImages:{!!json_encode($catalogOtherImages)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
		},
		mounted(){
			$('.fancybox-thumbs').fancybox({
                fitToView: false,
			    autoSize: false,
			    afterLoad: function () {
			        this.width = $(this.element).data("width");
			        this.height = $(this.element).data("height");
			    },
                /*prevEffect : true,
                nextEffect : true,

                closeBtn  : true,
                arrows    : true,
                nextClick : true,

                helpers : {
                    thumbs : {
                        width  : 50,
                        height : 50
                    }
                }*/
                
            });
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		methods: {
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    }
	  	},
	});
</script>
@endsection