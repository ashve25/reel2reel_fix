<div class="header-bottom-w3ls">
  <div class="col-md-12 navigation-agileits">
    <div class="col-md-10">
      <nav class="navbar navbar-default">
        <div class="navbar-header nav_2">
          <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div> 
        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
          <ul class="nav navbar-nav">
            <li ><a href="{{url('home')}}" class="hyper "><span>Home</span></a></li>  
            <li class="{{(Request::is('tape-recorders') || Request::is('tape-recorders/*')) || (Request::is('manufacturers') || Request::is('manufacturers/*')) ||  (Request::is('brands') || Request::is('brands/*')) ? 'active dropdown' : 'dropdown'}}">
              <a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span>Tape Recorders</span></a>
                <ul class="dropdown-menu multi" style="display: none;">
                  <div class="row">
                    <div class="col-sm-12">
                      <ul class="multi-column-dropdown">
                        <li class="{{Request::is('tape-recorders') && Request::input('tracks') == 2 ? 'active' : ''}}">
                          <a href="{{route('tape-recorders.index', ['tracks' => '2'])}}">
                            
                            2 Track
                          </a>
                        </li>
                        <li class="{{Request::is('tape-recorders') && Request::input('tracks') == 4 ? 'active' : ''}}">
                          <a href="{{route('tape-recorders.index', ['tracks' => '4'])}}">
                            4 Track</a>
                        </li>
                        <li><a href="#"> Pro Studio Machines</a></li>
                        
                        <li class="{{Request::is('manufacturers') == 2 ? 'active' : ''}}"><a href="{{url('manufacturers')}}">Manufacturers</a></li>
                        <li class="{{Request::is('brands') == 2 ? 'active' : ''}}"><a href="{{url('brands')}}">Brands</a></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>  
                </ul>
            </li>
            <li class="{{Request::is('tapes') || Request::is('tapes/*') ? 'active dropdown' : 'dropdown'}}">
                <a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> Tapes </span></a>
                <ul class="dropdown-menu multi multi1" style="display: none;">
                  <div class="row">
                    <div class="col-sm-12">
                      <ul class="multi-column-dropdown">
                        <li class="{{Request::is('tapes') && Request::input('tracks') == 2 ? 'active' : ''}}"><a href="{{route('tapes.index', ['tracks' => '2'])}}">
                          2 Track </a></li>
                        <li class="{{Request::is('tapes') && Request::input('tracks') == 4 ? 'active' : ''}}"><a href="{{route('tapes.index', ['tracks' => '4'])}}">
                          4 Track</a></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>  
                </ul>
            </li>
            <li class="{{Request::is('catalogs') || Request::is('catalogs/*') ? 'active dropdown' : 'dropdown'}}">
                <a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> R-R Resources </span></a>
                <ul class="dropdown-menu multi multi2" style="display: none;">
                  <div class="row">
                    <div class="col-sm-12">
                      <ul class="multi-column-dropdown">
                        <li class="{{Request::is('catalogs') && Request::input('type') == 1 ? 'active' : ''}}"><a href="{{route('catalogs.index', ['type' => '1'])}}">
                          Harrison Catalogs </a></li>
                        <li class="{{Request::is('catalogs') && Request::input('type') == 2 ? 'active' : ''}}"><a href="{{route('catalogs.index', ['type' => '2'])}}">
                          Ampex Catalogs</a></li>
                        <li class="{{Request::is('catalogs') && Request::input('type') == 3 ? 'active' : ''}}"><a href="{{route('catalogs.index', ['type' => '3'])}}">
                          Others Catalogs</a></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>  
                </ul>
            </li>
            <li  class="{{ Request::path() == 'tape-head-preamps' ? 'active dropdown' : 'dropdown' }} || {{ Request::path() == 'input-expanders' ? 'active dropdown' : 'dropdown' }}">
                <a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> R-R Misc </span></a>
                <ul class="dropdown-menu multi multi3" style="display: none;">
                  <div class="row">
                    <div class="col-sm-12">
                      <ul class="multi-column-dropdown">
                        <li class="{{ Request::path() == 'tape-head-preamps' ? 'active' : '' }}"><a href="{{route('tape-head-preamps.index')}}">
                          Tape Head Preamps </a></li>
                        <li class="{{ Request::path() == 'input-expanders' ? 'active' : '' }}"><a href="{{route('input-expanders.index')}}">
                          Input Expanders</a></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                  </div>  
                </ul>
            </li>
            <li><a href="#" class="hyper"><span>History</span></a></li>
            <li><a href="#" class="hyper"><span>Forum</span></a></li>

          </ul>
        </div>
      </nav>
    </div>
  </div>
  <div class="clearfix"></div>
</div>  