@extends('Frontend::master')
@section('css')
<!-- #303542 -->
<style type="text/css">
	.single-top-right{
		background:#1f232d; 
		padding: 30px;
	}
	.single-top-left{
		width: 60%;
	}
	.flex-viewport{
		min-height:500px;
		height: auto;
	}
	.row{
		margin-top:20px;
	}
	
	.full-row{
		width: 100%;
		height: 26px;
	}
	.half-row-left{
		width: 50%;
		float: left;
	}
	.half-row-right{
		width: 50%;
		float: right;
	}
	.custom-button{
		color: #fff;
		border: 1px solid #fff;
		width: 97%;
		text-align: left;
		padding-left:5px; 
		line-height: 22px;
	}
	.item_name{
		color: #fff !important;
	}
	#tech-details{
		background: #1f232d;
		color: #fff;
	}
	#tech-details .full-row{
		margin: 10px;
	}

	.li-left{
		float: left;
		width: 50%;
	}

	.li-right{
		float: right;
		width: 50%;
	}

	#details .full-row ul{
		list-style-type:none;
	}

	.header-bottom .full-row{
		height: 40px;
	}
	#dtl .custom-button{
		text-align: center ;
	}
	#add-details p{
		color: #000;
	}
	#add-details .col-md-12{
		border-bottom: 1px solid #eee;
		margin-top: 3px;
    	margin-bottom: 20px;
    	padding: 0px;
	}

	#add-details .col-md-6, .col-md-4{
		margin-bottom: 20px;
	}
</style>
@endsection
@section('content')
<div class="products" id="products" v-cloak>	 
	<div class="container">  
		<div class="single-page">
			<div class="single-page-row" id="detail-21">
				<div class="col-md-8 single-top-left">	
					<div class="flexslider">
						<ul class="slides">
							<li :data-thumb="image.image" v-for="image, index of tape.images">
								<div class="thumb-image"> 
									<img :src="image.image" data-imagezoom="true" class="img-responsive" alt=""> 
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 single-top-right">
					<div class="header-top">
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">
									<a href="{{url('tapes')}}" style="color:#fff;"><i class="fa fa-arrow-left"></i> Tapes</a>
								</div>
							</div>
						</div>
						<hr/>
						<h3 class="item_name"> @{{tape.title_composition}}</h3>
						<hr/>
						<div class="full-row">
							<div class="half-row-left" id="dtl" @click="detailShow">
								<div class="custom-button">Details <i class="fa fa-arrow-down"></i></div>
							</div>
						</div>
					</div>
					<hr/>
					<div class="header-bottom">
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button" @click="addAsFavorite($event)" :style="addedInFavourite ? 'border-color:#ccc; color:#ccc;' : ''">
									<span v-if="addedInFavourite">Favorite <i class="fa fa-star"></i></span>
									<span v-else>Favorite <i class="fa fa-star-o"></i></span>
								</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Favorites #: </div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Add to collection :</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Owners #: 0</div>
							</div>
						</div>

						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Price Paid : 0$</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Avg Price : 0$</div>
							</div>
						</div>

						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Musical Performance Rating : 0/10</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Avg. Musical Performance Rating : 0/10</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Sound Quality Rating : 0/10</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Avg. Sound Quality Rating : 0/10</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Detailed Review</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Detailed Review #:</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Email me if this tape comes up for sale</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Looking To Buy Tape #:</div>
							</div>
						</div>
						
					</div>		
				</div>
			   <div class="clearfix"> </div>  
			</div>
		</div> 
		<!-- collapse-tabs -->
		<div class="collpse tabs">
			<h3 class="w3ls-title">Details</h3>
			<hr/>
			<div class="row" id="tech-details">
				<div class="full-row">
					<div class="half-row-left">
						<ul>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Genre:
									</div> 
									<div class="li-right">
										@{{tape.genre_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Artist:
									</div> 
									<div class="li-right">
										@{{tape.artist_composer}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Label:
									</div> 
									<div class="li-right">
										@{{tape.label}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Tracks:
									</div> 
									<div class="li-right">
										@{{tape.track_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Speed:
									</div> 
									<div class="li-right">
										@{{tape.speed_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Dolby:
									</div> 
									<div class="li-right">
									<!-- @{{tape.channel_name}} -->
									</div>
								</div>
							</li>
						</ul> 
					</div>
					<div class="half-row-right">
						<ul>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Release Dates:
									</div> 
									<div class="li-right">
										@{{tape.release_date}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Duplicated By:
									</div> 
									<div class="li-right">
										@{{tape.duplication_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Master Dub:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Reel Size:
									</div> 
									<div class="li-right">
										@{{tape.reel_size_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Sound Quality Rating:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Performance Rating:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Record Levels:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
						</ul> 
					</div>
				</div>
			</div> 
			
		</div>
		<!-- //collapse -->
		<!-- collapse-tabs -->
		<div class="collpse tabs">
			<h3 class="w3ls-title">Description</h3>
			<hr/>
			<div class="row" id="add-details">
				<div class="col-md-12">
					<div class="col-md-8">
						<p>@{{tape.description}}</p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<p>Owners: <a href="#">admin</a></p>
					</div>
				</div>	
			</div> 
			
		</div>
		<!-- //collapse --> 
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	tapeDetailInstance = new Vue({
		el:"#products",
		data:{
			tape:{!!json_encode($tape)!!},
			user:{!!json_encode(Auth::user())!!},
			addedInFavourite:"{{$addedInFavourite}}",
		},
		mounted(){

		},
		methods:{
			detailShow:function () {
				$('html,body').animate({
    				scrollTop: $("#tech-details").offset().top
    			}, 'slow');
			},
			addAsFavorite:function(event){
				if(!this.user){
					bootbox.alert('Please Login with your account first for add in favorite list.');
					return false;
				}
				this.addedInFavourite = !this.addedInFavourite;
				if(this.addedInFavourite === true){
					$(event.target).css({"border-color":"#ccc", "color":"#ccc"});
					var url = "{{url('tapes/add-as-favourites')}}";
					var msg = 'Added into favourite list';

				}else{
					$(event.target).css({"border-color":"#fff", "color":"#fff"});
					var url = "{{url('tapes/delete-from-favourites')}}";
					var msg = 'Delete from favourite list';
				}

				$.ajax({
					type:"post",
					url:url,
					data:{tape_id:this.tape.id},
					success:function(response){
						if(response == 200){
							bootbox.alert(msg);
						}
					},
					error:function(error){
						console.log(error.responseText);
					}
				})
			}
		}
	});
</script>
@endsection