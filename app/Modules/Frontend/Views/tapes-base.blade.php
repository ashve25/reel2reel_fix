<div class="col-md-12 ruler">
	<div class="col-md-6">
		<a class="btn btn-primary btn-sm" @click="showAdvanceFilter"><i class="fa fa-search-plus"></i> Show Advanced Search</a>
	</div>
	@if(Auth::check())
	<div class="col-md-6">
		<a class="btn btn-primary btn-sm pull-right" @click="addNewTape">
			<i class="fa fa-plus"></i> Add New Tape
		</a>
	</div>
	@endif
</div>
<div class="col-md-12">
	<div class="col-md-4 pull-left results-meta-left">
		<span>
		@{{tapes.total}} Results
		</span>
	</div>
	<div class="col-md-8">
		<div class="pull-right">
			<div class="pagination">
	          <a href="#" v-if="tapes.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
	          <a href="#" v-if="tapes.last_page>tapes.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
	        </div>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="col-md-4 box" v-if="applyFilter">
		<div class="w3ls_dresses_grid_left_grid">
			<h3>TRACKS</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="track in tracks">
							<input type="checkbox" :value="track.id" v-model="filter.track_id" v-on:change="filterRecords">
							@{{track.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>GENRES</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="genre in genres">
							<input type="checkbox" :value="genre.id" v-model="filter.genre_id" v-on:change="filterRecords">
							@{{genre.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>CLASSICAL</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="classical in classicals">
							<input type="checkbox" :value="classical.id" v-model="filter.classical_id" v-on:change="filterRecords">
							@{{classical.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="w3ls_dresses_grid_left_grid">
			<h3>SPEEDS</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="speed in speeds">
							<input type="checkbox" :value="speed.id" v-model="filter.speed_id" v-on:change="filterRecords">
							@{{speed.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>CHANNELS</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="channel in channels">
							<input type="checkbox" :value="channel.id" v-model="filter.channel_id" v-on:change="filterRecords">
							@{{channel.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>REEL SIZES</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="reel_size in reel_sizes">
							<input type="checkbox" :value="reel_size.id" v-model="filter.reel_size_id" v-on:change="filterRecords">
							@{{reel_size.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>NOISE REDUCTION</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="noise_reduction in noise_reductions">
							<input type="checkbox" :value="noise_reduction.id" v-model="filter.noise_reduction_id" v-on:change="filterRecords">
							@{{noise_reduction.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="w3ls_dresses_grid_left_grid">
			<h3>DUPLICATIONS</h3>
			<div class="w3ls_dresses_grid_left_grid_sub">
				<div class="ecommerce_color ecommerce_size">
					<ul>
						<li v-for="duplication in duplications">
							<input type="checkbox" :value="duplication.id" v-model="filter.duplication_id" v-on:change="filterRecords">
							@{{duplication.name}}
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div :class="tapesListClass">
		<div v-if="tapes.data.length>0" class="row" id="tapes">
			<div v-for="tape, index in tapes.data" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
		  		<div class="card" style="background: white" @click="editTapeForm(tape.id)">
                	<img v-if="tape.thumbnail" class="img-responsive center-block" :src="tape.thumbnail" @error="showPlaceholder($event)">
                	<img v-else class="img-responsive center-block" :src="placeholder">
                	<p class="text-center">@{{tape.title_composition | capitalize}}</p>
            	</div>
		  	</div>
		  	<br>
		  	<br>
		</div>
		<div v-else>
			<center><b>No Record Found</b></center>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="col-md-4 pull-left results-meta-left">
		<span>
		@{{tapes.total}} Results
		</span>
	</div>
	<div class="col-md-8">
		<div class="pull-right">
			<div class="pagination">
	          <a href="#" v-if="tapes.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
	          <a href="#" v-if="tapes.last_page>tapes.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
	        </div>
		</div>
	</div>
</div>