@extends('Frontend::master')
@section('css')
<style type="text/css">
#form-div{
	margin-top:20px;
}
#reg-div{
	background: rgba(81, 193, 231, 0.14);
	
}

#signup-list-header{
	font-size: 18px;
    background: rgba(128, 128, 128, 0.09);
    padding: 10px 20px;
    color: #383e4d;
    margin: 20px;
}

h2{
	margin: 20px;
}

.col-md-6{
	padding: 20px;
}

#benifit ul{
	list-style: none;
	padding: 10px 20px;
}

#benifit li{
	font-size: 15px;
    padding: 13px 0;
    font-weight: 500;
    border-bottom: 1px solid rgba(128, 128, 128, 0.35);
}


</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6" id="reg-div">
				@if(session('success'))
					<div class="alert alert-success alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Success!</strong> {{session('success')}}
					</div>
				@endif
				@if(session('warning'))
					<div class="alert alert-danger alert-dismissible">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <strong>Warning!</strong>
					 {{session('warning')}}
					</div>
				@endif
				<h2 class="text-center">Create an Account</h2>
				<p>Registering for this site is easy, just fill in the fields below and we'll get a new account set up for you in no time.</p>
				<div id="form-div">
					<div class="row">
						<div class="col-md-12">
							<form action="{{url('register')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
							  <input type="hidden" name="user_type" value="3">
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Name*</label>
							      <input type="text" name="name" class="form-control" placeholder="Enter Name" required value="{{old('name')}}">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Username*</label>
							      <input type="text" name="username" class="form-control" placeholder="Enter User Name" required value="{{old('username')}}">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Email*</label>
							      <input type="email" name="email" class="form-control" placeholder="Enter Email" required value="{{old('email')}}">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Password*</label>
							      <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Confirm Password*</label>
							      <input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password" required>
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Country*</label>
							      <input type="text" name="country" class="form-control" placeholder="Enter Country" required value="{{old('country')}}">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>City</label>
							      <input type="text" name="city" class="form-control" placeholder="Enter City" value="{{old('city')}}">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Profile Picture</label>
							      <input type="file" name="profile_picture" class="form-control" placeholder="Enter Name">
							    </div>
							  </div>
							  <div class="form-group">
							    <div class="col-md-12">
							      <label>Terms and Conditions*</label>
							      <p><input type="checkbox" name="terms_and_conditions" @if(old('terms_and_conditions', null) == null) @else checked @endif>
							      I agree to the terms and conditions posted on this website</p>
							      <a href="#">Click here to read terms and conditions</a>
							    </div>
							  </div>
							  <div class="form-group">
							  	<div class="col-md-12">
							  		<button type="submit" class="btn btn-sm btn-success">Submit</button>
							  	</div>
							  </div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6" id="benifit">
				<h2>Member Benefits on Reel-Reel.com</h2> 
				<div id="signup-list-header">Get access to the following features:</div>
				<ul>
	             	<li><i class="fa fa-check"></i>User Reviews R2R Recorders &amp; Tapes</li>
	             	<li><i class="fa fa-check"></i>Price History R2R Recorders &amp; Tapes</li>
	             	<li><i class="fa fa-check"></i>Build Your Own Custom Collection Profile</li>
	             	<li><i class="fa fa-check"></i>Communicate &amp; Share your collection with Other Members</li>
	             	<li><i class="fa fa-check"></i>Follow Other Members Collections</li>
	             	<li><i class="fa fa-check"></i>Forum Access</li>
	             	<li><i class="fa fa-check"></i>Save Favorites and Get Updates if Items Become Available for Sale</li>
	             	<li><i class="fa fa-check"></i>Search the complete database</li>
	            </ul>
			</div>
		</div>
	</div>
</div>				
@endsection
@section('js')

@endsection