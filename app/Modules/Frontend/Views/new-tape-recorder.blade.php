@extends('Frontend::master')
@section('css')
<style type="text/css">
#new-tape-recorder > .col-lg-2,  #new-tape-recorder > .col-md-4,  #new-tape-recorder > .col-sm-6 #new-tape-recorder > .col-xs-12{
	padding-bottom: 30px;
}

#form-div{
	margin-top:30px; 
}

</style>
@endsection
@section('content')
<div class="container">
	<div id="new-tape-recorder" v-cloak>
		<h1 class="text-center">New Tape Recorder</h1>
		<div id="form-div">
			<div class="row">	
				<div class="col-md-12">
					<form id="form-tape-recorder" role="form" @submit="submitForm($event)">
						<input type="hidden" name="status" value="0">
						<input type="hidden" name="user_id" @if(Auth::user()) value="{{Auth::user()->id}}" @endif>
						<div class="box-body">
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Model*</label>
									<input required="" class="form-control" required type="text" name="model"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Brand*</label>
									<select class="form-control" name="brand_id" required>
										<option value="">Select Brand</option>
										<option v-for="brand in brands"  :value="brand.id">@{{brand.name}}</option>
									</select>
								</div>		
								<div class="form-group col-sm-4">
									<label>Serial Number*</label>
									<input required="" class="form-control" required type="text" name="serial_number"/>
								</div>							
							</div>						
							<div class="row">
	                            <div class="form-group col-sm-4">
									<label>Original Price*</label>
									<input required="" class="form-control" required type="text" name="original_price"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Manufacturer*</label>
									<select class="form-control" name="manufacturer_id" required>
										<option value="">Select Manufacturer</option>
										<option v-for="manufacturer in manufacturers" :value="manufacturer.id">@{{manufacturer.name}}</option>
									</select>
								</div>														
								<div class="form-group col-sm-4">
									<label>Country of Manufacturer</label>
									<input class="form-control" type="text" name="country_of_manufacturer"/>
								</div>						
							</div>
							<label>Images</label>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label>Image 1*</label>
									<input required="" type="file" class="form-control" name="images[0]"  accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 2</label>
									<input type="file" class="form-control" name="images[1]"  accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 3</label>
									<input type="file" class="form-control" name="images[2]"  accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 4</label>
									<input type="file" class="form-control" name="images[3]"  accept="image/jpeg,.jpg,.png">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label>Image 5</label>
									<input type="file" class="form-control" name="images[4]" accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 6</label>
									<input type="file" class="form-control" name="images[5]" accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 7</label>
									<input type="file" class="form-control" name="images[6]" accept="image/jpeg,.jpg,.png">
								</div>
								<div class="col-sm-3 form-group">
									<label>Image 8</label>
									<input type="file" class="form-control" name="images[7]" accept="image/jpeg,.jpg,.png">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Main Image*</label>
									<select required="" class="form-control" name="main_image">
										<option value="0">Image 1</option>
										<option value="1">Image 2</option>
										<option value="2">Image 3</option>
										<option value="3">Image 4</option>
										<option value="4">Image 5</option>
										<option value="5">Image 6</option>
										<option value="6">Image 7</option>
										<option value="7">Image 8</option>
									</select>
								</div>
								<div class="form-group col-sm-4">
									<label>Head Configuration</label>
									<select class="form-control" name="head_configuration_id">
										<option value="">Select Head Configuration</option>
										<option v-for="configuration in head_configuration" :value="configuration.id">@{{configuration.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-4">
									<label>Number of Heads</label>
									<select class="form-control" name="number_of_head_id">
										<option value="">Select Number of Head</option>
										<option v-for="number_of_head in number_of_heads" :value="number_of_head.id">@{{number_of_head.name}}</option>
									</select>
								</div>
							</div>
							<div class="row">							
								<div class="form-group col-sm-3">
									<label>Speed</label>
									<select class="form-control" name="speed_id">
										<option value="">Select Speed</option>
										<option v-for="speed in speeds" :value="speed.id">@{{speed.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Tracks</label>
									<select class="form-control" name="track_id">
										<option value="">Select Track</option>
										<option v-for="track in tracks" :value="track.id">@{{track.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Release Year(from)</label>
									<input class="form-control" type="number" name="release_from_date" min="0" />
								</div>
								<div class="form-group col-sm-3">
									<label>Release Year(to)</label>
									<input class="form-control" type="number" name="release_to_date" min="0" />
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Head Composition</label>
									<select class="form-control" name="head_compositio_id">
										<option value="">Select Head Composition</option>
										<option v-for="composition in head_composition" :value="composition.id">@{{composition.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Equalization</label>
									<select class="form-control" name="equalization_id">
										<option value="">Select Equalization</option>
										<option v-for="equalization in equalizations" :value="equalization.id">@{{equalization.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Voltage</label>
									<select class="form-control" name="voltage_id">
										<option value="">Select Voltage</option>
										<option v-for="voltage in voltages" :value="voltage.id">@{{voltage.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Auto Reverse</label>
									<select class="form-control" name="auto_reverse_id">
										<option value="">Select Auto Reverse</option>
										<option v-for="auto_reverse in auto_reverses" :value="auto_reverse.id">@{{auto_reverse.name}}</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Application</label>
									<select class="form-control" name="application_id">
										<option value="">Select Application</option>
										<option v-for="application in applications" :value="application.id">@{{application.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Max Reel Size</label>
									<select class="form-control" name="max_reel_size_id">
										<option value="">Select Max Reel Size</option>
										<option v-for="max_reel_size in max_reel_sizes" :value="max_reel_size.id">@{{max_reel_size.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Noise Reduction</label>
									<select class="form-control" name="noise_reduction_id">
										<option value="">Select Noise Reduction</option>
										<option v-for="noise_reduction in noise_reductions" :value="noise_reduction.id">@{{noise_reduction.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Motors</label>
									<select class="form-control" name="motor_id">
										<option value="">Select Motor</option>
										<option v-for="motor in motors" :value="motor.id">@{{motor.name}}</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Belt(s)(Type Size)</label>
									<input class="form-control" type="text" name="belts"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Frequency Response</label>
									<input class="form-control" type="text" name="frequency_response"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Wow and Flutter</label>
									<input class="form-control" type="text" name="wow_flutter"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Signal to Noise Ratio</label>
									<input class="form-control" type="text" name="signal_noise_ratio"/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label>Description</label>
									<textarea class="form-control" name="description"></textarea>
								</div>							
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<button @click="cancelForm($event)" type="button" class="btn btn-primary pull-right">Back</button>
									<button type="submit" class="btn btn-success pull-right" style="margin-right: 10px">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue({
  		el: "#new-tape-recorder",
		data:{
			brands:[],
			manufacturers: [],
			head_configuration:[],
			number_of_heads: [],
			speeds: [],
			tracks: [],
			head_composition: [],
			equalizations: [],
			voltages: [],
			auto_reverses: [],
			applications: [],
			max_reel_sizes: [],
			noise_reductions: [],
			motors: []
		},
		mounted: function(){
			this.getMasterData();
		},
		methods: {
			getMasterData: function(){
				var self = this;
				var url = "{{url('/admin/master-data/tape-recorders')}}";
				$.ajax({
					url: url,
					type: 'GET',
					success: function(response){
						console.log(response.data);
						var data = response.data;
			  			self.brands = data.brands;
			  			self.manufacturers = data.manufacturers;
			  			self.head_configuration = data.head_configuration;
			  			self.number_of_heads = data.number_of_heads;
			  			self.speeds = data.speeds;
			  			self.tracks = data.tracks;
			  			self.head_composition = data.head_composition;
			  			self.equalizations = data.equalizations;
			  			self.voltages = data.voltages;
			  			self.auto_reverses = data.auto_reverses;
			  			self.applications = data.applications;
			  			self.max_reel_sizes = data.max_reel_sizes;
			  			self.noise_reductions = data.noise_reductions;
			            self.motors = data.motors;  
					},
					error: function(error){
						console.log(error);
					}
				});
			},
			submitForm: function(event) {
				event.preventDefault();
				var self = this;
				$('input[type=file]').each(function(index, field){
					if(field.files[0] && field.files[0].size > 2000000){
						bootbox.alert('File size cannot be greater than 2MB');
						return false;
					}
				});
				var formData = new FormData($('#form-tape-recorder')[0]);
				var url = "{{url('admin/tape-recorders')}}";
				$.ajax({
					url: url,
					type: 'POST',
					data: formData,
					processData: false,
    				contentType: false,
					success: function(response){
						console.log(response);
						if(response.meta.http_code == 200){
							bootbox.alert('Tape recorder details added', function(){
								window.location = "{{url('tape-recorders')}}";
							});
						}
					},
					error:function(error){
						console.log(error.responseText);
						if(error.responseText.meta.error_code == 400){
							$.each(error.responseText.meta.message, function(index, item){
								bootbox.alert(item);
							});	
						}
					}
				});	
			},
			cancelForm: function(event){
				window.location = "{{url('tape-recorders')}}";
			}
		}
	});
</script>
@endsection