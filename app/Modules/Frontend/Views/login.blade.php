@extends('Frontend::master')
@section('css')
<style type="text/css">
</style>
@endsection
@section('content')
<center><img src="{{asset('images/logo.png')}}" style="width: 100px;"></center>
<div class="login">
	<div class="main-agileits">
		@if(session('success'))
	        <div class="alert alert-success alert-dismissible">
	          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	          <strong>Success!</strong> {{session('success')}}
	        </div>
	      @endif
	      @if(session('warning'))
	        <div class="alert alert-danger alert-dismissible">
	          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	          <strong>Warning!</strong>
	            {{session('warning')}}
	        </div>
	    @endif
		<div class="form-w3agile">
			<h3>Login</h3>
			<form action="{{url('login')}}" method="post">
				<div class="key">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					<input  type="text" name="email_or_username" required="" placeholder="Email Or Username" value="{{old('email_or_username')}}">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<i class="fa fa-lock" aria-hidden="true"></i>
					<input  type="password" name="password" required="" placeholder="Password">
					<div class="clearfix"></div>
				</div>
				<input type="submit" value="Login">
			</form>
		</div>
		<div class="forg">
			<a href="{{url('forgot-password')}}" class="forg-left">Forgot Password?</a>
			<a href="{{url('register')}}" class="forg-right">Register</a>
		<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection
@section('js')
@endsection			