@extends('Frontend::master')
@section('css')
<style type="text/css">
#catalogs > .col-lg-2,  #catalogs > .col-md-4,  #catalogs > .col-sm-6 #catalogs > .col-xs-12{
	padding-bottom: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="catalogs" v-cloak>
		<div class="row">
		@if(Auth::check())
			<div class="col-md-12">
				<a class="btn btn-primary btn-sm pull-right" @click="newCatalog"><i class="fa fa-plus"></i> Add New Catalog</a>
			</div>
		@endif	
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 pull-left results-meta-left">
					<span>
					@{{catalogs.total}} Results
					</span>
				</div>
				<div class="col-md-12">
					<div class="pull-right">
						<div class="pagination">
				          <a href="#" v-if="catalogs.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
				          <a href="#" v-if="catalogs.last_page>catalogs.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
				        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div :class="catalogsListClass">
					<div v-if="catalogs.data.length>0" class="row" id="catalogs">
						<div v-for="catalog, index in catalogs.data" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
					  		<div class="card" style="background: white" @click="editcatalogForm(catalog)">
			                	<img v-if="catalog.thumbnail_image" class="img-responsive center-block" :src="catalog.thumbnail_image" @error="showPlaceholder($event)">
			                	<img v-else class="img-responsive center-block" :src="placeholder">
			                	<p class="text-center">@{{catalog.title | capitalize}}</p>
			            	</div>
					  	</div>
					  	<br>
					  	<br>
					</div>
					<div v-else class="row">
						<center><b>No Record Found</b></center>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 pull-left results-meta-left">
					<span>
					@{{catalogs.total}} Results
					</span>
				</div>
				<div class="col-md-8">
					<div class="pull-right">
						<div class="pagination">
				          <a href="#" v-if="catalogs.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
				          <a href="#" v-if="catalogs.last_page>catalogs.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
				        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#catalogs',
		data:{
	        catalogs:{!!json_encode($catalogs)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        catalogRoute:"{{route('catalogs.index')}}",
	        applyFilter:false,
	        catalogsListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        type:"{{Request::input('type')}}",
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	    
		},
		methods: {
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editcatalogForm: function(catalog) {
		    	console.log(catalog.images.length);
		    	if(catalog.images.length>0){
		    		window.location = "{{url('/catalogs')}}/"+catalog.id+"/images";
		    	}
		        //this.$router.push('/catalogs/'+catalog.id+'/edit');
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getcatalogs();
		        console.log(this.catalogs);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getcatalogs();
		        console.log(this.catalogs);
		    },
		    getcatalogs:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true';
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.catalogs = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    },
		    newCatalog:function(){
		    	if(!this.type){
		    		window.location = "{{url('catalogs/create')}}";
		    	}else{
		    		window.location = "{{url('catalogs/create')}}?type="+this.type;
		    	}
		    }
	  	},
	});
</script>
@endsection