@extends('Frontend::master')
@section('css')
<style type="text/css">
#new-tape-head-preamp > .col-lg-2,  #new-tape-head-preamp > .col-md-4,  #new-tape-head-preamp > .col-sm-6 #new-tape-head-preamp > .col-xs-12{
	padding-bottom: 30px;
}

#form-div{
	margin-top:30px; 
}

</style>
@endsection
@section('content')
<div class="container">
	<div id="new-tape-head-preamp" v-cloak>
		<h1 class="text-center">New Tape Head Preamp</h1>
		<div id="form-div">
			<div class="row">	
				<div class="col-md-12">
					<form role="form" @submit="submitForm($event)">
						<input type="hidden" name="status" value="0">
						<input type="hidden" name="user_id" @if(Auth::user()) value="{{Auth::user()->id}}" @endif>
						<div class="box-body">
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Model*</label>
									<input required="" class="form-control" type="text" name="model"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Manufacturer*</label>
									<select class="form-control" name="manufacturer_id">
										<option value="">Select Manufacturer</option>
										<option v-for="manufacturer in manufacturers" :value="manufacturer.id">@{{manufacturer.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-4">
									<label>Electronics*</label>
									<select class="form-control" name="electronic_id">
										<option value="">Select Electronic</option>
										<option v-for="electronics in electronicsList" :value="electronics.id">@{{electronics.name}}</option>
									</select>
								</div>
							</div>
							<label>Images</label>
							<div class="row">
								<div class="col-sm-4 form-group">
									<label>Image 1*</label>
									<input required="" type="file" class="form-control" name="images[0]" accept="image/*">
								</div>
								<div class="col-sm-4 form-group">
									<label>Image 2</label>
									<input type="file" class="form-control" name="images[1]" accept="image/*">
								</div>
								<div class="col-sm-4 form-group">
									<label>Image 3</label>
									<input type="file" class="form-control" name="images[2]" accept="image/*">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4 form-group">
									<label>Image 4</label>
									<input type="file" class="form-control" name="images[3]" accept="image/*">
								</div>
								<div class="col-sm-4 form-group">
									<label>Image 5</label>
									<input type="file" class="form-control" name="images[4]" accept="image/*">
								</div>
								<div class="col-sm-4 form-group">
									<label>Image 6</label>
									<input type="file" class="form-control" name="images[5]" accept="image/*">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Main Image*</label>
									<select required="" class="form-control" name="main_image">
										<option value="0">Image 1</option>
										<option value="1">Image 2</option>
										<option value="2">Image 3</option>
										<option value="3">Image 4</option>
										<option value="4">Image 5</option>
										<option value="5">Image 6</option>
									</select>
								</div>
								<div class="form-group col-sm-4">
									<label>Inputs</label>
									<select class="form-control" name="input_id">
										<option value="">Select Input</option>
										<option v-for="inputs in inputList" :value="inputs.id">@{{inputs.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-4">
									<label>Outputs</label>
									<select class="form-control" name="output_id">
										<option value="">Select Output</option>
										<option v-for="outputs in outputList" :value="outputs.id">@{{outputs.name}}</option>
									</select>
								</div>
							</div>
							<div class="row">							
								<div class="form-group col-sm-4">
									<label>Adjustable Gain</label>
									<input class="form-control" type="text" name="adjustable_gain"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Adjustable Loading</label>
									<input class="form-control" type="text" name="adjustable_loading"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Playback EQ</label>
									<input class="form-control" type="text" name="playback_eq"/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Level Controls</label>
									<input class="form-control" type="text" name="level_controls"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Isolated Power Supply</label>
									<input class="form-control" type="text" name="isolated_power_supply"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Signal to Noise ratio</label>
									<input class="form-control" type="text" name="signal_noise_ratio"/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Tubes</label>
									<input class="form-control" type="text" name="tubes"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Suggested Input Wiring</label>
									<input class="form-control" type="text" name="suggested_input_wirings"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Impedance</label>
									<input class="form-control" type="text" name="impedance"/>
								</div>
								<div class="form-group col-sm-3">
									<label>Accesories</label>
									<input class="form-control" type="text" name="accesories"/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label>Description</label>
									<textarea class="form-control" name="description"></textarea>
								</div>
							</div>
							<div class="row">
								<!-- FRONTEND WILL NEED ARRAY OF COMMENTS -->
								<div class="form-group col-sm-12">
									<label>Comments</label>
									<div class="copy">
										<input class="form-control" type="text" name="comments[]"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<button @click="cancelForm($event)" type="button" class="btn btn-danger pull-right">Back</button>
									<button type="submit" class="btn btn-success pull-right" style="margin-right: 10px">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
	<script type="text/javascript">
		var TapeHeadPreampInstance = new Vue({
			el:"#new-tape-head-preamp",
			data:{
				manufacturers:[],
				electronicsList:[],
				inputList:[],
				outputList:[]
			},
			mounted: function(){
			  	this.getManufacturers();
			  	this.getInputs();
			  	this.getOutputs();
			  	this.getElectronicsList();
			},
			methods:{
				getManufacturers: function(){
			  		var self = this;
			  		$.ajax({
			  			type:'GET',
			  			url:"{{url('admin/data/manufacturers')}}",
			  			success:function(response){
			  				console.log(response);
			  				self.manufacturers = response.data;
			  			},
			  			error:function(error){
			  				console.log(error);
			  			}
			  		});
			  	},
			  	getInputs: function(){
			  		var self = this;
			  		$.ajax({
			  			type:'GET',
			  			url:"{{url('admin/data/inputs')}}",
			  			success:function(response){
			  				self.inputList = response.data;
			  			},
			  			error:function(error){
			  				console.log(error);
			  			}
			  		});
			  	},
			  	getOutputs: function(){
			  		var self = this;
			  		$.ajax({
			  			type:'GET',
			  			url:"{{url('admin/data/outputs')}}",
			  			success:function(response){
			  				self.outputList = response.data;
			  			},
			  			error:function(error){
			  				console.log(error);
			  			}
			  		});
			  	},
			  	getElectronicsList: function(){
			  		var self = this;
			  		$.ajax({
			  			type:'GET',
			  			url:"{{url('admin/data/electronics')}}",
			  			success:function(response){
			  				self.electronicsList = response.data;
			  			},
			  			error:function(error){
			  				console.log(error);
			  			}
			  		});
			  	},
				submitForm: function(event) {
					event.preventDefault();
					var self = this;
					$('input[type=file]').each(function(index, field){
						if(field.files[0] && field.files[0].size > 2000000){
							bootbox.alert('File size cannot be greater than 2MB');
							return false;
						}
					});

					var formData = new FormData($('form')[0]);
					$.ajax({
						type:'POST',
						url:"{{url('admin/tape-head-preamps')}}",
						data:formData,
						contentType:false,
						processData:false,
						success:function(response){
							console.log(response);
							if(response.meta.http_code == 200){
								bootbox.alert('Tape head preamp details added', function(){
									window.location = "{{url('/tape-head-preamps')}}";
								});
							}
						},
						error:function(error){
							console.log(error.responseText);
							if(error.responseText.meta.error_code == 400){
								$.each(error.responseText.meta.message, function(index, item){
									bootbox.alert(item);
								});	
							}
						}

					});
				},
				cancelForm:function(argument) {
					window.location = "{{url('tape-head-preamps')}}";
				}
			}
		});
	</script>
@endsection					