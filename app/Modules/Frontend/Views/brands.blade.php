@extends('Frontend::master')
@section('css')
<style type="text/css">
#brands > .col-lg-2,  #brands > .col-md-4,  #brands > .col-sm-6 #brands > .col-xs-12{
	padding-bottom: 30px;
}

ul li{
	list-style-type: none;
}

</style>
@endsection
@section('content')
<div class="container">
	<div id="brands" v-cloak>
		<h1 class="text-center">Brands</h1>
		<div class="col-md-12 ruler">
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{brands.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="brands.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="brands.last_page>brands.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
			        </div>
				</div>
			</div>
		</div>	
		<div class="col-md-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-4">
					<ul class="list-group">
					  	<li class="list-group-item" v-for="brand, index in brands.data">
					  		<a :href="'{{url('brands')}}/'+brand.id">
						  		<div class="row">
							  		<div class="col-md-12">
								  		<img class="pull-left" v-if="brand.images[0] && brand.images[0].image" :src="brand.images[0].image" style="height:30px; width:40px;" @error="showPlaceholder($event)">
								  		<img v-else class="pull-left" :src="placeholder" style="height:30px; width:40px;">
								  		<h3 style="margin-left:30px;" class="pull-left">@{{brand.name | capitalize}}</h3>
								  		<span style="margin-left:30px;">Info Needed</span>
									  	<a :href="'{{url('brands')}}/'+brand.id" style="margin-top:6px;" class="btn btn-sm btn-primary pull-right">View Info</a>
								  	</div>
						  		</div>
					  		</a>
					  	</li>
					</ul>
			  	</div>
			  	<br>
			  	<br>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{brands.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="brands.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="brands.last_page>brands.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#brands',
		data:{
	        brands:{!!json_encode($brands)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        page:"{{Request::input('page')}}",
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }  
		},
		methods: {
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getBrands();
		        console.log(this.brands);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getBrands();
		        console.log(this.brands);
		    },
		    getBrands:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true';
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.brands = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    }
	  	},
	});
</script>
@endsection