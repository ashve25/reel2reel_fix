@extends('Frontend::master')
@section('css')
<style type="text/css">
#new-tape > .col-lg-2,  #new-tape > .col-md-4,  #new-tape > .col-sm-6 #new-tape > .col-xs-12{
	padding-bottom: 30px;
}

#form-div{
	margin-top:30px; 
}

</style>
@endsection
@section('content')
<div class="container">
	<div id="new-tape" v-cloak>
		<h1 class="text-center">New Tape</h1>
		<div id="form-div">
			<div class="row">	
				<div class="col-md-12">
					<form id="form-tape" role="form" @submit="submitForm($event)">
						<input type="hidden" name="status" value="0">
						<input type="hidden" name="user_id" @if(Auth::user()) value="{{Auth::user()->id}}" @endif>
						<div class="box-body">
							<div class="row">
								<div class="form-group col-sm-4">
									<label>Title/Composition*</label>
									<input required="" class="form-control" type="text" name="title_composition"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Artist/Composer (First Name)*</label>
									<input required="" class="form-control" type="text" name="artist_composer_first_name" />
								</div>
								<div class="form-group col-sm-4">
									<label>Artist/Composer (Last Name)*</label>
									<input required="" class="form-control" type="text" name="artist_composer_last_name" />
								</div>
							</div>
							<div class="row">
								
								<div class="form-group col-sm-4">
									<label>Release Number*</label>
									<input required="" min="0" class="form-control" type="number" name="release_number"/>
								</div>
								<div class="form-group col-sm-4">
									<label>Release Date*</label>
									<input type="text" class="form-control" id="release_date" required name="release_date">
								</div>
								<div class="form-group col-sm-4">
									<label>Label*</label>
									<input required="" class="form-control" type="text" name="label"/>
								</div>
							</div>
							<label>Images</label>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label>Front Box*</label>
									<input required="" type="file" class="form-control" name="images[0]">
								</div>
								<div class="col-sm-3 form-group">
									<label>Rear Label</label>
									<input type="file" class="form-control" name="images[1]">
								</div>
								<div class="col-sm-3 form-group">
									<label>Back Box</label>
									<input type="file" class="form-control" name="images[2]">
								</div>
								<div class="col-sm-3 form-group">
									<label>Extra Image</label>
									<input type="file" class="form-control" name="images[3]">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Classical*</label>
									<select required="" class="form-control" name="classical_id">
										<option value="">Select Classical</option>
										<option v-for="classical in classicals" :value="classical.id">@{{classical.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Genre*</label>
									<select required="" class="form-control" name="genre_id">
										<option value="">Select Genre</option>
										<option v-for="genre in genres" :value="genre.id">@{{genre.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Speed*</label>
									<select required="" class="form-control" name="speed_id">
										<option value="">Select Speed</option>
										<option v-for="speed in speeds" :value="speed.id">@{{speed.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Reel Size*</label>
									<select required="" class="form-control" name="reel_size_id">
										<option value="">Select Reel Size</option>
										<option v-for="reel_size in reel_sizes" :value="reel_size.id">@{{reel_size.name}}</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Tracks</label>
									<select class="form-control" name="track_id">
										<option value="">Select Track</option>
										<option v-for="track in tracks" :value="track.id">@{{track.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Channels</label>
									<select class="form-control" name="channel_id">
										<option value="">Select Channels</option>
										<option v-for="channel in channels" :value="channel.id">@{{channel.name}}</option>
									</select>
								</div>
								<div class="form-group col-sm-3">
								<label>Noise Reduction</label>
								<select class="form-control" name="noise_reduction_id">
									<option value="">Select Noise Reduction</option>
									<option v-for="noise_reduction in noise_reductions" :value="noise_reduction.id">@{{noise_reduction.name}}</option>
								</select>
								</div>
								<div class="form-group col-sm-3">
									<label>Duplicated By</label>
									<select class="form-control" name="duplication_id">
										<option value="">Select Duplicated By</option>
										<option v-for="duplication in duplications" :value="duplication.id">@{{duplication.name}}</option>
									</select>
								</div>
							</div>
							<label>Credits</label>
							<div class="row">
								<div class="form-group col-sm-3">
									<label>Musician/Composer</label>
									<textarea class="form-control" name="credits[musician]" rows="3" style="margin-bottom: 10px"></textarea>
								</div>
								<div class="form-group col-sm-3">
									<label>Credit</label>
									<textarea class="form-control" name="credits[credit]" rows="3" style="margin-bottom: 10px"></textarea>
								</div>
								<div class="form-group col-sm-6">
									<label>Description</label>
									<textarea class="form-control" name="description" rows="3" style="margin-bottom: 10px"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6">
									<label>Track List (A)</label>
									<table class="table">
										<thead>
											<tr>
												<th>Title</th>
												<th>Time</th>
											</tr>
										</thead>
										<tbody v-for="(trackObject, index) in trackObjectsSideA">
											<tr>
												<td>
													<input type="text" :name="'tracks[a]['+index+'][title]'" v-model="trackObject.name" class="form-control" />
												</td>
												<td>
													<input type="text" class="form-control" :name="'tracks[a]['+index+'][time_length]'" v-model="trackObject.time">
												</td>
												<input type="hidden" :name="'tracks[a]['+index+'][side]'" value="a">
											</tr>
										</tbody>
									</table>
								</div>
								<div class="form-group col-sm-6">
									<label>Track List (B)</label>
									<table class="table">
										<thead>
											<tr>
												<th>Title</th>
												<th>Time</th>
											</tr>
										</thead>
										<tbody v-for="(trackObject, index) in trackObjectsSideB">
											<tr>
												<td>
													<input type="text" :name="'tracks[b]['+index+'][title]'" v-model="trackObject.name" class="form-control" /></td>
												<td>
													<input type="text" class="form-control" :name="'tracks[b]['+index+'][time_length]'" v-model="trackObject.time">
												</td>
												<input type="hidden" :name="'tracks[b]['+index+'][side]'" value="b">
											</tr>
										</tbody>
									</table>
									<button @click="cancelForm($event)" type="button" class="btn btn-default pull-right">Back</button>
									<button type="submit" class="btn btn-success pull-right" style="margin-right: 10px">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue({
		el:"#new-tape",
		data: {
			tracks: [],
			genres: [],
			classicals: [],
			speeds: [],
			channels: [],
			reel_sizes: [],
			noise_reductions: [],
			duplications: [],
			master_duplication: [],
			trackObjectsSideA: [],
			trackObjectsSideB: [],
			configTime: {
	          format: 'hh:mm',
	          useCurrent: false,
	        },
	        configDate: {
	          format: 'DD/MM/YYYY',
	          useCurrent: false,
	        }
		},
		mounted: function() {
			$("#release_date").datetimepicker({
				format:'DD/MM/YYYY',
				useCurrent: false,
			});
			this.getMasterData();
		},
		methods: {
			newTrackObject:function(){
				return {name: "", time: ""};
			},
			getMasterData: function() {
				var self = this;
				url = "{{url('admin/master-data/tapes')}}";
				$.ajax({
					type:"GET",
					url:url,
					success:function(response){
						console.log(response);
						//return false;
						var data = response.data;
						self.tracks = data.tracks;
						self.genres = data.genres;
						self.classicals = data.classicals;
						self.speeds = data.speeds;
						self.channels = data.channels;
						self.reel_sizes = data.reel_sizes;
						self.noise_reductions = data.noise_reductions;
						self.duplications = data.duplications;
						for(var i=0; i<10; i++) {
							self.trackObjectsSideA.push(self.newTrackObject());
							self.trackObjectsSideB.push(self.newTrackObject());
						}
						//fix sidebar
						self.$nextTick(function(){
						   $('.main-sidebar').css('min-height', $('#app').height());
						});
					},
					error:function(error){
						console.log(error.responseText);
					}
				});
			},
			submitForm: function(event) {
				event.preventDefault();
				var formData = new FormData($('form')[0]);
				var self = this;

				$('input[type=file]').each(function(index, field){
					if(field.files[0] && field.files[0].size > 2000000){
						bootbox.alert('File size cannot be greater than 2MB');
						return false;
					}
				});
				$.ajax({
					url:"{{url('admin/tapes')}}",
					type:"POST",
					data:formData,
					contentType:false,
					processData:false,
					success:function(response){
						console.log(response);
						if(response.meta.http_code == 200){
							bootbox.alert('Tape details added', function(){
								window.location = "{{url('tapes')}}";
							});
						}
					},
					error:function(error){
						console.log(error);
						if(error.responseText.meta.error_code == 400){
							$.each(error.responseText.meta.message, function(index, item){
								bootbox.alert(item);
							});	
						}
					}
				});
				
			},
			cancelForm: function(event) {
				window.location = "{{url('tapes')}}";
			}
		}
	});
</script>	
@endsection			