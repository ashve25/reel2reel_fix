@extends('Frontend::master')
@section('css')
<style type="text/css">
#inputExpanders > .col-lg-2,  #inputExpanders > .col-md-4,  #inputExpanders > .col-sm-6 #inputExpanders > .col-xs-12{
	padding-bottom: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="inputExpanders" v-cloak>
		<div class="col-md-12 ruler">
			<div class="col-md-6">
				<a class="btn btn-primary btn-sm" @click="showAdvanceFilter"><i class="fa fa-search-plus"></i> Show Advanced Search</a>
			</div>
			@if(Auth::check())
			<div class="col-md-6">
				<a class="btn btn-primary btn-sm pull-right" @click="newInputExpander"><i class="fa fa-plus"></i> Add New Input Expander</a>
			</div>
			@endif
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{inputExpanders.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="inputExpanders.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="inputExpanders.last_page>inputExpanders.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
			        </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-4 box" v-if="applyFilter">
				<div class="w3ls_dresses_grid_left_grid">
					<h3>BRANDS</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_dres-type">
							<ul>
								<li v-for="brand in brands" >
									<input type="checkbox" :value="brand.id" v-model="filter.brand_id" v-on:change="filterRecords">
									@{{brand.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div :class="inputExpandersListClass">
				<div v-if="inputExpanders.data.length>0" class="row" id="inputExpanders">
					<div v-for="inputExpander, index in inputExpanders.data" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				  		<div class="card" style="background: white" @click="editinputExpanderForm(inputExpander.id)">
		                	<img v-if="inputExpander.images && inputExpander.images[0] && inputExpander.images[0].thumbnail" class="img-responsive center-block" :src="inputExpander.images[0].thumbnail" @error="showPlaceholder($event)">
		                	<img v-else class="img-responsive center-block" :src="placeholder">
		                	<p class="text-center">@{{inputExpander.model | capitalize}}</p>
		            	</div>
				  	</div>
				  	<br>
				  	<br>
				</div>
				<div v-else class="row">
					<center><b>No Record Found</b></center>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{inputExpanders.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="inputExpanders.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="inputExpanders.last_page>inputExpanders.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#inputExpanders',
		data:{
	        inputExpanders:{!!json_encode($inputExpanders)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        inputExpanderRoute:"{{route('input-expanders.index')}}",
	        applyFilter:false,
	        inputExpandersListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        brands: {!!json_encode($brands)!!},
	        filter:{
	        	brand_id: []
	        }
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	   
		},
		methods: {
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editinputExpanderForm: function(id) {
		        window.location = "{{url('input-expanders')}}/"+id;
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.inputExpandersListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.inputExpandersListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getInputExpanders();
		        console.log(this.inputExpanders);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getInputExpanders();
		        console.log(this.inputExpanders);
		    },
		    getInputExpanders:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.inputExpanders = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getInputExpanders();
		    },
		    newInputExpander:function(){
		    	window.location = "{{url('input-expanders/create')}}";
		    }
	  	},
	});
</script>
@endsection