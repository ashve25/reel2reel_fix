@extends('Frontend::master')
@section('css')
<style type="text/css">
#new-catalog > .col-lg-2,  #new-catalog > .col-md-4,  #new-catalog > .col-sm-6 #new-catalog > .col-xs-12{
	padding-bottom: 30px;
}

#form-div{
	margin-top:30px; 
}

.cover-preview-div{
	width: 250px;
	margin-top: 25px;
}

.cover-preview-div img{
	border-radius: 6px; 
}

.other-preview-div{
	margin-top: 25px;
}

.other-preview-div img{
	border-radius: 6px; 
}


</style>
@endsection
@section('content')
<div class="container">
	<div id="new-catalog" v-cloak>
		<h1 class="text-center">New Catalog</h1>
		<div id="form-div">
			<div class="row">	
				<div class="col-md-12">
					<form role="form" @submit="submitForm($event)" method="post">
						<input type="hidden" name="status" value="0">
						<input type="hidden" name="user_id" @if(Auth::user()) value="{{Auth::user()->id}}" @endif>
						<div class="box-body">
							<div class="form-group">
								<label>Title*</label>
								<input type="text" name="title" required="" class="form-control">
							</div>
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="description"></textarea>
							</div>
							<div class="form-group row" v-if="!type">
								<div class="col-md-12">
								<label>Type*</label>
								<select name="type" class="form-control" required="">
									<option value="">Select Type</option>
									<option v-for="catalogType, index of catalogTypes" :value="catalogType.id">@{{catalogType.name}}</option>
								</select>
								</div>
							</div>
							<div class="form-group">
								<label>Cover Picture*</label>
								<a class="btn btn-sm btn-success" @click="clickForChangeCoverImage">
									<i class="fa fa-upload"></i>
									Upload Cover Picture
								</a>
								<input type='file' id="cover-picture" name="cover_picture" @change="changeCoverImage" class="form-control" style="display:none;" />
								<div class="cover-preview-div" v-if="coverPreview">
  									<img id="cover-preview" :src="coverSrc" alt="Cover Picture"/>

  									<center style="margin-top:20px;">
  										<a class="btn btn-danger btn-sm" @click="deleteCoverPreview(coverSrc)"><i class="fa fa-trash"></i> Delete</a>
  									</center>
  								</div>
							</div>
							<div class="form-group">
								<label>Other Pictures</label>
								<a class="btn btn-sm btn-success" @click="clickForChangeOtherImage">
									<i class="fa fa-upload"></i>
									Upload Other Picture
								</a>
								<input type='file' id="other-picture" @change="changeOtherImage" class="form-control" multiple="" style="display:none;" />
								<div class="other-preview-div row" v-if="otherPreview">
									<div class="col-md-12">
										<div class="col-md-2" v-for="otherSrc, key in otherImages">
		  									<img id="other-preview" :src="otherSrc" alt="Other Picture"/>
		  									<center style="margin-top:20px;">
		  										<a class="btn btn-danger btn-sm" @click="deleteOtherPreview(key)"><i class="fa fa-trash"></i> Delete</a>
		  									</center>
	  									</div>
  									</div>
  								</div>
							</div>
							<div class="form-group">
								<input type="submit" name="submit" value="Save" class="btn btn-sm btn-danger">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>	
@endsection
@section('js')
<script type="text/javascript">

	var newCatalogInstance = new Vue ({
		el: '#new-catalog',
		data:{
			image:'',
			otherImages:[],
			uploadedOtherImages:[],
			catalogTypes:{!!json_encode($catalogTypes)!!},
			type:"{{Request::input('type')}}", 
			coverPreview:false,
			coverSrc:'#',
			otherPreview:false
		},
		methods: {
			fileUpload:function(input){
				if (input.files && input.files[0]) {
				    var reader = new FileReader();

				    reader.onload = function(e) {
				    	newCatalogInstance.coverSrc = e.target.result;
				    	newCatalogInstance.coverPreview  = true;
				    }
				    reader.readAsDataURL(input.files[0]);
				}
			},
			changeCoverImage:function(event){
				console.log(event);
				this.fileUpload(event.target);
			},
			deleteCoverPreview:function(fileName){
				newCatalogInstance.coverSrc = '#';
				newCatalogInstance.coverPreview  = false;
				$("#cover-picture").val('');
			},
			changeOtherImage:function(event){
				this.otherFileUpload(event.target);
			},
			otherFileUpload:function(input){
				if (input.files && input.files[0]) {
				    $.each(input.files, function(index, item){
					    var reader = new FileReader();
					    reader.onload = function(e) {
					    	newCatalogInstance.uploadedOtherImages.push(item);
					    	newCatalogInstance.otherImages.push(e.target.result);
					    	newCatalogInstance.otherPreview  = true;
					    }
					    reader.readAsDataURL(item);
					});
				}
			},
			deleteOtherPreview:function(index){
				//console.log(index);
				this.otherImages.splice(index, 1);
				this.uploadedOtherImages.splice(index, 1);
				
				if(this.otherImages.length<1){
					this.otherPreview  = false;
				}
			},
			submitForm:function(event){
				event.preventDefault();
				var formData = new FormData($('form')[0]);
				$.each(this.uploadedOtherImages, function(index, item){
					formData.append('other_pictures['+index+']', item);
				});
				formData.append('type', this.type);
				var self = this;
				$.ajax({
					type:"POST",
					url:"{{url('admin/catalogs')}}",
					data:formData,
					contentType:false,
					processData:false,
					success:function(response){
						console.log(response);
						if(response.meta && response.meta.http_code == 200){
							bootbox.alert("Catalog Added", function(){
								window.location="{{url('catalogs')}}?type="+self.type;
							});
						}
					},
					error:function(error){
						console.log(error.responseText);
						if(error.responseText && error.responseText.meta.error_code == 400){
							$.each(error.responseText.meta.message, function(index, item){
								bootbox.alert(item);
							});	
						}
					}
				});
			},
			clickForChangeOtherImage:function(){
				$("#other-picture").click();
			},
			clickForChangeCoverImage:function(){
				$("#cover-picture").click();
			}
		}
	});
</script>
@endsection			