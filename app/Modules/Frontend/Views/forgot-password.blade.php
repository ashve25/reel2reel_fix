@extends('Frontend::master')
@section('css')
<style type="text/css">
</style>
@endsection
@section('content')
<center><img src="{{asset('images/logo.png')}}" style="width: 100px;"></center>
<div class="login">
	<div class="main-agileits">
		@if(session('success'))
	        <div class="alert alert-success alert-dismissible">
	          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	          <strong>Success!</strong> {{session('success')}}
	        </div>
	      @endif
	      @if(session('warning'))
	        <div class="alert alert-danger alert-dismissible">
	          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	          <strong>Warning!</strong>
	            {{session('warning')}}
	        </div>
	    @endif
		<div class="form-w3agile">
			<h3>Forgot Password</h3>
			<form action="{{url('forgot-password')}}" method="post">
				<div class="key">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					<input  type="text" name="email" required="" placeholder="Email" value="{{old('email')}}">
					<div class="clearfix"></div>
				</div>
				<input type="submit" value="Submit">
			</form>
		</div>
		<div class="forg">
			<a href="{{url('login')}}" class="forg-left">Login</a>
			<a href="{{url('register')}}" class="forg-right">Register</a>
		<div class="clearfix"></div>
		</div>
	</div>
</div>
@endsection
@section('js')
@endsection			