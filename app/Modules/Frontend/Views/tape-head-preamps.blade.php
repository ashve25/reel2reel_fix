@extends('Frontend::master')
@section('css')
<style type="text/css">
#tapeHeadPreamps > .col-lg-2,  #tapeHeadPreamps > .col-md-4,  #tapeHeadPreamps > .col-sm-6 #tapeHeadPreamps > .col-xs-12{
	padding-bottom: 30px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div id="tapeHeadPreamps" v-cloak>
		<div class="col-md-12 ruler">
			<div class="col-md-6">
				<a class="btn btn-primary btn-sm" @click="showAdvanceFilter"><i class="fa fa-search-plus"></i> Show Advanced Search</a>
			</div>
			@if(Auth::check())
			<div class="col-md-6">
				<a class="btn btn-primary btn-sm pull-right" @click="newTapHeadPreamp"><i class="fa fa-plus"></i> Add New Tape Head Preamps</a>
			</div>
			@endif
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{tapeHeadPreamps.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="tapeHeadPreamps.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="tapeHeadPreamps.last_page>tapeHeadPreamps.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
			        </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-4 box" v-if="applyFilter">
				<div class="w3ls_dresses_grid_left_grid">
					<h3>MANUFACTURERS</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_color ecommerce_size">
							<ul>
								<li v-for="manufacturer in manufacturers">
									<input type="checkbox" :value="manufacturer.id" v-model="filter.manufacturer_id" v-on:change="filterRecords">
									@{{manufacturer.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="w3ls_dresses_grid_left_grid">
					<h3>INPUTS</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_color ecommerce_size">
							<ul>
								<li v-for="input in inputList">
									<input type="checkbox" :value="input.id" v-model="filter.input_id" v-on:change="filterRecords">
									@{{input.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="w3ls_dresses_grid_left_grid">
					<h3>OUTPUTS</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_color ecommerce_size">
							<ul>
								<li v-for="output in outputList" :value="output.id">
									@{{output.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="w3ls_dresses_grid_left_grid">
					<h3>ELECTRONICS</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_color ecommerce_size">
							<ul>
								<li v-for="electronic in electronicsList">
									<input type="checkbox" :value="electronic.id" v-model="filter.electronic_id" v-on:change="filterRecords">
									@{{electronic.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div :class="tapeHeadPreampsListClass">
				<div v-if="tapeHeadPreamps.data.length>0" class="row" id="tapeHeadPreamps">
					<div v-for="tapeHeadPreamp, index in tapeHeadPreamps.data" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				  		<div class="card" style="background: white" @click="editTapeHeadPreampForm(tapeHeadPreamp.id)">
		                	<img v-if="tapeHeadPreamp.images && tapeHeadPreamp.images[0] && tapeHeadPreamp.images[0].thumbnail" class="img-responsive center-block" :src="tapeHeadPreamp.images[0].thumbnail" @error="showPlaceholder($event)">
		                	<img v-else class="img-responsive center-block" :src="placeholder">
		                	<p class="text-center">@{{tapeHeadPreamp.manufacturer | capitalize}} @{{tapeHeadPreamp.model | capitalize}}</p>
		            	</div>
				  	</div>
				  	<br>
				  	<br>
				</div>
				<div v-else class="row">
					<center><b>No Record Found</b></center>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-4 pull-left results-meta-left">
				<span>
				@{{tapeHeadPreamps.total}} Results
				</span>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<div class="pagination">
			          <a href="#" v-if="tapeHeadPreamps.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
			          <a href="#" v-if="tapeHeadPreamps.last_page>tapeHeadPreamps.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#tapeHeadPreamps',
		data:{
	        tapeHeadPreamps:{!!json_encode($tapeHeadPreamps)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}",
	        tapeHeadPreampRoute:"{{route('tape-head-preamps.index')}}",
	        applyFilter:false,
	        tapeHeadPreampsListClass:"col-md-12",
	        page:"{{Request::input('page')}}",
	        manufacturers: [],
			inputList: [],
			outputList: [],
			electronicsList: [],
			filter: {
				manufacturer_id: [],
				input_id: [],
				output_id: [],
				electronic_id: [],
			}
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
		   if(!this.page){
		   	this.page = 1;
		   }	 
		    this.getTapeHeadPreamps();
		    this.getManufacturers();
			this.getInputs();
			this.getOutputs();
			this.getElectronicsList();
		},
		methods: {
			getManufacturers: function(){
				var self = this;
				$.ajax({
					url: "{{url('/admin/data/manufacturers')}}",
					type: 'get',
					success: function(response){
						console.log(response.data);
						self.manufacturers = response.data;		
					},
					error: function(error){
						console.log(error);
					}
				});
			},
			getInputs: function(){
				var self = this;
				$.ajax({
					url: "{{url('/admin/data/inputs')}}",
					type: 'get',
					success: function(response){
						self.inputList = response.data;		
					},
					error: function(error){
						console.log(error);
					}
				});
			},
			getOutputs: function(){
				var self = this;
				$.ajax({
					url: "{{url('/admin/data/outputs')}}",
					type: 'get',
					success: function(response){
						self.outputList = response.data;		
					},
					error: function(error){
						console.log(error);
					}
				});
			},
			getElectronicsList: function(){
				var self = this;
				$.ajax({
					url: "{{url('/admin/data/electronics')}}",
					type: 'get',
					success: function(response){
						self.electronicsList = response.data;		
					},
					error: function(error){
						console.log(error);
					}
				});
			},
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeHeadPreampForm: function(id) {
		        window.location = "{{url('tape-head-preamps')}}/"+id;
		    },
		    showAdvanceFilter:function(){
		    	if(this.applyFilter === true){
		    		this.applyFilter = false;
		    	}else{
		    		this.applyFilter = true;
		    	}
		    	this.tapeHeadPreampsListClass = 'col-md-12';
		    	if(this.applyFilter === true){
		    		this.tapeHeadPreampsListClass = 'col-md-8';
		    	}
		    },
		    getNextPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)+parseInt(1);
		        this.getTapeHeadPreamps();
		        console.log(this.tapeHeadPreamps);
		    },
		    getPreviousPage:function(event, page){
		        event.preventDefault();
		        this.page = parseInt(page)-parseInt(1);
		        this.getTapeHeadPreamps();
		        console.log(this.tapeHeadPreamps);
		    },
		    getTapeHeadPreamps:function(){
		    	var url = "{{Request::url()}}?page="+this.page+'&ajax=true&filters='+JSON.stringify(this.filter);
		    	var self = this;
		    	$.ajax({
		    		type:"GET",
		    		url:url,
		    		success:function(response){
		    			self.tapeHeadPreamps = response;
		    			console.log(response);
		    		},
		    		error:function(error){
		    			//console.log(error.responseText);
		    		},
		    	});
		    },
		    filterRecords: function(){
		    	var self = this;
		    	console.log(this.filter);
		    	this.getTapeHeadPreamps();
		    },
		    newTapHeadPreamp:function(){
		    	window.location = "{{url('tape-head-preamps/create')}}";;
		    }
	  	},
	});
</script>
@endsection