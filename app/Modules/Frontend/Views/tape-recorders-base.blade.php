<div class="row">	
	<div class="col-md-12 ruler">
		<div class="col-md-6">
			<a class="btn btn-primary btn-sm" @click="showAdvanceFilter"><i class="fa fa-search-plus"></i> Show Advanced Search</a>
		</div>
		@if(Auth::check())
		<div class="col-md-6">
			<a class="btn btn-primary btn-sm pull-right" @click="addNewTapeRecorder">
				<i class="fa fa-plus"></i> Add New Tape Recorder
			</a>
		</div>
		@endif
	</div>
</div>
<div class="row">	
	<div class="col-md-12">
		<div class="col-md-4 pull-left results-meta-left">
			<span>
			@{{tapeRecorders.total}} Results
			</span>
		</div>
		<div class="col-md-8">
			<div class="pull-right">
				<div class="pagination">
		          <a href="#" v-if="tapeRecorders.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
		          <a href="#" v-if="tapeRecorders.last_page>tapeRecorders.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i></a>
		        </div>
			</div>
		</div>
	</div>
</div>
<div class="row">	
	<div class="col-md-12">
		<div class="col-md-4 box" v-if="applyFilter">
			<div class="w3ls_dresses_grid_left_grid">
				<h3>MANUFACTURERS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_dres-type">
						<ul>
							<li v-for="manufacturer in manufacturers">
								<input type="checkbox" :value="manufacturer.id" v-model="filter.manufacturer_id" v-on:change="filterRecords">
								@{{manufacturer.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>BRANDS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_dres-type">
						<ul>
							<li v-for="brand in brands" >
								<input type="checkbox" :value="brand.id" v-model="filter.brand_id" v-on:change="filterRecords">
								@{{brand.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>HEAD CONFIGURATION</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color">
						<ul>
							<li v-for="head_configuration in head_configurations">
								<input type="checkbox" :value="head_compositions.id" v-model="filter.head_configuration_id" v-on:change="filterRecords">
								@{{head_configuration.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>NUMBER OF HEADS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="number_of_head in number_of_heads">
								<input type="checkbox" :value="number_of_head.id" v-model="filter.number_of_head_id" v-on:change="filterRecords">
								@{{number_of_head.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>SPEEDS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="speed in speeds">
								<input type="checkbox" :value="speed.id" v-model="filter.speed_id" v-on:change="filterRecords">
								@{{speed.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>TRACKS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="track in tracks">
								<input type="checkbox" :value="track.id" v-model="filter.track_id" v-on:change="filterRecords">
								@{{track.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>HEAD COMPOSITION</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="head_composition in head_compositions">
								<input type="checkbox" :value="head_composition.id" v-model="filter.head_composition_id" v-on:change="filterRecords">
								@{{head_composition.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>EQUALIZATION</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="equalization in equalizations">
								<input type="checkbox" :value="equalization.id" v-model="filter.equalization_id" v-on:change="filterRecords">
								@{{equalization.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>VOLTAGES</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="voltage in voltages">
								<input type="checkbox" :value="voltage.id" v-model="filter.voltage_id" v-on:change="filterRecords">
								@{{voltage.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>AUTO REVERSE</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="auto_reverse in auto_reverses">
								<input type="checkbox" :value="auto_reverse.id" v-model="filter.auto_reverse_id" v-on:change="filterRecords">
								@{{auto_reverse.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>APPLICATIONS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="application in applications">
								<input type="checkbox" :value="application.id" v-model="filter.application_id" v-on:change="filterRecords">
								@{{application.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>MAX REEL SIZES</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="max_reel_size in max_reel_sizes">
								<input type="checkbox" :value="max_reel_size.id" v-model="filter.max_reel_size_id" v-on:change="filterRecords">
								@{{max_reel_size.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>NOISE REDUCTION</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="noise_reduction in noise_reductions">
								<input type="checkbox" :value="noise_reduction.id" v-model="filter.noise_reduction_id" v-on:change="filterRecords">
								@{{noise_reduction.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>MOTORS</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li v-for="motor in motors">
								<input type="checkbox" :value="motor.id" v-model="filter.motor_id" v-on:change="filterRecords">
								@{{motor.name}}
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div :class="tapeRecordersListClass">
			<div v-if="tapeRecorders.data.length" class="row" id="tape-recorders">
				<div v-for="tape_recorder, index in tapeRecorders.data" class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
			  		<div class="card" style="background: white" @click="editTapeRecorderForm(tape_recorder.id)">
	                	<img v-if="tape_recorder.images && tape_recorder.images[0] && tape_recorder.images[0].thumbnail" class="img-responsive center-block" :src="tape_recorder.images[0].thumbnail" @error="showPlaceholder($event)">
	                	<img v-else class="img-responsive center-block" :src="placeholder">
	                	<p class="text-center">@{{tape_recorder.manufacturer_name | capitalize}} @{{tape_recorder.model | capitalize}}</p>
	                	<p class="text-center">@{{tape_recorder.brand_name | capitalize}}</p>
	            	</div>
			  	</div>
			  	<br>
			  	<br>
			</div>
			<div v-else class="row">
				<center><b>No Record Found</b></center>
			</div>
		</div>
	</div>
</div>
<div class="row">	
	<div class="col-md-12">
		<div class="col-md-4 pull-left results-meta-left">
			<span>
			@{{tapeRecorders.total}} Results
			</span>
		</div>
		<div class="col-md-8">
			<div class="pull-right">
				<div class="pagination">
		          <a href="#" v-if="tapeRecorders.current_page>1" class="btn btn-md btn-primary" @click="getPreviousPage($event, page)"> <i class="fa fa-chevron-left"></i> Prev</a>
		          <a href="#" v-if="tapeRecorders.last_page>tapeRecorders.current_page" class="btn btn-md btn-primary" @click="getNextPage($event, page)">Next <i class="fa fa-chevron-right"></i> </a>
		        </div>
			</div>
		</div>
	</div>
</div>