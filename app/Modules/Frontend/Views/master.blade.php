<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reel-Reel</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/materia/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"> -->

     <link href="{{asset('theme/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('theme/css/style.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('theme/css/font-awesome.min.css')}}" type="text/css" media="all" /> 

    <!-- <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}"> -->
    <!-- <link rel="stylesheet" href="{{asset('css/skin-red.min.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">    
    
    <!-- font -->
    <!-- <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'> -->


    <link href="https://fonts.googleapis.com/css?family=Roboto:300, 400" rel="stylesheet">



    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

    <link rel="stylesheet" href="{{asset('theme/css/flexslider.css')}}" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" href="{{asset('fancybox/css/jquery.fancybox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fancybox/css/jquery.fancybox-thumbs.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <style type="text/css">
        .home-section{
            background-image: url(images/bg.jpg);
            background-position: center;
            background-repeat: no-repeat;
            padding: 30px;
        }
        [v-cloak] > * { display:none; }

        .results-meta-left{
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .results-meta-left span{
            text-transform: uppercase;
            padding: 6px 10px;
            font-size: 12px;
            border: 1px solid #ddd;
        }

        .main-footer {
          margin-left: 0px !important;
          text-align: center;
        }

        .font-white {
            color: white;
        }

        .ruler {
            border-bottom: 2px solid #ededed;
            padding-bottom: 10px;
        }

        .box{
            border: 1px solid #ededed;
            padding-bottom: 20px;
        }

        .card{
            padding: 5px;
            color: #383e4d;
            text-transform: uppercase;
            font-weight: bold;
        }

        hr {
            border-top: 1px solid #bdbdbd;
        }

        /** {
          border: 1px solid #f00;
        }*/

        .row {
            margin-right: 0px;
        }

        /*Media Query For Dynamic Data In Row*/
        @media (min-width: 1200px) {
            .row > .col-lg-2:nth-child(6n+1) {
                clear: left;
            }
        }

        @media (min-width: 992px) and (max-width:1199px) {
            .row > .col-md-3:nth-child(4n+1) {
                clear: left;
            }
        }

        @media (min-width: 768px) and (max-width:991px) {
            .row > .col-sm-4:nth-child(3n+1) {
                clear: left;
            }
        }

        @media (max-width: 768px){
            .row > .col-sm-6:nth-child(2n+1) {
                clear: left;
            }
        }

        #fancybox-loading, .fancybox-close, .fancybox-prev span, .fancybox-next span {
            background-image: url("{{url('images/fancybox_sprite.png')}}");
        }

        #div-loading{
            margin: 0px; 
            padding: 0px; 
            position: fixed; 
            right: 0px; 
            top: 0px; 
            width: 100%; 
            height: 100%; 
            background-color: rgb(102, 102, 102); 
            z-index: 30001; 
            opacity: 0.8;
            display:none;
        }

        #div-loading img{
            position: absolute; 
            top: 45%; 
            left: 45%;
            height: 100px;
            width: 100px;
        }

        .sub-header {
            margin-bottom: 25px;
            border-bottom: 1px solid #ededed;
            padding-bottom: 10px
        }

        .card>.text-center{
          white-space: nowrap;
          width: 145px;
          overflow: hidden;
          text-overflow: ellipsis;
        }
    </style>
    @yield('css')
  </head>
  <body>
    @include('Frontend::navbar')
    <div class="content">
        <div class="container sub-header">
            <div class="row">
                <div class="col-md-4">
                  <img class="pull-left" src="{{asset('images/logo.png')}}" style="width: 30%;">
                </div>
                <h3 class="col-md-4 text-center" style="padding-top: 10px">{{$title or ''}}</h3>
                <div class="col-md-4">
                    <div class="pull-right">
                    @if(Auth::user())
                    <a href="{{url('login')}}" class="btn btn-sm btn-primary">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Profile
                    </a>
                    <a href="{{url('logout')}}" id="logout" class="btn btn-sm btn-primary">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        Logout
                    </a>
                    @else
                    <a href="{{url('login')}}" class="btn btn-sm btn-primary">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        Login
                    </a>
                    <a href="{{url('register')}}" class="btn btn-sm btn-primary">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        Register
                    </a>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    @yield('content')
    </div>

    @include('Frontend::register-modal')
    <div id="div-loading">
        <img src="{{asset('images/loader-3.gif')}}">
    </div>

    <!-- Main Footer -->
    <footer class="main-footer text-center" style="padding: 20px; background: #f8f8f8;">
      <!-- Default to the left -->
      <strong>Copyright &copy; 2018 <a href="#">Reel-Reel</a>.</strong> All rights reserved.
    </footer>
    <!-- built files will be auto injected -->
    <script src="{{asset('theme/js/jquery-1.11.1.min.js')}}"></script>
    <!-- <script src="{{asset('theme/js/jquery-1.11.1.min.js')}}"></script> -->
    <script src="{{asset('theme/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <!-- <script src="{{asset('js/adminlte.min.js')}}"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script> -->
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    
    
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

    <!-- <script type="text/javascript" src="{{asset('js/frontend.js')}}"></script> -->
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.js"></script>
    <!--flex slider-->
    <script defer src="{{asset('theme/js/jquery.flexslider.js')}}"></script>
    <script>
        // Can also be used with $(document).ready()
        $(window).load(function() {
          $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
          });
        });
    </script>
    <script src="{{asset('theme/js/imagezoom.js')}}"></script>
    <script type="text/javascript" src="{{asset('fancybox/js/jquery.fancybox.js')}}"></script>
    <script type="text/javascript" src="{{asset('fancybox/js/jquery.fancybox-thumbs.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    <!--flex slider-->
    @yield('js')
    <script>
        $(document).ready(function(){
          $(".dropdown").hover(            
            function() {
              $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
              $(this).toggleClass('open');        
            },
            function() {
              $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
              $(this).toggleClass('open');       
            }
          );
        });

        $(document).ajaxStart(function () {
            //ajax request went so show the loading image
            $("#div-loading").show();
         }).ajaxStop(function () {
            //got response so hide the loading image
            $("#div-loading").hide();
        });


        $("#logout").click(function(event){
            event.preventDefault();
            $.ajax({
                type:'post',
                url:"{{route('logout')}}",
                success:function(response){
                    console.log(response);
                    if(response == 200){
                        window.location = "{{url('login')}}";
                    }
                },
                error:function(error){
                    console.log(error.responseText);
                }
            });
        });
    </script>
  </body>
</html>
