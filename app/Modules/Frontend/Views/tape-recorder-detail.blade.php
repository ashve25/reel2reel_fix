@extends('Frontend::master')
@section('css')
<!-- #303542 -->
<style type="text/css">
	.single-top-right{
		background:#1f232d; 
		padding: 30px;
	}
	.single-top-left{
		width: 60%;
	}
	.flex-viewport{
		min-height:500px;
		height: auto;
	}
	.row{
		margin-top:20px;
	}
	
	.full-row{
		width: 100%;
		height: 26px;
	}
	.half-row-left{
		width: 50%;
		float: left;
	}
	.half-row-right{
		width: 50%;
		float: right;
	}
	.custom-button{
		color: #fff;
		border: 1px solid #fff;
		width: 97%;
		text-align: left;
		padding-left:5px; 
		line-height: 22px;
	}
	.item_name{
		color: #fff !important;
	}
	#tech-details{
		background: #1f232d;
		color: #fff;
	}
	#tech-details .full-row{
		margin: 10px;
	}

	.li-left{
		float: left;
		width: 50%;
	}

	.li-right{
		float: right;
		width: 50%;
	}

	#details .full-row ul{
		list-style-type:none;
	}

	.header-bottom .full-row{
		height: 40px;
	}
	#dtl .custom-button{
		text-align: center ;
	}
	#add-details p{
		color: #000;
	}
	#add-details .col-md-12{
		border-bottom: 1px solid #eee;
		margin-top: 3px;
    	margin-bottom: 20px;
    	padding: 0px;
	}

	#add-details .col-md-6, .col-md-4{
		margin-bottom: 20px;
	}
</style>
@endsection
@section('content')
<div class="products" id="products" v-cloak>	 
	<div class="container">  
		<div class="single-page">
			<div class="single-page-row" id="detail-21">
				<div class="col-md-8 single-top-left">	
					<div class="flexslider">
						<ul class="slides">
							<li :data-thumb="image.image" v-for="image, index of tapeRecorder.images">
								<div class="thumb-image"> 
									<img :src="image.image" data-imagezoom="true" class="img-responsive" alt=""> 
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 single-top-right">
					<div class="header-top">
						<div class="full-row">
							<div class="half-row-left">
								<div href="{{url('tape-recorders')}}" class="custom-button">
									<a href="{{url('tape-recorders')}}" style="color:#fff;"><i class="fa fa-arrow-left"></i> Tape Recorders</a>
								</div>
							</div>
						</div>
						<hr/>
						<h3 class="item_name"> @{{tapeRecorder.model}}</h3>
						<hr/>
						<div class="full-row">
							<div class="half-row-left" id="dtl" @click="detailShow">
								<div class="custom-button">Details <i class="fa fa-arrow-down"></i></div>
							</div>
						</div>
					</div>
					<hr/>
					<div class="header-bottom">
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button" @click="addAsFavorite($event)" :style="addedInFavourite ? 'border-color:#ccc; color:#ccc;' : ''">
									<span v-if="addedInFavourite">Favorite <i class="fa fa-star"></i></span>
									<span v-else>Favorite <i class="fa fa-star-o"></i></span>
								</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">User Rating</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Avg Price :</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Views : </div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button"># Owners : 2</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Ask Q's to Owner</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button"># Reviews : 0</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">Rate & Review this R-R</div>
							</div>
						</div>
						<div class="full-row">
							<div class="half-row-left">
								<div class="custom-button">Email me if IC 2000 comes up for sale</div>
							</div>
							<div class="half-row-right">
								<div class="custom-button">I own this R-R</div>
							</div>
						</div>
					</div>		
				</div>
			   <div class="clearfix"> </div>  
			</div>
		</div> 
		<!-- collapse-tabs -->
		<div class="collpse tabs">
			<h3 class="w3ls-title">Technical Details</h3>
			<hr/>
			<div class="row" id="tech-details">
				<div class="full-row">
					<div class="half-row-left">
						<ul>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Manufacturer:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.manufacturer_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Model:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.model}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Country Of Manufacture:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.country_of_manufacturer}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Original Price:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.original_price}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Tracks:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.track_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Speeds:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.speed_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Channels:
									</div> 
									<div class="li-right">
									<!-- @{{tapeRecorder.channel_name}} -->
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Number Of Heads :
									</div> 
									<div class="li-right">
										@{{tapeRecorder.number_of_head_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Head Composition:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.head_composition_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Head Configuration:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.head_configuration_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> # Motors:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.motor_name}}
									</div>
								</div>
							</li>
						</ul> 
					</div>
					<div class="half-row-right">
						<ul>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Release Dates:
									</div> 
									<div class="li-right" v-if="tapeRecorder.release_from_date">
										@{{tapeRecorder.release_to_date}}  - @{{tapeRecorder.release_from_date}}
									</div>
									<div class="li-right" v-else>
										@{{tapeRecorder.release_to_date}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Auto Reverse?:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.auto_reverse_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Voltage(S):
									</div> 
									<div class="li-right">
										@{{tapeRecorder.voltage_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Frequency Response:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.frequency_response}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Wow And Flutter:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.wow_flutter}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Signal-To-Noise Ratio:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.signal_noise_ratio}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Application:
									</div> 
									<div class="li-right">
										@{{tapeRecorder.application_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Max Reel Size("):
									</div> 
									<div class="li-right">
										@{{tapeRecorder.max_reel_size_name}}
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Sound Quality Rating:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
							<li>
								<div class="col-md-12">
									<div class="li-left">
										<i class="fa fa-chevron-right" aria-hidden="true"></i> Long-Term Reliability Rating:
									</div> 
									<div class="li-right">
										
									</div>
								</div>
							</li>
						</ul> 
					</div>
				</div>
			</div> 
			
		</div>
		<!-- //collapse -->
		<!-- collapse-tabs -->
		<div class="collpse tabs">
			<h3 class="w3ls-title">Aditional Details</h3>
			<hr/>
			<div class="row" id="add-details">
				<div class="col-md-12">
					<div class="col-md-6">
						<p><b>Description</b></p>
						<p>@{{tapeRecorder.description}}</p>
					</div>
					<div class="col-md-6">
						<p><b>Additional Info</b></p>
						<p></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<p><b>Modifications</b></p>
					</div>
					<div class="col-md-4">
						<p><b>Service Shop</b></p>
					</div>
					<div class="col-md-4">
						<p><b>Accessories</b></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<p><b>Comments</b></p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<p>Owners: <a href="#">admin</a></p>
					</div>
				</div>	
			</div> 
			
		</div>
		<!-- //collapse --> 
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	tapeRecorderDetailInstance = new Vue({
		el:"#products",
		data:{
			tapeRecorder:{!!json_encode($tapeRecorder)!!},
			user:{!!json_encode(Auth::user())!!},
			addedInFavourite:"{{$addedInFavourite}}",
		},
		mounted(){
		},
		methods:{
			detailShow:function () {
				$('html,body').animate({
    				scrollTop: $("#tech-details").offset().top
    			}, 'slow');
			},
			addAsFavorite:function(event){
				if(!this.user){
					bootbox.alert('Please Login with your account first for add in favorite list.');
					return false;
				}
				this.addedInFavourite = !this.addedInFavourite;
				if(this.addedInFavourite === true){
					$(event.target).css({"border-color":"#ccc", "color":"#ccc"});
					var url = "{{url('tape-recorders/add-as-favourites')}}";
					var msg = 'Added into favourite list';

				}else{
					$(event.target).css({"border-color":"#fff", "color":"#fff"});
					var url = "{{url('tape-recorders/delete-from-favourites')}}";
					var msg = 'Delete from favourite list';
				}

				$.ajax({
					type:"post",
					url:url,
					data:{tape_recorder_id:this.tapeRecorder.id},
					success:function(response){
						if(response == 200){
							bootbox.alert(msg);
						}
					},
					error:function(error){
						console.log(error.responseText);
					}
				})
			}
		}
	});
</script>
@endsection