<div id="register" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Registration</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('register')}}" method="post" class="form-horizontal">
          <div class="form-group">
            <div class="col-md-12">
              <label>Name*</label>
              <input type="text" name="name" class="form-control" placeholder="Enter Name" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Email*</label>
              <input type="email" name="name" class="form-control" placeholder="Enter Email" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Password*</label>
              <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Confirm Password*</label>
              <input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Country*</label>
              <input type="text" name="country" class="form-control" placeholder="Enter Country" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>City</label>
              <input type="text" name="city" class="form-control" placeholder="Enter City">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Profile Picture</label>
              <input type="file" name="name" class="form-control" placeholder="Enter Name">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Terms and Conditions*</label>
              <p><input type="checkbox" name="terms_and_condtions" checked>
              I agree to the terms and conditions posted on this website</p>
              <a href="#">Click here to read terms and conditions</a>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>