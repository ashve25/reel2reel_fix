@extends('Frontend::master')
@section('css')
<style type="text/css">
	

	.testimonials {
	    background: white;
	    padding: 30px;
	}
	#home > h1 {
		margin-top: 20px;
		margin-bottom: 30px
	}

	#tapes, #tape-recorders {
		margin-top: 20px;
		padding-top: 50px;
		padding-bottom: 50px;
	}
	
	#tape-recorders h1 {
		margin-bottom: 40px;
	}

	#tapes h1 {
		margin-bottom: 40px;
	}

	#tapes {
		background: url("{{asset('img/symphony.png')}}") repeat;
	}

</style>
@endsection
@section('content')
<div id="home">
	<div class="home-section" style="color: white;">
		<p class="lead text-center" style="font-size: 18px; font-weight: 700; color: #383e4d">THE DIRECTORY OF<br/><span style="color: #AB4817">EVERYTHING REEL TO REEL </span></p>
	  	<h1 class="display-4 text-center font-white" style="color: #383e4d">Past, Present &amp; Future</h1>
	  	<hr style="margin-left: 45%; margin-right: 45%; border-top: 2px solid #ad4700">
	  	<p class="lead text-center font-white"> WE INVITE MEMBERS TO MAKE THIS THE MOST COMPLETE SUMMARY OF ALL<br/> THAT HAS HAPPENED IN OPEN REEL TO REEL SINCE 1950</p>

	  	<p class="lead text-center">
		    <a class="btn btn-primary btn-lg" href="#" role="button">SHOW US YOUR STUFF <i class="fa fa-arrow-right"></i></a>
		</p>
	</div>
	<div id="tape-recorders" class="container-fluid">
		<h1 class="text-center">Tape Recorders</h1>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<a href="{{route('tape-recorders.index')}}">
						<span style="text-transform: uppercase; color: #383e4d;">View All Tape Recorders</span>
					</a>
				</div>
		    </div>
	    </div>
	    <hr>
	    <br>
	    <div class="row">
			<div class="col-md-12 tape-recorder responsive">
				<div v-for="tape_recorder, index in tape_recorders" class="col-md-2">
			  		<div class="card" @click="editTapeRecorderForm(tape_recorder.id)">
		            	<img v-if="tape_recorder.images && tape_recorder.images[0] && tape_recorder.images[0].thumbnail" class="img-responsive center-block" :src="tape_recorder.images[0].thumbnail" @error="showPlaceholder($event)">
		            	<img v-else class="img-responsive center-block" :src="placeholder">
		            	<p class="text-center">@{{tape_recorder.model | capitalize}}</p>
		        	</div>
			  	</div>
			</div>
		</div>
	</div>
	<div id="tapes" class="container-fluid">
		<h1 class="display-5 text-center">Pre-Recorded Tapes</h1>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<a href="{{route('tapes.index')}}">
						<span style="text-transform: uppercase; color: #383e4d;">View All Pre-Recorded Tapes</span>
					</a>
				</div>
		    </div>
	    </div>
	    <hr>
	    <br>
	    <div class="row">
			<div class="col-md-12 tape responsive">
				<div class="col-md-2" v-for="tape, index in tapes">
		            <div class="card" @click="editTapeForm(tape.tape_id)" >
		                <img v-if="tape.thumbnail" class="img-responsive center-block" :src="tape.thumbnail" @error="showPlaceholder($event)">
		            	<img v-else class="img-responsive center-block" :src="placeholder">
		                <p class="text-center">@{{tape.title_composition | capitalize}}</p>
		            </div>    
		        </div>
			</div>
		</div>
	</div>
	<div class="newsletter">
		<div class="container text-center">
			<div class="row">
				<h3>Subscribe to <img src="{{asset('img/Logo-R-R-in-White.png')}}"></h3>
				<p><i class="fa fa-envelope-o" aria-hidden="true"></i>Newsletter</p>
				<div class="col-md-4 col-md-offset-4 w3agile_newsletter_right">
					<form action="#" method="post" id="newsletter">
						<input type="email" name="Email" placeholder="Get interesting R-R stuff straight to your inbox!" required="">
						<button type="submit" class="btn btn-primary subscribe-button btn-red">SUBSCRIBE <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row testimonials" style="margin: 20px;">
		<div class="col-md-4">
		   <div class="test-inner test-inner-blue">
		      <h3>Experts/Editors/Enthusiasts Wanted!</h3>
		      <hr style="border-top: 1px solid #fff;">
		      
			  <p>If you are an industry insider or have reason to believe that you have a wealth of knowledge about Open Reel and would like to participate at a higher level, drop us a line.</p>
			  <div class="text-center">
			      <a href="#" class="btn btn-primary btn-red subscribe-button" style="text-transform: none;"><i class="fa fa-edit"></i> Contact Us</a>
			  </div>
		   </div>
		</div>
		<div class="col-md-4">
		   <div class="test-inner test-inner-red">
		      <h3><i class="fa fa-thumbs-up"></i> Member Benefits</h3>
		      <hr style="border-top: 1px solid #fff;">
			    <p>
			      	<ul >
			      		<li>Read Member Reviews on Tapes and Machines</li>
						<li>Contribute your own Reviews</li>
						<li>Share questions with other Members</li>
						<li>Participate in the Forum</li>
						<li>Receive Notifications when Tapes or R2R Machines you are looking for become Available for Sale</li>
			      	</ul>
			    </p>
			    <div class="text-center">
		      		<a href="{{url('register')}}" class="btn btn-primary"><i class="fa fa-user-plus"></i> Register</a>
		      	</div>
		   </div>
		</div>
		<div class="col-md-4">
		   <div class="test-inner test-inner-blue">
		      <h3>Member Forum</h3>
		      <hr style="border-top: 1px solid #fff;">
		      <p>
		      	<ul >
		      		<li>Look here to find out the latest news, updates and announcements</li>
					<li>Have a suggestion? Want to make a comment? Registered Members Post away</li>
					<li>A great platform for users to come together, communicate and share</li>
		      	</ul>
		      </p>
		      <div class="text-center">
			      <a href="#" class="btn btn-primary btn-red subscribe-button" style="text-transform: none;"><i class="fa fa-comments"></i> Join the Discussion!</a>
			  </div>
		   </div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	new Vue ({
		el: '#home',
		data:{
	        tape_recorders:{!!json_encode($tapeRecorders)!!},
	        tapes:{!!json_encode($tapes)!!},
	        placeholder:"{{ asset('images/placeholder.jpg') }}"
		},
		filters: {
		  capitalize: function (value) {
		    if (!value) return ''
		    value = value.toString()
		    return value.charAt(0).toUpperCase() + value.slice(1)
		  }
		},
		mounted: function() {
			$('.tape-recorder, .tape').slick({
				autoplay: true,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 5,
				arrows: false,
				responsive: [
			    {
			    	breakpoint: 1024,
			      	settings: {
				        dots: false,
						slidesToShow: 3,
						slidesToScroll: 3,
				    }
			    },
			    {
			    	breakpoint: 600,
			      	settings: {
				        dots: false,
						slidesToShow: 2,
						slidesToScroll: 2,
				    }
			    },
			    {
			    	breakpoint: 480,
			      	settings: {
				        dots: false,
						slidesToShow: 1,
						slidesToScroll: 1,
				    }
			    },
			    ]
		  	});	    
		},
		methods: {
		    showPlaceholder: function(event){
		        var target = $(event.target);
		        target.attr('src', this.placeholder);
		    },
		    editTapeRecorderForm: function(id) {
		        this.$router.push('/tape-recorders/'+id+'/edit');
		    },
		    editTapeForm: function(id) {
		        this.$router.push('/tapes/'+id+'/edit');
		    }
	  	},
	});
</script>
@endsection