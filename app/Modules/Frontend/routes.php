<?php 

Route::group(['namespace' => 'App\Modules\Frontend\Controllers'], function(){

	Route::get('login', 'AuthController@login');
	Route::post('login', 'AuthController@checkLogin');
	Route::get('register', 'AuthController@register');
	Route::post('register', 'AuthController@saveRegistration');
	Route::get('users/{id}/verification/{key}', 'AuthController@verifyEmail');
	Route::get('forgot-password', 'AuthController@forgotPassword');
	Route::post('forgot-password', 'AuthController@forgotPasswordRequest');
	Route::get('users/{id}/reset-pass/{key}', 'AuthController@resetPasswordForm');
	Route::post('users/{id}/reset-pass/{key}', 'AuthController@resetPassword');


	//After Login 
	Route::group(['middleware' => 'auth.user'], function(){
		Route::post('logout', 'AuthController@logout')->name('logout');
		Route::get('profile', 'AuthController@profile')->name('profile');
		Route::post('profile', 'AuthController@updateProfile');
		//Route::get('change-password', 'AuthController@changePassword')->name('change-password');

		Route::post('tapes/add-as-favourites', 'TapeController@addTapeAsFavourite');
		Route::post('tape-recorders/add-as-favourites', 'TapeRecorderController@addTapeRecorderAsFavourite');
		Route::post('tape-head-preamps/add-as-favourites', 'TapeHeadPreampController@addTapeHeadPreampAsFavourite');
		Route::post('input-expanders/add-as-favourites', 'InputExpanderController@addInputExpanderAsFavourite');


		Route::post('tapes/delete-from-favourites', 'TapeController@deleteTapeFromFavourite');
		Route::post('tape-recorders/delete-from-favourites', 'TapeRecorderController@deleteTapeRecorderFromFavourite');
		Route::post('tape-head-preamps/delete-from-favourites', 'TapeHeadPreampController@deleteTapeHeadPreampFromFavourite');
		Route::post('input-expanders/delete-from-favourites', 'InputExpanderController@deleteInputExpanderFromFavourite');


		Route::get('tapes/create', 'TapeController@newTape');

		Route::get('catalogs/create', 'CatalogController@newCatalog');
		Route::post('catalogs/upload-images', 'CatalogController@uploadImages');
		Route::post('catalogs', 'CatalogController@saveCatalog');

		Route::get('tape-recorders/create', 'TapeRecorderController@newTapeRecorder');
		
		Route::get('tape-head-preamps/create', 'TapeHeadPreampController@newTapeHeadPreamp');

		Route::get('input-expanders/create', 'InputExpanderController@newInputExpander');
	});

	Route::get('home', 'HomeController@homePage');
	Route::get('manufacturers', 'ManufacturerController@getManufacturers');
	Route::get('manufacturers/{id}', 'ManufacturerController@getManufacturer');

	Route::get('tape-recorders', 'TapeRecorderController@getTapeRecorders')->name('tape-recorders.index');
	Route::get('tape-recorders/{id}', 'TapeRecorderController@getTapeRecorderDetails');
	
	Route::get('tapes', 'TapeController@getTapes')->name('tapes.index');
	Route::get('tapes/{id}', 'TapeController@getTape');

	Route::get('catalogs','CatalogController@getCatalogs')->name('catalogs.index');
	Route::get('catalogs/{id}/images', 'CatalogController@getCatalogsOtherImages');

	Route::get('tape-head-preamps', 'TapeHeadPreampController@getTapeHeadPreamps')->name('tape-head-preamps.index');
	Route::get('tape-head-preamps/{id}', 'TapeHeadPreampController@getTapeHeadPreamp');

	Route::get('input-expanders', 'InputExpanderController@getInputExpanders')->name('input-expanders.index');
	Route::get('input-expanders/{id}', 'InputExpanderController@getInputExpander');

	Route::get('brands', 'BrandController@getBrandList')->name('brands.index');
	Route::get('brands/{id}', 'BrandController@getBrand');

	Route::get('test-file', function(){

		
	});

});	


