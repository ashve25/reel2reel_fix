<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\InputExpander;
use App\Modules\Base\Models\FavouriteInputExpander;
use Illuminate\Http\Request;
use App\Modules\Base\Models\Brand;
use Auth;

class InputExpanderController extends Controller{

	public $inputExpander;
	public $favouriteInputExpander;
	
	public function __construct(InputExpander $inputExpander, FavouriteInputExpander $favouriteInputExpander){
		$this->inputExpander = $inputExpander;
		$this->favouriteInputExpander = $favouriteInputExpander;
	}


	public function getInputExpanders(Request $request){
		$brand = new Brand;
		$brands = $brand->getBrands($request);
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$inputExpanders = $this->inputExpander->getInputExpanderList($request);
		if($request->has('ajax')){
			return $inputExpanders;
		}
		
		$title  = 'Input Expanders';
		return view('Frontend::input-expanders', compact('inputExpanders', 'brands', 'title'));
	}


	public function getInputExpander($inputExpanderId){
		$inputExpander = $this->inputExpander->getInputExpanderDetails($inputExpanderId);
		$addedInFavourite = false;
		if(Auth::check()){
			$addedInFavourite = $this->favouriteInputExpander->checkInputExpanderAddedAsFavourite($inputExpanderId);
		}
		return view('Frontend::input-expander-detail', compact('inputExpander', 'addedInFavourite'));
	}

	public function newInputExpander(){
		return view('Frontend::new-input-expander');
	}

	public function addTapeAsFavourite(Request $request){
		$this->favouriteTape->addTapeAsFavourite($request);
		return 200;
	}

	public function addInputExpanderAsFavourite(Request $request){
		$this->favouriteInputExpander->addInputExpanderAsFavourite($request);
		return 200;
	}

	public function deleteInputExpanderFromFavourite(Request $request){
		$this->favouriteInputExpander->deleteInputExpanderFromFavourite($request);
		return 200;
	}
}	 
