<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\TapeRecorder;
use App\Modules\Base\Models\FavouriteTapeRecorder;
use Illuminate\Http\Request;
use Auth;

class TapeRecorderController extends Controller{

	public $tapeRecorder;
	public $favouriteTapeRecorder;
	
	public function __construct(TapeRecorder $tapeRecorder, FavouriteTapeRecorder $favouriteTapeRecorder){
		$this->tapeRecorder = $tapeRecorder;
		$this->favouriteTapeRecorder = $favouriteTapeRecorder;
	}


	public function getTapeRecorders(Request $request){
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$tapeRecorders = $this->tapeRecorder->tapeRecorderList($request);
		if($request->has('ajax')){
			return $tapeRecorders;
		}
		$title = 'Tapes Recorders';
		return view('Frontend::tape-recorders', compact('tapeRecorders', 'title'));
	}


	public function getTapeRecorderDetails($tapeRecorderId){
		$tapeRecorder = $this->tapeRecorder->getTapeRecorderDetails($tapeRecorderId);
		$addedInFavourite = false;
		if(Auth::check()){
			$addedInFavourite = $this->favouriteTapeRecorder->checkTapeRecorderAddedAsFavourite($tapeRecorderId);
		}
		return view('Frontend::tape-recorder-detail', compact('tapeRecorder', 'addedInFavourite'));
	}

	public function newTapeRecorder(){
		return view('Frontend::new-tape-recorder');
	}


	public function addTapeRecorderAsFavourite(Request $request){
		$this->favouriteTapeRecorder->addTapeRecorderAsFavourite($request);
		return 200;
	}

	public function deleteTapeRecorderFromFavourite(Request $request){
		$this->favouriteTapeRecorder->deleteTapeRecorderFromFavourite($request);
		return 200;
	}


}	 
