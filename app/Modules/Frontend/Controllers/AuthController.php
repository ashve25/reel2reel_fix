<?php

namespace App\Modules\Frontend\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;
use Hash;
use App\Modules\Base\Models\Tape;
use App\Modules\Base\Models\TapeRecorder;

class AuthController extends Controller
{
	private $user;
    
    public function __construct(User $user)
    {
    	$this->user = $user;
    }


    public function login(){
    	if(Auth::check()){
    		return redirect('home');
    	}
    	return view('Frontend::login');
    }


    public function checkLogin(Request $request){
    	$validator = Validator::make($request->all(), [
    		'email_or_username' => 'required',
    		'password' => 'required'
    	]);
    	if($validator->fails()){
    		return back()->with('warning', implode(' ',  $validator->errors()->all()))->withInput($request->except('password'));
    	}
    	$user = $this->user->where(function($query) use ($request){
    			$query->where('email', $request->email_or_username)
    			->orWhere('username', $request->email_or_username);
    		})->first();

    	if(empty($user)){
    		return back()->with('warning', config('strings.warning.email_or_username_invalid'))->withInput($request->except('password'));
    	}
        if($user->verified !== 1){
            return back()->with('warning', config('strings.warning.unverified_user'))->withInput($request->except('password'));
        }
        if(Hash::check($request->password, $user->password) === false){
    		return back()->with('warning', config('strings.warning.incorrect_password'))->withInput($request->except('password'));
    	}
    	Auth::login($user);	
    	return redirect('home');
    }


    public function register(){
    	if(Auth::check()){
    		return redirect('home');
    	}
    	return view('Frontend::register');
    }

    public function saveRegistration(Request $request){
    	$validator = Validator::make($request->all(), [
    		'name' => 'required',
    		'username' => 'required|unique:users',
    		'email' => 'required|unique:users',
    		'password'  => 'required|string|min:6|same:confirm_password',
    		'country' => 'required',
    		'terms_and_conditions' => 'required'
    	]);
        //return count($validator->errors()->all());
    	if($validator->fails()){
            $errors = implode(" ", $validator->errors()->all());
            return back()->with('warning', $errors)->withInput($request->except('password', 'confirm_password'));
    	}
    	$this->user->registerUser($request);
    	return back()->with('success', config('strings.success.account_created'));
    }

    public function verifyEmail($userId, $key){
    	$user = $this->user->find($userId);
        if($user->verification_key !== $key){
            return redirect('login')->with('warning', config('strings.warning.invalid_link'));
        }
        $user->verifyMe();
        return redirect('login')->with('success', config('strings.success.verified'));
    }

    public function profile(Request $request){
        $user = $this->user->find(Auth::user()->id);
        if($user && $user->profile_picture){
            $user->profile_picture = \Storage::url($user->profile_picture);
        }
        if(!$request->has('page')){
            $request->merge(['page'=> 1]);
        }

        $tapeRecorder = new TapeRecorder;
        $tapeRecorders = $tapeRecorder->getUserTapeRecorders(Auth::user()->id, $request);
        
        $tape = new Tape;
        $tapes = $tape->getUserTapes(Auth::user()->id, $request);
        return view('Frontend::profile', compact('user', 'tapeRecorders', 'tapes'));
    }


    public function updateProfile(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users,username,'.Auth::user()->id,
            'email' => 'required|unique:users,email,'.Auth::user()->id,
            'country' => 'required'
        ]);
        if($validator->fails()){
            $errors = implode(" ", $validator->errors()->all());
            return back()->with('warning', $errors);
        }
        if($request->has('password') && $request->has('confirm_password') && $request->password !== $request->confirm_password && !empty($request->password) && !empty($request->confirm_password)){
            return back()->with('warning', config('strings.passwords_not_matched'));
        }
        elseif($request->has('password') && !empty($request->password)){
            return back()->with('warning', config('strings.confirm_password_is_empty'));
        }
        $user = $this->user->find(Auth::user()->id);
        $user->updateProfile($request);
        return back()->with('success', config('strings.success.profile_updated'));
    }

    public function logout(){
    	Auth::logout();
    	return 200;	
    }

    public function forgotPassword(){
    	return view('Frontend::forgot-password');
    }

    public function forgotPasswordRequest(Request $request){
    	$validator = Validator::make($request->all(), [
    		'email' => 'required|exists:users',
    	]);
    	if($validator->fails()){
            $errors = implode(" ", $validator->errors()->all());
    		return back()->with('warning', $errors)->withInput($request->all());
    	}
    	$user = $this->user->where('email', $request->email)->first();
        if(empty($user)){
            return back()->with('warning', config('strings.warning.invalid_user'));
        }
    	$user->sendResetPasswordMail();
    	return back()->with('success', config('strings.success.reset_password_link_sent'));
    }

    public function resetPasswordForm($userId, $key){
    	$user = $this->user->find($userId);
        if($user->verification_key !== $key){
            return redirect('login')->with('warning', config('strings.warning.invalid_link'));
        }
        return view('Frontend::reset-password');
    }

    public function resetPassword($userId, $key, Request $request){
    	$validator = Validator::make($request->all(), [
    		'password' => 'required|min:6|string|same:confirm_password',
    	]);
    	if($validator->fails()){
    		$errors = implode(" ", $validator->errors()->all());
            return back()->with('warning', $errors);
    	}

    	$user = $this->user->find($userId);
    	if(empty($user)){
            return back()->with('warning', config('strings.warning.invalid_user'));
        }
    	if($user->verification_key !== $key){
            return redirect('login')->with('warning', config('strings.warning.invalid_link'));
        }
        $user->savePassword($request);
        return redirect('login')->with('success', config('strings.success.password_reset_successfully'));
    }
}
