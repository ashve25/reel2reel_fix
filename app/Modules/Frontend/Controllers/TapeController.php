<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Tape;
use App\Modules\Base\Models\FavouriteTape;
use Illuminate\Http\Request;
use Auth;

class TapeController extends Controller{

	public $tape;
	public $favouriteTape;

	public function __construct(Tape $tape, FavouriteTape $favouriteTape){
		$this->tape = $tape;
		$this->favouriteTape = $favouriteTape;
	}


	public function getTapes(Request $request){
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$tapes = $this->tape->tapeList($request);
		if($request->has('ajax')){
			return $tapes;
		}
		$title = 'Tapes';
		return view('Frontend::tapes', compact('tapes', 'title'));
	}

	public function getTape($id){
		$tape = $this->tape->tapeDetails($id);
		$addedInFavourite = false;
		if(Auth::check()){
			$addedInFavourite = $this->favouriteTape->checkTapeAddedAsFavourite($id);
		}
		return view('Frontend::tape-detail', compact('tape', 'addedInFavourite'));
	}

	public function newTape(){
		return view('Frontend::new-tape');
	}

	public function addTapeAsFavourite(Request $request){
		$this->favouriteTape->addTapeAsFavourite($request);
		return 200;
	}

	public function deleteTapeFromFavourite(Request $request){
		$this->favouriteTape->deleteTapeFromFavourite($request);
		return 200;
	}
}	 
