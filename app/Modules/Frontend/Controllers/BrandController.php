<?php

namespace App\Modules\Frontend\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Brand;
use App\Modules\Base\Models\TapeRecorder;
use App\Http\Traits\APIResponseTrait;

class BrandController extends Controller
{
	use APIResponseTrait;

	protected $brand;
	protected $tapeRecorder;

	public function __construct(Brand $brand, TapeRecorder $tapeRecorder){
		$this->brand = $brand;
		$this->tapeRecorder = $tapeRecorder;
	}

	public function getBrandList(Request $request){
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$brands = $this->brand->getBrands($request);
		if($request->has('ajax')){
			return $brands;
		}
		return view('Frontend::brands', compact('brands'));
	}

	public function getBrands(Request $request){
		$brands = $this->brand->get();
		return $this->success($brands);
	} 

	public function getBrand($brandId, Request $request){
		$brand = $this->brand->getBrand($brandId);
		if(!$request->has('page')){
			$request->merge(['page' => 1]);
		}
		$request->merge(['brand_id' => $brandId]);
		$tapeRecorders = $this->tapeRecorder->tapeRecorderList($request);

		return view('Frontend::brand-detail', compact('brand', 'tapeRecorders'));
	}
}