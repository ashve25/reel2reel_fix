<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\TapeHeadPreamp;
use App\Modules\Base\Models\FavouriteTapeHeadPreamp;
use Illuminate\Http\Request;
use Auth;

class TapeHeadPreampController extends Controller{

	public $tapeHeadPreamp;
	public $favouriteTapeHeadPreamp;
	public function __construct(TapeHeadPreamp $tapeHeadPreamp, FavouriteTapeHeadPreamp $favouriteTapeHeadPreamp){
		$this->tapeHeadPreamp = $tapeHeadPreamp;
		$this->favouriteTapeHeadPreamp = $favouriteTapeHeadPreamp;
	}


	public function getTapeHeadPreamps(Request $request){
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$tapeHeadPreamps = $this->tapeHeadPreamp->getTapeHeadPreampList($request);
		if($request->has('ajax')){
			return $tapeHeadPreamps;
		}
		$title = 'Tape Head Preams';
		return view('Frontend::tape-head-preamps', compact('tapeHeadPreamps', 'title'));
	}

	public function getTapeHeadPreamp($tapeHeadPreampId){
		$tapeHeadPreamp = $this->tapeHeadPreamp->getTapeHeadPreampDetails($tapeHeadPreampId);
		$addedInFavourite = false;
		if(Auth::check()){
			$addedInFavourite = $this->favouriteTapeHeadPreamp->checkTapeHeadPreampAddedAsFavourite($tapeHeadPreampId);
		}
		return view('Frontend::tape-head-preamp-detail', compact('tapeHeadPreamp', 'addedInFavourite'));
	}

	public function newTapeHeadPreamp(){
		return view('Frontend::new-tape-head-preamp');
	}

	public function addTapeHeadPreampAsFavourite(Request $request){
		$this->favouriteTapeHeadPreamp->addTapeHeadPreampAsFavourite($request);
		return 200;
	}

	public function deleteTapeHeadPreampFromFavourite(Request $request){
		$this->favouriteTapeHeadPreamp->deleteTapeHeadPreampFromFavourite($request);
		return 200;
	}
}	 
