<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\TapeRecorder;
use App\Modules\Base\Models\Tape;
use Illuminate\Http\Request;

class HomeController extends Controller{

	public $tapeRecorder;
	public $tape;
	public function __construct(TapeRecorder $tapeRecorder, Tape $tape){
		$this->tapeRecorder = $tapeRecorder;
		$this->tape = $tape;
	} 

	public function homePage(Request $request){
		$tapeRecorders = $this->tapeRecorder->tapeRecorderList($request);
		$tapes = $this->tape->tapeList($request);
		return view('Frontend::home', compact('tapeRecorders', 'tapes'));
	}
}