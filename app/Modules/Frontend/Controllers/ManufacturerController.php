<?php
namespace App\Modules\Frontend\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Manufacturer;
use App\Modules\Base\Models\TapeRecorder;
use Illuminate\Http\Request;

class ManufacturerController extends Controller{

	public $manufacturer;
	
	public function __construct(Manufacturer $manufacturer,  TapeRecorder $tapeRecorder){
		$this->manufacturer = $manufacturer;
		$this->tapeRecorder = $tapeRecorder;
	}


	public function getManufacturers(Request $request){
		if(!$request->has('page')){
			$request->merge(['page' => 1]);
		}
		$manufacturers = $this->manufacturer->getManufacturers($request);
		if($request->has('ajax')){
			return $manufacturers;
		}
		return view('Frontend::manufacturers', compact('manufacturers'));
	}


	public function getManufacturer($manufacturerId, Request $request){
		$manufacturer = $this->manufacturer->getManufacturer($manufacturerId);
		//$tapeRecorders  = $this->tapeRecorder->getManufacturerTapeRecorders($manufacturerId);
		if(!$request->has('page')){
			$request->merge(['page' => 1]);
		}
		$request->merge(['manufacturer_id' => $manufacturerId]);
		$tapeRecorders  = $this->tapeRecorder->tapeRecorderList($request);
		return view('Frontend::manufacturer-detail', compact('manufacturer', 'tapeRecorders'));
	}




}	
