<?php
namespace App\Modules\Frontend\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Catalog;
use App\Modules\Base\Models\CatalogImage;
use Illuminate\Http\Request;
use DB;

class CatalogController extends Controller{

	public $catalog;
	public function __construct(Catalog $catalog){
		$this->catalog = $catalog;
	}

	public function getCatalogs(Request $request){
		if(!$request->has('page')){
			$request->merge(['page'=>1]);
		}
		$catalogs = $this->catalog->getCatalogs($request);
		if($request->has('ajax')){
			return $catalogs;
		}
		$title = '';
		if($request->has('type')){
			$catalogType = DB::table('catalog_types')->find($request->type);
			if(!empty($catalogType)){
				$title = $catalogType->name;
			}
		}
		return view('Frontend::catalogs', compact('catalogs', 'title'));
	}

	public function getCatalogsOtherImages($catalogId){
		$catalogTitle = $this->catalog->getCatalogTitle($catalogId);
		$catalogImage = new CatalogImage;
        $catalogOtherImages = $catalogImage->getOtherImages($catalogId);
        return view('Frontend::catalog-other-images', compact('catalogOtherImages', 'catalogTitle'));
	}

	public function newCatalog(){
		$catalogTypes = DB::table('catalog_types')->get();
		return view('Frontend::new-catalog', compact('catalogTypes'));
	}

	public function uploadImages(Request $request){
		if($request->hasFile('file')){
			return $file = $request->file('file');
			//return $file->getClientOriginalName();
		}	
		return $request->all();
		
	}

	public function savaCatalog(Request $request){
		return $request->all();
	}
}	 
