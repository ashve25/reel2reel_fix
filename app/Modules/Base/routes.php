<?php
use App\User;
use \Firebase\JWT\JWT;

Route::group(['namespace' => 'App\Modules\Base\Controllers'], function(){
    Route::post('admin/register','AuthController@register');
    Route::get('users/{id}/verify/{key}', 'AuthController@verifyEmail');
    Route::post('/admin/login','AuthController@login');
    Route::post('admin/forgot-password', 'AuthController@forgotPassword');
    Route::get('users/{id}/reset-password/{key}', 'AuthController@resetPasswordForm');
    Route::post('users/{id}/reset-password', 'AuthController@resetPassword');
    Route::post('users/{id}/logout', 'AuthController@logout');

    Route::get('admin', 'AuthController@adminLoginForm');

    // Route::get('/test', function(){
    //     $jwt = str_replace("Bearer ", "", 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9yZWVsLmNvbSIsImF1ZCI6Imh0dHA6XC9cL3JlZWwuY29tIiwidXNlcl9pZCI6NCwidGltZXN0YW1wIjoiMjAxOC0wMS0yNCAxMzoyMDoxOCJ9.zORRlraq8l5geaRoVJf8y951s3lv_bz8ToL_DAkjgko');
    //     $key = config('app.jwt_key');
    //     $decoded = JWT::decode($jwt, $key, array('HS256')); 
    //     $user = new User;
    //     $user->id = $decoded->user_id;
    //     $user->token_timestamp_updated_at = $decoded->timestamp;
    //     return $user;
    // });

    Route::resource('admin/users', 'UserController');
    Route::get('user-types', function(){
        return \DB::table('user_types')->get();
    });

    Route::post('admin/profile', 'UserController@updateProfile');

    Route::get('admin/master-data/tapes', 'TapeController@getMasterData');
    Route::resource('admin/tapes', 'TapeController');

    Route::get('admin/master-data/tape-recorders', 'TapeRecorderController@getMasterData');
    Route::resource('admin/tape-recorders', 'TapeRecorderController');
    
    Route::resource('admin/manufacturers', 'ManufacturerController');
    
    Route::post('admin/manufacturers/images/{id}/delete','ManufacturerController@deleteImage');

    Route::resource('admin/brands', 'BrandController');
    Route::post('admin/brands/images/{id}/delete','BrandController@deleteImage');

    Route::resource('admin/catalogs', 'CatalogController');
    Route::get('admin/catalogs/{id}/other-images', 'CatalogController@getCatalogOtherImages');
    Route::get('admin/catelog-types', 'CatalogController@getCatelogTypes');

    Route::resource('admin/tape-head-preamps', 'TapeHeadPreampController');
    Route::resource('admin/input-expanders', 'InputExpanderController');
    Route::resource('admin/history', 'HistoryController');

});

Route::get('create-demo-account', function () {
        $user = App\User::where('email', 'admin@gmail.com')->first();
        if(empty($user)){
            $user = new App\User; 
        }
		$user->name = 'Admin';
		$user->email = 'admin@gmail.com';
		$user->password = \Hash::make('abc123');
		$user->save();
		return json_encode($user);
	});

Route::post('resize', "App\Modules\Admin\Controllers\AuthController@resize");