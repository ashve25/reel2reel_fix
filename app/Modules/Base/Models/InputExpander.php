<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Base\Models\InputExpanderImage;
use DB;

class InputExpander extends Model
{
	use SoftDeletes;

    protected $table = 'expanders'; 
 
    protected $fillable = ['tape_recorders_usable', 'model', 'outputs_for_record', 'copy_option_decks', 'brand_id', 'main_image', 'status', 'user_id'];

	//protected $table = 'tape_head_preamps';

    private function getBaseQuery(){
        return self::select('expanders.*', 'brands.name as brand_name')
            ->leftjoin('brands', 'brands.id', '=', 'expanders.brand_id');
    }

    public function getInputExpanderList($request){
    	$inputExpanders = $this->getBaseQuery();

        if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            info($filters);
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $inputExpanders = $inputExpanders->whereIn($key, $value);
            }
        }

    	if($request->has('page')){
    		$inputExpanders = $inputExpanders->paginate(config('app.paginate'));
    	}
    	else{
    		$inputExpanders = $inputExpanders->get();	
    	}
    	foreach ($inputExpanders as $key => $inputExpander) {
    		$inputExpanderImage = new InputExpanderImage;
    		$inputExpander->images = $inputExpanderImage->getInputExpanderImages($inputExpander->id);

    		foreach($inputExpander->images as $image) {
                if($inputExpander->main_image == $image->type){
                    $image->thumbnail = \Storage::url($image->thumbnail);
                }
                if(!empty($image->image)){
                    $image->image = \Storage::url($image->image);
                }
            }
    	}
    	return $inputExpanders;
    }

    public function getInputExpanderDetails($inputExpanderId){
    	$inputExpander = $this->getBaseQuery()->where('expanders.id', $inputExpanderId)->first();
    	$inputExpanderImage = new InputExpanderImage;
		$inputExpander->images = $inputExpanderImage->getInputExpanderImagesOrderByType($inputExpander->id);

        foreach($inputExpander->images as $image) {
            if(!empty($image->image)){
        	   $image->image = \Storage::url($image->image);
            }
            if(!empty($image->thumbnail)){
                $image->thumbnail = \Storage::url($image->thumbnail);
            }
        }
        info($inputExpander);
        return $inputExpander;
    }

	public function createInputExpander($request)
	{
		DB::transaction(function() use ($request) {
			$inputExpander = self::create($request->except('images'));
			if(!empty($inputExpander) && $request->has('images')){
				$inputExpanderImages = new InputExpanderImage; 
				$inputExpanderImages->insertInputExpanderImages($inputExpander->id, $request);
			}

		});
	}


	public function updateInputExpander($request, $id)
	{
		DB::transaction(function() use ($request, $id) {
			$inputExpander = self::where('id', $id)->first();
			$inputExpander->update($request->except(['_method', 'images']));
			if(!empty($inputExpander) && $request->has('images')){
				$inputExpanderImages = new InputExpanderImage; 
				$inputExpanderImages->updateInputExpanderImages($inputExpander->id, $request);
			}
		});
	}

}