<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use App\Http\Traits\APIResponseTrait;
use Auth;

class FavouriteTapeHeadPreamp extends Model
{
	protected $table = 'favourite_tape_head_preamps';

	protected $fillable = ['user_id','tape_head_preamp_id'];


	public function addTapeHeadPreampAsFavourite($request)
	{
		$this->user_id = Auth::user()->id;
		$this->tape_head_preamp_id = $request->tape_head_preamp_id;
		$this->save();
	}

	public function deleteTapeHeadPreampFromFavourite($request){
		$sql = self::where('tape_head_preamp_id', $request->tape_head_preamp_id)->where('user_id', Auth::user()->id);
		$find = $sql->first();

		if(!empty($find)){
			$sql->delete();
		}
	}


	public function checkTapeHeadPreampAddedAsFavourite($tapeHeadPreamId)
    {
        return self::where('tape_head_preamp_id', $tapeHeadPreamId)->where('user_id', Auth::user()->id)->count()>0;
    }

}