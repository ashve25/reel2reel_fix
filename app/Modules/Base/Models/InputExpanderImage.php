<?php

namespace App\Modules\Base\Models;
use App\Modules\Base\Models\FileResizeHelper;
use Illuminate\Database\Eloquent\Model;

class InputExpanderImage extends Model
{
	use FileResizeHelper;

	protected $table = 'expander_images'; 

	public function getInputExpanderImages($inputExpanderId){
		return self::where('expander_id', $inputExpanderId)->get();
	}

	public function getInputExpanderImagesOrderByType($inputExpanderId){
		return self::where('expander_id', $inputExpanderId)->orderBy('type', 'asc')->get();
	}

	public function insertInputExpanderImages($inputExpanderId, $request)
	{
		foreach($request->images as $index => $image) {
            if(!empty($image)){
                $thumbnail = $this->resizeAndStore($image, "expanders", 300, 300);
                if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
				$path = $image->store('expanders');
				//info($path);
				$inputExpanderImage = new self;
				$inputExpanderImage->expander_id = $inputExpanderId;
				$inputExpanderImage->type = $index;
				$inputExpanderImage->image = $path;
                $inputExpanderImage->thumbnail = $thumbnail;
				$inputExpanderImage->save();
            }
		}
	}


	public function updateInputExpanderImages($inputExpanderId, $request)
	{
		foreach($request->images as $index => $image) {
            if(!empty($image)){
                $thumbnail = $this->resizeAndStore($image, "expanders", 300, 300);
                if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
				$path = $image->store('expanders');
				//info($path);
				$inputExpanderImage = self::where('type', $index)->where('expander_id', $inputExpanderId)->first();
                if(empty($inputExpanderImage)){
                	$inputExpanderImage = new self;
                	$inputExpanderImage->expander_id = $inputExpanderId;
					$inputExpanderImage->type = $index;
					$inputExpanderImage->image = $path;
	                $inputExpanderImage->thumbnail = $thumbnail;
					$inputExpanderImage->save();
                }else{
					$inputExpanderImage->image = $path;
	                $inputExpanderImage->thumbnail = $thumbnail;
					$inputExpanderImage->save();
                }
            }
		}
	}

}