<?php

namespace App\Modules\Base\Models;
use App\Modules\Base\Models\FileResizeHelper;
use Illuminate\Database\Eloquent\Model;

class TapeHeadPreampImage extends Model
{
	use FileResizeHelper;
	protected $table = 'tape_head_preamp_images';

	public function getTapeHeadPreamImages($tapeHeadPreampId){
		return self::where('tape_head_preamp_id', $tapeHeadPreampId)->get();
	}

	public function getTapeHeadPreamImagesOrderByType($tapeHeadPreampId){
		return self::where('tape_head_preamp_id', $tapeHeadPreampId)->orderBy('type', 'asc')->get();
	}

	public function insertTapeHeadPreamImages($tapeHeadPreampId, $request)
	{
		foreach($request->images as $index => $image) {
            if(!empty($image)){
                $thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
                if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
				$path = $image->store('tapes');
				//info($path);
				$tapeHeadPreampImage = new self;
				$tapeHeadPreampImage->tape_head_preamp_id = $tapeHeadPreampId;
				$tapeHeadPreampImage->type = $index;
				$tapeHeadPreampImage->image = $path;
                $tapeHeadPreampImage->thumbnail = $thumbnail;
				$tapeHeadPreampImage->save();
            }
		}
	}


	public function updateTapeHeadPreamImages($tapeHeadPreampId, $request)
	{
		foreach($request->images as $index => $image) {
            if(!empty($image)){
                $thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
                if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
				$path = $image->store('tapes');
				//info($path);
				$tapeHeadPreampImage = self::where('type', $index)->where('tape_head_preamp_id', $tapeHeadPreampId)->first();
                if(empty($tapeHeadPreampImage)){
                	$tapeHeadPreampImage = new self;
                	$tapeHeadPreampImage->tape_head_preamp_id = $tapeHeadPreampId;
					$tapeHeadPreampImage->type = $index;
					$tapeHeadPreampImage->image = $path;
	                $tapeHeadPreampImage->thumbnail = $thumbnail;
					$tapeHeadPreampImage->save();
                }else{
					$tapeHeadPreampImage->image = $path;
	                $tapeHeadPreampImage->thumbnail = $thumbnail;
					$tapeHeadPreampImage->save();
                }
            }
		}
	}

}