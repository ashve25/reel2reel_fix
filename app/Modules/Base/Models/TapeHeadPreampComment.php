<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;

class TapeHeadPreampComment extends Model
{
	protected $table = 'tape_head_preamp_comments';

}