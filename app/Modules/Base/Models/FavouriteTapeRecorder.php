<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use App\Http\Traits\APIResponseTrait;
use Auth;

class FavouriteTapeRecorder extends Model
{
	protected $table = 'favourite_tape_recorders';

	protected $fillable = ['user_id','tape_recorder_id'];

	public function addTapeRecorderAsFavourite($request)
	{
		$this->user_id = Auth::user()->id;
		$this->tape_recorder_id = $request->tape_recorder_id;
		$this->save();
	}

	public function deleteTapeRecorderFromFavourite($request){
		$sql = self::where('tape_recorder_id', $request->tape_recorder_id)->where('user_id', Auth::user()->id);
		$find = $sql->first();

		if(!empty($find)){
			$sql->delete();
		}
	}


	public function checkTapeRecorderAddedAsFavourite($tapeRecorderId)
    {
        return self::where('tape_recorder_id', $tapeRecorderId)->where('user_id', Auth::user()->id)->count()>0;
    }

}