<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use App\Http\Traits\APIResponseTrait;
use Auth;

class FavouriteTape extends Model
{
	protected $table = 'favourite_tapes';

	protected $fillable = ['user_id','tape_id'];


	public function addTapeAsFavourite($request)
	{
		$this->user_id = Auth::user()->id;
		$this->tape_id = $request->tape_id;
		$this->save();
	}

	public function deleteTapeFromFavourite($request){
		$sql = self::where('tape_id', $request->tape_id)->where('user_id', Auth::user()->id);
		$find = $sql->first();

		if(!empty($find)){
			$sql->delete();
		}
	}


	public function checkTapeAddedAsFavourite($tapeId)
    {
        return self::where('tape_id', $tapeId)->where('user_id', Auth::user()->id)->count()>0;
    }

}