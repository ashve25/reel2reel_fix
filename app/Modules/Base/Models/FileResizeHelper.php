<?php
namespace App\Modules\Base\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait FileResizeHelper {
	
	public function resizeAndStore($file, $path, $width, $height) {
		$image = \Image::make($file)->resize($width, $height);//->save('images/resized1.png');
    	
    	if ($path) {
            $path = rtrim($path, '/').'/';
        }

        $hash = Str::random(40);

        $name = $path.$hash.'.'.$file->getClientOriginalExtension();

    	$stored = \Storage::put($name, (string)$image->encode($file->getClientOriginalExtension(), 100));

        if($stored) return $name;
        else return "";
	}

    public function resizeAndStoreBase64($file, $path, $width, $height) {
        $image = \Image::make($file)->resize($width, $height);//->save('images/resized1.png');
        if ($path) {
            $path = rtrim($path, '/').'/';
        }

        $hash = Str::random(40);

        $name = $path.$hash.'.png';

        $stored = \Storage::put($name, (string)$image->encode("png", 100));

        if($stored) return $name;
        else return "";
    }
}