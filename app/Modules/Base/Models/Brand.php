<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Modules\Base\Models\BrandImage;
use DB;
use App\Http\Traits\APIResponseTrait;

class Brand extends Model
{
	protected $table = 'brands';

	protected $fillable = ['name','manufacturer_id', 'location', 'year_to', 'year_from', 'description', 'innovations','information'];

	use APIResponseTrait;

	private function getBaseQuery() 
	{
		return self::select('brands.*', 'manufacturers.name as manufacturer_name')
			->leftjoin('manufacturers', 'manufacturers.id', '=', 'brands.manufacturer_id');
	}

	public function getBrands(Request $request){

		if($request->has('page'))
			$brands = $this->getBaseQuery()->paginate(config('app.paginate'));
		else
			$brands = $this->getBaseQuery()->get();

		foreach ($brands as $key => $brand) {
			$brand->images = BrandImage::where('brand_id', $brand->id)->orderBy('type','asc')->get();

	        foreach ($brand->images as $image) {
	        	if(!empty($image->image)){
	        		$image->image = \Storage::url($image->image);
	        	}
	        }
		}
		return $brands;
	}

	public function createBrand(Request $request){
		DB::transaction(function() use($request){
			$brand = self::create($request->except(['founder', 'images']));
			if($request->has('founder')){
				$path = $request->founder->store('public/images');
				$brand_image = new BrandImage();
				$brand_image->brand_id = $brand->id;
				$brand_image->type = 0;
				$brand_image->image = $path;
				$brand_image->save();
			}

			if($request->has('images')){
				foreach($request->images as $index => $image){
					$path = $image->store('public/images');
					$brand_image = new BrandImage();
					$brand_image->brand_id = $brand->id;
					$brand_image->type = 1;
					$brand_image->image = $path;
					$brand_image->save();
				}
			}
		});
		$data = "success";
		$this->success($data);
	}

	public function getBrand($id){
		$brand = $this->getBaseQuery()->where('brands.id', $id)->first();
        $brand->images = BrandImage::where('brand_id', $id)->orderBy('type','asc')->get();

        foreach ($brand->images as $image) {
        	if(!empty($image->image)){
        		$image->image = \Storage::url($image->image);
        	}
        }
        info($brand);
        return $brand;
	}

	public function updateBrand(Request $request, $id){
		DB::transaction(function() use($request, $id){
			$brand = self::where('id', $id)->update($request->except(['_method','founder', 'images']));
			if($request->has('founder')){
				$path = $request->founder->store('public/images');
				$brand_image = new BrandImage();
				$brand_image->brand_id = $id;
				$brand_image->type = 0;
				$brand_image->image = $path;
				$brand_image->save();
			}
			//PENDING IMAGE UPDATE
			if($request->has('images')){
				foreach($request->images as $index => $image){
					$path = $image->store('public/images');
					$brand_image = new BrandImage();
					$brand_image->brand_id = $id;
					$brand_image->type = 1;
					$brand_image->image = $path;
					$brand_image->save();
				}
			}
		});
		/*$data = "success";
		$this->success($data);*/
	}

	public function deleteBrand($id){
		DB::transaction(function() use ($id){
            BrandImage::where('brand_id',$id)->delete();
            Brand::find($id)->delete();
        });
	}

	public function deleteImage($id){
		info('delete image');
		BrandImage::find($id)->delete();
		$data = "success";
		$this->success($data);
	}

}