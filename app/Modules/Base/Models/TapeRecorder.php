<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Modules\Base\Models\TapeRecorderImage;
use App\Modules\Base\Models\Manufacturer;
use App\Modules\Base\Models\Brand;
use DB;
use App\Modules\Base\Models\FileResizeHelper;

class TapeRecorder extends Model {
    use FileResizeHelper;
    protected $table = 'tape_recorders';

    protected $fillable = ['model','brand_id','serial_number',
                    'original_price','manufacturer_id','country_of_manufacturer',
                    'main_image','head_configuration_id','number_of_head_id','speed_id',
                    'track_id','release_from_date','release_to_date','head_composition_id',
                    'equalization_id','voltage_id','auto_reverse_id','application_id','max_reel_size_id','noise_reduction_id','motor_id','belts','frequency_response','wow_flutter','signal_noise_ratio','description', 'user_id', 'status'];


    private function getBaseQuery(){
        return self::select('tape_recorders.*', 'manufacturers.name as manufacturer_name', 'brands.name as brand_name', 'motors.name as motor_name', 'head_configuration.name as head_configuration_name', 'number_of_heads.name as number_of_head_name', 'speeds.name as speed_name', 'tracks.name as track_name', 'head_composition.name as head_composition_name', 'equalization.name as equalization_name', 'voltage.name as voltage_name', 'auto_reverse.name as auto_reverse_name', 'max_reel_size.name as max_reel_size_name', 'noise_reductions.name as noise_reduction_name')
                    ->join('manufacturers', 'manufacturers.id', '=', 'tape_recorders.manufacturer_id')
                    ->join('brands', 'brands.id', '=', 'tape_recorders.brand_id')
                    ->leftjoin('motors', 'motors.id', '=', 'tape_recorders.motor_id')
                    ->leftjoin('head_configuration', 'head_configuration.id', '=', 'tape_recorders.head_configuration_id')
                    ->leftjoin('number_of_heads', 'number_of_heads.id', '=', 'tape_recorders.number_of_head_id')
                    ->leftjoin('speeds', 'speeds.id', '=', 'tape_recorders.speed_id')
                    ->leftjoin('tracks', 'tracks.id', '=', 'tape_recorders.track_id')
                    ->leftjoin('head_composition', 'head_composition.id', '=', 'tape_recorders.head_composition_id')
                    ->leftjoin('equalization', 'equalization.id', '=', 'tape_recorders.equalization_id')
                    ->leftjoin('voltage', 'voltage.id', '=', 'tape_recorders.voltage_id')
                    ->leftjoin('auto_reverse', 'auto_reverse.id', '=', 'tape_recorders.auto_reverse_id')
                    ->leftjoin('application', 'application.id', '=', 'tape_recorders.application_id')
                    ->leftjoin('max_reel_size', 'max_reel_size.id', '=', 'tape_recorders.max_reel_size_id')
                    ->leftjoin('noise_reductions', 'noise_reductions.id', '=', 'tape_recorders.noise_reduction_id');
    }                
                    

    public function createTapeRecorder(Request $request) {
        $result = DB::transaction(function() use ($request) {
            $tapeRecorder = self::create($request->all());

            foreach($request->images as $index => $image) {
                if(!empty($image)){
                    $thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
                    if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
    				$path = $image->store('tapes');
    				//info($path);
    				$tapeRecorderImage = new TapeRecorderImage;
    				$tapeRecorderImage->tape_recorder_id = $tapeRecorder->id;
    				$tapeRecorderImage->type = $index;
    				$tapeRecorderImage->image = $path;
                    $tapeRecorderImage->thumbnail = $thumbnail;
    				$tapeRecorderImage->save();
                }
			}
        });

        return $result;
    }

    public function getTapeRecorderDetails($id){
        $tapeRecorder = $this->getBaseQuery()->where('tape_recorders.id', $id)->first();
        $tapeRecorder->images = TapeRecorderImage::where('tape_recorder_id', $id)->orderBy('type','asc')->get();

        foreach($tapeRecorder->images as $image) {
            if(!empty($image->thumbnail)){
        	   $image->thumbnail = \Storage::url($image->thumbnail);
            }
            if(!empty($image->image)){
                $image->image = \Storage::url($image->image);
            }

        }
        info($tapeRecorder);
        return $tapeRecorder;
    }

    public function tapeRecorderImageById($id){
        return DB::table('tape_recorder_images')->where('tape_recorder_id',$id)->get();

    }

    public function tapeRecorderList($request){
        if($request->has('tracks')){
            if($request->tracks == '2'){
                $tapeRecorders = $this->getBaseQuery()->whereIn('tape_recorders.track_id',['2','3']);

            }elseif($request->tracks == '4'){
                $tapeRecorders = $this->getBaseQuery()->whereIn('tape_recorders.track_id',['1','4']);
            }
        }else{
            $tapeRecorders = $this->getBaseQuery();
        }

        if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $tapeRecorders = $tapeRecorders->whereIn($key, $value);
            }
        }

        if($request->has('manufacturer_id')){
            $tapeRecorders = $tapeRecorders->where('tape_recorders.manufacturer_id', $request->manufacturer_id);   
        }
        
        if($request->has('page')){
           $tapeRecorders = $tapeRecorders->paginate(config('app.paginate'));
        }else{  
           $tapeRecorders = $tapeRecorders->get();
        }

        //info($tapeRecorders);
        foreach($tapeRecorders as $tapeRecorder){
            $manufacturer = Manufacturer::find($tapeRecorder->manufacturer_id);
            $tapeRecorder->manufacturer_name = '';
            if(!empty($manufacturer)){
                $tapeRecorder->manufacturer_name = $manufacturer->name;
            }
            $tapeRecorder->brand_name = '';
            $brand = Brand::find($tapeRecorder->brand_id);
            if(!empty($brand)){
                $tapeRecorder->brand_name = $brand->name;
            }
            $images = array();
            $images = $this->tapeRecorderImageById($tapeRecorder->id);            
            foreach($images as $image) {
                if($tapeRecorder->main_image == $image->type){
                    if(!empty($image->thumbnail)){
                        $image->thumbnail = \Storage::url($image->thumbnail);
                    }
                }
                if(!empty($image->image)){
                    $image->image = \Storage::url($image->image);
                }
            }
            $tapeRecorder->images = $images;
        }
		return $tapeRecorders;
    }

    public function updateTapeRecorder(Request $request, $id) {
        $fillable = $this->fillable;
        DB::transaction(function() use ($request, $id, $fillable) {
            $tapeRecorder = self::find($id)->update($request->only($fillable));

            if($request->has('images')){
                foreach($request->images as $index => $image) {
                    $thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
                    if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
                    $path = $image->store('tapes');
                    $tapeRecorderImage = TapeRecorderImage::where('type', $index)->where('tape_recorder_id', $id)->first();
                    if(empty($tapeRecorderImage)){
                        $tapeRecorderImage = new TapeRecorderImage;
                        $tapeRecorderImage->tape_recorder_id = $id;
                        $tapeRecorderImage->type = $index;
                        $tapeRecorderImage->image = $path;
                        $tapeRecorderImage->thumbnail = $thumbnail;
                        $tapeRecorderImage->save();
                    }else{
                        $tapeRecorderImage->image = $path;
                        $tapeRecorderImage->thumbnail = $thumbnail;
                        $tapeRecorderImage->save();
                    }
    			}
            }
        });
    }

    public function deleteTapeRecorder($id){
        DB::transaction(function() use ($id){
            TapeRecorderImage::where('tape_recorder_id',$id)->delete();
            TapeRecorder::find($id)->delete();
        });
    }

    public function deleteImage($id){
        info('delete image');
        TapeRecorderImage::find($id)->delete();
        $data = "success";
        $this->success($data);
    }

    public function getManufacturerTapeRecorders($manufacturerId){
        $tapeRecorders = $this->getBaseQuery()->where('tape_recorders.manufacturer_id', $manufacturerId)->paginate(config('app.paginate'));
        return $tapeRecorders;
    }

     public function getUserTapeRecorders($userId, $request){
        $tapeRecorders = $this->getBaseQuery()
                ->where('tape_recorders.user_id', $userId)
                ->where('tape_recorders.status', 1);

        if($request->has('tracks')){
            if($request->tracks == '2'){
                $tapeRecorders = $tapeRecorders->whereIn('tape_recorders.track_id',['2','3']);

            }elseif($request->tracks == '4'){
                $tapeRecorders = $tapeRecorders->whereIn('tape_recorders.track_id',['1','4']);
            }
        }

        if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $tapeRecorders = $tapeRecorders->whereIn($key, $value);
            }
        }

        if($request->has('manufacturer_id')){
            $tapeRecorders = $tapeRecorders->where('tape_recorders.manufacturer_id', $request->manufacturer_id);   
        }
        
        if($request->has('page')){
           $tapeRecorders = $tapeRecorders->paginate(config('app.paginate'));
        }else{  
           $tapeRecorders = $tapeRecorders->get();
        }

        //info($tapeRecorders);
        foreach($tapeRecorders as $tapeRecorder){
            $manufacturer = Manufacturer::find($tapeRecorder->manufacturer_id);
            $tapeRecorder->manufacturer_name = '';
            if(!empty($manufacturer)){
                $tapeRecorder->manufacturer_name = $manufacturer->name;
            }
            $tapeRecorder->brand_name = '';
            $brand = Brand::find($tapeRecorder->brand_id);
            if(!empty($brand)){
                $tapeRecorder->brand_name = $brand->name;
            }
            $images = array();
            $images = $this->tapeRecorderImageById($tapeRecorder->id);            
            foreach($images as $image) {
                if($tapeRecorder->main_image == $image->type){
                    if(!empty($image->thumbnail)){
                        $image->thumbnail = \Storage::url($image->thumbnail);
                    }
                }
                if(!empty($image->image)){
                    $image->image = \Storage::url($image->image);
                }
            }
            $tapeRecorder->images = $images;
        }
        return $tapeRecorders;
    }
}