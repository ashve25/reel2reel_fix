<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Base\Models\TapeHeadPreampImage;
use DB;

class TapeHeadPreamp extends Model
{
	use SoftDeletes;
 
    protected $fillable = ['manufacturer_id', 'model', 'adjustable_gain', 'adjustable_loading', 'playback_eq', 'level_controls', 'isolated_power_supply', 'signal_noise_ratio', 'tubes', 'suggested_input_wiring', 'impedance', 'accessories', 'description', 'main_image', 'electronic_id', 'input_id', 'output_id', 'user_id', 'status'];

	//protected $table = 'tape_head_preamps';

    private function getBaseQuery(){
        return self::select('tape_head_preamps.*', 'manufacturers.name as manufacturer_name', 'electronics.name as electronic_name', 'inputs.name as input_name', 'outputs.name as output_name')
            ->leftjoin('manufacturers', 'manufacturers.id', '=', 'tape_head_preamps.manufacturer_id')
            ->leftjoin('electronics', 'electronics.id', '=', 'tape_head_preamps.electronic_id')
            ->leftjoin('inputs', 'inputs.id', '=', 'tape_head_preamps.input_id')
            ->leftjoin('outputs', 'outputs.id', '=', 'tape_head_preamps.output_id');
    }


    public function getTapeHeadPreampList($request){
    	$tapeHeadPreamps = $this->getBaseQuery();

        if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            info($filters);
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $tapeHeadPreamps = $tapeHeadPreamps->whereIn($key, $value);
            }
        }
        
    	if($request->has('page')){
    		$tapeHeadPreamps = $tapeHeadPreamps->paginate(config('app.paginate'));
    	}
    	else{
    		$tapeHeadPreamps = $tapeHeadPreamps->get();	
    	}
    	foreach ($tapeHeadPreamps as $key => $tapeHeadPreamp) {
    		$tapeHeadPreampImages = new TapeHeadPreampImage;
    		$tapeHeadPreamp->images = $tapeHeadPreampImages->getTapeHeadPreamImages($tapeHeadPreamp->id);

    		foreach($tapeHeadPreamp->images as $image) {
                if($tapeHeadPreamp->main_image == $image->type){
                    if(!empty($image->thumbnail)){
                        $image->thumbnail = \Storage::url($image->thumbnail);
                    }
                }
                if(!empty($image->image)){
                    $image->image = \Storage::url($image->image);
                }
            }
    	}
    	return $tapeHeadPreamps;
    }

    public function getTapeHeadPreampDetails($tapeHeadPreampId){
    	$tapeHeadPreamp = $this->getBaseQuery()->where('tape_head_preamps.id', $tapeHeadPreampId)->first();
    	$tapeHeadPreampImages = new TapeHeadPreampImage;
		$tapeHeadPreamp->images = $tapeHeadPreampImages->getTapeHeadPreamImagesOrderByType($tapeHeadPreamp->id);

        foreach($tapeHeadPreamp->images as $image) {
        	if(!empty($image->image)){
                $image->image = \Storage::url($image->image);
            }
            if(!empty($image->thumbnail)){
                $image->thumbnail = \Storage::url($image->thumbnail);
            }
        }
        info($tapeHeadPreamp);
        return $tapeHeadPreamp;
    }

	public function createTapeHeadPreamp($request)
	{
		DB::transaction(function() use ($request) {
			$tapeHeadPreamp = self::create($request->all());
			if(!empty($tapeHeadPreamp) && $request->has('images')){
				$tapeHeadPreampImages = new TapeHeadPreampImage; 
				$tapeHeadPreampImages->insertTapeHeadPreamImages($tapeHeadPreamp->id, $request);
			}
		});
	}


	public function updateTapeHeadPreamp($request, $id)
	{
		DB::transaction(function() use ($request, $id) {
			$tapeHeadPreamp = self::where('id', $id)->first();
			$tapeHeadPreamp->update($request->except('_method'));
			if(!empty($tapeHeadPreamp) && $request->has('images')){
				$tapeHeadPreampImages = new TapeHeadPreampImage; 
				$tapeHeadPreampImages->updateTapeHeadPreamImages($tapeHeadPreamp->id, $request);
			}
		});
	}

}