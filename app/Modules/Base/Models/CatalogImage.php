<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Modules\Base\Models\FileResizeHelper;
use Illuminate\Support\Str;

class CatalogImage extends Model
{
	use FileResizeHelper;

    public function getOtherImages($id)
    {
    	$filePath = config('filesystems.disks.s3.endpoint').'/'.config('filesystems.disks.s3.bucket').'/';
    	return self::select(DB::raw("CONCAT('".$filePath."', catalog_images.image) as other_image"), DB::raw("CONCAT('".$filePath."', catalog_images.thumbnail) as thumbnail_image"), 'catalog_images.id')
                ->where('catalog_images.catalog_id', $id)
    			->where('catalog_images.type', 1)
    			->get();
    }


    public function storeCatalogImages($catalog, $request){
    	   if($request->has('cover_picture') && !empty($request->cover_picture)){
                $catalogImage = new self;
                $catalogImage->catalog_id = $catalog->id;
                $catalogImage->type = 0;

                $thumbnail = $this->resizeAndStoreBase64($request->cover_picture, "catalogs", 300, 300);
                if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
                $catalogImage->thumbnail = $thumbnail;

                $coverPicture = str_replace('data:image/jpeg;base64,', '', $request->cover_picture);
                $coverPicture = file_get_contents($coverPicture);
                //$coverPicture = base64_decode($coverPicture);
                $hash = Str::random(40);
                $coverPictureName = "catalogs/".$hash.".png";
                \Storage::put($coverPictureName, $coverPicture);
                $catalogImage->image = $coverPictureName;
                $catalogImage->save();
            }

            if($request->has('other_pictures') && !empty($request->other_pictures)){
                foreach ($request->other_pictures as $key => $otherPicture) {
                    $catalogImage = new self;
                    $catalogImage->catalog_id = $catalog->id;
                    $catalogImage->type = 1;

                    $hash = Str::random(40);
                    $otherPictureName = "catalogs/".$hash.".png";
                    $otherPicture = str_replace('data:image/jpeg;base64,', '', $otherPicture);
                    if($request->has('backend')){
                        $otherPicture = base64_decode($otherPicture);
                    }
                    else{
                        $otherPicture = file_get_contents($otherPicture);
                    }
                    
                    \Storage::put($otherPictureName, $otherPicture);
                    $catalogImage->image = $otherPictureName;
                    
                    $thumbnail = $this->resizeAndStoreBase64($otherPicture, "catalogs", 300, 300);
                    if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
                    $catalogImage->thumbnail = $thumbnail;
                    $catalogImage->save();
                }
            }
    }


    public function updateCatalogImages($catalog, $request){

        if($request->has('cover_picture')  && !empty($request->cover_picture)){
            $catalogImage = self::find($request->cover_image_id);
            $catalogImage->catalog_id = $catalog->id;
            $catalogImage->type = 0;

            $coverPicture = str_replace('data:image/jpeg;base64,', '', $request->cover_picture);
            $coverPicture = file_get_contents($coverPicture);
            $hash = Str::random(40);
            $coverPictureName = "catalogs/".$hash.".png";
            \Storage::put($coverPictureName, $coverPicture);
            $catalogImage->image = $coverPictureName;

            $thumbnail = $this->resizeAndStoreBase64($request->cover_picture, "catalogs", 300, 300);
            if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
            $catalogImage->thumbnail = $thumbnail;

            $catalogImage->save();
        }

        $otherPicSql = self::where('catalog_id', $catalog->id)->where('type', 1);
        if($request->has('other_pictures')  && !empty($request->other_pictures)){
            $newIds = [];
            foreach ($request->other_pictures as $key => $otherPicture) {
                $otherPicture = json_decode($otherPicture, true);
                $ids = array_only($otherPicture, 'id');
                if(isset($ids['id']) && !empty($ids['id'])){
                    array_push($newIds, $ids['id']);
                }
                $catalogImage = new self;
                if(isset($otherPicture['id']) && !empty($otherPicture['id'])){
                    $catalogImage = self::find($otherPicture['id']);
                }
                $catalogImage->catalog_id = $catalog->id;
                $catalogImage->type = 1;
                if (str_contains($otherPicture['other_image'], 'data:image/jpeg;base64')){
                    $otherImage = str_replace('data:image/jpeg;base64,', '', $otherPicture['other_image']);
                    $otherImage = base64_decode($otherImage);
                    $hash = Str::random(40);
                    $otherPictureName = "catalogs/".$hash.".png";
                    \Storage::put($otherPictureName, $otherImage);
                    $catalogImage->image = $otherPictureName;
                    
                    $thumbnail = $this->resizeAndStoreBase64($otherPicture['other_image'], "catalogs", 300, 300);
                    if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
                    $catalogImage->thumbnail = $thumbnail;
                }
                $catalogImage->save();

                array_push($newIds, $catalogImage->id); 
            }
            if(!empty($newIds))
            {   
                $newIds = array_unique($newIds);
                info($newIds);
                $otherPicSql->whereNotIn('id', $newIds)->delete();
            }
        }else{
            $otherPicSql->delete();
        }
    }
}
