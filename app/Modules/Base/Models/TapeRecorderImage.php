<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class TapeRecorderImage extends Model
{
	protected $table = 'tape_recorder_images';

}