<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;

class BrandImage extends Model
{
	protected $table = 'brand_images';

}