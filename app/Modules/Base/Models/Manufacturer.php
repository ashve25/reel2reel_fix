<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Modules\Base\Models\ManufacturerImage;
use DB;
use App\Http\Traits\APIResponseTrait;

class Manufacturer extends Model
{
	protected $table = 'manufacturers';

	protected $fillable = ['name', 'origin_country', 'year_to', 'year_from', 'history'];

	use APIResponseTrait;


	private function getBaseQuery(){
		return self::select('manufacturers.*');
	}

	public function createManufacturer(Request $request){
		DB::transaction(function() use($request){
			$manufacturer = self::create($request->except(['founder', 'images']));
			if($request->has('founder')){
				$path = $request->founder->store('public/images');
				$manufacturer_image = new ManufacturerImage();
				$manufacturer_image->manufacturer_id = $manufacturer->id;
				$manufacturer_image->type = 0;
				$manufacturer_image->image = $path;
				$manufacturer_image->save();
			}

			if($request->has('images')){
				foreach($request->images as $index => $image){
					$path = $image->store('public/images');
					$manufacturer_image = new ManufacturerImage();
					$manufacturer_image->manufacturer_id = $manufacturer->id;
					$manufacturer_image->type = 1;
					$manufacturer_image->image = $path;
					$manufacturer_image->save();
				}
			}
		});
		$data = "success";
		$this->success($data);
	}


	public function getManufacturers($request){
		$manufacturers = $this->getBaseQuery();
        if($request->has('page')){
            $manufacturers = $manufacturers->paginate(config('app.paginate'));
        }else{  
           $manufacturers = $manufacturers->get();
        }
        foreach ($manufacturers as $key => $manufacturer) {
        	$manufacturer->images = ManufacturerImage::where('manufacturer_id', $manufacturer->id)->orderBy('type','asc')->get();

	        foreach ($manufacturer->images as $image) {
	        	$image->image = \Storage::url($image->image);
	        }
        }
        return $manufacturers;
	}

	public function getManufacturer($id){
		$manufacturer = $this->getBaseQuery()->where('manufacturers.id', $id)->first();
        $manufacturer->images = ManufacturerImage::where('manufacturer_id', $id)->orderBy('type','asc')->get();

        foreach ($manufacturer->images as $image) {
        	$image->image = \Storage::url($image->image);
        }
        info($manufacturer);
        return $manufacturer;
	}

	public function updateManufacturer(Request $request, $id){
		info($request);
		DB::transaction(function() use($request, $id){
			$manufacturer = self::where('id', $id)->update($request->except(['_method', 'founder', 'images']));
			if($request->has('founder')){
				$path = $request->founder->store('public/images');
				$manufacturer_image = new ManufacturerImage();
				$manufacturer_image->manufacturer_id = $id;
				$manufacturer_image->type = 0;
				$manufacturer_image->image = $path;
				$manufacturer_image->save();
			}
			//PENDING IMAGE UPDATE
			if($request->has('images')){
				foreach($request->images as $index => $image){
					$path = $image->store('public/images');
					$manufacturer_image = new ManufacturerImage();
					$manufacturer_image->manufacturer_id = $id;
					$manufacturer_image->type = 1;
					$manufacturer_image->image = $path;
					$manufacturer_image->save();
				}
			}
		});
		$data = "success";
		$this->success($data);
	}

	public function deleteManufacturer($id){
		DB::transaction(function() use ($id){
            ManufacturerImage::where('manufacturer_id',$id)->delete();
            Manufacturer::find($id)->delete();
        });
	}

	public function deleteImage($id){
		info('delete image');
		ManufacturerImage::find($id)->delete();
		$data = "success";
		$this->success($data);
	}

}