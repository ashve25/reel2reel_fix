<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Modules\Base\Models\TapeImage;
use App\Modules\Base\Models\TapeTrackList;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\Base\Models\FileResizeHelper;

class Tape extends Model
{
	use SoftDeletes;
	use FileResizeHelper;
    //
    protected $fillable = ['title_composition', 'artist_composer', 'release_number', 'release_date', 'credits', 'label',
							'classical_id', 'genre_id', 'track_id', 'speed_id', 'channel_id', 'reel_size_id', 'noise_reduction_id', 'duplication_id',
							'master_duplication', 'description', 'user_id', 'status'];


	private function getBaseQuery(){
		return self::select('tapes.*', 'classical.name as classical_name', 'genres.name as genre_name', 'tracks.name as track_name', 'channels.name as channel_name', 'reel_sizes.name as reel_size_name', 'noise_reductions.name as noise_reduction_name', 'duplications.name as duplication_name', 'tape_images.thumbnail', 'tape_images.image')
				->leftjoin('classical', 'classical.id', '=', 'tapes.classical_id')
				->leftjoin('genres', 'genres.id', '=', 'tapes.genre_id')
				->leftjoin('tracks', 'tracks.id', '=', 'tapes.track_id')
				->leftjoin('reel_sizes', 'reel_sizes.id', '=', 'tapes.reel_size_id')
				->leftjoin('channels', 'channels.id', '=', 'tapes.channel_id')
				->leftjoin('noise_reductions', 'noise_reductions.id', '=', 'tapes.noise_reduction_id')
				->leftjoin('duplications', 'duplications.id', '=', 'tapes.duplication_id')
				->leftjoin('tape_images', "tape_images.tape_id", '=', 'tapes.id')
				->where('tape_images.type', '0');;
				
	}							



	
	public function createTape(Request $request) {
		DB::transaction(function() use ($request) {
			$releaseDate = Carbon::createFromFormat('d/m/Y', $request->release_date)->toDateString();

			$request->merge(['release_date' => $releaseDate]);
			$request->merge(['artist_composer'=> $request->artist_composer_first_name . ' '. $request->artist_composer_last_name]);
			$request->merge(['credits'=> json_encode($request->credits)]);
			
			$tape = self::create($request->all());
			foreach($request->images as $index => $image) {
				$thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
				if(empty($thumbnail)) throw new \Exception("Unable to resize image!");
				$fullImagePath = $image->store("tapes");
				$tapeImage = new TapeImage;
				$tapeImage->tape_id = $tape->id;
				$tapeImage->type = $index;
				$tapeImage->image = $fullImagePath;
				$tapeImage->thumbnail = $thumbnail;
				$tapeImage->save();
			}

			foreach($request->tracks['a'] as $key => $value) {
				$value['tape_id'] = $tape->id;
				TapeTrackList::create($value);
			}

			foreach($request->tracks['b'] as $key => $value) {
				$value['tape_id'] = $tape->id;
				TapeTrackList::create($value);
			}

		});
	}

	public function tapeDetails($id) {
		$tape = $this->getBaseQuery()->where('tapes.id', $id)->first();
		if(!empty($tape)){
			$tape->images = TapeImage::where('tape_id', $id)->orderBy('type', 'asc')->get();
	        $tape->trackObjectsSideA = TapeTrackList::where('tape_id', $id)->where('side', 'a')->orderBy('id', 'asc')->get();
	        $tape->trackObjectsSideB = TapeTrackList::where('tape_id', $id)->where('side', 'b')->orderBy('id', 'asc')->get();

	        $artistName = explode(" ", $tape->artist_composer);
	        $tape->artist_composer_first_name = isset($artistName[0]) ? $artistName[0] : "";
	        $tape->artist_composer_last_name = isset($artistName[1]) ? $artistName[1] : "";
	        $tape->release_date = Carbon::createFromFormat("Y-m-d", $tape->release_date)->format('d/m/Y');

	        foreach($tape->images as $image) {
	        	if(!empty($image->image)){
	        		$image->image = \Storage::url($image->image);
	        	}
	        	if(!empty($image->thumbnail)){
	        		$image->thumbnail = \Storage::url($image->thumbnail);
	        	}
	        }
		}
        return ($tape);
	}

	public function updateTape(Request $request, $id) {
		$fillable = $this->fillable;
		DB::transaction(function() use($request, $id, $fillable) {
			$releaseDate = Carbon::createFromFormat('d/m/Y', $request->release_date)->toDateString();

			$request->merge(['release_date' => $releaseDate]);
			$request->merge(['artist_composer'=> $request->artist_composer_first_name . ' '. $request->artist_composer_last_name]);
			$request->merge(['credits'=> json_encode($request->credits)]);

			$tape = self::where('id', $id)->first();
			$tape->update($request->only($fillable));
			if($request->has('images')){
				foreach($request->images as $index => $image) {
					
					$fullImagePath = $image->store("tapes");
					$thumbnail = $this->resizeAndStore($image, "tapes", 300, 300);
					if(empty($thumbnail)) throw new \Exception("Unable to resize image!");

					$tapeImage = TapeImage::where('type', $index)->where('tape_id', $id)->first();
					if(empty($tapeImage)){
						$tapeImage = new TapeImage();
						$tapeImage->tape_id = $id;
						$tapeImage->type = $index;
						$tapeImage->image = $fullImagePath;
						$tapeImage->thumbnail = $thumbnail;
						$tapeImage->save();
					}
					$tapeImage->image = $fullImagePath;
					$tapeImage->thumbnail = $thumbnail;
					$tapeImage->save();
				}
			}

			foreach($request->tracks['a'] as $key => $value) {
				$value['tape_id'] = $tape->id;
				TapeTrackList::where('id', $value['id'])->update($value);
			}

			foreach($request->tracks['b'] as $key => $value) {
				$value['tape_id'] = $tape->id;
				TapeTrackList::where('id', $value['id'])->update($value);
			}

		});
	}

	public function tapeList($request){
		info($request->all());

		$tapes = $this->getBaseQuery();

		if($request->has('tracks')){
			if($request->tracks == '2'){
				$tapes = $tapes->whereIn('tapes.track_id',['2','3']);

			}elseif($request->tracks == '4'){
				$tapes = $tapes->whereIn('tapes.track_id',['1','4']);
			}
		}

		if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            info($filters);
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $tapes = $tapes->whereIn($key, $value);
            }
        }
		
		if($request->has('page')){
           $tapes = $tapes->orderBy('tapes.id', 'asc')->paginate(config('app.paginate'));
        }else{  
           $tapes = $tapes->orderBy('tapes.id', 'asc')->get();
        }		

		foreach($tapes as $tape) {
			if(!empty($tape->thumbnail))
				$tape->thumbnail = \Storage::url($tape->thumbnail);
			else
				$tape->thumbnail = "";

			if(!empty($tape->image)){
				$tape->image = \Storage::url($tape->image);
			}
		}
		return $tapes;
	}


	public function getUserTapes($userId, Request $request){
		$tapes = $this->getBaseQuery()->where('tapes.user_id', $userId)->where('tapes.status', 1);

		if($request->has('tracks')){
			if($request->tracks == '2'){
				$tapes = $tapes->whereIn('tapes.track_id',['2','3']);

			}elseif($request->tracks == '4'){
				$tapes = $tapes->whereIn('tapes.track_id',['1','4']);
			}
		}

		if($request->has('filters')){
            $filters = json_decode($request->filters, true);
            info($filters);
            foreach ($filters as $key => $value) {
                if(!empty($value))
                    $tapes = $tapes->whereIn($key, $value);
            }
        }
		
		if($request->has('page')){
           $tapes = $tapes->paginate(config('app.paginate'));
        }else{  
           $tapes = $tapes->get();
        }		

		foreach($tapes as $tape) {
			if(!empty($tape->thumbnail))
				$tape->thumbnail = \Storage::url($tape->thumbnail);
			else
				$tape->thumbnail = "";

			if(!empty($tape->image)){
				$tape->image = \Storage::url($tape->image);
			}
		}
		return $tapes;	
	}
}