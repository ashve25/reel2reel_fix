<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;

class ManufacturerImage extends Model
{
	protected $table = 'manufacturer_images';

}