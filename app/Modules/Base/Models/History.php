<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;


class History extends Model {

    protected $table = 'history';

    protected $fillable = ['name','user_id','description', 'slug', 't_count'];


    private function getBaseQuery(){
        return self::select('*');
    }                
                    

    public function createHistory(Request $request) {
        return self::create($request->all());
        
    }

    public function getHistory($id){
        return $this->getBaseQuery()->where('id', $id)->first();
    }

    public function historyList($request){
       
        $historyList = $this->getBaseQuery();
        if($request->has('page')){
           $historyList = $historyList->paginate(config('app.paginate'));
        }else{  
           $historyList = $historyList->get();
        }
		return $historyList;
    }

    public function updateHistory(Request $request, $id) {
        $fillable = $this->fillable;
        self::find($id)->update($request->only($fillable));
    }

    public function deleteHistory($id){
       self::find($id)->delete();
    }
}