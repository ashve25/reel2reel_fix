<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Modules\Base\Models\CatalogImage;
use Illuminate\Database\Eloquent\SoftDeletes;
class Catalog extends Model
{
    use SoftDeletes;
    private function getBaseQuery()
    {
    	$filePath = config('filesystems.disks.s3.endpoint').'/'.config('filesystems.disks.s3.bucket').'/'; 
    	return self::select('catalogs.*', 'catalog_types.name as catalog_type', 
    			DB::raw("CONCAT('".$filePath."', catalog_images.image) as cover_image"), DB::raw("CONCAT('".$filePath."', catalog_images.thumbnail) as thumbnail_image"), 'catalog_images.id as cover_image_id')
    		->join('catalog_types', 'catalog_types.id', '=', 'catalogs.type')
    		->join('catalog_images', 'catalog_images.catalog_id', '=', 'catalogs.id')
    		->where('catalog_images.type', 0);
    }


    public function getCatalogs($request){
        $catalogs = $this->getBaseQuery();
        if($request->has('type')){
            $catalogs->where('catalogs.type', $request->type);
        }
    	$catalogs = $catalogs->orderBy('catalogs.updated_at', 'desc')->paginate(config('app.paginate'));
        foreach ($catalogs as $key => $catalog) {
            $catalogImage = new CatalogImage;
            $catalog->images = $catalogImage->getOtherImages($catalog->id);
        }
        return $catalogs;
    }

    public function getCatalog($id){
    	return $catalogs = $this->getBaseQuery()->where('catalogs.id', $id)->first();
    }

    public function storeCatalog($request){
         DB::transaction(function() use($request){
            $catalog = new self;
            $catalog->title = $request->title;
            $catalog->type = $request->type;
            $catalog->description = $request->description;
            if($request->has('status')){
                $catalog->status = $request->status;    
            }
            if($request->has('user_id')){
                $catalog->user_id = $request->user_id;    
            }
            $catalog->save();

            $catalogImage = new CatalogImage;
            $catalogImage->storeCatalogImages($catalog, $request);
        });
    }

    public function updateCatalog($request, $id){
        DB::transaction(function() use($request, $id){
            $catalog = self::find($id);
            $catalog->title = $request->title;
            $catalog->type = $request->type;
            $catalog->description = $request->description;
            $catalog->save();
            
            $catalogImage = new CatalogImage;
            $catalogImage->updateCatalogImages($catalog, $request);
        }); 
    }

    public function getCatalogTitle($catalogId){
        return self::select('title')->where('id', $catalogId)->first()->title;
    }
}
