<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use App\Http\Traits\APIResponseTrait;
use Auth;

class FavouriteInputExpander extends Model
{
	protected $table = 'favourite_expanders';

	protected $fillable = ['user_id','expander_id'];

	public function addInputExpanderAsFavourite($request)
	{
		$this->user_id = Auth::user()->id;
		$this->expander_id = $request->expander_id;
		$this->save();
	}

	public function deleteInputExpanderFromFavourite($request){
		$sql = self::where('expander_id', $request->expander_id)->where('user_id', Auth::user()->id);
		$find = $sql->first();

		if(!empty($find)){
			$sql->delete();
		}
	}

	public function checkInputExpanderAddedAsFavourite($expanderId)
    {
        return self::where('expander_id', $expanderId)->where('user_id', Auth::user()->id)->count()>0;
    }

}