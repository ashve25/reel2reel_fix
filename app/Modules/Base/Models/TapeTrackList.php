<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Modules\Base\Models\TapeImage;

class TapeTrackList extends Model
{
	protected $table = 'tape_track_list';

	protected $fillable = ['tape_id', 'time_length', 'title', 'side'];
}