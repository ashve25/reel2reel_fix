<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\APIResponseTrait;
use DB;
use App\Modules\Base\Models\Tape as Tape;
use Carbon\Carbon;
use Validator;

class TapeController extends Controller
{
    use APIResponseTrait;

    private $tape;

    public function __construct(Tape $tape) {
        $this->tape = $tape;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tapes = $this->tape->tapeList($request);
        
        return $this->success($tapes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_composition' => 'required|unique:tapes',
            'artist_composer_first_name' => 'required',
            'artist_composer_last_name' => 'required',
            'release_number' => 'required',
            'release_date' => 'required',
            'label' => 'required',
            'classical_id'=> 'required',
            'genre_id' => 'required',
            'speed_id' => 'required',
            'reel_size_id' => 'required',
            'images' => 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        //$tape = new Tape;
        //info($request->all());
        $this->tape->createTape($request);
        return $this->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->tape->tapeDetails($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title_composition' => 'required|unique:tapes,title_composition,'.$id,
            'artist_composer_first_name' => 'required',
            'artist_composer_last_name' => 'required',
            'release_number' => 'required',
            'release_date' => 'required',
            'label' => 'required',
            'classical_id'=> 'required',
            'genre_id' => 'required',
            'speed_id' => 'required',
            'reel_size_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->tape->updateTape($request, $id);
        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMasterData() {
        $data['classicals'] = DB::table('classical')->whereNull('deleted_at')->get();
        $data['genres'] = DB::table('genres')->whereNull('deleted_at')->get();
        $data['tracks'] = DB::table('tracks')->whereNull('deleted_at')->get();
        $data['speeds'] = DB::table('speeds')->whereNull('deleted_at')->get();
        $data['channels'] = DB::table('channels')->whereNull('deleted_at')->get();
        $data['reel_sizes'] = DB::table('reel_sizes')->whereNull('deleted_at')->get();
        $data['noise_reductions'] = DB::table('noise_reductions')->whereNull('deleted_at')->get();
        $data['duplications'] = DB::table('duplications')->whereNull('deleted_at')->get();

        return $this->success($data);
    }
}
