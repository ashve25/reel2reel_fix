<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Base\Models\History;
use App\Http\Traits\APIResponseTrait;
use Validator;
class HistoryController extends Controller
{
    use APIResponseTrait;

    private $history;

    public function __construct(History $history){
        $this->history = $history;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $historyList = $this->history->historyList($request);
        return $this->success($historyList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:history',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->history->createHistory($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success($this->history->getHistory($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
            'name' => 'required|unique:history,name,'.$id,
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->history->updateHistory($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->history->deleteHistory($id);
    }
}
