<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\APIResponseTrait;
use DB;
use App\Modules\Base\Models\TapeHeadPreamp;
use Carbon\Carbon;
use Validator;

class TapeHeadPreampController extends Controller
{
    use APIResponseTrait;

    private $tapeHeadPreamp;

    public function __construct(TapeHeadPreamp $tapeHeadPreamp) {
        $this->tapeHeadPreamp = $tapeHeadPreamp;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->success($this->tapeHeadPreamp->getTapeHeadPreampList($request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'required|unique:tape_head_preamps,model',
            'manufacturer_id' => 'required',
            'electronic_id' => 'required',
            'images' => 'required',
            'main_image' => 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->tapeHeadPreamp->createTapeHeadPreamp($request);
        return $this->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->tapeHeadPreamp->getTapeHeadPreampDetails($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'required|unique:tape_head_preamps,model,'.$id,
            'manufacturer_id' => 'required',
            'electronic_id' => 'required',
            'main_image' => 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        return $this->tapeHeadPreamp->updateTapeHeadPreamp($request, $id);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
