<?php
namespace App\Modules\Base\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\APIResponseTrait;
use App\Http\Traits\JWTTrait;
use App\Events\SendMailEvent;
use App\User;
use Carbon\Carbon;
use Validator;
use Hash;

class AuthController extends Controller {

	use APIResponseTrait, JWTTrait;

	public function __construct(User $user) {
		$this->user = $user;
	}

	public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'same:password',
            'type' => 'required',
        ]);
        if($validator->fails()){
            $errorMessages = $validator->errors()->all();
			$errorMessages = implode(', ', $errorMessages);
            return $this->error(400, $errorMessages, 400);
		}
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = Hash::make($request->password);
		$user->type = $request->type;//Hidden Field
		$key = $this->generateKey();
		$user->verification_key = $key;		
		$user->token_timestamp_updated_at = Carbon::now()->toDateTimeString();		
		$user->save();
		$eventData['data'] = ['full_name' => $user->name, 
			'msg' => config('strings.emails.messages.verify_email'), 
			'link' => url('/users/'.$user->id.'/verify/'.$user->verification_key)
		];
		$eventData['email'] = $user->email;
		$eventData['subject'] = config('strings.emails.subjects.verify_email');
		$eventData['page'] = 'Base::emails.account-verify-email';
		info($eventData);
		event(new SendMailEvent($eventData));
		$response['user_registered'] = true;
        return $this->success($response);

	}

	public function verifyEmail($id, $key, Request $request){
		$user = $this->user->find($id);
        if($user->verification_key !== $key){
            return $this->error(400, config('strings.warning.invalid_link'), 400);
        }
        $user->verifyMe();
        return $this->success();
	}

	public function login(Request $request){
		info($request);
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required|min:6'
		]);
		if($validator->fails()){
			$errorMessages = $validator->errors()->all();
            $errorMessages = implode(', ', $errorMessages);
            return $this->error(400, $errorMessages, 400);
		}
		$user = $this->user->where('email', $request->email)->first(); 
		if($user !== null){
			if (!Hash::check($request->password, $user->password)) {
				return $this->error(400, config('strings.warning.incorect_password'), 400);
			} 
			if($user->verified != 1){
				return $this->error(400, config('strings.warning.unverified_user'), 400);        
			}
			$user->token_timestamp_updated_at = Carbon::now()->toDateTimeString();
			$user->save();
			if($user->type == 'Subscriber' || $user->type == 'Author'){
				session()->put('user_id', $user->id);
				return view('Frontend::home');	
			}
			if($user->type == 'Admin' || $user->type == 'Moderator'){
				$data['token'] = $this->tokenGenerator($user->id, $user->user_type, $user->token_timestamp_updated_at);
				$data['user'] = $this->user->getUser($user->id);
                info($this->user->getUser($user->id));
				return $this->success($data);
			}
		} 
		else{
			return $this->error(400, config('strings.warning.invalid_user'), 400);
		}

	}

	public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            $errorMessages = implode(', ', $errorMessages);
            return $this->error(400, $errorMessages, 400);
        }else{

            $user = $this->user->where('email', $request->email)->first();
            if($user == null){
                return $this->error(401, config('strings.warning.unauthorized_user'), 401);
            }
            $email = $user->email;
            $key = $this->generateKey();
            $user->verification_key = $key;
            $user->save();
            
            $eventData['data'] = ['full_name' => $user->name, 
                                'msg' => config('strings.emails.messages.forgot_password'), 
                                'link' => url('/users/'.$user->id.'/reset-password/'.$key)
                            ];
            $eventData['email'] = $user->email;
            $eventData['subject'] = config('strings.emails.subjects.forgot_password');
            $eventData['page'] = 'Base::emails.forgot-password-email';
			info($eventData);
            event(new SendMailEvent($eventData));
            $response['send'] = true;
            return $this->success($response);
        }    
    }

	public function resetPasswordForm($id, $key, Request $request){
		$user = $this->user->find($id);
        if($user->verification_key !== $key){
            // $response['data'] = $this->error(400, config('strings.warning.invalid_link'), 400);
            return view('vue')->with('error',config('strings.warning.invalid_link'));
        }else{
            info("in user");
            return view('reset-password')->withId($id);
		}
	}

    public function resetPassword($id, Request $request){
		
        $validator = Validator::make($request->user, [
            'password' => 'required|min:6',
            'confirm_password' => 'same:password'
        ]);
        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            $errorMessages = implode(', ', $errorMessages);
            return $this->error(400, $errorMessages, 400);
        }
		$user = $this->user->find($id);
        if(!$user){
            return $this->error(400, config('strings.warning.invalid_user'), 400);
        }
        
        $user->password = Hash::make($request->user['password']);
        $user->verified = 1;
        $user->verification_key = NULL;
        $user->save();
        return $this->success();
    }

    public function adminLoginForm() {
    	return view('vue');
    }

    public function logout($id){
    	$user = User::find($id);
    	$user->token_timestamp_updated_at = null;
    	$user->save();
    	return $this->success();
    }
}