<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Brand;
use App\Http\Traits\APIResponseTrait;
use Validator;

class BrandController extends Controller
{
    use APIResponseTrait;

    private $brand;

    public function __construct(Brand $brand){
        $this->brand = $brand;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $brands = Brand::select('*');
        if($request->has('page')){
            $brands = $brands->paginate(config('app.paginate'));
        }else{
            $brands = $brands->get();   
        }
        return $this->success($brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:brands',
            'location' => 'required',
            'manufacturer_id' => 'required',
            'founder'=> 'required',
            'year_from' => 'required',
            'year_to' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->brand->createBrand($request);
        return $this->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success($this->brand->getBrand($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:brands,name,'.$id,
            'location' => 'required',
            'manufacturer_id' => 'required',
            'year_from' => 'required',
            'year_to' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->brand->updateBrand($request, $id);
        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->brand->deleteBrand($id);
    }

    public function deleteImage($id){
        $this->brand->deleteImage($id);
    }
}
