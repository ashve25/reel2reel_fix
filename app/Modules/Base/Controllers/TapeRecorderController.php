<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\APIResponseTrait;
use DB;
use App\Modules\Base\Models\TapeRecorder;
use Validator;

class TapeRecorderController extends Controller
{
    use APIResponseTrait;

    private $tapeRecorder;

    public function __construct(TapeRecorder $tapeRecorder) {
        $this->tapeRecorder = $tapeRecorder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tapeRecorders = $this->tapeRecorder->tapeRecorderList($request);
        return $this->success($tapeRecorders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'required|unique:tape_recorders',
            'brand_id' => 'required',
            'serial_number' => 'required',
            'original_price' => 'required',
            'manufacturer_id' => 'required',
            'images' => 'required',
            'main_image'=> 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $data = $this->tapeRecorder->createTapeRecorder($request);
        return $this->success($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->tapeRecorder->getTapeRecorderDetails($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'required|unique:tape_recorders,model,'.$id,
            'brand_id' => 'required',
            'serial_number' => 'required',
            'original_price' => 'required',
            'manufacturer_id' => 'required',
            'main_image'=> 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->tapeRecorder->updateTapeRecorder($request, $id);
        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $this->tapeRecorder->deleteTapeRecorder($id);
    }

    public function getMasterData() {
        $data['brands'] = DB::table('brands')->whereNull('deleted_at')->get();
        $data['manufacturers'] = DB::table('manufacturers')->whereNull('deleted_at')->get();
        $data['head_configuration'] = DB::table('head_configuration')->whereNull('deleted_at')->get();
        $data['number_of_heads'] = DB::table('number_of_heads')->whereNull('deleted_at')->get();
        $data['speeds'] = DB::table('speeds')->whereNull('deleted_at')->get();
        $data['tracks'] = DB::table('tracks')->whereNull('deleted_at')->get();
        $data['head_composition'] = DB::table('head_composition')->whereNull('deleted_at')->get();
        $data['equalizations'] = DB::table('equalization')->whereNull('deleted_at')->get();
        $data['voltages'] = DB::table('voltage')->whereNull('deleted_at')->get();
        $data['auto_reverses'] = DB::table('auto_reverse')->whereNull('deleted_at')->get();
        $data['applications'] = DB::table('application')->whereNull('deleted_at')->get();
        $data['max_reel_sizes'] = DB::table('max_reel_size')->whereNull('deleted_at')->get();
        $data['noise_reductions'] = DB::table('noise_reductions')->whereNull('deleted_at')->get();
        $data['motors'] = DB::table('motors')->whereNull('deleted_at')->get();
        info($data);
        
        return $this->success($data);
    }

    public function deleteImage($id){
        $this->tapeRecorder->deleteImage($id);
    }
}
