<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Base\Models\Manufacturer;
use App\Http\Traits\APIResponseTrait;
use Validator;
class ManufacturerController extends Controller
{
    use APIResponseTrait;

    private $manufacturer;

    public function __construct(Manufacturer $manufacturer){
        $this->manufacturer = $manufacturer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $manufacturers = $this->manufacturer->getManufacturers($request);
        return $this->success($manufacturers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:manufacturers',
            'origin_country' => 'required',
            'year_from' => 'required',
            'year_to' => 'required',
            'founder'=> 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->manufacturer->createManufacturer($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->success($this->manufacturer->getManufacturer($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
            'name' => 'required|unique:manufacturers,name,'.$id,
            'origin_country' => 'required',
            'year_from' => 'required',
            'year_to' => 'required'
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->manufacturer->updateManufacturer($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->manufacturer->deleteManufacturer($id);
    }

    public function deleteImage($id){
        $this->manufacturer->deleteImage($id);
    }
}
