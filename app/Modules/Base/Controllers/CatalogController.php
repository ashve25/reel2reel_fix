<?php

namespace App\Modules\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Modules\Base\Models\Catalog;
use DB;
use App\Modules\Base\Models\CatalogImage;
use App\Http\Traits\APIResponseTrait;
use Validator;
class CatalogController extends Controller
{
    use APIResponseTrait;
    private $catalog;
    private $catalogImage;

    public function __construct(Catalog $catalog, CatalogImage $catalogImage){
        $this->catalog = $catalog;
        $this->catalogImage = $catalogImage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $catalogs = $this->catalog->getCatalogs($request);
        return $this->success($catalogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:catalogs',
            'cover_picture' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->catalog->storeCatalog($request); 
        return $this->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catalog =  $this->catalog->getCatalog($id);
        return $this->success($catalog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:catalogs,title,'.$id,
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessages = $validator->errors()->all();
            return $this->error(400, $errorMessages, 400);
        }
        $this->catalog->updateCatalog($request, $id);
        return $this->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCatelogTypes(){
        return DB::table('catalog_types')->get();
    }

    public function getCatalogOtherImages($id){
        $otherImages = $this->catalogImage->getOtherImages($id);
        return $this->success($otherImages);
    }
}
