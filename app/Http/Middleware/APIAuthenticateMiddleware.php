<?php

namespace App\Http\Middleware;
use Closure;
use App\Http\Traits\APIResponseTrait;
use App\Http\Traits\JWTTrait;
use App\User;

class APIAuthenticateMiddleware
{
    use JWTTrait, APIResponseTrait;

    public function handle($request, Closure $next){
        info($request);
        try{
            $authUser = $this->decodeToken($request);
            $user = User::find($authUser->id);
            info("authuser ".$authUser->token_timestamp_updated_at);
            info("user ".$user->token_timestamp_updated_at);
            if($authUser !== null){
                info('middleware');
                return $next($request);
            }else{
                return $this->error(400, config('strings.warning.invalid_token'), 2000);
            }       
        }
        catch(Exception $e) {
            return $this->error(400, config('strings.warning.invalid_token'), 2000);
        }
    }
}