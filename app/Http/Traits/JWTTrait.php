<?php
namespace App\Http\Traits;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
use App\User;

trait JWTTrait
{
    public function tokenGenerator($userId, $userType, $timeStamp){
        $key = config('app.jwt_key');
        $token = array(
            "iss" => "http://reel.com",
            "aud" => "http://reel.com",
            "user_id" => $userId,
            "timestamp" =>  $timeStamp,
        );
        return $jwt = JWT::encode($token, $key);
    }  

    public function decodeToken($request){
        $jwt = str_replace("Bearer ", "", $request->header('Authorization'));
        $key = config('app.jwt_key');
        $decoded = JWT::decode($jwt, $key, array('HS256')); 
        $user = new User;
        $user->id = $decoded->user_id;
        $user->token_timestamp_updated_at = $decoded->timestamp;
        return $user;
    } 

    public function getCurrentUser($request){
        if(session()->get('user') == null){
            if($request->header('Authorization')){
                return $this->decodeToken($request);
            }
            return null;
        }else{
            return session()->get('user');
        }
    }

    public function fromToken($jwt){
        $key = config('app.jwt_key');
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $userDetails = User::find($decoded->user_id);
        $user = new User;
        $user->id = $decoded->user_id;
        if(property_exists($decoded, 'timestamp')){
            $decoded->timestamp = $decoded->timestamp;
        }else{
            $decoded->timestamp = Carbon::now()->toDateTimeString();
        }
        $user->token_timestamp_updated_at = $decoded->timestamp;
        return $user;   
    }
}

