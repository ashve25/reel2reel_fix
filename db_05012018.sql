--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE application_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE application (
    id integer DEFAULT nextval('application_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE application OWNER TO postgres;

--
-- Name: auto_reverse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_reverse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auto_reverse_id_seq OWNER TO postgres;

--
-- Name: auto_reverse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auto_reverse (
    id integer DEFAULT nextval('auto_reverse_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE auto_reverse OWNER TO postgres;

--
-- Name: brand_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brand_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brand_images_id_seq OWNER TO postgres;

--
-- Name: brand_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brand_images (
    id integer DEFAULT nextval('brand_images_id_seq'::regclass) NOT NULL,
    brand_id integer,
    title text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name text
);


ALTER TABLE brand_images OWNER TO postgres;

--
-- Name: brands_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brands_id_seq OWNER TO postgres;

--
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brands (
    id integer DEFAULT nextval('brands_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    manufacturer_id text[],
    business_to_year text,
    business_from_year text,
    tape_recorders_to_year text,
    tape_recorders_from_year text,
    description text,
    technical_innovations text,
    general_information text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE brands OWNER TO postgres;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    _lft integer DEFAULT 0 NOT NULL,
    _rgt integer DEFAULT 0 NOT NULL,
    parent_id integer,
    name text,
    id integer NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq1 OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq1 OWNED BY categories.id;


--
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE channels (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE channels OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE channels_id_seq OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE channels_id_seq OWNED BY channels.id;


--
-- Name: classical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE classical (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE classical OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE classical_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE classical_id_seq OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE classical_id_seq OWNED BY classical.id;


--
-- Name: duplications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE duplications (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE duplications OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE duplications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE duplications_id_seq OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE duplications_id_seq OWNED BY duplications.id;


--
-- Name: electronics_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE electronics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE electronics_id_seq OWNER TO postgres;

--
-- Name: electronics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE electronics (
    id integer DEFAULT nextval('electronics_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE electronics OWNER TO postgres;

--
-- Name: equalization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE equalization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE equalization_id_seq OWNER TO postgres;

--
-- Name: equalization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE equalization (
    id integer DEFAULT nextval('equalization_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE equalization OWNER TO postgres;

--
-- Name: expander_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expander_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expander_images_seq OWNER TO postgres;

--
-- Name: expander_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expander_images (
    id integer DEFAULT nextval('expander_images_seq'::regclass) NOT NULL,
    expander_id integer,
    image text,
    main_image integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expander_images OWNER TO postgres;

--
-- Name: expanders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expanders (
    id integer NOT NULL,
    manufacturer text,
    model text,
    tape_recorders_usable integer,
    outputs_for_record integer,
    recorder_inputs integer,
    copy_option_decks integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expanders OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expanders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expanders_id_seq OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE expanders_id_seq OWNED BY expanders.id;


--
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE genres (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE genres OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE genres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genres_id_seq OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE genres_id_seq OWNED BY genres.id;


--
-- Name: head_composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_composition_id_seq OWNER TO postgres;

--
-- Name: head_composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_composition (
    id integer DEFAULT nextval('head_composition_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_composition OWNER TO postgres;

--
-- Name: head_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_configuration_id_seq OWNER TO postgres;

--
-- Name: head_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_configuration (
    id integer DEFAULT nextval('head_configuration_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_configuration OWNER TO postgres;

--
-- Name: inputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inputs_id_seq OWNER TO postgres;

--
-- Name: inputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inputs (
    id integer DEFAULT nextval('inputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE inputs OWNER TO postgres;

--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturer_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturer_images_id_seq OWNER TO postgres;

--
-- Name: manufacturer_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturer_images (
    id integer DEFAULT nextval('manufacturer_images_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    name text,
    title text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE manufacturer_images OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturers (
    id integer DEFAULT nextval('manufacturers_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    origin_country text NOT NULL,
    year_to integer,
    year_from integer,
    history jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE manufacturers OWNER TO postgres;

--
-- Name: max_reel_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE max_reel_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE max_reel_size_id_seq OWNER TO postgres;

--
-- Name: max_reel_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE max_reel_size (
    id integer DEFAULT nextval('max_reel_size_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE max_reel_size OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: motors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE motors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE motors_id_seq OWNER TO postgres;

--
-- Name: motors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE motors (
    id integer DEFAULT nextval('motors_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE motors OWNER TO postgres;

--
-- Name: noise_reductions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE noise_reductions (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE noise_reductions OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE noise_reductions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE noise_reductions_id_seq OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE noise_reductions_id_seq OWNED BY noise_reductions.id;


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE number_of_heads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE number_of_heads_id_seq OWNER TO postgres;

--
-- Name: number_of_heads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE number_of_heads (
    id integer DEFAULT nextval('number_of_heads_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE number_of_heads OWNER TO postgres;

--
-- Name: outputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE outputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outputs_id_seq OWNER TO postgres;

--
-- Name: outputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE outputs (
    id integer DEFAULT nextval('outputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE outputs OWNER TO postgres;

--
-- Name: password_resets_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE password_resets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE password_resets_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    id integer DEFAULT nextval('password_resets_seq'::regclass) NOT NULL
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: reel_sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reel_sizes (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE reel_sizes OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reel_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reel_sizes_id_seq OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE reel_sizes_id_seq OWNED BY reel_sizes.id;


--
-- Name: speeds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE speeds (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE speeds OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE speeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE speeds_id_seq OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE speeds_id_seq OWNED BY speeds.id;


--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp (
    id integer DEFAULT nextval('tape_head_preamp_id_seq'::regclass) NOT NULL,
    name text,
    manufacturer_id integer,
    brand_id integer,
    model text,
    adjustable_gain text,
    adjustable_loading text,
    playback_eq text,
    level_controls text,
    isolated_power_supply text,
    signal_noise_ratio text,
    tubes text,
    suggested_input_wiring text,
    impedance text,
    accessories text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp OWNER TO postgres;

--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_images_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_images (
    id integer DEFAULT nextval('tape_head_preamp_images_id_seq'::regclass) NOT NULL,
    tape_head_preamp_id integer NOT NULL,
    title text,
    description text,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp_images OWNER TO postgres;

--
-- Name: tape_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_images_seq OWNER TO postgres;

--
-- Name: tape_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_images (
    id integer DEFAULT nextval('tape_images_seq'::regclass) NOT NULL,
    tape_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_images OWNER TO postgres;

--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorder_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorder_images_id_seq OWNER TO postgres;

--
-- Name: tape_recorder_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorder_images (
    id integer DEFAULT nextval('tape_recorder_images_id_seq'::regclass) NOT NULL,
    tape_recorder_id integer NOT NULL,
    title text,
    description text,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_recorder_images OWNER TO postgres;

--
-- Name: tape_recorders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorders_id_seq OWNER TO postgres;

--
-- Name: tape_recorders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorders (
    id integer DEFAULT nextval('tape_recorders_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    brand_id integer,
    name text,
    model text,
    country_of_manufacture text,
    serial_number text,
    orignal_price numeric,
    release_to_date date,
    release_from_date date,
    belts text,
    frequency_response text,
    wow_flutter text,
    signal_noice_ratio text,
    description text,
    details jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_recorders OWNER TO postgres;

--
-- Name: tapes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tapes (
    id integer NOT NULL,
    title_composition text,
    artist_composer text,
    release_number text,
    release_date date,
    track_list text[],
    credits jsonb,
    label text,
    classical integer,
    genre integer,
    track integer,
    speed integer,
    channel integer,
    reel_size integer,
    noise_reduction integer,
    duplication integer,
    master_duplication integer,
    desciption text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tapes OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tapes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tapes_id_seq OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tapes_id_seq OWNED BY tapes.id;


--
-- Name: tracks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tracks (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tracks OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tracks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tracks_id_seq OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tracks_id_seq OWNED BY tracks.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    verification_key text,
    type text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    verified smallint,
    token_timestamp_updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: voltage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE voltage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE voltage_id_seq OWNER TO postgres;

--
-- Name: voltage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE voltage (
    id integer DEFAULT nextval('voltage_id_seq'::regclass) NOT NULL,
    name text,
    created_at time without time zone,
    updated_at time without time zone
);


ALTER TABLE voltage OWNER TO postgres;

--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq1'::regclass);


--
-- Name: channels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels ALTER COLUMN id SET DEFAULT nextval('channels_id_seq'::regclass);


--
-- Name: classical id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical ALTER COLUMN id SET DEFAULT nextval('classical_id_seq'::regclass);


--
-- Name: duplications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications ALTER COLUMN id SET DEFAULT nextval('duplications_id_seq'::regclass);


--
-- Name: expanders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders ALTER COLUMN id SET DEFAULT nextval('expanders_id_seq'::regclass);


--
-- Name: genres id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres ALTER COLUMN id SET DEFAULT nextval('genres_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: noise_reductions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions ALTER COLUMN id SET DEFAULT nextval('noise_reductions_id_seq'::regclass);


--
-- Name: reel_sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes ALTER COLUMN id SET DEFAULT nextval('reel_sizes_id_seq'::regclass);


--
-- Name: speeds id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds ALTER COLUMN id SET DEFAULT nextval('speeds_id_seq'::regclass);


--
-- Name: tapes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes ALTER COLUMN id SET DEFAULT nextval('tapes_id_seq'::regclass);


--
-- Name: tracks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks ALTER COLUMN id SET DEFAULT nextval('tracks_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO application VALUES (1, 'Consumer', NULL, NULL, NULL);
INSERT INTO application VALUES (2, 'Portable', NULL, NULL, NULL);
INSERT INTO application VALUES (3, 'Semi-Pro', NULL, NULL, NULL);
INSERT INTO application VALUES (4, 'Studio', NULL, NULL, NULL);


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('application_id_seq', 4, true);


--
-- Data for Name: auto_reverse; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auto_reverse VALUES (1, 'Yes', NULL, NULL, NULL);
INSERT INTO auto_reverse VALUES (2, 'No', NULL, NULL, NULL);


--
-- Name: auto_reverse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_reverse_id_seq', 2, true);


--
-- Data for Name: brand_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: brand_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brand_images_id_seq', 1, false);


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: brands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brands_id_seq', 1, false);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: categories_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq1', 1, false);


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO channels VALUES (1, 'Stero', NULL, NULL, NULL);
INSERT INTO channels VALUES (2, 'Mono', NULL, NULL, NULL);
INSERT INTO channels VALUES (3, 'Quad', NULL, NULL, NULL);
INSERT INTO channels VALUES (4, 'trest', NULL, NULL, '2018-01-05 11:00:36');
INSERT INTO channels VALUES (5, 'test', NULL, NULL, '2018-01-05 11:00:43');
INSERT INTO channels VALUES (10, 'test7', NULL, NULL, '2018-01-05 11:02:33');
INSERT INTO channels VALUES (11, 'test6', NULL, NULL, '2018-01-05 11:02:39');
INSERT INTO channels VALUES (9, 'test5', NULL, NULL, '2018-01-05 11:07:41');
INSERT INTO channels VALUES (7, 'test3', NULL, NULL, '2018-01-05 11:18:39');
INSERT INTO channels VALUES (8, 'test4', NULL, NULL, '2018-01-05 11:18:42');
INSERT INTO channels VALUES (6, 'test1', NULL, NULL, '2018-01-05 11:18:45');


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('channels_id_seq', 11, true);


--
-- Data for Name: classical; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO classical VALUES (1, 'Not Classical', NULL, NULL, NULL);
INSERT INTO classical VALUES (2, 'Orchastra', NULL, NULL, NULL);
INSERT INTO classical VALUES (3, 'concerto ', NULL, NULL, NULL);
INSERT INTO classical VALUES (4, 'sontanta ', NULL, NULL, NULL);


--
-- Name: classical_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('classical_id_seq', 9, true);


--
-- Data for Name: duplications; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO duplications VALUES (1, 'Ampex', NULL, NULL, NULL);
INSERT INTO duplications VALUES (2, 'Sterotape ', NULL, NULL, NULL);
INSERT INTO duplications VALUES (3, 'Columbia', NULL, NULL, NULL);
INSERT INTO duplications VALUES (4, 'Magtec', NULL, NULL, NULL);
INSERT INTO duplications VALUES (5, 'Barclay Croker', NULL, NULL, NULL);
INSERT INTO duplications VALUES (6, 'RCA', NULL, NULL, NULL);
INSERT INTO duplications VALUES (7, 'Mecury', NULL, NULL, NULL);
INSERT INTO duplications VALUES (8, 'Other', NULL, NULL, NULL);


--
-- Name: duplications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('duplications_id_seq', 8, true);


--
-- Data for Name: electronics; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO electronics VALUES (1, 'Solid State', NULL, NULL, NULL);
INSERT INTO electronics VALUES (2, 'Tube', NULL, NULL, NULL);
INSERT INTO electronics VALUES (3, 'Hybrid', NULL, NULL, NULL);


--
-- Name: electronics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('electronics_id_seq', 3, true);


--
-- Data for Name: equalization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO equalization VALUES (1, 'NAB', NULL, NULL, NULL);
INSERT INTO equalization VALUES (2, 'IEC', NULL, NULL, NULL);
INSERT INTO equalization VALUES (3, 'AES', NULL, NULL, NULL);
INSERT INTO equalization VALUES (4, 'Other', NULL, NULL, NULL);


--
-- Name: equalization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('equalization_id_seq', 4, true);


--
-- Data for Name: expander_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expander_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expander_images_seq', 1, false);


--
-- Data for Name: expanders; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expanders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expanders_id_seq', 1, false);


--
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO genres VALUES (1, 'Blues ', NULL, NULL, NULL);
INSERT INTO genres VALUES (2, 'Country', NULL, NULL, NULL);
INSERT INTO genres VALUES (3, 'Electronic ', NULL, NULL, NULL);
INSERT INTO genres VALUES (4, 'Folk', NULL, NULL, NULL);
INSERT INTO genres VALUES (5, 'Jazz', NULL, NULL, NULL);
INSERT INTO genres VALUES (6, 'Latin', NULL, NULL, NULL);
INSERT INTO genres VALUES (7, 'New Age', NULL, NULL, NULL);
INSERT INTO genres VALUES (8, 'Pop', NULL, NULL, NULL);
INSERT INTO genres VALUES (9, 'Reggae', NULL, NULL, NULL);
INSERT INTO genres VALUES (10, 'Rock', NULL, NULL, NULL);
INSERT INTO genres VALUES (11, 'Soul', NULL, NULL, NULL);
INSERT INTO genres VALUES (12, 'Soundtrack', NULL, NULL, NULL);
INSERT INTO genres VALUES (13, 'Misc', NULL, NULL, NULL);
INSERT INTO genres VALUES (14, 'CP (CrockPot)', NULL, NULL, NULL);
INSERT INTO genres VALUES (15, 'Other', NULL, NULL, NULL);


--
-- Name: genres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('genres_id_seq', 15, true);


--
-- Data for Name: head_composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_composition VALUES (1, 'F&F', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (2, 'Metal', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (3, 'Glass', NULL, NULL, NULL);


--
-- Name: head_composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_composition_id_seq', 3, true);


--
-- Data for Name: head_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_configuration VALUES (2, 'Mono - Full Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (3, 'Mono -Dual Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (4, 'Mono - Half Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (5, 'sterero ', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (6, 'Sterero - Stacked', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (7, 'Sterero - Staggered', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (8, 'Quad', NULL, NULL, NULL);


--
-- Name: head_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_configuration_id_seq', 7, true);


--
-- Data for Name: inputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO inputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO inputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO inputs VALUES (3, 'XLR (Unbalanced)', NULL, NULL, NULL);


--
-- Name: inputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inputs_id_seq', 3, true);


--
-- Data for Name: manufacturer_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturer_images_id_seq', 1, false);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 1, false);


--
-- Data for Name: max_reel_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO max_reel_size VALUES (1, '3', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (2, '5', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (3, '5 3/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (4, '8 1/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (5, '10 1/2', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (6, '10 1/2+', NULL, NULL, NULL);


--
-- Name: max_reel_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('max_reel_size_id_seq', 6, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations VALUES (3, '2017_12_21_130726_create_categories_nested_set', 1);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 3, true);


--
-- Data for Name: motors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO motors VALUES (1, '1', NULL, NULL, NULL);
INSERT INTO motors VALUES (2, '2', NULL, NULL, NULL);
INSERT INTO motors VALUES (3, '3', NULL, NULL, NULL);
INSERT INTO motors VALUES (4, '4', NULL, NULL, NULL);


--
-- Name: motors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('motors_id_seq', 4, true);


--
-- Data for Name: noise_reductions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO noise_reductions VALUES (1, 'Doblby B', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (2, 'Dobly C', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (3, 'DBX', NULL, NULL, NULL);


--
-- Name: noise_reductions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('noise_reductions_id_seq', 3, true);


--
-- Data for Name: number_of_heads; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO number_of_heads VALUES (1, '1 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (2, '2 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (3, '3 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (4, '4 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (5, '5 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (6, '6 (PB)', NULL, NULL, NULL);


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('number_of_heads_id_seq', 5, true);


--
-- Data for Name: outputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO outputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO outputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (3, 'XLR (unbalanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (4, 'CCIR/IEC1', NULL, NULL, NULL);
INSERT INTO outputs VALUES (5, 'NAB/IEC2', NULL, NULL, NULL);
INSERT INTO outputs VALUES (6, 'Class A', NULL, NULL, NULL);
INSERT INTO outputs VALUES (7, 'Class A/B', NULL, NULL, NULL);


--
-- Name: outputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('outputs_id_seq', 7, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: password_resets_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('password_resets_seq', 1, false);


--
-- Data for Name: reel_sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reel_sizes VALUES (1, '5"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (2, '7"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (3, '10"', NULL, NULL, NULL);


--
-- Name: reel_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reel_sizes_id_seq', 3, true);


--
-- Data for Name: speeds; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO speeds VALUES (1, '3 3/4', NULL, NULL, NULL);
INSERT INTO speeds VALUES (2, '7 1/2', NULL, NULL, NULL);
INSERT INTO speeds VALUES (3, '15 i.ps.', NULL, NULL, NULL);
INSERT INTO speeds VALUES (4, '30 i.p.s', NULL, NULL, NULL);


--
-- Name: speeds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('speeds_id_seq', 4, true);


--
-- Data for Name: tape_head_preamp; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_id_seq', 1, false);


--
-- Data for Name: tape_head_preamp_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_images_id_seq', 1, false);


--
-- Data for Name: tape_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_images_seq', 1, false);


--
-- Data for Name: tape_recorder_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorder_images_id_seq', 1, false);


--
-- Data for Name: tape_recorders; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_recorders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorders_id_seq', 1, false);


--
-- Data for Name: tapes; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tapes_id_seq', 1, false);


--
-- Data for Name: tracks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tracks VALUES (1, '4 Track', NULL, NULL, NULL);
INSERT INTO tracks VALUES (2, '2 Track Inline/Stacked', NULL, NULL, NULL);
INSERT INTO tracks VALUES (3, '2 Track Staggered', NULL, NULL, NULL);
INSERT INTO tracks VALUES (4, 'Quad', NULL, NULL, NULL);


--
-- Name: tracks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tracks_id_seq', 4, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'ashish', 'ashish@citrusleaf.in', '$2y$10$XewljynHY4tUK7EKFzWd4u0CCeFAN9C5aQSXPhMvbnm8nz.RoyI8G', NULL, 'U47FsYeh', 'Admin', '2017-12-23 17:35:25', '2017-12-23 17:35:25', NULL, NULL, NULL);
INSERT INTO users VALUES (2, 'ashish', 'ashish+1@citrusleaf.in', '$2y$10$CqPv0zZga7AR62oOr88zSOFe8HnGgAHa5Smqic6rwATlwCz9uJncm', NULL, 'N6COqRLS', 'Admin', '2017-12-23 17:36:25', '2017-12-23 17:36:25', NULL, NULL, NULL);
INSERT INTO users VALUES (3, 'ashish', 'ashish+2@citrusleaf.in', '$2y$10$1PWsw0gysZW6ecQ0EfiOBuM7vYtRSub5N5qw.L2SUsltnn0D/SgAa', NULL, 'fFiXI1HP', 'Admin', '2017-12-23 17:37:40', '2017-12-23 17:37:40', NULL, NULL, NULL);
INSERT INTO users VALUES (5, 'ashish', 'ashish.verma250990+1@gmail.com', '$2y$10$Itz8nKtn2.xjupeJQVWd9urLi5mgg67Rv6OimWyunl8ctKvtrtkyy', NULL, 'XUS4Zwrg', 'Admin', '2017-12-23 17:54:55', '2017-12-23 17:54:55', NULL, NULL, NULL);
INSERT INTO users VALUES (6, 'ashish', 'ashish.verma250990+2@gmail.com', '$2y$10$/aaaIUidyZAQEktsO3LAIOCFUZkaOY1rxThjkjVKjqM9.37SNxA7a', NULL, 'ghAZbnTd', 'Admin', '2017-12-23 17:56:51', '2017-12-23 17:56:51', NULL, NULL, NULL);
INSERT INTO users VALUES (7, 'ashish', 'ashish.verma250990+3@gmail.com', '$2y$10$1WZi.pCId3BGHH86SAUTKe46BpUmNeUSnsjKtOfWJh730XyJU3rZC', NULL, 'N3KzZTdY', 'Admin', '2017-12-23 17:59:48', '2017-12-23 17:59:48', NULL, NULL, NULL);
INSERT INTO users VALUES (8, 'ashish', 'ashish.verma250990+4@gmail.com', '$2y$10$ZzXJL0z0vwmBcepSix58u.kC.z2ilPvABGNhzKA.6luOYaXD70WCu', NULL, 'gFv2aEh0', 'Admin', '2017-12-24 11:33:45', '2017-12-24 11:33:45', NULL, NULL, NULL);
INSERT INTO users VALUES (9, 'ashish', 'ashish.verma250990+5@gmail.com', '$2y$10$Gn0v/hmLZo4oRk7OOWUpUuerCDLZqSbQlQRbhBEA.k.Al3PGZ4E2G', NULL, NULL, 'Admin', '2017-12-24 11:35:37', '2017-12-24 11:53:28', 1, NULL, NULL);
INSERT INTO users VALUES (10, 'ashish', 'ashish.verma250990+6@gmail.com', '$2y$10$uj4y13hdIML.XSXijQgC5.Wk5RtZmn6KfbcvC3VZTfPLYLM4bVb3q', NULL, NULL, 'Subscriber', '2017-12-24 11:54:56', '2017-12-24 17:27:08', 1, NULL, NULL);
INSERT INTO users VALUES (4, 'ashish', 'admin@gmail.com', '$2y$10$L632YhUq1Jh9d9loyXAhmuOWrtdwp4c7nS7R3kG4vrJet.2JX6W9y', NULL, NULL, 'Admin', '2017-12-23 17:53:44', '2018-01-03 09:15:24', 1, NULL, NULL);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 10, true);


--
-- Data for Name: voltage; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO voltage VALUES (1, '100v Japan', NULL, NULL);
INSERT INTO voltage VALUES (2, '110-120v', NULL, NULL);
INSERT INTO voltage VALUES (3, 'Multi', NULL, NULL);
INSERT INTO voltage VALUES (4, 'other', NULL, NULL);


--
-- Name: voltage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('voltage_id_seq', 4, true);


--
-- Name: application application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- Name: auto_reverse auto_reverse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auto_reverse
    ADD CONSTRAINT auto_reverse_pkey PRIMARY KEY (id);


--
-- Name: brand_images brand_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_brand_id_key UNIQUE (brand_id);


--
-- Name: brand_images brand_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_pkey PRIMARY KEY (id);


--
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: classical classical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical
    ADD CONSTRAINT classical_pkey PRIMARY KEY (id);


--
-- Name: duplications duplications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications
    ADD CONSTRAINT duplications_pkey PRIMARY KEY (id);


--
-- Name: electronics electronics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY electronics
    ADD CONSTRAINT electronics_pkey PRIMARY KEY (id);


--
-- Name: equalization equalization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equalization
    ADD CONSTRAINT equalization_pkey PRIMARY KEY (id);


--
-- Name: expander_images expander_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_pkey PRIMARY KEY (id);


--
-- Name: expanders expanders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders
    ADD CONSTRAINT expanders_pkey PRIMARY KEY (id);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id);


--
-- Name: head_composition head_composition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_composition
    ADD CONSTRAINT head_composition_pkey PRIMARY KEY (id);


--
-- Name: head_configuration head_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_configuration
    ADD CONSTRAINT head_configuration_pkey PRIMARY KEY (id);


--
-- Name: inputs inputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inputs
    ADD CONSTRAINT inputs_pkey PRIMARY KEY (id);


--
-- Name: manufacturer_images manufacturer_images_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: manufacturer_images manufacturer_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_pkey PRIMARY KEY (id);


--
-- Name: max_reel_size max_reel_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY max_reel_size
    ADD CONSTRAINT max_reel_size_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: motors motors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY motors
    ADD CONSTRAINT motors_pkey PRIMARY KEY (id);


--
-- Name: noise_reductions noise_reductions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions
    ADD CONSTRAINT noise_reductions_pkey PRIMARY KEY (id);


--
-- Name: number_of_heads number_of_heads_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY number_of_heads
    ADD CONSTRAINT number_of_heads_pkey PRIMARY KEY (id);


--
-- Name: outputs outputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY outputs
    ADD CONSTRAINT outputs_pkey PRIMARY KEY (id);


--
-- Name: reel_sizes reel_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes
    ADD CONSTRAINT reel_sizes_pkey PRIMARY KEY (id);


--
-- Name: speeds speeds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds
    ADD CONSTRAINT speeds_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp tape_head_preamp_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_brand_id_key UNIQUE (brand_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_brand_id_key UNIQUE (tape_head_preamp_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp tape_head_preamp_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: tape_head_preamp tape_head_preamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp
    ADD CONSTRAINT tape_head_preamp_pkey PRIMARY KEY (id);


--
-- Name: tape_images tape_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorders tape_recorders_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_brand_id_key UNIQUE (brand_id);


--
-- Name: tape_recorders tape_recorders_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: tape_recorders tape_recorders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_pkey PRIMARY KEY (id);


--
-- Name: tapes tapes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_pkey PRIMARY KEY (id);


--
-- Name: tracks tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks
    ADD CONSTRAINT tracks_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: voltage voltage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY voltage
    ADD CONSTRAINT voltage_pkey PRIMARY KEY (id);


--
-- Name: categories__lft__rgt_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories__lft__rgt_parent_id_index ON categories USING btree (_lft, _rgt, parent_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: expander_images expander_images_expander_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_expander_id_fkey FOREIGN KEY (expander_id) REFERENCES expanders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_images tape_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tapes tapes_channel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_channel_fkey FOREIGN KEY (channel) REFERENCES channels(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_classical_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_classical_fkey FOREIGN KEY (classical) REFERENCES classical(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_duplication_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_duplication_fkey FOREIGN KEY (duplication) REFERENCES duplications(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_genre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_genre_fkey FOREIGN KEY (genre) REFERENCES genres(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_noise_reduction_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_noise_reduction_fkey FOREIGN KEY (noise_reduction) REFERENCES noise_reductions(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_reel_size_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_reel_size_fkey FOREIGN KEY (reel_size) REFERENCES reel_sizes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_speed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_speed_fkey FOREIGN KEY (speed) REFERENCES speeds(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_track_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_track_fkey FOREIGN KEY (track) REFERENCES tracks(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: ashish
--

GRANT ALL ON SCHEMA public TO postgres;


--
-- PostgreSQL database dump complete
--

