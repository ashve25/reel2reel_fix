--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE application_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE application (
    id integer DEFAULT nextval('application_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE application OWNER TO postgres;

--
-- Name: auto_reverse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auto_reverse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auto_reverse_id_seq OWNER TO postgres;

--
-- Name: auto_reverse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auto_reverse (
    id integer DEFAULT nextval('auto_reverse_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE auto_reverse OWNER TO postgres;

--
-- Name: brand_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brand_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brand_images_id_seq OWNER TO postgres;

--
-- Name: brand_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brand_images (
    id integer DEFAULT nextval('brand_images_id_seq'::regclass) NOT NULL,
    brand_id integer,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type integer,
    deleted_at timestamp without time zone
);


ALTER TABLE brand_images OWNER TO postgres;

--
-- Name: brands_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE brands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brands_id_seq OWNER TO postgres;

--
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE brands (
    id integer DEFAULT nextval('brands_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    description text,
    innovations text,
    information text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    manufacturer integer,
    location text,
    year_to integer,
    year_from integer
);


ALTER TABLE brands OWNER TO postgres;

--
-- Name: catalog_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_images (
    id integer NOT NULL,
    catalog_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalog_images OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_images_id_seq OWNER TO postgres;

--
-- Name: catalog_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_images_id_seq OWNED BY catalog_images.id;


--
-- Name: catalog_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalog_types (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalog_types OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalog_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalog_types_id_seq OWNER TO postgres;

--
-- Name: catalog_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalog_types_id_seq OWNED BY catalog_types.id;


--
-- Name: catalogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE catalogs (
    id integer NOT NULL,
    title text,
    type integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE catalogs OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE catalogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogs_id_seq OWNER TO postgres;

--
-- Name: catalogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE catalogs_id_seq OWNED BY catalogs.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    _lft integer DEFAULT 0 NOT NULL,
    _rgt integer DEFAULT 0 NOT NULL,
    parent_id integer,
    name text,
    id integer NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq1 OWNER TO postgres;

--
-- Name: categories_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq1 OWNED BY categories.id;


--
-- Name: channels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE channels (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE channels OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE channels_id_seq OWNER TO postgres;

--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE channels_id_seq OWNED BY channels.id;


--
-- Name: classical; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE classical (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE classical OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE classical_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE classical_id_seq OWNER TO postgres;

--
-- Name: classical_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE classical_id_seq OWNED BY classical.id;


--
-- Name: duplications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE duplications (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE duplications OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE duplications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE duplications_id_seq OWNER TO postgres;

--
-- Name: duplications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE duplications_id_seq OWNED BY duplications.id;


--
-- Name: electronics_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE electronics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE electronics_id_seq OWNER TO postgres;

--
-- Name: electronics; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE electronics (
    id integer DEFAULT nextval('electronics_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE electronics OWNER TO postgres;

--
-- Name: equalization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE equalization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE equalization_id_seq OWNER TO postgres;

--
-- Name: equalization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE equalization (
    id integer DEFAULT nextval('equalization_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE equalization OWNER TO postgres;

--
-- Name: expander_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expander_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expander_images_seq OWNER TO postgres;

--
-- Name: expander_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expander_images (
    id integer DEFAULT nextval('expander_images_seq'::regclass) NOT NULL,
    expander_id integer,
    image text,
    main_image integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expander_images OWNER TO postgres;

--
-- Name: expanders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expanders (
    id integer NOT NULL,
    manufacturer text,
    model text,
    tape_recorders_usable integer,
    outputs_for_record integer,
    recorder_inputs integer,
    copy_option_decks integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE expanders OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expanders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expanders_id_seq OWNER TO postgres;

--
-- Name: expanders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE expanders_id_seq OWNED BY expanders.id;


--
-- Name: genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE genres (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE genres OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE genres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genres_id_seq OWNER TO postgres;

--
-- Name: genres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE genres_id_seq OWNED BY genres.id;


--
-- Name: head_composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_composition_id_seq OWNER TO postgres;

--
-- Name: head_composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_composition (
    id integer DEFAULT nextval('head_composition_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_composition OWNER TO postgres;

--
-- Name: head_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE head_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE head_configuration_id_seq OWNER TO postgres;

--
-- Name: head_configuration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE head_configuration (
    id integer DEFAULT nextval('head_configuration_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE head_configuration OWNER TO postgres;

--
-- Name: inputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inputs_id_seq OWNER TO postgres;

--
-- Name: inputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inputs (
    id integer DEFAULT nextval('inputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE inputs OWNER TO postgres;

--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturer_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturer_images_id_seq OWNER TO postgres;

--
-- Name: manufacturer_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturer_images (
    id integer DEFAULT nextval('manufacturer_images_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    image text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    type integer
);


ALTER TABLE manufacturer_images OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manufacturers (
    id integer DEFAULT nextval('manufacturers_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    origin_country text NOT NULL,
    year_to integer,
    year_from integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    history text
);


ALTER TABLE manufacturers OWNER TO postgres;

--
-- Name: max_reel_size_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE max_reel_size_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE max_reel_size_id_seq OWNER TO postgres;

--
-- Name: max_reel_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE max_reel_size (
    id integer DEFAULT nextval('max_reel_size_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE max_reel_size OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: motors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE motors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE motors_id_seq OWNER TO postgres;

--
-- Name: motors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE motors (
    id integer DEFAULT nextval('motors_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE motors OWNER TO postgres;

--
-- Name: noise_reductions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE noise_reductions (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE noise_reductions OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE noise_reductions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE noise_reductions_id_seq OWNER TO postgres;

--
-- Name: noise_reductions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE noise_reductions_id_seq OWNED BY noise_reductions.id;


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE number_of_heads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE number_of_heads_id_seq OWNER TO postgres;

--
-- Name: number_of_heads; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE number_of_heads (
    id integer DEFAULT nextval('number_of_heads_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE number_of_heads OWNER TO postgres;

--
-- Name: outputs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE outputs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outputs_id_seq OWNER TO postgres;

--
-- Name: outputs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE outputs (
    id integer DEFAULT nextval('outputs_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE outputs OWNER TO postgres;

--
-- Name: password_resets_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE password_resets_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE password_resets_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    id integer DEFAULT nextval('password_resets_seq'::regclass) NOT NULL
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: reel_sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE reel_sizes (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE reel_sizes OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE reel_sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE reel_sizes_id_seq OWNER TO postgres;

--
-- Name: reel_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE reel_sizes_id_seq OWNED BY reel_sizes.id;


--
-- Name: speeds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE speeds (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE speeds OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE speeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE speeds_id_seq OWNER TO postgres;

--
-- Name: speeds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE speeds_id_seq OWNED BY speeds.id;


--
-- Name: tape_head_preamp_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_comments (
    id integer NOT NULL,
    user_id integer,
    comment text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp_comments OWNER TO postgres;

--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamp_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamp_images_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamp_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamp_images (
    id integer DEFAULT nextval('tape_head_preamp_images_id_seq'::regclass) NOT NULL,
    tape_head_preamp_id integer NOT NULL,
    title text,
    description text,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_head_preamp_images OWNER TO postgres;

--
-- Name: tape_head_preamps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_head_preamps (
    id integer DEFAULT nextval('tape_head_preamp_id_seq'::regclass) NOT NULL,
    manufacturer_id integer,
    model text,
    adjustable_gain text,
    adjustable_loading text,
    playback_eq text,
    level_controls text,
    isolated_power_supply text,
    signal_noise_ratio text,
    tubes text,
    suggested_input_wiring text,
    impedance text,
    accessories text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    main_image integer,
    electronics integer,
    inputs integer,
    outputs integer
);


ALTER TABLE tape_head_preamps OWNER TO postgres;

--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_head_preamps_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_head_preamps_comments_id_seq OWNER TO postgres;

--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tape_head_preamps_comments_id_seq OWNED BY tape_head_preamp_comments.id;


--
-- Name: tape_images_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_images_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_images_seq OWNER TO postgres;

--
-- Name: tape_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_images (
    id integer DEFAULT nextval('tape_images_seq'::regclass) NOT NULL,
    tape_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    thumbnail text
);


ALTER TABLE tape_images OWNER TO postgres;

--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorder_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorder_images_id_seq OWNER TO postgres;

--
-- Name: tape_recorder_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorder_images (
    id integer DEFAULT nextval('tape_recorder_images_id_seq'::regclass) NOT NULL,
    tape_recorder_id integer,
    image text,
    type integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_recorder_images OWNER TO postgres;

--
-- Name: tape_recorders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_recorders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_recorders_id_seq OWNER TO postgres;

--
-- Name: tape_recorders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_recorders (
    id integer DEFAULT nextval('tape_recorders_id_seq'::regclass) NOT NULL,
    manufacturer integer,
    brand integer,
    model text,
    country_of_manufacturer text,
    serial_number text,
    original_price double precision,
    belts text,
    frequency_response text,
    wow_flutter text,
    signal_noise_ratio text,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    release_to_date integer,
    release_from_date integer,
    head_configuration integer,
    number_of_heads integer,
    speeds integer,
    tracks integer,
    head_composition integer,
    equalization integer,
    voltage integer,
    auto_reverse integer,
    application integer,
    max_reel_size integer,
    noise_reduction integer,
    motors integer,
    main_image integer
);


ALTER TABLE tape_recorders OWNER TO postgres;

--
-- Name: tape_track_list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tape_track_list (
    id integer NOT NULL,
    side text,
    title text,
    time_length text,
    tape_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tape_track_list OWNER TO postgres;

--
-- Name: TABLE tape_track_list; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tape_track_list IS 'contains track lists in a tape for both sides';


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tape_track_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tape_track_list_id_seq OWNER TO postgres;

--
-- Name: tape_track_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tape_track_list_id_seq OWNED BY tape_track_list.id;


--
-- Name: tapes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tapes (
    id integer NOT NULL,
    title_composition text,
    artist_composer text,
    release_number text,
    release_date date,
    credits jsonb,
    label text,
    classical integer,
    genre integer,
    track integer,
    speed integer,
    channel integer,
    reel_size integer,
    noise_reduction integer,
    duplication integer,
    master_duplication integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tapes OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tapes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tapes_id_seq OWNER TO postgres;

--
-- Name: tapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tapes_id_seq OWNED BY tapes.id;


--
-- Name: tracks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tracks (
    id integer NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE tracks OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tracks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tracks_id_seq OWNER TO postgres;

--
-- Name: tracks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tracks_id_seq OWNED BY tracks.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    verification_key text,
    type text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    verified smallint,
    token_timestamp_updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: voltage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE voltage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE voltage_id_seq OWNER TO postgres;

--
-- Name: voltage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE voltage (
    id integer DEFAULT nextval('voltage_id_seq'::regclass) NOT NULL,
    name text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


ALTER TABLE voltage OWNER TO postgres;

--
-- Name: catalog_images id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images ALTER COLUMN id SET DEFAULT nextval('catalog_images_id_seq'::regclass);


--
-- Name: catalog_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types ALTER COLUMN id SET DEFAULT nextval('catalog_types_id_seq'::regclass);


--
-- Name: catalogs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs ALTER COLUMN id SET DEFAULT nextval('catalogs_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq1'::regclass);


--
-- Name: channels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels ALTER COLUMN id SET DEFAULT nextval('channels_id_seq'::regclass);


--
-- Name: classical id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical ALTER COLUMN id SET DEFAULT nextval('classical_id_seq'::regclass);


--
-- Name: duplications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications ALTER COLUMN id SET DEFAULT nextval('duplications_id_seq'::regclass);


--
-- Name: expanders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders ALTER COLUMN id SET DEFAULT nextval('expanders_id_seq'::regclass);


--
-- Name: genres id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres ALTER COLUMN id SET DEFAULT nextval('genres_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: noise_reductions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions ALTER COLUMN id SET DEFAULT nextval('noise_reductions_id_seq'::regclass);


--
-- Name: reel_sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes ALTER COLUMN id SET DEFAULT nextval('reel_sizes_id_seq'::regclass);


--
-- Name: speeds id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds ALTER COLUMN id SET DEFAULT nextval('speeds_id_seq'::regclass);


--
-- Name: tape_head_preamp_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_comments ALTER COLUMN id SET DEFAULT nextval('tape_head_preamps_comments_id_seq'::regclass);


--
-- Name: tape_track_list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list ALTER COLUMN id SET DEFAULT nextval('tape_track_list_id_seq'::regclass);


--
-- Name: tapes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes ALTER COLUMN id SET DEFAULT nextval('tapes_id_seq'::regclass);


--
-- Name: tracks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks ALTER COLUMN id SET DEFAULT nextval('tracks_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO application VALUES (1, 'Consumer', NULL, NULL, NULL);
INSERT INTO application VALUES (2, 'Portable', NULL, NULL, NULL);
INSERT INTO application VALUES (3, 'Semi-Pro', NULL, NULL, NULL);
INSERT INTO application VALUES (4, 'Studio', NULL, NULL, NULL);


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('application_id_seq', 4, true);


--
-- Data for Name: auto_reverse; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO auto_reverse VALUES (1, 'Yes', NULL, NULL, NULL);
INSERT INTO auto_reverse VALUES (2, 'No', NULL, NULL, NULL);


--
-- Name: auto_reverse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auto_reverse_id_seq', 2, true);


--
-- Data for Name: brand_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO brand_images VALUES (5, 4, 'public/images/cEY2Y46GRvISpbCQzTi2hDq5F6k48LDcDE3alwzY.png', '2018-01-30 13:07:49', '2018-01-30 13:07:49', 0, NULL);
INSERT INTO brand_images VALUES (7, 4, 'public/images/nZg9L1I0FcUOmfqfGeoS6DsZJXUDo32ftFGaxfFd.png', '2018-01-30 13:07:49', '2018-01-30 13:07:49', 1, NULL);
INSERT INTO brand_images VALUES (8, 4, 'public/images/glAfxH9iiLeLzArg51X9TwF3xQIWy6gNcUKlVHEI.png', '2018-01-30 13:48:44', '2018-01-30 13:48:44', 1, NULL);


--
-- Name: brand_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brand_images_id_seq', 8, true);


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO brands VALUES (4, 'test', 'djl', 'fjlk', 'kjllkj', '2018-01-30 13:07:49', '2018-01-30 13:07:49', NULL, NULL, 'test', 90, 90);
INSERT INTO brands VALUES (5, 'test', 'dsfjk', 'jdfl', 'jldfsk', '2018-01-30 14:00:41', '2018-01-30 14:00:41', NULL, 22, 'sfdlkj', 354, 453);
INSERT INTO brands VALUES (6, 'dsjf', 'kjsfl', 'dskljf', 'jklv', '2018-02-02 06:50:41', '2018-02-02 06:50:41', NULL, 17, 'sdfklj', 897, 324);


--
-- Name: brands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('brands_id_seq', 6, true);


--
-- Data for Name: catalog_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalog_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_images_id_seq', 1, false);


--
-- Data for Name: catalog_types; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalog_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalog_types_id_seq', 1, false);


--
-- Data for Name: catalogs; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: catalogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('catalogs_id_seq', 1, false);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: categories_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq1', 1, false);


--
-- Data for Name: channels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO channels VALUES (1, 'Stero', NULL, NULL, NULL);
INSERT INTO channels VALUES (2, 'Mono', NULL, NULL, NULL);
INSERT INTO channels VALUES (3, 'Quad', NULL, NULL, NULL);
INSERT INTO channels VALUES (4, 'trest', NULL, NULL, '2018-01-05 11:00:36');
INSERT INTO channels VALUES (5, 'test', NULL, NULL, '2018-01-05 11:00:43');
INSERT INTO channels VALUES (10, 'test7', NULL, NULL, '2018-01-05 11:02:33');
INSERT INTO channels VALUES (11, 'test6', NULL, NULL, '2018-01-05 11:02:39');
INSERT INTO channels VALUES (9, 'test5', NULL, NULL, '2018-01-05 11:07:41');
INSERT INTO channels VALUES (7, 'test3', NULL, NULL, '2018-01-05 11:18:39');
INSERT INTO channels VALUES (8, 'test4', NULL, NULL, '2018-01-05 11:18:42');
INSERT INTO channels VALUES (6, 'test1', NULL, NULL, '2018-01-05 11:18:45');


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('channels_id_seq', 11, true);


--
-- Data for Name: classical; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO classical VALUES (1, 'Not Classical', NULL, NULL, NULL);
INSERT INTO classical VALUES (2, 'Orchastra', NULL, NULL, NULL);
INSERT INTO classical VALUES (3, 'concerto ', NULL, NULL, NULL);
INSERT INTO classical VALUES (4, 'sontanta ', NULL, NULL, NULL);


--
-- Name: classical_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('classical_id_seq', 9, true);


--
-- Data for Name: duplications; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO duplications VALUES (1, 'Ampex', NULL, NULL, NULL);
INSERT INTO duplications VALUES (2, 'Sterotape ', NULL, NULL, NULL);
INSERT INTO duplications VALUES (3, 'Columbia', NULL, NULL, NULL);
INSERT INTO duplications VALUES (4, 'Magtec', NULL, NULL, NULL);
INSERT INTO duplications VALUES (5, 'Barclay Croker', NULL, NULL, NULL);
INSERT INTO duplications VALUES (6, 'RCA', NULL, NULL, NULL);
INSERT INTO duplications VALUES (7, 'Mecury', NULL, NULL, NULL);
INSERT INTO duplications VALUES (8, 'Other', NULL, NULL, NULL);


--
-- Name: duplications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('duplications_id_seq', 8, true);


--
-- Data for Name: electronics; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO electronics VALUES (1, 'Solid State', NULL, NULL, NULL);
INSERT INTO electronics VALUES (2, 'Tube', NULL, NULL, NULL);
INSERT INTO electronics VALUES (3, 'Hybrid', NULL, NULL, NULL);


--
-- Name: electronics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('electronics_id_seq', 3, true);


--
-- Data for Name: equalization; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO equalization VALUES (1, 'NAB', NULL, NULL, NULL);
INSERT INTO equalization VALUES (2, 'IEC', NULL, NULL, NULL);
INSERT INTO equalization VALUES (3, 'AES', NULL, NULL, NULL);
INSERT INTO equalization VALUES (4, 'Other', NULL, NULL, NULL);


--
-- Name: equalization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('equalization_id_seq', 4, true);


--
-- Data for Name: expander_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expander_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expander_images_seq', 1, false);


--
-- Data for Name: expanders; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: expanders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('expanders_id_seq', 1, false);


--
-- Data for Name: genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO genres VALUES (1, 'Blues ', NULL, NULL, NULL);
INSERT INTO genres VALUES (2, 'Country', NULL, NULL, NULL);
INSERT INTO genres VALUES (3, 'Electronic ', NULL, NULL, NULL);
INSERT INTO genres VALUES (4, 'Folk', NULL, NULL, NULL);
INSERT INTO genres VALUES (5, 'Jazz', NULL, NULL, NULL);
INSERT INTO genres VALUES (6, 'Latin', NULL, NULL, NULL);
INSERT INTO genres VALUES (7, 'New Age', NULL, NULL, NULL);
INSERT INTO genres VALUES (8, 'Pop', NULL, NULL, NULL);
INSERT INTO genres VALUES (9, 'Reggae', NULL, NULL, NULL);
INSERT INTO genres VALUES (10, 'Rock', NULL, NULL, NULL);
INSERT INTO genres VALUES (11, 'Soul', NULL, NULL, NULL);
INSERT INTO genres VALUES (12, 'Soundtrack', NULL, NULL, NULL);
INSERT INTO genres VALUES (13, 'Misc', NULL, NULL, NULL);
INSERT INTO genres VALUES (14, 'CP (CrockPot)', NULL, NULL, NULL);
INSERT INTO genres VALUES (15, 'Other', NULL, NULL, NULL);


--
-- Name: genres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('genres_id_seq', 15, true);


--
-- Data for Name: head_composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_composition VALUES (1, 'F&F', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (2, 'Metal', NULL, NULL, NULL);
INSERT INTO head_composition VALUES (3, 'Glass', NULL, NULL, NULL);


--
-- Name: head_composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_composition_id_seq', 3, true);


--
-- Data for Name: head_configuration; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO head_configuration VALUES (2, 'Mono - Full Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (3, 'Mono -Dual Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (4, 'Mono - Half Track', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (5, 'sterero ', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (6, 'Sterero - Stacked', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (7, 'Sterero - Staggered', NULL, NULL, NULL);
INSERT INTO head_configuration VALUES (8, 'Quad', NULL, NULL, NULL);


--
-- Name: head_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('head_configuration_id_seq', 7, true);


--
-- Data for Name: inputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO inputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO inputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO inputs VALUES (3, 'XLR (Unbalanced)', NULL, NULL, NULL);


--
-- Name: inputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inputs_id_seq', 3, true);


--
-- Data for Name: manufacturer_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO manufacturer_images VALUES (20, 22, 'public/images/mHUCS5bIDJLmwaMml01q3stMWEvU23EyCkxpJ0PI.png', '2018-01-29 11:53:52', '2018-01-29 11:53:52', NULL, 0);
INSERT INTO manufacturer_images VALUES (22, 22, 'public/images/2OpjJrTRf99lWjdFAbDprV5vpH4U6HFqYbH6fEpb.png', '2018-01-29 11:53:52', '2018-01-29 11:53:52', NULL, 1);
INSERT INTO manufacturer_images VALUES (23, 22, 'public/images/E0fFqnxnGGPAehLk6PoG5xKyLQ6DEjRIzsPLjCNL.png', '2018-01-29 18:35:54', '2018-01-29 18:35:54', NULL, 1);


--
-- Name: manufacturer_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturer_images_id_seq', 23, true);


--
-- Data for Name: manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO manufacturers VALUES (17, 'djsl', 'dfklj', 908, 90, '2018-01-29 10:57:23', '2018-01-29 10:57:23', NULL, NULL);
INSERT INTO manufacturers VALUES (18, 'sdjl', 'sdjkl', 98, 90, '2018-01-29 10:59:30', '2018-01-29 10:59:30', NULL, NULL);
INSERT INTO manufacturers VALUES (22, 'test1', 'india', 32, 113, '2018-01-29 11:53:52', '2018-01-29 11:53:52', NULL, NULL);


--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 22, true);


--
-- Data for Name: max_reel_size; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO max_reel_size VALUES (1, '3', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (2, '5', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (3, '5 3/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (4, '8 1/4', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (5, '10 1/2', NULL, NULL, NULL);
INSERT INTO max_reel_size VALUES (6, '10 1/2+', NULL, NULL, NULL);


--
-- Name: max_reel_size_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('max_reel_size_id_seq', 6, true);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations VALUES (3, '2017_12_21_130726_create_categories_nested_set', 1);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 3, true);


--
-- Data for Name: motors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO motors VALUES (1, '1', NULL, NULL, NULL);
INSERT INTO motors VALUES (2, '2', NULL, NULL, NULL);
INSERT INTO motors VALUES (3, '3', NULL, NULL, NULL);
INSERT INTO motors VALUES (4, '4', NULL, NULL, NULL);


--
-- Name: motors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('motors_id_seq', 4, true);


--
-- Data for Name: noise_reductions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO noise_reductions VALUES (1, 'Doblby B', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (2, 'Dobly C', NULL, NULL, NULL);
INSERT INTO noise_reductions VALUES (3, 'DBX', NULL, NULL, NULL);


--
-- Name: noise_reductions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('noise_reductions_id_seq', 3, true);


--
-- Data for Name: number_of_heads; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO number_of_heads VALUES (1, '1 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (2, '2 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (3, '3 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (4, '4 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (5, '5 (PB)', NULL, NULL, NULL);
INSERT INTO number_of_heads VALUES (6, '6 (PB)', NULL, NULL, NULL);


--
-- Name: number_of_heads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('number_of_heads_id_seq', 5, true);


--
-- Data for Name: outputs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO outputs VALUES (1, 'RCA', NULL, NULL, NULL);
INSERT INTO outputs VALUES (2, 'XLR (Balanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (3, 'XLR (unbalanced)', NULL, NULL, NULL);
INSERT INTO outputs VALUES (4, 'CCIR/IEC1', NULL, NULL, NULL);
INSERT INTO outputs VALUES (5, 'NAB/IEC2', NULL, NULL, NULL);
INSERT INTO outputs VALUES (6, 'Class A', NULL, NULL, NULL);
INSERT INTO outputs VALUES (7, 'Class A/B', NULL, NULL, NULL);


--
-- Name: outputs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('outputs_id_seq', 7, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: password_resets_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('password_resets_seq', 1, false);


--
-- Data for Name: reel_sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO reel_sizes VALUES (1, '5"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (2, '7"', NULL, NULL, NULL);
INSERT INTO reel_sizes VALUES (3, '10"', NULL, NULL, NULL);


--
-- Name: reel_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('reel_sizes_id_seq', 3, true);


--
-- Data for Name: speeds; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO speeds VALUES (1, '3 3/4', NULL, NULL, NULL);
INSERT INTO speeds VALUES (2, '7 1/2', NULL, NULL, NULL);
INSERT INTO speeds VALUES (3, '15 i.ps.', NULL, NULL, NULL);
INSERT INTO speeds VALUES (4, '30 i.p.s', NULL, NULL, NULL);


--
-- Name: speeds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('speeds_id_seq', 4, true);


--
-- Data for Name: tape_head_preamp_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_id_seq', 1, false);


--
-- Data for Name: tape_head_preamp_images; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamp_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamp_images_id_seq', 1, false);


--
-- Data for Name: tape_head_preamps; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tape_head_preamps_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_head_preamps_comments_id_seq', 1, false);


--
-- Data for Name: tape_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_images VALUES (1, 1, 'public/images/fYOUth3MNkyhOaG6kHVVdGp8MeTQTwiXveH48oxB.jpeg', 0, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL, NULL);
INSERT INTO tape_images VALUES (2, 2, 'public/images/mwwPMSCGXcT1grVGpOxYb1irTItL0uzfvf9vTvBt.jpeg', 0, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL, NULL);
INSERT INTO tape_images VALUES (3, 3, 'public/images/waGzYM75Lj98tgiF0pNrXNfnT5rCYFXHHyfTYpE4.jpeg', 0, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL, NULL);
INSERT INTO tape_images VALUES (4, 4, 'public/images/zTkxF4CE3ikymuQa5edS3BBKdfKdljfXaHDEPphq.jpeg', 0, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL, NULL);
INSERT INTO tape_images VALUES (5, 5, 'public/images/6lQ51sXodTnwiRYGAWYdIswIcSO0uvTKbM5qWdJQ.jpeg', 0, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL, NULL);
INSERT INTO tape_images VALUES (6, 6, 'public/images/BuLQ1sQ4lV38wcLueBMgy9WEq5SgSZelcBvK5vZO.jpeg', 0, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL, NULL);
INSERT INTO tape_images VALUES (7, 7, 'public/images/x1kBOhZ8sMdtamcuO6uxw7B6DSt7s8D6z2AFsw7j.jpeg', 0, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL, NULL);
INSERT INTO tape_images VALUES (8, 8, 'public/images/8gWEMguPo5pr30HIC9wUkIkyx6IjO3plAnftbnxt.jpeg', 0, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL, NULL);
INSERT INTO tape_images VALUES (9, 9, 'public/images/BfvuLWDY4njrEhknNcYpxAIrLlHdO1xjUcHhbdSt.jpeg', 0, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL, NULL);
INSERT INTO tape_images VALUES (10, 10, 'public/images/Io87BzcnIwRVLcSaOcua4MV0w0kGGlsTSq5u80Sx.jpeg', 0, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL, NULL);
INSERT INTO tape_images VALUES (11, 11, 'public/images/aAqOyZ92To2sOUqrLXUHyRh4vld5RD4lCdyOr7CA.jpeg', 0, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL, NULL);
INSERT INTO tape_images VALUES (12, 12, 'public/images/3oFjNNKvcVlYcgkBDPPtub15kVfEynfccYUn6tO4.jpeg', 0, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL, NULL);
INSERT INTO tape_images VALUES (13, 13, 'public/images/pH0v0vqvvZjI1CC7caaODUzmvuRhHTEIEado0Hhe.jpeg', 0, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL, NULL);
INSERT INTO tape_images VALUES (14, 14, 'public/images/JqnhT63Lo3WJSV0q35FSpxPIunfoxwHKEHI6Ld1f.jpeg', 0, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL, NULL);
INSERT INTO tape_images VALUES (15, 15, 'public/images/3FmuRyQQsrgdnpj6pdPUUNSwGuQ78KJ6pbOWNQWp.jpeg', 0, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL, NULL);
INSERT INTO tape_images VALUES (16, 16, 'public/images/FdOOJNBnCE2CNiwfWVXGFmSjnKVCSrUVu7tcAuyO.jpeg', 0, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL, NULL);
INSERT INTO tape_images VALUES (17, 17, 'public/images/YcYQuSYfLs2bSERAufiuXZPqwbSCAClJSxuchJZR.jpeg', 0, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL, NULL);
INSERT INTO tape_images VALUES (18, 18, 'public/images/W8XIofPUOO3gF2hAAV2f1keWFyPx7b6YFZzZ1BDe.jpeg', 0, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL, NULL);
INSERT INTO tape_images VALUES (19, 19, 'public/images/BRHbeh4IkopemNuJL8Eet3UcvMQTxshfLGrOPzfv.jpeg', 0, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL, NULL);
INSERT INTO tape_images VALUES (20, 20, 'public/images/kBiqnK0uTQnX8kAnHWOBiURFBVihzgBmb04dfOYL.jpeg', 0, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL, NULL);
INSERT INTO tape_images VALUES (21, 21, 'public/images/89XwdmTrTgPA2ZOe8oDpUpVbx48vQmEKeiUu2UCh.jpeg', 0, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL, NULL);
INSERT INTO tape_images VALUES (22, 22, 'public/images/EDZ9PNEg468e4eMVfHG7vDrHnMGPBDVYVJrhQXAU.jpeg', 0, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL, NULL);
INSERT INTO tape_images VALUES (23, 23, 'public/images/BpAzjMHUpO2zU0olH66NlFdJxicfqvpgEElWYiCH.jpeg', 0, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL, NULL);
INSERT INTO tape_images VALUES (24, 24, 'public/images/q9JMGvUYpnMUvdNy5uWKOqWfiRy7h8h7xTc82ZKH.jpeg', 0, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL, NULL);
INSERT INTO tape_images VALUES (25, 25, 'public/images/oDNjkHchjbNrRH9AlhS027X692ot3hWaHVB0mSMp.jpeg', 0, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL, NULL);
INSERT INTO tape_images VALUES (26, 26, 'public/images/l6NaoGpBqHKMXwadGFlwIExNQjPVrrVb5hiLMEiL.jpeg', 0, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL, NULL);
INSERT INTO tape_images VALUES (27, 27, 'public/images/QV87lP27eRaFbaUGat4ouW2XQfNqoPrRxfeUbgeY.jpeg', 0, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL, NULL);
INSERT INTO tape_images VALUES (28, 28, 'public/images/s1qwaqu37SaTT2R4hJ919tS4ScB6ieIHJRjd4Kbq.jpeg', 0, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL, NULL);
INSERT INTO tape_images VALUES (29, 29, 'public/images/Llaxd3QiiBN5jEa1o4Ol0NDGm6uv9DOpMmJ2YB4h.jpeg', 0, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL, NULL);
INSERT INTO tape_images VALUES (30, 30, 'public/images/tzpJ0UZohzyP9Y06mtBUIETJ7sGOXtNdGyd8Ziza.jpeg', 0, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL, NULL);
INSERT INTO tape_images VALUES (31, 31, 'public/images/2BcE42NLryB1gunBTZrKy4NP3JeYfaHjdmLxA3bW.jpeg', 0, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL, NULL);
INSERT INTO tape_images VALUES (32, 32, 'public/images/4A1Y8UEkmm6oUDIAZCXHnk7blG36HGP9xT39iTII.jpeg', 0, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL, NULL);
INSERT INTO tape_images VALUES (33, 33, 'public/images/sBA6fFVQNJK0mS6sc7TXdi7aJjcTZwY4UX6INQlZ.jpeg', 0, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL, NULL);
INSERT INTO tape_images VALUES (34, 34, 'public/images/lZBkZAPAaKho1e6KZaF6F1H7iGsxN2AUFHg7xwZ6.jpeg', 0, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL, NULL);
INSERT INTO tape_images VALUES (35, 35, 'public/images/gcgiWSw4pPKYU0Spu8WZ6DVj962Cl6lHfE67Bnri.jpeg', 0, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL, NULL);
INSERT INTO tape_images VALUES (36, 36, 'public/images/nAY5XnBDj5405zbjOR1StF9WqhsPrisJEbYVfouL.jpeg', 0, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL, NULL);
INSERT INTO tape_images VALUES (37, 37, 'public/images/i1hcVGov60VwqeSrxR5I153kjfYxKyh3qlaWvFPh.jpeg', 0, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL, NULL);
INSERT INTO tape_images VALUES (38, 38, 'public/images/CXb1KzOFcHB3sLRF1qM1aeE5lVkx5txMaFcJEZKl.jpeg', 0, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL, NULL);
INSERT INTO tape_images VALUES (39, 39, 'public/images/8xltulrWhjuhlokWGt6oJ1HgBVUqZNQhbDYatVoh.jpeg', 0, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL, NULL);
INSERT INTO tape_images VALUES (40, 40, 'public/images/zpTcWqVXb8BlVQtM4KaTQm9zUDSJvaW2ptHVrdtM.jpeg', 0, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL, NULL);


--
-- Name: tape_images_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_images_seq', 40, true);


--
-- Data for Name: tape_recorder_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_recorder_images VALUES (1, 4, 'public/images/IUQ8nEFC59ZEPin76xKZOx51pkLWsj5sIy4jEMk8.png', 0, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (2, 4, 'public/images/i3pQLrI2K389OJYUoaL2fgk3n6LVsUj3Kl1eyKVT.png', 1, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (3, 4, 'public/images/MM6Fh5jYzdamIR1EG98VQFnsuvwMwvZ7DKt79jy2.png', 2, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (4, 4, 'public/images/B7aHlVI3ioBQoiW5Xn87WCbiDqc2ujdqoqzCCjpa.png', 3, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (5, 4, 'public/images/rUXejxkE2RSyAgH0sab5VlJHEY1eIhe4cLroCbWX.png', 4, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (6, 4, 'public/images/a6wXnTerFLCRXmV273AcgdsOfyj1rJSMaQrWKH2Q.png', 5, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (7, 4, 'public/images/PIa7uzwLp82TMcf1hAP6Inn7HjcTUV3W9lAZ1DkE.png', 6, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);
INSERT INTO tape_recorder_images VALUES (8, 4, 'public/images/6RfBCUalv9sLe1TIDnlUNuNiiR8Xw3xeoJuo1tGC.png', 7, '2018-01-10 10:31:34', '2018-01-10 10:31:34', NULL);


--
-- Name: tape_recorder_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorder_images_id_seq', 8, true);


--
-- Data for Name: tape_recorders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_recorders VALUES (4, 17, 4, 'test model', 'new test', 'new test serial', 123456, 'new etst', 'new test', 'new twest', 'new test', 'asdf test', '2018-01-10 10:31:33', '2018-02-02 12:45:25', NULL, 2333, 1234, 3, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 4, 4);


--
-- Name: tape_recorders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_recorders_id_seq', 4, true);


--
-- Data for Name: tape_track_list; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tape_track_list VALUES (1, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (2, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (3, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (4, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (5, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (6, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (7, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (8, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (9, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (10, 'a', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (11, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (12, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (13, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (14, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (15, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (16, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (17, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (18, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (19, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (20, 'b', NULL, NULL, 1, '2018-01-12 13:14:33', '2018-01-12 13:14:33', NULL);
INSERT INTO tape_track_list VALUES (21, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (22, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (23, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (24, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (25, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (26, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (27, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (28, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (29, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (30, 'a', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (31, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (32, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (33, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (34, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (35, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (36, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (37, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (38, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (39, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (40, 'b', NULL, NULL, 2, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (41, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (42, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (43, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (44, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (45, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (46, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (47, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (48, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (49, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (50, 'a', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (51, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (52, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (53, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (54, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (55, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (56, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (57, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (58, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (59, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (60, 'b', NULL, NULL, 3, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tape_track_list VALUES (61, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (62, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (63, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (64, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (65, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (66, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (67, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (68, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (69, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (70, 'a', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (71, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (72, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (73, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (74, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (75, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (76, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (77, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (78, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (79, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (80, 'b', NULL, NULL, 4, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (81, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (82, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (83, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (84, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (85, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (86, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (87, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (88, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (89, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (90, 'a', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (91, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (92, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (93, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (94, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (95, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (96, 'b', NULL, NULL, 5, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tape_track_list VALUES (97, 'b', NULL, NULL, 5, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (98, 'b', NULL, NULL, 5, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (99, 'b', NULL, NULL, 5, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (100, 'b', NULL, NULL, 5, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (101, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (102, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (103, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (104, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (105, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (106, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (107, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (108, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (109, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (110, 'a', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (111, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (112, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (113, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (114, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (115, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (116, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (117, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (118, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (119, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (120, 'b', NULL, NULL, 6, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tape_track_list VALUES (121, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (122, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (123, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (124, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (125, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (126, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (127, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (128, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (129, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (130, 'a', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (131, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (132, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (133, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (134, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (135, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (136, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (137, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (138, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (139, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (140, 'b', NULL, NULL, 7, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (141, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (142, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (143, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (144, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (145, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (146, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (147, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (148, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (149, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (150, 'a', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (151, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (152, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (153, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (154, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (155, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (156, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (157, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (158, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (159, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (160, 'b', NULL, NULL, 8, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tape_track_list VALUES (161, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (162, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (163, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (164, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (165, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (166, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (167, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (168, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (169, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (170, 'a', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (171, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (172, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (173, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (174, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (175, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (176, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (177, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (178, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (179, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (180, 'b', NULL, NULL, 9, '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tape_track_list VALUES (181, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (182, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (183, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (184, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (185, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (186, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (187, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (188, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (189, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (190, 'a', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (191, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (192, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (193, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (194, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (195, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (196, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (197, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (198, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (199, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (200, 'b', NULL, NULL, 10, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (201, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (202, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (203, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (204, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (205, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (206, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (207, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (208, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (209, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (210, 'a', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (211, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (212, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (213, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (214, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (215, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (216, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (217, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (218, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (219, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (220, 'b', NULL, NULL, 11, '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tape_track_list VALUES (221, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (222, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (223, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (224, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (225, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (226, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (227, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (228, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (229, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (230, 'a', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (231, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (232, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (233, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (234, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (235, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (236, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (237, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (238, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (239, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (240, 'b', NULL, NULL, 12, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (241, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (242, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (243, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (244, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (245, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (246, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (247, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (248, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (249, 'a', NULL, NULL, 13, '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tape_track_list VALUES (250, 'a', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (251, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (252, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (253, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (254, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (255, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (256, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (257, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (258, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (259, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (260, 'b', NULL, NULL, 13, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (261, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (262, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (263, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (264, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (265, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (266, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (267, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (268, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (269, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (270, 'a', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (271, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (272, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (273, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (274, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (275, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (276, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (277, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (278, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (279, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (280, 'b', NULL, NULL, 14, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (281, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (282, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (283, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (284, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (285, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (286, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (287, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (288, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (289, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (290, 'a', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (291, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (292, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (293, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (294, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (295, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (296, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (297, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (298, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (299, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (300, 'b', NULL, NULL, 15, '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tape_track_list VALUES (301, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (302, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (303, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (304, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (305, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (306, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (307, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (308, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (309, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (310, 'a', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (311, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (312, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (313, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (314, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (315, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (316, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (317, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (318, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (319, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (320, 'b', NULL, NULL, 16, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (321, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (322, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (323, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (324, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (325, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (326, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (327, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (328, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (329, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (330, 'a', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (331, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (332, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (333, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (334, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (335, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (336, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (337, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (338, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (339, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (340, 'b', NULL, NULL, 17, '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tape_track_list VALUES (341, 'a', 's243242', '08:40', 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (342, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (343, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (344, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (345, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (346, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (347, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (348, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (349, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (350, 'a', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (351, 'b', '42342', '08:40', 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (352, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (353, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (354, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (355, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (356, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (357, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (358, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (359, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (360, 'b', NULL, NULL, 18, '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tape_track_list VALUES (361, 'a', 's243242', '08:40', 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (362, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (363, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (364, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (365, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (366, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (367, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (368, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (369, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (370, 'a', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (371, 'b', '42342', '08:40', 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (372, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (373, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (374, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (375, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (376, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (377, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (378, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (379, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (380, 'b', NULL, NULL, 19, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (381, 'a', 's243242', '08:40', 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (382, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (383, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (384, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (385, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (386, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (387, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (388, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (389, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (390, 'a', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (391, 'b', '42342', '08:40', 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (392, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (393, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (394, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (395, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (396, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (397, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (398, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (399, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (400, 'b', NULL, NULL, 20, '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tape_track_list VALUES (401, 'a', 's243242', '08:40', 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (402, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (403, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (404, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (405, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (406, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (407, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (408, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (409, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (410, 'a', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (411, 'b', '42342', '08:40', 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (412, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (413, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (414, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (415, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (416, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (417, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (418, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (419, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (420, 'b', NULL, NULL, 21, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (421, 'a', 's243242', '08:40', 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (422, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (423, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (424, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (425, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (426, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (427, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (428, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (429, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (430, 'a', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (431, 'b', '42342', '08:40', 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (432, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (433, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (434, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (435, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (436, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (437, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (438, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (439, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (440, 'b', NULL, NULL, 22, '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tape_track_list VALUES (441, 'a', 's243242', '08:40', 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (442, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (443, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (444, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (445, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (446, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (447, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (448, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (449, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (450, 'a', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (451, 'b', '42342', '08:40', 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (452, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (453, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (454, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (455, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (456, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (457, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (458, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (459, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (460, 'b', NULL, NULL, 23, '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tape_track_list VALUES (461, 'a', 's243242', '08:40', 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (462, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (463, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (464, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (465, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (466, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (467, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (468, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (469, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (470, 'a', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (471, 'b', '42342', '08:40', 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (472, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (473, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (474, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (475, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (476, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (477, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (478, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (479, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (480, 'b', NULL, NULL, 24, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (481, 'a', 's243242', '08:40', 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (482, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (483, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (484, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (485, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (486, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (487, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (488, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (489, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (490, 'a', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (491, 'b', '42342', '08:40', 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (492, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (493, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (494, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (495, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (496, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (497, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (498, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (499, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (500, 'b', NULL, NULL, 25, '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tape_track_list VALUES (501, 'a', 's243242', '08:40', 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (502, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (503, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (504, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (505, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (506, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (507, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (508, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (509, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (510, 'a', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (511, 'b', '42342', '08:40', 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (512, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (513, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (514, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (515, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (516, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (517, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (518, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (519, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (520, 'b', NULL, NULL, 26, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (521, 'a', 's243242', '08:40', 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (522, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (523, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (524, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (525, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (526, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (527, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (528, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (529, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (530, 'a', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (531, 'b', '42342', '08:40', 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (532, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (533, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (534, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (535, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (536, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (537, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (538, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (539, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (540, 'b', NULL, NULL, 27, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (541, 'a', 's243242', '08:40', 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (542, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (543, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (544, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (545, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (546, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (547, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (548, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (549, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (550, 'a', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (551, 'b', '42342', '08:40', 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (552, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (553, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (554, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (555, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (556, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (557, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (558, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (559, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (560, 'b', NULL, NULL, 28, '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tape_track_list VALUES (561, 'a', 's243242', '08:40', 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (562, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (563, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (564, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (565, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (566, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (567, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (568, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (569, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (570, 'a', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (571, 'b', '42342', '08:40', 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (572, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (573, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (574, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (575, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (576, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (577, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (578, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (579, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (580, 'b', NULL, NULL, 29, '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tape_track_list VALUES (581, 'a', 's243242', '08:40', 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (582, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (583, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (584, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (585, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (586, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (587, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (588, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (589, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (590, 'a', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (591, 'b', '42342', '08:40', 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (592, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (593, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (594, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (595, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (596, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (597, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (598, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (599, 'b', NULL, NULL, 30, '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tape_track_list VALUES (600, 'b', NULL, NULL, 30, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (601, 'a', 's243242', '08:40', 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (602, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (603, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (604, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (605, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (606, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (607, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (608, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (609, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (610, 'a', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (611, 'b', '42342', '08:40', 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (612, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (613, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (614, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (615, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (616, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (617, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (618, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (619, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (620, 'b', NULL, NULL, 31, '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tape_track_list VALUES (621, 'a', 's243242', '08:40', 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (622, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (623, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (624, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (625, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (626, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (627, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (628, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (629, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (630, 'a', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (631, 'b', '42342', '08:40', 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (632, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (633, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (634, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (635, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (636, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (637, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (638, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (639, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (640, 'b', NULL, NULL, 32, '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tape_track_list VALUES (641, 'a', 's243242', '08:40', 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (642, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (643, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (644, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (645, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (646, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (647, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (648, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (649, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (650, 'a', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (651, 'b', '42342', '08:40', 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (652, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (653, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (654, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (655, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (656, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (657, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (658, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (659, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (660, 'b', NULL, NULL, 33, '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tape_track_list VALUES (661, 'a', 's243242', '08:40', 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (662, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (663, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (664, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (665, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (666, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (667, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (668, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (669, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (670, 'a', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (671, 'b', '42342', '08:40', 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (672, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (673, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (674, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (675, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (676, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (677, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (678, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (679, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (680, 'b', NULL, NULL, 34, '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tape_track_list VALUES (681, 'a', 's243242', '08:40', 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (682, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (683, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (684, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (685, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (686, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (687, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (688, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (689, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (690, 'a', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (691, 'b', '42342', '08:40', 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (692, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (693, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (694, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (695, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (696, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (697, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (698, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (699, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (700, 'b', NULL, NULL, 35, '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tape_track_list VALUES (701, 'a', 's243242', '08:40', 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (702, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (703, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (704, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (705, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (706, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (707, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (708, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (709, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (710, 'a', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (711, 'b', '42342', '08:40', 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (712, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (713, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (714, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (715, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (716, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (717, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (718, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (719, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (720, 'b', NULL, NULL, 36, '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tape_track_list VALUES (721, 'a', 's243242', '08:40', 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (722, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (723, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (724, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (725, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (726, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (727, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (728, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (729, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (730, 'a', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (731, 'b', '42342', '08:40', 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (732, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (733, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (734, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (735, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (736, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (737, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (738, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (739, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (740, 'b', NULL, NULL, 37, '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tape_track_list VALUES (741, 'a', 's243242', '08:40', 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (742, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (743, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (744, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (745, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (746, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (747, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (748, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (749, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (750, 'a', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (751, 'b', '42342', '08:40', 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (752, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (753, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (754, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (755, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (756, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (757, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (758, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (759, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (760, 'b', NULL, NULL, 38, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (761, 'a', 's243242', '08:40', 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (762, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (763, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (764, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (765, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (766, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (767, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (768, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (769, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (770, 'a', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (771, 'b', '42342', '08:40', 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (772, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (773, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (774, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (775, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (776, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (777, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (778, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (779, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (780, 'b', NULL, NULL, 39, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (781, 'a', 's243242', '08:40', 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (782, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (783, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (784, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (785, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (786, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (787, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (788, 'a', NULL, NULL, 40, '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tape_track_list VALUES (789, 'a', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (790, 'a', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (791, 'b', '42342', '08:40', 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (792, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (793, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (794, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (795, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (796, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (797, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (798, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (799, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);
INSERT INTO tape_track_list VALUES (800, 'b', NULL, NULL, 40, '2018-01-12 13:17:19', '2018-01-12 13:17:19', NULL);


--
-- Name: tape_track_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tape_track_list_id_seq', 800, true);


--
-- Data for Name: tapes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tapes VALUES (1, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:32', '2018-01-12 13:14:32', NULL);
INSERT INTO tapes VALUES (2, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tapes VALUES (3, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:34', '2018-01-12 13:14:34', NULL);
INSERT INTO tapes VALUES (4, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tapes VALUES (5, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:35', '2018-01-12 13:14:35', NULL);
INSERT INTO tapes VALUES (6, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:36', '2018-01-12 13:14:36', NULL);
INSERT INTO tapes VALUES (7, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tapes VALUES (8, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": null, "musician": null}', 'Dummy', 2, 10, NULL, 2, NULL, 1, NULL, NULL, NULL, NULL, '2018-01-12 13:14:37', '2018-01-12 13:14:37', NULL);
INSERT INTO tapes VALUES (9, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:02', '2018-01-12 13:15:02', NULL);
INSERT INTO tapes VALUES (10, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (11, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (12, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:03', '2018-01-12 13:15:03', NULL);
INSERT INTO tapes VALUES (13, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:04', '2018-01-12 13:15:04', NULL);
INSERT INTO tapes VALUES (14, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tapes VALUES (15, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:05', '2018-01-12 13:15:05', NULL);
INSERT INTO tapes VALUES (16, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tapes VALUES (17, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:06', '2018-01-12 13:15:06', NULL);
INSERT INTO tapes VALUES (18, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:27', '2018-01-12 13:15:27', NULL);
INSERT INTO tapes VALUES (19, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tapes VALUES (20, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:28', '2018-01-12 13:15:28', NULL);
INSERT INTO tapes VALUES (21, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tapes VALUES (22, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:29', '2018-01-12 13:15:29', NULL);
INSERT INTO tapes VALUES (23, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:30', '2018-01-12 13:15:30', NULL);
INSERT INTO tapes VALUES (24, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tapes VALUES (25, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:31', '2018-01-12 13:15:31', NULL);
INSERT INTO tapes VALUES (26, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (27, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (28, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:32', '2018-01-12 13:15:32', NULL);
INSERT INTO tapes VALUES (29, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:33', '2018-01-12 13:15:33', NULL);
INSERT INTO tapes VALUES (30, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:34', '2018-01-12 13:15:34', NULL);
INSERT INTO tapes VALUES (31, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:15:35', '2018-01-12 13:15:35', NULL);
INSERT INTO tapes VALUES (32, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:47', '2018-01-12 13:16:47', NULL);
INSERT INTO tapes VALUES (33, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:48', '2018-01-12 13:16:48', NULL);
INSERT INTO tapes VALUES (34, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:16:49', '2018-01-12 13:16:49', NULL);
INSERT INTO tapes VALUES (35, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:15', '2018-01-12 13:17:15', NULL);
INSERT INTO tapes VALUES (36, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:16', '2018-01-12 13:17:16', NULL);
INSERT INTO tapes VALUES (37, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:17', '2018-01-12 13:17:17', NULL);
INSERT INTO tapes VALUES (38, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tapes VALUES (39, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);
INSERT INTO tapes VALUES (40, 'Test', 'Demo User', '12313213', '2018-01-05', '{"credit": "sfs", "musician": "fdsf"}', 'Dummy', 2, 10, NULL, 2, NULL, 1, 3, 1, NULL, 'sdfsf', '2018-01-12 13:17:18', '2018-01-12 13:17:18', NULL);


--
-- Name: tapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tapes_id_seq', 40, true);


--
-- Data for Name: tracks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tracks VALUES (1, '4 Track', NULL, NULL, NULL);
INSERT INTO tracks VALUES (2, '2 Track Inline/Stacked', NULL, NULL, NULL);
INSERT INTO tracks VALUES (3, '2 Track Staggered', NULL, NULL, NULL);
INSERT INTO tracks VALUES (4, 'Quad', NULL, NULL, NULL);


--
-- Name: tracks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tracks_id_seq', 4, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'ashish', 'ashish@citrusleaf.in', '$2y$10$XewljynHY4tUK7EKFzWd4u0CCeFAN9C5aQSXPhMvbnm8nz.RoyI8G', NULL, 'U47FsYeh', 'Admin', '2017-12-23 17:35:25', '2017-12-23 17:35:25', NULL, NULL, NULL);
INSERT INTO users VALUES (2, 'ashish', 'ashish+1@citrusleaf.in', '$2y$10$CqPv0zZga7AR62oOr88zSOFe8HnGgAHa5Smqic6rwATlwCz9uJncm', NULL, 'N6COqRLS', 'Admin', '2017-12-23 17:36:25', '2017-12-23 17:36:25', NULL, NULL, NULL);
INSERT INTO users VALUES (3, 'ashish', 'ashish+2@citrusleaf.in', '$2y$10$1PWsw0gysZW6ecQ0EfiOBuM7vYtRSub5N5qw.L2SUsltnn0D/SgAa', NULL, 'fFiXI1HP', 'Admin', '2017-12-23 17:37:40', '2017-12-23 17:37:40', NULL, NULL, NULL);
INSERT INTO users VALUES (5, 'ashish', 'ashish.verma250990+1@gmail.com', '$2y$10$Itz8nKtn2.xjupeJQVWd9urLi5mgg67Rv6OimWyunl8ctKvtrtkyy', NULL, 'XUS4Zwrg', 'Admin', '2017-12-23 17:54:55', '2017-12-23 17:54:55', NULL, NULL, NULL);
INSERT INTO users VALUES (6, 'ashish', 'ashish.verma250990+2@gmail.com', '$2y$10$/aaaIUidyZAQEktsO3LAIOCFUZkaOY1rxThjkjVKjqM9.37SNxA7a', NULL, 'ghAZbnTd', 'Admin', '2017-12-23 17:56:51', '2017-12-23 17:56:51', NULL, NULL, NULL);
INSERT INTO users VALUES (7, 'ashish', 'ashish.verma250990+3@gmail.com', '$2y$10$1WZi.pCId3BGHH86SAUTKe46BpUmNeUSnsjKtOfWJh730XyJU3rZC', NULL, 'N3KzZTdY', 'Admin', '2017-12-23 17:59:48', '2017-12-23 17:59:48', NULL, NULL, NULL);
INSERT INTO users VALUES (8, 'ashish', 'ashish.verma250990+4@gmail.com', '$2y$10$ZzXJL0z0vwmBcepSix58u.kC.z2ilPvABGNhzKA.6luOYaXD70WCu', NULL, 'gFv2aEh0', 'Admin', '2017-12-24 11:33:45', '2017-12-24 11:33:45', NULL, NULL, NULL);
INSERT INTO users VALUES (9, 'ashish', 'ashish.verma250990+5@gmail.com', '$2y$10$Gn0v/hmLZo4oRk7OOWUpUuerCDLZqSbQlQRbhBEA.k.Al3PGZ4E2G', NULL, NULL, 'Admin', '2017-12-24 11:35:37', '2017-12-24 11:53:28', 1, NULL, NULL);
INSERT INTO users VALUES (10, 'ashish', 'ashish.verma250990+6@gmail.com', '$2y$10$uj4y13hdIML.XSXijQgC5.Wk5RtZmn6KfbcvC3VZTfPLYLM4bVb3q', NULL, NULL, 'Subscriber', '2017-12-24 11:54:56', '2017-12-24 17:27:08', 1, NULL, NULL);
INSERT INTO users VALUES (4, 'ashish', 'admin@gmail.com', '$2y$10$L632YhUq1Jh9d9loyXAhmuOWrtdwp4c7nS7R3kG4vrJet.2JX6W9y', NULL, NULL, 'Admin', '2017-12-23 17:53:44', '2018-02-02 12:20:31', 1, '2018-02-02 12:20:31', NULL);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 10, true);


--
-- Data for Name: voltage; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO voltage VALUES (1, '100v Japan', NULL, NULL, NULL);
INSERT INTO voltage VALUES (2, '110-120v', NULL, NULL, NULL);
INSERT INTO voltage VALUES (3, 'Multi', NULL, NULL, NULL);
INSERT INTO voltage VALUES (4, 'other', NULL, NULL, NULL);


--
-- Name: voltage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('voltage_id_seq', 4, true);


--
-- Name: application application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- Name: auto_reverse auto_reverse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auto_reverse
    ADD CONSTRAINT auto_reverse_pkey PRIMARY KEY (id);


--
-- Name: brand_images brand_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brand_images
    ADD CONSTRAINT brand_images_pkey PRIMARY KEY (id);


--
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);


--
-- Name: catalog_images catalog_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_pkey PRIMARY KEY (id);


--
-- Name: catalog_types catalog_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_types
    ADD CONSTRAINT catalog_types_pkey PRIMARY KEY (id);


--
-- Name: catalogs catalogs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: classical classical_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY classical
    ADD CONSTRAINT classical_pkey PRIMARY KEY (id);


--
-- Name: duplications duplications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY duplications
    ADD CONSTRAINT duplications_pkey PRIMARY KEY (id);


--
-- Name: electronics electronics_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY electronics
    ADD CONSTRAINT electronics_pkey PRIMARY KEY (id);


--
-- Name: equalization equalization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equalization
    ADD CONSTRAINT equalization_pkey PRIMARY KEY (id);


--
-- Name: expander_images expander_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_pkey PRIMARY KEY (id);


--
-- Name: expanders expanders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expanders
    ADD CONSTRAINT expanders_pkey PRIMARY KEY (id);


--
-- Name: genres genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY genres
    ADD CONSTRAINT genres_pkey PRIMARY KEY (id);


--
-- Name: head_composition head_composition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_composition
    ADD CONSTRAINT head_composition_pkey PRIMARY KEY (id);


--
-- Name: head_configuration head_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY head_configuration
    ADD CONSTRAINT head_configuration_pkey PRIMARY KEY (id);


--
-- Name: inputs inputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inputs
    ADD CONSTRAINT inputs_pkey PRIMARY KEY (id);


--
-- Name: manufacturer_images manufacturer_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manufacturer_images
    ADD CONSTRAINT manufacturer_images_pkey PRIMARY KEY (id);


--
-- Name: max_reel_size max_reel_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY max_reel_size
    ADD CONSTRAINT max_reel_size_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: motors motors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY motors
    ADD CONSTRAINT motors_pkey PRIMARY KEY (id);


--
-- Name: noise_reductions noise_reductions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY noise_reductions
    ADD CONSTRAINT noise_reductions_pkey PRIMARY KEY (id);


--
-- Name: number_of_heads number_of_heads_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY number_of_heads
    ADD CONSTRAINT number_of_heads_pkey PRIMARY KEY (id);


--
-- Name: outputs outputs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY outputs
    ADD CONSTRAINT outputs_pkey PRIMARY KEY (id);


--
-- Name: reel_sizes reel_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reel_sizes
    ADD CONSTRAINT reel_sizes_pkey PRIMARY KEY (id);


--
-- Name: speeds speeds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY speeds
    ADD CONSTRAINT speeds_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_brand_id_key UNIQUE (tape_head_preamp_id);


--
-- Name: tape_head_preamp_images tape_head_preamp_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamp_images
    ADD CONSTRAINT tape_head_preamp_images_pkey PRIMARY KEY (id);


--
-- Name: tape_head_preamps tape_head_preamp_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamp_manufacturer_id_key UNIQUE (manufacturer_id);


--
-- Name: tape_head_preamps tape_head_preamp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_head_preamps
    ADD CONSTRAINT tape_head_preamp_pkey PRIMARY KEY (id);


--
-- Name: tape_images tape_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorder_images tape_recorder_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorder_images
    ADD CONSTRAINT tape_recorder_images_pkey PRIMARY KEY (id);


--
-- Name: tape_recorders tape_recorders_brand_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_brand_id_key UNIQUE (brand);


--
-- Name: tape_recorders tape_recorders_manufacturer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_manufacturer_id_key UNIQUE (manufacturer);


--
-- Name: tape_recorders tape_recorders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorders
    ADD CONSTRAINT tape_recorders_pkey PRIMARY KEY (id);


--
-- Name: tape_track_list tape_track_list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_pkey PRIMARY KEY (id);


--
-- Name: tapes tapes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_pkey PRIMARY KEY (id);


--
-- Name: tracks tracks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tracks
    ADD CONSTRAINT tracks_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: voltage voltage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY voltage
    ADD CONSTRAINT voltage_pkey PRIMARY KEY (id);


--
-- Name: categories__lft__rgt_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX categories__lft__rgt_parent_id_index ON categories USING btree (_lft, _rgt, parent_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: catalog_images catalog_images_catalog_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalog_images
    ADD CONSTRAINT catalog_images_catalog_id_fkey FOREIGN KEY (catalog_id) REFERENCES catalogs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: catalogs catalogs_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY catalogs
    ADD CONSTRAINT catalogs_type_fkey FOREIGN KEY (type) REFERENCES catalog_types(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expander_images expander_images_expander_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expander_images
    ADD CONSTRAINT expander_images_expander_id_fkey FOREIGN KEY (expander_id) REFERENCES expanders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_images tape_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_images
    ADD CONSTRAINT tape_images_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_recorder_images tape_recorder_images_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_recorder_images
    ADD CONSTRAINT tape_recorder_images_tape_id_fkey FOREIGN KEY (tape_recorder_id) REFERENCES tape_recorders(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tape_track_list tape_track_list_tape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tape_track_list
    ADD CONSTRAINT tape_track_list_tape_id_fkey FOREIGN KEY (tape_id) REFERENCES tapes(id) ON UPDATE CASCADE ON DELETE SET DEFAULT;


--
-- Name: tapes tapes_channel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_channel_fkey FOREIGN KEY (channel) REFERENCES channels(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_classical_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_classical_fkey FOREIGN KEY (classical) REFERENCES classical(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_duplication_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_duplication_fkey FOREIGN KEY (duplication) REFERENCES duplications(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_genre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_genre_fkey FOREIGN KEY (genre) REFERENCES genres(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_noise_reduction_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_noise_reduction_fkey FOREIGN KEY (noise_reduction) REFERENCES noise_reductions(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_reel_size_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_reel_size_fkey FOREIGN KEY (reel_size) REFERENCES reel_sizes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_speed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_speed_fkey FOREIGN KEY (speed) REFERENCES speeds(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tapes tapes_track_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tapes
    ADD CONSTRAINT tapes_track_fkey FOREIGN KEY (track) REFERENCES tracks(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: ashish
--

GRANT ALL ON SCHEMA public TO postgres;


--
-- PostgreSQL database dump complete
--

