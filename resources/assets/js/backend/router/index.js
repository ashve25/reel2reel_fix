import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login'
import ForgotPassword from '../components/ForgotPassword'
import Dashboard from '../components/Dashboard'
import MasterData from '../components/MasterData'
import NewTapeForm from '../components/Tapes/NewTapeForm'
import TapeList from '../components/Tapes/TapeList'
import EditTapeForm from '../components/Tapes/EditTapeForm'
import NewTapeRecorderForm from '../components/TapeRecorders/NewTapeRecorderForm'
import TapeRecorderList from '../components/TapeRecorders/TapeRecorderList'
import EditTapeRecorderForm from '../components/TapeRecorders/EditTapeRecorderForm'
import ManufacturerList from '../components/Misc/ManufacturersList'
import NewManufacturerForm from '../components/Misc/NewManufacturerForm'
import EditManufacturerForm from '../components/Misc/EditManufacturerForm'
import BrandList from '../components/Misc/BrandList'
import NewBrandForm from '../components/Misc/NewBrandForm'
import EditBrandForm from '../components/Misc/EditBrandForm'
import TapeHeadPreampsList from '../components/Misc/TapeHeadPreampsList'
import NewTapeHeadPreampsForm from '../components/Misc/NewTapeHeadPreampsForm'
import EditTapeHeadPreampsForm from '../components/Misc/EditTapeHeadPreampsForm'
import InputExpanderList from '../components/Misc/InputExpanderList'
import NewInputExpanderForm from '../components/Misc/NewInputExpanderForm'
import EditInputExpanderForm from '../components/Misc/EditInputExpanderForm'
import CatalogList from '../components/Catalogs/CatalogList'
import NewCatalogForm from '../components/Catalogs/NewCatalogForm'
import EditCatalogForm from '../components/Catalogs/EditCatalogForm'
import UserList  from '../components/Users/UserList'
import EditProfile from '../components/Users/EditProfile'
import HistoryList from '../components/TapeRecorders/HistoryList' 
import NewHistory from '../components/TapeRecorders/NewHistory'
import EditHistory from '../components/TapeRecorders/EditHistory'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {requiresAuth: false}
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: ForgotPassword,
      meta: {requiresAuth: false}
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      props: true,
      meta: {requiresAuth: true}
    },
    {
      path: '/master-data',
      name: 'MasterData',
      component: MasterData,
      meta: {requiresAuth: true}
    },
    {
      path: '/tapes/submit-tape',
      name: 'NewTapeForm',
      component: NewTapeForm,
      meta: {requiresAuth: true}
    },
    {
      path: '/tapes',
      name: 'TapeList',
      component: TapeList,
      meta: {requiresAuth: true}
    },
    {
      path: '/tapes/:id/edit',
      name: 'EditTapeForm',
      component: EditTapeForm,
      meta: {requiresAuth: true}
    },
    {
      path: '/tape-recorders/submit-tape-recorder',
      name: 'NewTapeRecorderForm',
      component: NewTapeRecorderForm,
      meta: {requiresAuth: true}
    },
    {
      path: '/tape-recorders',
      name: 'TapeRecorderList',
      component: TapeRecorderList,
      meta: {requiresAuth: true}
    },
    {
      path: '/tape-recorders/:id/edit',
      name: 'EditTapeRecorderForm',
      component: EditTapeRecorderForm,
      meta: {requiresAuth: true}
    },
    {
      path: '/manufacturers',
      name: 'ManufacturerList',
      component: ManufacturerList,
      meta: { requiresAuth: true}
    },
    {
      path: '/manufacturers/create',
      name: 'NewManufacturerForm',
      component: NewManufacturerForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/manufacturers/:id/edit',
      name: 'EditManufacturerForm',
      component: EditManufacturerForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/brands',
      name: 'BrandList',
      component: BrandList,
      meta: { requiresAuth: true}
    },
    {
      path: '/brands/create',
      name: 'NewBrandForm',
      component: NewBrandForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/brands/:id/edit',
      name: 'EditBrandForm',
      component: EditBrandForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/tape-head-preamps',
      name: 'TapeHeadPreampsList',
      component: TapeHeadPreampsList,
      meta: { requiresAuth: true}
    },
    {
      path: '/tape-head-preamps/create',
      name: 'NewTapeHeadPreampsForm',
      component: NewTapeHeadPreampsForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/tape-head-preamps/:id/edit',
      name: 'EditTapeHeadPreampsForm',
      component: EditTapeHeadPreampsForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/input-expanders',
      name: 'InputExpanderList',
      component: InputExpanderList,
      meta: { requiresAuth: true}
    },
    {
      path: '/input-expanders/create',
      name: 'NewInputExpanderForm',
      component: NewInputExpanderForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/input-expanders/:id/edit',
      name: 'EditInputExpanderForm',
      component: EditInputExpanderForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/catalogs',
      name: 'CatalogList',
      component: CatalogList,
      meta: {requiresAuth: true}
    },
    {
      path: '/catalogs/create',
      name: 'NewCatalogForm',
      component: NewCatalogForm,
      meta: {requiresAuth: true}
    },
    {
      path: '/catalogs/:id/edit',
      name: 'EditCatalogForm',
      component: EditCatalogForm,
      meta: { requiresAuth: true}
    },
    {
      path: '/users',
      name: 'UserList',
      component: UserList,
      meta: {requiresAuth: true}
    },
    {
      path: '/profile',
      name: 'EditProfile',
      component: EditProfile,
      meta: {requiresAuth: true}
    },
    {
      path: '/history',
      name: 'HistoryList',
      component: HistoryList,
      meta : {requiresAuth: true}
    },
    {
      path: '/history/create',
      name: 'NewHistory',
      component: NewHistory,
      meta : {requiresAuth: true}
    },
    {
      path: '/history/:id/edit',
      name: 'EditHistory',
      component:EditHistory,
      meta: {requiresAuth: true}
    }
  ]
})