// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import * as Cookies from "js-cookie"
import VueMaterial from 'vue-material'
import Snotify, {SnotifyPosition} from 'vue-snotify';
import Paginator from 'laravel-vue-pagination';
import PictureInput from './components/PictureInput';
import Toastr from 'vue-toastr';
import 'vue-toastr/dist/vue-toastr.css';


Vue.config.productionTip = false
Vue.use(VueAxios, axios);
Vue.use(VueMaterial);
Vue.use(Toastr);
Vue.axios.defaults.baseURL = document.head.querySelector('meta[name="admin-base-url"]').content;
Vue.prototype.$placeholder = document.head.querySelector('meta[name="placeholder-url"]').content;
Vue.prototype.loaderUrl = document.head.querySelector('meta[name="loader-url"]').content;
Vue.component('pagination', Paginator);
Vue.component('picture-input', PictureInput);
Vue.component('vue-toastr',Toastr);


var snotifyOptions = {
	toast: {
		position: SnotifyPosition.rightTop
	}
};

Vue.use(Snotify, snotifyOptions);

router.beforeEach((to, from, next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		//check if the cookies are set
		if(!Cookies.get().token){
			next({
				path: '/login',
				query: { redirect: to.fullPath }
			})
		} 
		else {
			Vue.axios.defaults.headers.common.Authorization = "Bearer " + Cookies.get().token;
			next()
		}
	} 
	else {
		if(!Cookies.get().token){
			next()
		}else{
			next({
				path: '/'				
			})
		}		
	}
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
