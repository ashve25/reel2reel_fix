<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="admin-base-url" content="{{ url('/admin') }}" />
    <meta name="base-url" content="{{ url('/') }}" />
    <meta name="previous-url" content="{{url()->previous().'/'}}" />
    <meta name="placeholder-url" content="{{ asset('images/placeholder.jpg') }}" />
    <meta name="loader-url" content="{{ asset('images/loader-3.gif') }}" />

    <title>Reel-Reel | Admin</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/skin-red.min.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/jsgrid.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
  </head>
  <body class="skin-red hold-transition sidebar-mini">
    
    <div class="login-box">
      <div class="login-logo">
        <b>Reel-Reel</b>
      </div>
      <!-- /.login-logo -->
      <div id="resetPassword">
        <div class="alert alert-warning alert-dismissible" style="display:none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span id="status"></span>
        </div>
          <div class="login-box-body card">
            <p class="login-box-msg">Reset Password</p>
            <form>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="New Password" v-model="user.password" required="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Confirm Password" v-model="user.confirm_password" required="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <button type="submit" class="btn btn-primary btn-block btn-flat text-center" @click="submit($event)">Reset</button>
            </form>
            <a href="{{url('admin')}}" class="pull-right" style="margin-top: 10px">Back to login</a> <br>

          </div>
          <!-- /.login-box-body -->
        </div>
    </div>

    <footer class="main-footer">
      <!-- Default to the left -->
      <strong>Copyright &copy; 2018 <a href="#">Reel-Reel</a>.</strong> All rights reserved.
    </footer>

    <!-- built files will be auto injected -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>

    <script type="text/javascript">
        console.log("id " + {!!json_encode($id)!!});
        var resetPassword = new Vue({
            el: '#resetPassword',
            data:{
                id: {!!json_encode($id)!!},
                user: {},
            },            
            methods: {
                submit: function(event) {
                    event.preventDefault();
                    console.log("reset");      
                    
                    var url = "{{url('/users')}}/" + resetPassword.id + '/reset-password';
                    
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: {user: this.user},
                        success: function(response){
                            console.log(response);
                            window.location.href = "{{url('admin')}}";
                        },
                        error: function(response){
                            console.log(response);
                            $('.alert').css('display','block');
                            $('#status').html(response.responseJSON.meta.message);
                        }
                    });

                    // this.axios.post('/users/' + id + '/reset-password/', {id:id, password: this.password, confirm_password: this.confirm_password})
                    // .then(function(response) {
                    //     console.log(response);
                    //     self.$router.push('/login');
                    // })
                    // .catch(function(error){
                    //     console.log(error);
                    //     if(error.response.data.meta.error_code == 400){
                    //         $('.alert').css('display','block');
                    //         $.each(error.response.data.meta.message, function(index, item){
                    
                    //         }); 
                    //     }
                    // });
                }
            }
        });
    </script>

    <style type="text/css">
        body {
            background: #ededed; 
        }
        .card{
            border-radius: 2px;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

        .main-footer {
            margin-left: 0px !important;
            text-align: center;
        }
    </style>
  </body>
</html>
