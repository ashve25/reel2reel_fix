<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="base-url" content="{{ url('/') }}" />
    <meta name="previous-url" content="{{url()->previous().'/'}}" />
    <meta name="placeholder-url" content="{{ asset('images/placeholder.jpg') }}" />
    <meta name="loader-url" content="{{ asset('images/loader-3.gif') }}" />

    <title>Reel-Reel | Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/materia/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css/skin-red.min.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/jsgrid.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
    <style type="text/css">
        .home-section{
            background-image: url('images/bg.jpg');
            background-position: center;
            background-repeat: no-repeat;
            padding: 30px;
        }
    </style>
  </head>
  <body class="skin-red hold-transition sidebar-mini">
    <div id="frontend"></div>
    <!-- built files will be auto injected -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <!-- <script src="{{asset('js/adminlte.min.js')}}"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.core.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.load-indicator.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.load-strategies.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.sort-strategies.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.validation.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.field.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.text.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.checkbox.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.control.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.number.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.select.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.textarea.js')}}"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript" src="{{asset('js/frontend.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
  </body>
</html>
