<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('vue');
});

Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('test', function() {
	return \DB::select("select tape.id, row_to_json(tape) as tapes
						from(
						  select t.*, 
						  (select json_agg(image)
						  from (
						    select * from tape_images where tape_id = t.id
						  ) image
						)
						from tapes as t) tape");
});

//genres
Route::get('import', function() {
	$row = 1;
	if(($handle = fopen("/Users/rathi/Documents/CitrusLeaf/reel-reel/Tape/Duplication-Table 1.csv", "r"))!== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			if($row == 1) {
				$row++;
				continue;
			}
			if(!empty($data[0]))
				\DB::insert("insert into duplications (name) values (?)", [$data[0]]);
	    }
	    fclose($handle);
	}
});

